
/* 
 * Title       : mkgrb.c
 * Description : Make Geber file.
 * Author      : KAWAMOTO "nanashino" Yasuhisa
 * URL         : http://homepage3.nifty.com/mujirushi/
 * Date        : 01/20/2002
 */

//
// PCBEが出力したgout.lstと*.grb(複数指定可能)ファイルから
// RS274-Xフォーマットのガーバーデータを生成して標準出力に出力する。
// *.grbファイルを複数指定した場合はマージする。
// mkgrb gout.lst xxx.grb ... > yyy.grb
//

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

#define SHAPE_ROUND  0
#define SHAPE_SQUARE 1
char maru[] = "丸";
char kaku[] = "角";

int main(int argc, char *argv[])
{
	FILE *lst = NULL;
	FILE *grb = NULL;
	char s[80];
	char c;
	char *p;
	int dcode, shape;
	float xsize;
	int i;
	
	if(argc < 3)
	{
		fprintf(stderr, "Usage: mkgrb gout.lst xxx.grb ... > yyy.grb\n");
		return(-1);
	}
	
	if((lst = fopen(argv[1], "r")) == NULL)
	{
		fprintf(stderr, "%s open error.\n", argv[1]);
		return(-1);
	}
	
	fprintf(stdout, "%%MOMM*%%\n");
	fprintf(stdout, "%%FSLAX43Y43*%%\n");		// 01/20/2002 余分な*を削除
	while((fscanf(lst, "%s", s)) != EOF)
	{
		if((strncmp(s, maru, strlen(maru)) == 0)
				|| (strncmp(s, kaku, strlen(kaku)) == 0))
		{
			if(strncmp(s, maru, 2) == 0)
				shape = SHAPE_ROUND;
			else
				shape = SHAPE_SQUARE;
			if(strstr(s, "D") != NULL)
				dcode = atoi(strstr(s, "D")+1);
			fscanf(lst, "%s", s);
			fscanf(lst, "%f", &xsize);
			
			if(shape == SHAPE_ROUND)
				fprintf(stdout, "%%ADD%dC,%.3f*%%\n", dcode, xsize);
			else
				fprintf(stdout, "%%ADD%dR,%.3fX%.3f*%%\n", dcode, xsize, xsize);
		}
	}
	
	fclose(lst);
	
	for(i = 2; i < argc; i++)
	{
		if((grb = fopen(argv[i], "r")) == NULL)
		{
			fprintf(stderr, "%s open error.\n", argv[i]);
			return(-1);
		}
		
		p = s;
		while((fscanf(grb, "%c", &c)) != EOF)
		{
			if((c != '\n') && (c != ' ')) *p++ = c;
			if(c == '*')
			{
				*p++ = '\0';
				if((strcmp(s, "G71*") != 0)
						&& (strcmp(s, "G90*") != 0)
						&& (strcmp(s, "M00*") != 0))
					fprintf(stdout, "%s\n", s);
				else if(((strcmp(s, "G71*") == 0) || (strcmp(s, "G90*") == 0)) && (i == 2))
					fprintf(stdout, "%s\n", s);
				else if((strcmp(s, "M00*") == 0) && (i == argc-1))
					fprintf(stdout, "%s\n", s);
				p = s;
			}
		}
		
		fclose(grb);
	}
	
	return(0);
}
