
 ■□■  PCBEお助けツール 01/20/2002  □■□

1. 概要

  フリーで公開されているCADソフト、PCBEが出力するガーバー/ドリルファイルから
  アパチャーファイル、RS274-Xフォーマットのガーバーファイル
  EXCELLONフォーマットのドリルファイルを生成するツールです。


2. パッケージ内容

  readme.txt -- このファイル
  mkapt.exe  -- gout.lstからアパチャーファイルを生成
  mkgrb.exe  -- gout.lstと*.grbからRS274-Xフォーマットのガーバーファイルを生成
                (*.grbファイルを複数指定した場合はマージ)
  mkhol.exe  -- hout.lstと*.holからEXCELLONフォーマットのドリルファイルを生成
  mkapt.c    -- mkapt.exeのソースファイル
  mkhol.c    -- mkhol.exeのソースファイル
  mkgrb.c    -- mkgrb.exeのソースファイル
  makefile   -- makeファイル
  build.bat  -- mkapt.exeとmkhol.exeの使用例
  buildx.bat -- mkgrb.exeとmkhol.exeの使用例
  order.txt  -- 発注するときにデータに添付するreadme.txtの例


3. 動作確認環境

  組み立てAT互換機(Windows98SE DOS窓)


4. 使用例

  build.batもしくはbuildx.batを参考にしてください。


5. 注意

  「単位：mm、整数部：4、小数部：3」のフォーマットを前提に作っています。(手抜きです(^^ゞ)


6. 改版履歴

  01/16/2002 → 01/20/2002
    ・mkgrb.exe が余分な*を出力していたのを修正


7. 著作権＆免責

  著作権は、私(ななしの)にあります。しかし改造や流用は自由です。
  このソフトを使用して発生した問題について、私(ななしの)は責任を負いません。


8. 謝辞

  PCBEのようなすばらしいCADソフトをフリーで公開してくださった
  PCBEの作者には大変感謝しています。

