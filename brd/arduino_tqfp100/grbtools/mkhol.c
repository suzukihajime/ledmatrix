
/* 
 * Title       : mkhol.c
 * Description : Make Drill file.
 * Author      : KAWAMOTO "nanashino" Yasuhisa
 * URL         : http://homepage3.nifty.com/mujirushi/
 * Date        : 01/16/2002
 */

//
// PCBEが出力したhout.lstとdri.holファイルから
// ドリルデータを生成して標準出力に出力する。
// mkhol hout.lst dri.hol > dri.cnc
//

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	FILE *lst = NULL;
	FILE *hol = NULL;
	char s[80];
	int tcode;
	float size;
	char *c;
	
	if(argc < 3)
	{
		fprintf(stderr, "Usage: mkhol hout.lst dri.hol > dri.cnc\n");
		return(-1);
	}
	
	if((lst = fopen(argv[1], "r")) == NULL)
	{
		fprintf(stderr, "%s open error.\n", argv[1]);
		return(-1);
	}
	if((hol = fopen(argv[2], "r")) == NULL)
	{
		fprintf(stderr, "%s open error.\n", argv[2]);
		fclose(lst);
		return(-1);
	}
	
	fprintf(stdout, "M48\n");
	fprintf(stdout, "METRIC\n");
	while((fscanf(lst, "%s", s)) != EOF)
	{
		if(strncmp(s, "T", 1) == 0)
		{
			tcode = atoi(strstr(s, "T")+1);
			fscanf(lst, "%s", s);
			fscanf(lst, "%f", &size);
			
			fprintf(stdout, "T%dC%.3f\n", tcode, size);
		}
	}
	
	while((fscanf(hol, "%s", s)) != EOF)
	{
		if(strcmp(s, "X0Y0") != 0)
		{
			if(strcmp(s, "M02") == 0)
				fprintf(stdout, "M30\n");
			else
			{
				if((c = strstr(s, "M05")) != NULL)
					*c = '\0';
				fprintf(stdout, "%s\n", s);
			}
		}
	}
	
	fclose(lst);
	fclose(hol);
	
	return(0);
}
