------------------------------------------------------------------------------
[Name/company]
  
------------------------------------------------------------------------------
[Shipping address]
  
------------------------------------------------------------------------------
[Shipping option]
  ( ) Courier
  ( ) Express Mail
  ( ) Airmail
  
------------------------------------------------------------------------------
[Files]
  ( ) apt.lst : Apertures file.
  ( ) cpu.grb : TOP copper layer.
  ( ) cpl.grb : BOTTOM copper layer.
  ( ) msu.grb : TOP solder mask layer.
  ( ) msl.grb : BOTTOM solder mask layer.
  ( ) sku.grb : TOP component print layer.
  ( ) skl.grb : BOTTOM component print layer.
  ( ) bol.grb : PCB outline.
  ( ) dri.cnc : NC drill file.

------------------------------------------------------------------------------
[Note]
  Gerber format : 4.3, Leading zeros suppressed, Absolute, Metric
  Drill format  : 4.3, Leading zeros suppressed, Absolute, Metric

------------------------------------------------------------------------------
