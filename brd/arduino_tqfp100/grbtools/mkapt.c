
/* 
 * Title       : mkapt.c
 * Description : Make Aperture Descriptions.
 * Author      : KAWAMOTO "nanashino" Yasuhisa
 * URL         : http://homepage3.nifty.com/mujirushi/
 * Date        : 01/16/2002
 */

//
// PCBEが出力したgout.lstファイルから
// アパチャリストを生成して標準出力に出力する。
// mkapt gout.lst > apt.lst
//

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

#define SHAPE_ROUND  0
#define SHAPE_SQUARE 1
char maru[] = "丸";
char kaku[] = "角";

int main(int argc, char *argv[])
{
	FILE *lst = NULL;
	char s[80];
	int dcode, shape;
	float xsize;
	
	if(argc < 2)
	{
		fprintf(stderr, "Usage: mkapt gout.lst > apt.lst\n");
		return(-1);
	}
	
	if((lst = fopen(argv[1], "r")) == NULL)
	{
		fprintf(stderr, "%s open error.\n", argv[1]);
		return(-1);
	}
	
	fprintf(stdout, "Aperture Descriptions\n");
	fprintf(stdout, "============================\n");
	fprintf(stdout, "D-Code  Shape   X(mm)  Y(mm)\n");
	fprintf(stdout, "----------------------------\n");
	while((fscanf(lst, "%s", s)) != EOF)
	{
		if((strncmp(s, maru, strlen(maru)) == 0)
				|| (strncmp(s, kaku, strlen(kaku)) == 0))
		{
			if(strncmp(s, maru, 2) == 0)
				shape = SHAPE_ROUND;
			else
				shape = SHAPE_SQUARE;
			if(strstr(s, "D") != NULL)
				dcode = atoi(strstr(s, "D")+1);
			fscanf(lst, "%s", s);
			fscanf(lst, "%f", &xsize);
			
			fprintf(stdout, "D%-7d%-8s%-7.3f0\n", dcode, shape==SHAPE_ROUND?"ROUND":"SQUARE", xsize);
		}
	}
	fprintf(stdout, "----------------------------\n");
	
	fclose(lst);
	
	return(0);
}
