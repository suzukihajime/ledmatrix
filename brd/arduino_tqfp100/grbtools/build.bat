@ECHO OFF

REM cpu.grb : パターン(部品面)
REM cpl.grb : パターン(ハンダ面)
REM msu.grb : レジスト(部品面)
REM msl.grb : レジスト(ハンダ面)
REM sku.grb : シルク(部品面)
REM skl.grb : シルク(ハンダ面)
REM bol.grb : 外形

mkdir gerber
mkapt gout.lst > gerber\apt.lst
mkhol hout.lst dri.hol > gerber\dri.cnc
copy cpu.grb gerber
copy cpl.grb gerber
copy msu.grb gerber
copy msl.grb gerber
copy sku.grb gerber
copy skl.grb gerber
copy bol.grb gerber
copy order.txt gerber\readme.txt

IF "%1" =="" GOTO END
cd gerber
pkzip -a %1 apt.lst
pkzip -a %1 cpu.grb
pkzip -a %1 cpl.grb
pkzip -a %1 msu.grb
pkzip -a %1 msl.grb
pkzip -a %1 sku.grb
pkzip -a %1 skl.grb
pkzip -a %1 bol.grb
pkzip -a %1 dri.cnc
pkzip -a %1 readme.txt
:END
