@ECHO OFF

REM nfl.grb : 補助(他のレイヤに重ねる)
REM cpu.grb : パターン(部品面)
REM cpl.grb : パターン(ハンダ面)
REM msu.grb : レジスト(部品面)
REM msl.grb : レジスト(ハンダ面)
REM sku.grb : シルク(部品面)
REM skl.grb : シルク(ハンダ面)
REM bol.grb : 外形

mkdir gerber
mkhol hout.lst hole.hol > gerber\psocboard.TXT
mkgrb gout.lst patterntop.grb > gerber\psocboard.GTL
mkgrb gout.lst patternbottom.grb > gerber\psocboard.GBL
mkgrb gout.lst silktop.grb > gerber\psocboard.GTO
mkgrb gout.lst silkbottom.grb > gerber\psocboard.GBO
mkgrb gout.lst registtop.grb > gerber\psocboard.GTS
mkgrb gout.lst registbottom.grb > gerber\psocboard.GBS
copy order.txt gerber\readme.txt

IF "%1" == "" GOTO END
cd gerber
pkzip -a %1 cpu.grb
pkzip -a %1 cpl.grb
pkzip -a %1 msu.grb
pkzip -a %1 msl.grb
pkzip -a %1 sku.grb
pkzip -a %1 skl.grb
pkzip -a %1 bol.grb
pkzip -a %1 dri.cnc
pkzip -a %1 readme.txt
:END
