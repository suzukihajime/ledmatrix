# ledmatrix

An archive for ledmatrix project. PSoC5-powered dotmatrix controller and driver.

* PCB artworks for dotmatrix baseboard and controller board (in brd/)
* PSoC5 (5LP) workspaces (prj/)
* brief article on this project ([matrix.pdf](https://bitbucket.org/suzukihajime/ledmatrix/raw/master/matrix.pdf))

## Gallery

![picture 1](https://bitbucket.org/suzukihajime/ledmatrix/raw/master/pic/pic1.jpg)

![picture 2](https://bitbucket.org/suzukihajime/ledmatrix/raw/master/pic/pic2.jpg)

![picture 3](https://bitbucket.org/suzukihajime/ledmatrix/raw/master/pic/pic3.jpg)

![picture 4](https://bitbucket.org/suzukihajime/ledmatrix/raw/master/pic/pic4.jpg)

## Copyright and License

Hajime Suzuki, 2012-2020