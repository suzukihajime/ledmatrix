//`#start header` -- edit after this line, do not edit this line
// ========================================
//
// Copyright YOUR COMPANY, THE YEAR
// All Rights Reserved
// UNPUBLISHED, LICENSED SOFTWARE.
//
// CONFIDENTIAL AND PROPRIETARY INFORMATION
// WHICH IS THE PROPERTY OF your company.
//
// ========================================
`include "cypress.v"
//`#end` -- edit above this line, do not edit this line

//`#start body` -- edit after this line, do not edit this line

//        Your code goes here
module matdrv(clock, reset, sda, sck, rck);
	input clock;
	input reset;
	output sda;
	output sck;
	output rck;
	
	reg dck;
	always @(negedge dck or posedge reset) begin
		if(reset) begin
			dck = 1'b0;
		end else begin
			dck = dck + 1'b1;
		end
	end
	
/*	reg [7:0] creg;
	always @(negedge dck or posedge reset) begin
		if(reset) begin
			creg <= 8'b00000000;
		end else if(creg < 8'd192) begin
			creg <= creg + 8'b00000001;
		end
	end*/
	
	assign sck = dck;
	assign sda = dck;//creg[3];
	assign rck = dck;
endmodule
//`#end` -- edit above this line, do not edit this line

//`#start footer` -- edit after this line, do not edit this line
//`#end` -- edit above this line, do not edit this line


//[] END OF FILE
