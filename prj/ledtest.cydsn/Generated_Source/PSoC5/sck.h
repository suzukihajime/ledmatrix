/*******************************************************************************
* File Name: sck.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_sck_H) /* Pins sck_H */
#define CY_PINS_sck_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "sck_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sck__PORT == 15 && (sck__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    sck_Write(uint8 value) ;
void    sck_SetDriveMode(uint8 mode) ;
uint8   sck_ReadDataReg(void) ;
uint8   sck_Read(void) ;
uint8   sck_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define sck_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define sck_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define sck_DM_RES_UP          PIN_DM_RES_UP
#define sck_DM_RES_DWN         PIN_DM_RES_DWN
#define sck_DM_OD_LO           PIN_DM_OD_LO
#define sck_DM_OD_HI           PIN_DM_OD_HI
#define sck_DM_STRONG          PIN_DM_STRONG
#define sck_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define sck_MASK               sck__MASK
#define sck_SHIFT              sck__SHIFT
#define sck_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define sck_PS                     (* (reg8 *) sck__PS)
/* Data Register */
#define sck_DR                     (* (reg8 *) sck__DR)
/* Port Number */
#define sck_PRT_NUM                (* (reg8 *) sck__PRT) 
/* Connect to Analog Globals */                                                  
#define sck_AG                     (* (reg8 *) sck__AG)                       
/* Analog MUX bux enable */
#define sck_AMUX                   (* (reg8 *) sck__AMUX) 
/* Bidirectional Enable */                                                        
#define sck_BIE                    (* (reg8 *) sck__BIE)
/* Bit-mask for Aliased Register Access */
#define sck_BIT_MASK               (* (reg8 *) sck__BIT_MASK)
/* Bypass Enable */
#define sck_BYP                    (* (reg8 *) sck__BYP)
/* Port wide control signals */                                                   
#define sck_CTL                    (* (reg8 *) sck__CTL)
/* Drive Modes */
#define sck_DM0                    (* (reg8 *) sck__DM0) 
#define sck_DM1                    (* (reg8 *) sck__DM1)
#define sck_DM2                    (* (reg8 *) sck__DM2) 
/* Input Buffer Disable Override */
#define sck_INP_DIS                (* (reg8 *) sck__INP_DIS)
/* LCD Common or Segment Drive */
#define sck_LCD_COM_SEG            (* (reg8 *) sck__LCD_COM_SEG)
/* Enable Segment LCD */
#define sck_LCD_EN                 (* (reg8 *) sck__LCD_EN)
/* Slew Rate Control */
#define sck_SLW                    (* (reg8 *) sck__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define sck_PRTDSI__CAPS_SEL       (* (reg8 *) sck__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define sck_PRTDSI__DBL_SYNC_IN    (* (reg8 *) sck__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define sck_PRTDSI__OE_SEL0        (* (reg8 *) sck__PRTDSI__OE_SEL0) 
#define sck_PRTDSI__OE_SEL1        (* (reg8 *) sck__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define sck_PRTDSI__OUT_SEL0       (* (reg8 *) sck__PRTDSI__OUT_SEL0) 
#define sck_PRTDSI__OUT_SEL1       (* (reg8 *) sck__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define sck_PRTDSI__SYNC_OUT       (* (reg8 *) sck__PRTDSI__SYNC_OUT) 


#if defined(sck__INTSTAT)  /* Interrupt Registers */

    #define sck_INTSTAT                (* (reg8 *) sck__INTSTAT)
    #define sck_SNAP                   (* (reg8 *) sck__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins sck_H */

#endif
/* [] END OF FILE */
