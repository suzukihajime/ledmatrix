/*******************************************************************************
* File Name: row.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "row.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 row__PORT == 15 && (row__MASK & 0xC0))

/*******************************************************************************
* Function Name: row_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void row_Write(uint8 value) 
{
    uint8 staticBits = row_DR & ~row_MASK;
    row_DR = staticBits | ((value << row_SHIFT) & row_MASK);
}


/*******************************************************************************
* Function Name: row_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void row_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(row_0, mode);
	CyPins_SetPinDriveMode(row_1, mode);
	CyPins_SetPinDriveMode(row_2, mode);
	CyPins_SetPinDriveMode(row_3, mode);
}


/*******************************************************************************
* Function Name: row_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro row_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 row_Read(void) 
{
    return (row_PS & row_MASK) >> row_SHIFT;
}


/*******************************************************************************
* Function Name: row_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 row_ReadDataReg(void) 
{
    return (row_DR & row_MASK) >> row_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(row_INTSTAT) 

    /*******************************************************************************
    * Function Name: row_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 row_ClearInterrupt(void) 
    {
        return (row_INTSTAT & row_MASK) >> row_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
