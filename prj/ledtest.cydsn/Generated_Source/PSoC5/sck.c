/*******************************************************************************
* File Name: sck.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "sck.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sck__PORT == 15 && (sck__MASK & 0xC0))

/*******************************************************************************
* Function Name: sck_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void sck_Write(uint8 value) 
{
    uint8 staticBits = sck_DR & ~sck_MASK;
    sck_DR = staticBits | ((value << sck_SHIFT) & sck_MASK);
}


/*******************************************************************************
* Function Name: sck_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void sck_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(sck_0, mode);
}


/*******************************************************************************
* Function Name: sck_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro sck_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 sck_Read(void) 
{
    return (sck_PS & sck_MASK) >> sck_SHIFT;
}


/*******************************************************************************
* Function Name: sck_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 sck_ReadDataReg(void) 
{
    return (sck_DR & sck_MASK) >> sck_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(sck_INTSTAT) 

    /*******************************************************************************
    * Function Name: sck_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 sck_ClearInterrupt(void) 
    {
        return (sck_INTSTAT & sck_MASK) >> sck_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
