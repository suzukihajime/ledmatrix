/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#define matdrv2_1_FIFO0_PTR    (  (reg8 *) matdrv2_1_dp0__F0_REG )
#define matdrv2_1_FIFO1_PTR    (  (reg8 *) matdrv2_1_dp0__F1_REG )
/*
#define matdrv_1_dp1_u0__A0_A1_REG CYREG_B1_UDB11_A0_A1
#define matdrv_1_dp1_u0__A0_REG CYREG_B1_UDB11_A0
#define matdrv_1_dp1_u0__A1_REG CYREG_B1_UDB11_A1
#define matdrv_1_dp1_u0__D0_D1_REG CYREG_B1_UDB11_D0_D1
#define matdrv_1_dp1_u0__D0_REG CYREG_B1_UDB11_D0
#define matdrv_1_dp1_u0__D1_REG CYREG_B1_UDB11_D1
#define matdrv_1_dp1_u0__DP_AUX_CTL_REG CYREG_B1_UDB11_ACTL
#define matdrv_1_dp1_u0__F0_F1_REG CYREG_B1_UDB11_F0_F1
#define matdrv_1_dp1_u0__F0_REG CYREG_B1_UDB11_F0
#define matdrv_1_dp1_u0__F1_REG CYREG_B1_UDB11_F1
*/

void matdrv2_1_Start(void);
void matdrv2_1_Stop(void);

//[] END OF FILE
