//`#start header` -- edit after this line, do not edit this line
// ========================================
//
// Copyright YOUR COMPANY, THE YEAR
// All Rights Reserved
// UNPUBLISHED, LICENSED SOFTWARE.
//
// CONFIDENTIAL AND PROPRIETARY INFORMATION
// WHICH IS THE PROPERTY OF your company.
//
// ========================================
`include "cypress.v"
//`#end` -- edit above this line, do not edit this line

//`#start body` -- edit after this line, do not edit this line

//        Your code goes here

module matdrv(clock, reset, sda, sck, rck, row, dmareq, dmareset);
	input clock;
	input reset;
	output sda;
	output sck;
	output rck;
	output [3:0] row;
	output dmareq;
	output dmareset;
	
	reg [8:0] creg;
	wire fifoload;
//	wire dload;
	wire fifostat;
	wire [7:0] fifodata;
	wire [3:0] rowdata;
	
	rowcounter rowcounter1(rck, reset, rowdata);
	function sckgen;
		input [8:0] creg;
		begin
			if(creg < 9'd384) begin
				sckgen = creg[0];
			end else begin
				sckgen = 1'b0;
			end
		end
	endfunction
	
	function rckgen;
		input [8:0] creg;
		begin
			if(creg > 9'd384) begin
				rckgen = 1'b1;
			end else begin
				rckgen = 1'b0;
			end
		end
	endfunction
	
	function dmagen;
		input [8:0] creg;
		begin
			dmagen = creg[1];
		end
	endfunction
	
	function dmaresgen;
		input [8:0] creg;
		begin
			if(creg[8:0] == 9'd396) begin
				dmaresgen = 1'b1;
			end else begin
				dmaresgen = 1'b0;
			end
		end
	endfunction
	
	function fifogen;
		input [8:0] creg;
		input fifostat;
		begin
			if(creg[8:0] < 9'd384 && fifostat) begin
				fifogen = ~creg[0] & ~creg[1];
			end else begin
				fifogen = 1'b0;
			end
		end
	endfunction
	
	always @(posedge clock or posedge reset) begin
		if(reset) begin
			creg <= 9'b000000000;
		end else begin
			if(creg == 9'd399) begin
				creg <= 9'd0;
			end else begin
				creg <= creg + 9'b000000001;
			end
		end
	end
	
	assign sck = sckgen(creg);
	assign sda = fifodata[0];
	assign rck = rckgen(creg);
	assign dmareq = dmagen(creg);
	assign dmareset = dmaresgen(creg);
	assign fifoload = fifogen(creg, fifostat);
	assign row = rowdata;
//	assign dload = 1'b0;

	cy_psoc3_dp #(.cy_dpconfig(
	{
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_A1,
		`CS_SHFT_OP_PASS, `CS_A0_SRC___F0, `CS_A1_SRC___F1,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM0: */
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM1:   */
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM2:   */
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM3:   */
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM4:   */
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM5:   */
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM6:   */
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM7:   */
		  8'hFF, 8'h00,	/*CFG9:   */
		  8'hFF, 8'hFF,	/*CFG11-10:   */
		`SC_CMPB_A1_D1, `SC_CMPA_A1_D1, `SC_CI_B_ARITH,
		`SC_CI_A_ARITH, `SC_C1_MASK_DSBL, `SC_C0_MASK_DSBL,
		`SC_A_MASK_DSBL, `SC_DEF_SI_0, `SC_SI_B_DEFSI,
		`SC_SI_A_DEFSI, /*CFG13-12:   */
		`SC_A0_SRC_PIN, `SC_SHIFT_SL, 1'h0,
		1'h0, `SC_FIFO1_BUS, `SC_FIFO0_BUS,
		`SC_MSB_DSBL, `SC_MSB_BIT0, `SC_MSB_NOCHN,
		`SC_FB_NOCHN, `SC_CMP1_NOCHN,
		`SC_CMP0_NOCHN, /*CFG15-14:   */
		 10'h00, `SC_FIFO_CLK__DP,`SC_FIFO_CAP_AX,
		`SC_FIFO_LEVEL,`SC_FIFO__SYNC,`SC_EXTCRC_DSBL,
		`SC_WRK16CAT_DSBL /*CFG17-16:   */
	})) dp0(
		/* input */ .clk(clock), 			// Clock
		/* input [02:00] */ .cs_addr(3'b000), // Control Store RAM address
		/* input */ .route_si(1'b0), 		// Shift in from routing
		/* input */ .route_ci(1'b0), 		// Carry in from routing
		/* input */ .f0_load(1'b0), 		// Load FIFO 0
		/* input */ .f1_load(1'b0), 		// Load FIFO 1
		/* input */ .d0_load(fifoload), 		// Load Data Register 0
		/* input */ .d1_load(1'b0), 		// Load Data Register 1
		/* output */ .ce0(), 			// Accumulator 0 = Data register 0
		/* output */ .cl0(), 			// Accumulator 0 < Data register 0
		/* output */ .z0(), 			// Accumulator 0 = 0
		/* output */ .ff0(), 			// Accumulator 0 = FF
		/* output */ .ce1(), 			// Accumulator [0|1] = Data register 1
		/* output */ .cl1(), 			// Accumulator [0|1] < Data register 1
		/* output */ .z1(), 			// Accumulator 1 = 0
		/* output */ .ff1(), 			// Accumulator 1 = FF
		/* output */ .ov_msb(), 		// Operation over flow
		/* output */ .co_msb(), 		// Carry out
		/* output */ .cmsb(), 			// Carry out
		/* output */ .so(), 			// Shift out
		/* output */ .f0_bus_stat(/*f0_bus_stat*/), 	// FIFO 0 status to uP
		/* output */ .f0_blk_stat(fifostat/*f0_blk_stat*/), 	// FIFO 0 status to DP
		/* output */ .f1_bus_stat(), 	// FIFO 1 status to uP
		/* output */ .f1_blk_stat(), 	// FIFO 1 status to DP
		/* input */ .ci(1'b0), 				// Carry in from previous stage
		/* output */ .co(), 			// Carry out to next stage
		/* input */ .sir(1'b0), 			// Shift in from right side
		/* output */ .sor(), 			// Shift out to right side
		/* input */ .sil(1'b0), 			// Shift in from left side
		/* output */ .sol(), 			// Shift out to left side
		/* input */ .msbi(1'b0), 			// MSB chain in
		/* output */ .msbo(), 			// MSB chain out
		/* input [01:00] */ .cei(2'b0), 	// Compare equal in from prev stage
		/* output [01:00] */ .ceo(), 	// Compare equal out to next stage
		/* input [01:00] */ .cli(2'b0), 	// Compare less than in from prv stage
		/* output [01:00] */ .clo(), 	// Compare less than out to next stage
		/* input [01:00] */ .zi(2'b0), 		// Zero detect in from previous stage
		/* output [01:00] */ .zo(), 	// Zero detect out to next stage
		/* input [01:00] */ .fi(2'b0), 		// 0xFF detect in from previous stage
		/* output [01:00] */ .fo(), 	// 0xFF detect out to next stage
		/* input [01:00] */ .capi(2'b0),	// Capture in from previous stage
		/* output [01:00] */ .capo(),		// Capture out to next stage
		/* input */ .cfbi(1'b0), 			// CRC Feedback in from previous stage
		/* output */ .cfbo(), 			// CRC Feedback out to next stage
		/* input [07:00] */ .pi(), 		// Parallel data port
		/* output [07:00] */ .po(fifodata)	 	// Parallel data port
	);
endmodule

module rowcounter(clock, reset, row);
	input clock;
	input reset;
	output [3:0] row;
	
	reg [3:0] cnt;
	
	always @(posedge clock or posedge reset) begin
		if(reset) begin
			cnt <= 4'b0000;
		end else begin
			cnt <= cnt + 4'b0001;
		end
	end
	
	assign row[3:1] = cnt[2:0];
	assign row[0] = cnt[3];
endmodule


//`#end` -- edit above this line, do not edit this line

//`#start footer` -- edit after this line, do not edit this line
//`#end` -- edit above this line, do not edit this line


//[] END OF FILE






