/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

void `$INSTANCE_NAME`_Start(void)
{
	/* nothing to do */
	return;
}

void `$INSTANCE_NAME`_Stop(void)
{
	/* nothing to do */
	return;
}

/* [] END OF FILE */
