/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "utf8.h"

static char fifo[FIFO_SIZE];
static int32 rptr = 0, wptr = 1;

inline char fifoGetChar(void)
{
	register char c;
	c = fifo[rptr];
	rptr = (rptr+1) % FIFO_SIZE;
	return c;
}

inline void fifoPushChar(char c)
{
	fifo[wptr] = c;
	wptr = (wptr+1) % FIFO_SIZE;
	return;
}

inline void fifoPushString(char *str)
{
	char c;
	while((c = *str++)) {
		fifoPushChar(c);
	}
	return;
}

inline uint16 fifoGetContentSize(void)
{
	return((wptr - rptr) % FIFO_SIZE);
}

inline uint16 fifoGetVacancySize(void)
{
	int32 offset;
	offset = (wptr >= rptr) ? FIFO_SIZE : 0;
	return(rptr + offset - wptr);
}

uint16 fifoGetNextUtf8(void)
{
	register char c;
	register uint16 uni;
	c = fifoGetChar();
	if(c & 0x80) {
		// when msb of the first byte is 1
		if(c & 0x20) {
			if(c & 0x10) {
				uni = c & 0x07; uni <<= 6;
				uni |= fifoGetChar() & 0x3f; uni <<= 6;
				uni |= fifoGetChar() & 0x3f; uni <<= 6;
				uni |= fifoGetChar() & 0x3f;
			} else {
				uni = c & 0x0f; uni <<= 6;
				uni |= fifoGetChar() & 0x3f; uni <<= 6;
				uni |= fifoGetChar() & 0x3f;
			}
		} else {
			uni = c & 0x1f; uni <<= 6;
			uni |= fifoGetChar() & 0x3f;
		}
	} else {
		// ascii
		uni = c;
	}
	return(uni);
}

/* [] END OF FILE */
