/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "`$INSTANCE_NAME`_addrgen.h"

void `$INSTANCE_NAME`_Start(void)
{
	`$INSTANCE_NAME`_COMP_REG = 96;
	return;
}

void `$INSTANCE_NAME`_Stop(void)
{
	return;
}

void `$INSTANCE_NAME`_SetBaseAddress(uint16 addr)
{
//	`$INSTANCE_NAME`_ADDR_BASE_0_REG = addr;
/*	`$INSTANCE_NAME`_ADDR_BASE_1_REG = addr>>4;
	`$INSTANCE_NAME`_ADDR_BASE_2_REG = addr>>8;
	`$INSTANCE_NAME`_ADDR_BASE_3_REG = addr>>12;*/
	`$INSTANCE_NAME`_ADDR_BASE_HI_REG = addr>>8;
	`$INSTANCE_NAME`_ADDR_BASE_LO_REG = addr;
	return;
}

uint16 `$INSTANCE_NAME`_GetBaseAddress(void)
{
	uint16 addr;
/*
	addr = `$INSTANCE_NAME`_ADDR_BASE_3_REG;
	addr <<= 4;
	addr |= (`$INSTANCE_NAME`_ADDR_BASE_2_REG & 0x0f);
	addr <<= 4;
	addr |= (`$INSTANCE_NAME`_ADDR_BASE_1_REG & 0x0f);
	addr <<= 4;*/
	addr = `$INSTANCE_NAME`_ADDR_BASE_HI_REG;
	addr <<= 8;
	addr |= `$INSTANCE_NAME`_ADDR_BASE_LO_REG;
//	addr |= (`$INSTANCE_NAME`_ADDR_BASE_0_REG & 0x0f);
	return(addr);
}

/* [] END OF FILE */
