/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "cytypes.h"
#include "cyfitter.h"

//#define `$INSTANCE_NAME`_NEXT_ADDR_HI_REG_PTR    (  (reg8 *) `$INSTANCE_NAME`_adder_hi__F1_REG )
//#define `$INSTANCE_NAME`_NEXT_ADDR_LO_REG_PTR    (  (reg8 *) `$INSTANCE_NAME`_adder_lo__F1_REG )
//#define `$INSTANCE_NAME`_NEXT_ADDR_HI_REG        *(  (reg8 *) `$INSTANCE_NAME`_adder_hi__F1_REG )
//#define `$INSTANCE_NAME`_NEXT_ADDR_LO_REG        *(  (reg8 *) `$INSTANCE_NAME`_adder_lo__F1_REG )
#define `$INSTANCE_NAME`_ADDR_BASE_HI_REG_PTR    (  (reg8 *) `$INSTANCE_NAME`_adder_hi__A1_REG )
#define `$INSTANCE_NAME`_ADDR_BASE_LO_REG_PTR    (  (reg8 *) `$INSTANCE_NAME`_adder_lo__A1_REG )
#define `$INSTANCE_NAME`_ADDR_BASE_HI_REG        *(  (reg8 *) `$INSTANCE_NAME`_adder_hi__A1_REG )
#define `$INSTANCE_NAME`_ADDR_BASE_LO_REG        *(  (reg8 *) `$INSTANCE_NAME`_adder_lo__A1_REG )
#define `$INSTANCE_NAME`_ADDR_BASE_16BIT_REG_PTR	(  (reg8 *) `$INSTANCE_NAME`_adder_lo__16BIT_A1_REG
#define `$INSTANCE_NAME`_ADDR_BASE_16BIT_REG	*(  (reg8 *) `$INSTANCE_NAME`_adder_lo__16BIT_A1_REG

#define	`$INSTANCE_NAME`_NEXT_ADDR_LO_REG_PTR	`$INSTANCE_NAME`_adder_lo__A0_REG
#define	`$INSTANCE_NAME`_NEXT_ADDR_HI_REG_PTR	`$INSTANCE_NAME`_adder_hi__A0_REG
#define	`$INSTANCE_NAME`_NEXT_ADDR_LO_REG		*((uint8 *)`$INSTANCE_NAME`_adder_lo__A0_REG)
#define	`$INSTANCE_NAME`_NEXT_ADDR_HI_REG		*((uint8 *)`$INSTANCE_NAME`_adder_hi__A0_REG)
#define `$INSTANCE_NAME`_NEXT_ADDR_16BIT_REG_PTR	(  (reg8 *) `$INSTANCE_NAME`_adder_lo__16BIT_A0_REG)
#define `$INSTANCE_NAME`_NEXT_ADDR_16BIT_REG	*(  (reg8 *) `$INSTANCE_NAME`_adder_lo__16BIT_A0_REG)

#define	`$INSTANCE_NAME`_COMP_REG_PTR	`$INSTANCE_NAME`_dma_counter__D0_REG
#define	`$INSTANCE_NAME`_COMP_REG		*((uint8 *)`$INSTANCE_NAME`_dma_counter__D0_REG)


#if 0
#define	`$INSTANCE_NAME`_NEXT_ADDR_LO_REG_PTR	`$INSTANCE_NAME`_StatusReg_lo__STATUS_REG
#define	`$INSTANCE_NAME`_NEXT_ADDR_HI_REG_PTR	`$INSTANCE_NAME`_StatusReg_hi__STATUS_REG
#define	`$INSTANCE_NAME`_NEXT_ADDR_LO_REG		*((uint8 *)`$INSTANCE_NAME`_StatusReg_lo__STATUS_REG)
#define	`$INSTANCE_NAME`_NEXT_ADDR_HI_REG		*((uint8 *)`$INSTANCE_NAME`_StatusReg_hi__STATUS_REG)
#endif

#if 0
//#define	`$INSTANCE_NAME`_ADDR_BASE_0_REG_PTR	`$INSTANCE_NAME`_ControlReg0__CONTROL_REG
#define	`$INSTANCE_NAME`_ADDR_BASE_1_REG_PTR	`$INSTANCE_NAME`_ControlReg1__CONTROL_REG
#define	`$INSTANCE_NAME`_ADDR_BASE_2_REG_PTR	`$INSTANCE_NAME`_ControlReg2__CONTROL_REG
#define	`$INSTANCE_NAME`_ADDR_BASE_3_REG_PTR	`$INSTANCE_NAME`_ControlReg3__CONTROL_REG
#define	`$INSTANCE_NAME`_ADDR_BASE_0_REG		*((uint8 *)`$INSTANCE_NAME`_ControlReg0__CONTROL_REG)
#define	`$INSTANCE_NAME`_ADDR_BASE_1_REG		*((uint8 *)`$INSTANCE_NAME`_ControlReg1__CONTROL_REG)
#define	`$INSTANCE_NAME`_ADDR_BASE_2_REG		*((uint8 *)`$INSTANCE_NAME`_ControlReg2__CONTROL_REG)
#define	`$INSTANCE_NAME`_ADDR_BASE_3_REG		*((uint8 *)`$INSTANCE_NAME`_ControlReg3__CONTROL_REG)
#endif
void `$INSTANCE_NAME`_Start(void);
void `$INSTANCE_NAME`_Stop(void);
void `$INSTANCE_NAME`_SetBaseAddress(uint16 addr);
uint16 `$INSTANCE_NAME`_GetBaseAddress(void);

//[] END OF FILE
