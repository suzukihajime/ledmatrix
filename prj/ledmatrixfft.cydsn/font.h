/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "sdc/ff.h"

#define	READ_BUFFER_SIZE			256
#define	FONT_BITMAP_BUFFER_SIZE		32

enum {
	FONT_ROP_COPY = 1,
	FONT_ROP_ADD,
	FONT_ROP_OR,
	FONT_ROP_BITWISE_OR,
	FONT_ROP_AND,
	FONT_ROP_BITWISE_AND
};

typedef FIL		HFONT;
typedef FIL		HTABLE;

uint8 openFont(char *font, char *table);
uint8 closeFont(void);
uint32 getFontAddress(uint32 unicode);
void getFontBitmap(uint32 base);		// do not use
void expandFontBitmap(uint32 base, uint8 *dest, uint8 color, uint8 mode);

//[] END OF FILE
