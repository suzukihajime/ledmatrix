/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <device.h>
#include "font.h"
#include "uart.h"
#include "utf8.h"
#include "matrix.h"
#include "fft.h"
#include "clock.h"

void mixdisplaybufer(void);
void initialize(void);
void buf_initialize(uint8 buf[][16]);
void delay_ms(uint16);
uint8 disp_buf[96][16]; // __attribute__((aligned(16), section(".data2")));

extern uint8 clock_disp_buf[96][16];
extern uint8 fft_disp_buf[96][16];

void main()
{
	int i;
	initialize();
	while(1) {
		for(i = 0; i < 100000; i++) {}
	}
}

void mixdisplaybuffer(void)
{
	// clock_disp_bufとfft_disp_bufをmixする。
	int i, j;
	register uint8 buf;
	for(j = 0; j < 96; j++) {
		for(i = 0; i < 16; i++) {
			buf = (fft_disp_buf[j][i] & 0x77) + (clock_disp_buf[j][i] & 0x77);
			if(buf & 0x08) { buf |= 0x07; }
			if(buf & 0x80) { buf |= 0x70; }
			disp_buf[j][i] = buf & 0x77;
		}
	}
	return;
}

void initialize(void)
{
	uint8 ret = 0;
	
	UART_Start();
	Timer_Start();
	Timer_WriteCounter(0);
	buf_initialize(disp_buf);
	buf_initialize(fft_disp_buf);
	buf_initialize(clock_disp_buf);
	Matrix_Start();
	Matrix_SetBaseAddress((uint8*)disp_buf);
//	ret = openFont("JISKAN1.BDF", "CTABLE.TXT");
	FFT_Start();
	clock_Start();
	
	CyGlobalIntEnable;
//	shift_irq_StartEx(shift_isr);
//	rx_irq_StartEx(rx_isr);
	disp_buf[ret][3] = 0xff;
	return;
}

void buf_initialize(uint8 buf[][16])
{
	int16 i, j;
	for(i = 0; i < 192; i++) {
		for(j = 0; j < 16; j++) {
			buf[i][j] = 0x00;
		}
	}
	return;
}

void delay_ms(uint16 delay)
{
	Timer_WriteCounter(delay);
	while(Timer_ReadCounter() != 0) {}
	return;
}

/* [] END OF FILE */
