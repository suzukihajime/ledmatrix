/*******************************************************************************
* File Name: Counter_ADC_PM.c  
* Version 2.30
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "Counter_ADC.h"

static Counter_ADC_backupStruct Counter_ADC_backup;


/*******************************************************************************
* Function Name: Counter_ADC_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Counter_ADC_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void Counter_ADC_SaveConfig(void) 
{
    #if (!Counter_ADC_UsingFixedFunction)
        /* Backup the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            Counter_ADC_backup.CounterUdb = Counter_ADC_ReadCounter();
            Counter_ADC_backup.CounterPeriod = Counter_ADC_ReadPeriod();
            Counter_ADC_backup.CompareValue = Counter_ADC_ReadCompare();
            Counter_ADC_backup.InterruptMaskValue = Counter_ADC_STATUS_MASK;
        #endif /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            Counter_ADC_backup.CounterUdb = Counter_ADC_ReadCounter();
            Counter_ADC_backup.InterruptMaskValue = Counter_ADC_STATUS_MASK;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!Counter_ADC_ControlRegRemoved)
            Counter_ADC_backup.CounterControlRegister = Counter_ADC_ReadControlRegister();
        #endif /* (!Counter_ADC_ControlRegRemoved) */
    #endif /* (!Counter_ADC_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: Counter_ADC_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Counter_ADC_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void Counter_ADC_RestoreConfig(void) 
{      
    #if (!Counter_ADC_UsingFixedFunction)     
        /* Restore the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            /* Interrupt State Backup for Critical Region*/
            uint8 Counter_ADC_interruptState;
        
            Counter_ADC_WriteCounter(Counter_ADC_backup.CounterUdb);
            Counter_ADC_WritePeriod(Counter_ADC_backup.CounterPeriod);
            Counter_ADC_WriteCompare(Counter_ADC_backup.CompareValue);
            /* Enter Critical Region*/
            Counter_ADC_interruptState = CyEnterCriticalSection();
        
            Counter_ADC_STATUS_AUX_CTRL |= Counter_ADC_STATUS_ACTL_INT_EN_MASK;
            /* Exit Critical Region*/
            CyExitCriticalSection(Counter_ADC_interruptState);
            Counter_ADC_STATUS_MASK = Counter_ADC_backup.InterruptMaskValue;
        #endif  /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            Counter_ADC_WriteCounter(Counter_ADC_backup.CounterUdb);
            Counter_ADC_STATUS_MASK = Counter_ADC_backup.InterruptMaskValue;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!Counter_ADC_ControlRegRemoved)
            Counter_ADC_WriteControlRegister(Counter_ADC_backup.CounterControlRegister);
        #endif /* (!Counter_ADC_ControlRegRemoved) */
    #endif /* (!Counter_ADC_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: Counter_ADC_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Counter_ADC_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void Counter_ADC_Sleep(void) 
{
    #if(!Counter_ADC_ControlRegRemoved)
        /* Save Counter's enable state */
        if(Counter_ADC_CTRL_ENABLE == (Counter_ADC_CONTROL & Counter_ADC_CTRL_ENABLE))
        {
            /* Counter is enabled */
            Counter_ADC_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            Counter_ADC_backup.CounterEnableState = 0u;
        }
    #else
        Counter_ADC_backup.CounterEnableState = 1u;
        if(Counter_ADC_backup.CounterEnableState != 0u)
        {
            Counter_ADC_backup.CounterEnableState = 0u;
        }
    #endif /* (!Counter_ADC_ControlRegRemoved) */
    
    Counter_ADC_Stop();
    Counter_ADC_SaveConfig();
}


/*******************************************************************************
* Function Name: Counter_ADC_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  Counter_ADC_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void Counter_ADC_Wakeup(void) 
{
    Counter_ADC_RestoreConfig();
    #if(!Counter_ADC_ControlRegRemoved)
        if(Counter_ADC_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            Counter_ADC_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!Counter_ADC_ControlRegRemoved) */
    
}


/* [] END OF FILE */
