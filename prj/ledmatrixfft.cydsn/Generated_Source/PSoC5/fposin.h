/*******************************************************************************
* File Name: fposin.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_fposin_H) /* Pins fposin_H */
#define CY_PINS_fposin_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "fposin_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 fposin__PORT == 15 && (fposin__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    fposin_Write(uint8 value) ;
void    fposin_SetDriveMode(uint8 mode) ;
uint8   fposin_ReadDataReg(void) ;
uint8   fposin_Read(void) ;
uint8   fposin_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define fposin_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define fposin_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define fposin_DM_RES_UP          PIN_DM_RES_UP
#define fposin_DM_RES_DWN         PIN_DM_RES_DWN
#define fposin_DM_OD_LO           PIN_DM_OD_LO
#define fposin_DM_OD_HI           PIN_DM_OD_HI
#define fposin_DM_STRONG          PIN_DM_STRONG
#define fposin_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define fposin_MASK               fposin__MASK
#define fposin_SHIFT              fposin__SHIFT
#define fposin_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define fposin_PS                     (* (reg8 *) fposin__PS)
/* Data Register */
#define fposin_DR                     (* (reg8 *) fposin__DR)
/* Port Number */
#define fposin_PRT_NUM                (* (reg8 *) fposin__PRT) 
/* Connect to Analog Globals */                                                  
#define fposin_AG                     (* (reg8 *) fposin__AG)                       
/* Analog MUX bux enable */
#define fposin_AMUX                   (* (reg8 *) fposin__AMUX) 
/* Bidirectional Enable */                                                        
#define fposin_BIE                    (* (reg8 *) fposin__BIE)
/* Bit-mask for Aliased Register Access */
#define fposin_BIT_MASK               (* (reg8 *) fposin__BIT_MASK)
/* Bypass Enable */
#define fposin_BYP                    (* (reg8 *) fposin__BYP)
/* Port wide control signals */                                                   
#define fposin_CTL                    (* (reg8 *) fposin__CTL)
/* Drive Modes */
#define fposin_DM0                    (* (reg8 *) fposin__DM0) 
#define fposin_DM1                    (* (reg8 *) fposin__DM1)
#define fposin_DM2                    (* (reg8 *) fposin__DM2) 
/* Input Buffer Disable Override */
#define fposin_INP_DIS                (* (reg8 *) fposin__INP_DIS)
/* LCD Common or Segment Drive */
#define fposin_LCD_COM_SEG            (* (reg8 *) fposin__LCD_COM_SEG)
/* Enable Segment LCD */
#define fposin_LCD_EN                 (* (reg8 *) fposin__LCD_EN)
/* Slew Rate Control */
#define fposin_SLW                    (* (reg8 *) fposin__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define fposin_PRTDSI__CAPS_SEL       (* (reg8 *) fposin__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define fposin_PRTDSI__DBL_SYNC_IN    (* (reg8 *) fposin__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define fposin_PRTDSI__OE_SEL0        (* (reg8 *) fposin__PRTDSI__OE_SEL0) 
#define fposin_PRTDSI__OE_SEL1        (* (reg8 *) fposin__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define fposin_PRTDSI__OUT_SEL0       (* (reg8 *) fposin__PRTDSI__OUT_SEL0) 
#define fposin_PRTDSI__OUT_SEL1       (* (reg8 *) fposin__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define fposin_PRTDSI__SYNC_OUT       (* (reg8 *) fposin__PRTDSI__SYNC_OUT) 


#if defined(fposin__INTSTAT)  /* Interrupt Registers */

    #define fposin_INTSTAT                (* (reg8 *) fposin__INTSTAT)
    #define fposin_SNAP                   (* (reg8 *) fposin__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins fposin_H */

#endif
/* [] END OF FILE */
