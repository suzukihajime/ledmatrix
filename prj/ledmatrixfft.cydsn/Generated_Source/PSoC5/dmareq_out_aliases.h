/*******************************************************************************
* File Name: dmareq_out.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_dmareq_out_ALIASES_H) /* Pins dmareq_out_ALIASES_H */
#define CY_PINS_dmareq_out_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"

/***************************************
*              Constants        
***************************************/
#define dmareq_out_0		dmareq_out__0__PC
#define dmareq_out_1		dmareq_out__1__PC
#define dmareq_out_2		dmareq_out__2__PC
#define dmareq_out_3		dmareq_out__3__PC
#define dmareq_out_4		dmareq_out__4__PC
#define dmareq_out_5		dmareq_out__5__PC

#endif /* End Pins dmareq_out_ALIASES_H */

/* [] END OF FILE */
