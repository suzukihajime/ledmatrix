/*******************************************************************************
* File Name: rck.c  
* Version 1.80
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "rck.h"

/* APIs are not generated for P15[7:6] on PSoC 5 */
#if !(CY_PSOC5A &&\
	 rck__PORT == 15 && ((rck__MASK & 0xC0) != 0))


/*******************************************************************************
* Function Name: rck_Write
********************************************************************************
*
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  None
*  
*******************************************************************************/
void rck_Write(uint8 value) 
{
    uint8 staticBits = (rck_DR & (uint8)(~rck_MASK));
    rck_DR = staticBits | ((uint8)(value << rck_SHIFT) & rck_MASK);
}


/*******************************************************************************
* Function Name: rck_SetDriveMode
********************************************************************************
*
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  None
*
*******************************************************************************/
void rck_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(rck_0, mode);
}


/*******************************************************************************
* Function Name: rck_Read
********************************************************************************
*
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  None
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro rck_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 rck_Read(void) 
{
    return (rck_PS & rck_MASK) >> rck_SHIFT;
}


/*******************************************************************************
* Function Name: rck_ReadDataReg
********************************************************************************
*
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  None 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 rck_ReadDataReg(void) 
{
    return (rck_DR & rck_MASK) >> rck_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(rck_INTSTAT) 

    /*******************************************************************************
    * Function Name: rck_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  None 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 rck_ClearInterrupt(void) 
    {
        return (rck_INTSTAT & rck_MASK) >> rck_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif /* CY_PSOC5A... */

    
/* [] END OF FILE */
