/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "addrgen2_1_addrgen.h"

void addrgen2_1_Start(void)
{
	addrgen2_1_COMP_REG = 96;
	return;
}

void addrgen2_1_Stop(void)
{
	return;
}

void addrgen2_1_SetBaseAddress(uint16 addr)
{
//	addrgen2_1_ADDR_BASE_0_REG = addr;
/*	addrgen2_1_ADDR_BASE_1_REG = addr>>4;
	addrgen2_1_ADDR_BASE_2_REG = addr>>8;
	addrgen2_1_ADDR_BASE_3_REG = addr>>12;*/
	addrgen2_1_ADDR_BASE_HI_REG = addr>>8;
	addrgen2_1_ADDR_BASE_LO_REG = addr;
	return;
}

uint16 addrgen2_1_GetBaseAddress(void)
{
	uint16 addr;
/*
	addr = addrgen2_1_ADDR_BASE_3_REG;
	addr <<= 4;
	addr |= (addrgen2_1_ADDR_BASE_2_REG & 0x0f);
	addr <<= 4;
	addr |= (addrgen2_1_ADDR_BASE_1_REG & 0x0f);
	addr <<= 4;*/
	addr = addrgen2_1_ADDR_BASE_HI_REG;
	addr <<= 8;
	addr |= addrgen2_1_ADDR_BASE_LO_REG;
//	addr |= (addrgen2_1_ADDR_BASE_0_REG & 0x0f);
	return(addr);
}

/* [] END OF FILE */
