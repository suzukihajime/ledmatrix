/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "cytypes.h"
#include "cyfitter.h"


#define matdrv3_1_FIFO0_PTR    (  (reg8 *) matdrv3_1_dp0__F0_REG )
#define matdrv3_1_FIFO1_PTR    (  (reg8 *) matdrv3_1_dp0__F1_REG )

#define matdrv3_1_ROWCOUNTER_CONTROL_REG (* (reg8 *)  matdrv3_1_rowcounter1__CONTROL_AUX_CTL_REG)
#define matdrv3_1_ROWCOUNTER_CONTROL_PTR (  (reg8 *)  matdrv3_1_rowcounter1__CONTROL_AUX_CTL_REG)
#define matdrv3_1_GRADCOUNTER_CONTROL_REG (* (reg8 *)  matdrv3_1_gradcounter1__CONTROL_AUX_CTL_REG)
#define matdrv3_1_GRADCOUNTER_CONTROL_PTR (  (reg8 *)  matdrv3_1_gradcounter1__CONTROL_AUX_CTL_REG)
#define matdrv3_1_COUNTER_LO_CONTROL_REG (* (reg8 *)  matdrv3_1_counter_low__CONTROL_AUX_CTL_REG)
#define matdrv3_1_COUNTER_LO_CONTROL_PTR (  (reg8 *)  matdrv3_1_counter_low__CONTROL_AUX_CTL_REG)
#define matdrv3_1_COUNTER_HI_CONTROL_REG (* (reg8 *)  matdrv3_1_counter_high__CONTROL_AUX_CTL_REG)
#define matdrv3_1_COUNTER_HI_CONTROL_PTR (  (reg8 *)  matdrv3_1_counter_high__CONTROL_AUX_CTL_REG)


#define matdrv3_1_CNTR_ENABLE                 (0x20u)   

/*
#define matdrv_1_dp1_u0__A0_A1_REG CYREG_B1_UDB11_A0_A1
#define matdrv_1_dp1_u0__A0_REG CYREG_B1_UDB11_A0
#define matdrv_1_dp1_u0__A1_REG CYREG_B1_UDB11_A1
#define matdrv_1_dp1_u0__D0_D1_REG CYREG_B1_UDB11_D0_D1
#define matdrv_1_dp1_u0__D0_REG CYREG_B1_UDB11_D0
#define matdrv_1_dp1_u0__D1_REG CYREG_B1_UDB11_D1
#define matdrv_1_dp1_u0__DP_AUX_CTL_REG CYREG_B1_UDB11_ACTL
#define matdrv_1_dp1_u0__F0_F1_REG CYREG_B1_UDB11_F0_F1
#define matdrv_1_dp1_u0__F0_REG CYREG_B1_UDB11_F0
#define matdrv_1_dp1_u0__F1_REG CYREG_B1_UDB11_F1
*/

void matdrv3_1_Start(void);
void matdrv3_1_Stop(void);

//[] END OF FILE
