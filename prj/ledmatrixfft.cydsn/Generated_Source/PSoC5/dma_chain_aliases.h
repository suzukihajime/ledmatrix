/*******************************************************************************
* File Name: dma_chain.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_dma_chain_ALIASES_H) /* Pins dma_chain_ALIASES_H */
#define CY_PINS_dma_chain_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"

/***************************************
*              Constants        
***************************************/
#define dma_chain_0		dma_chain__0__PC
#define dma_chain_1		dma_chain__1__PC
#define dma_chain_2		dma_chain__2__PC

#endif /* End Pins dma_chain_ALIASES_H */

/* [] END OF FILE */
