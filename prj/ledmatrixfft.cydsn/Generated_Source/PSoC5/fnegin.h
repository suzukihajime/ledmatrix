/*******************************************************************************
* File Name: fnegin.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_fnegin_H) /* Pins fnegin_H */
#define CY_PINS_fnegin_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "fnegin_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 fnegin__PORT == 15 && (fnegin__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    fnegin_Write(uint8 value) ;
void    fnegin_SetDriveMode(uint8 mode) ;
uint8   fnegin_ReadDataReg(void) ;
uint8   fnegin_Read(void) ;
uint8   fnegin_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define fnegin_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define fnegin_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define fnegin_DM_RES_UP          PIN_DM_RES_UP
#define fnegin_DM_RES_DWN         PIN_DM_RES_DWN
#define fnegin_DM_OD_LO           PIN_DM_OD_LO
#define fnegin_DM_OD_HI           PIN_DM_OD_HI
#define fnegin_DM_STRONG          PIN_DM_STRONG
#define fnegin_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define fnegin_MASK               fnegin__MASK
#define fnegin_SHIFT              fnegin__SHIFT
#define fnegin_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define fnegin_PS                     (* (reg8 *) fnegin__PS)
/* Data Register */
#define fnegin_DR                     (* (reg8 *) fnegin__DR)
/* Port Number */
#define fnegin_PRT_NUM                (* (reg8 *) fnegin__PRT) 
/* Connect to Analog Globals */                                                  
#define fnegin_AG                     (* (reg8 *) fnegin__AG)                       
/* Analog MUX bux enable */
#define fnegin_AMUX                   (* (reg8 *) fnegin__AMUX) 
/* Bidirectional Enable */                                                        
#define fnegin_BIE                    (* (reg8 *) fnegin__BIE)
/* Bit-mask for Aliased Register Access */
#define fnegin_BIT_MASK               (* (reg8 *) fnegin__BIT_MASK)
/* Bypass Enable */
#define fnegin_BYP                    (* (reg8 *) fnegin__BYP)
/* Port wide control signals */                                                   
#define fnegin_CTL                    (* (reg8 *) fnegin__CTL)
/* Drive Modes */
#define fnegin_DM0                    (* (reg8 *) fnegin__DM0) 
#define fnegin_DM1                    (* (reg8 *) fnegin__DM1)
#define fnegin_DM2                    (* (reg8 *) fnegin__DM2) 
/* Input Buffer Disable Override */
#define fnegin_INP_DIS                (* (reg8 *) fnegin__INP_DIS)
/* LCD Common or Segment Drive */
#define fnegin_LCD_COM_SEG            (* (reg8 *) fnegin__LCD_COM_SEG)
/* Enable Segment LCD */
#define fnegin_LCD_EN                 (* (reg8 *) fnegin__LCD_EN)
/* Slew Rate Control */
#define fnegin_SLW                    (* (reg8 *) fnegin__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define fnegin_PRTDSI__CAPS_SEL       (* (reg8 *) fnegin__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define fnegin_PRTDSI__DBL_SYNC_IN    (* (reg8 *) fnegin__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define fnegin_PRTDSI__OE_SEL0        (* (reg8 *) fnegin__PRTDSI__OE_SEL0) 
#define fnegin_PRTDSI__OE_SEL1        (* (reg8 *) fnegin__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define fnegin_PRTDSI__OUT_SEL0       (* (reg8 *) fnegin__PRTDSI__OUT_SEL0) 
#define fnegin_PRTDSI__OUT_SEL1       (* (reg8 *) fnegin__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define fnegin_PRTDSI__SYNC_OUT       (* (reg8 *) fnegin__PRTDSI__SYNC_OUT) 


#if defined(fnegin__INTSTAT)  /* Interrupt Registers */

    #define fnegin_INTSTAT                (* (reg8 *) fnegin__INTSTAT)
    #define fnegin_SNAP                   (* (reg8 *) fnegin__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins fnegin_H */

#endif
/* [] END OF FILE */
