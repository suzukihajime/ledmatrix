/*******************************************************************************
* File Name: fnegin.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "fnegin.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 fnegin__PORT == 15 && (fnegin__MASK & 0xC0))

/*******************************************************************************
* Function Name: fnegin_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void fnegin_Write(uint8 value) 
{
    uint8 staticBits = fnegin_DR & ~fnegin_MASK;
    fnegin_DR = staticBits | ((value << fnegin_SHIFT) & fnegin_MASK);
}


/*******************************************************************************
* Function Name: fnegin_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void fnegin_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(fnegin_0, mode);
}


/*******************************************************************************
* Function Name: fnegin_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro fnegin_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 fnegin_Read(void) 
{
    return (fnegin_PS & fnegin_MASK) >> fnegin_SHIFT;
}


/*******************************************************************************
* Function Name: fnegin_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 fnegin_ReadDataReg(void) 
{
    return (fnegin_DR & fnegin_MASK) >> fnegin_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(fnegin_INTSTAT) 

    /*******************************************************************************
    * Function Name: fnegin_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 fnegin_ClearInterrupt(void) 
    {
        return (fnegin_INTSTAT & fnegin_MASK) >> fnegin_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
