/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "cytypes.h"
#include "cyfitter.h"

#define	addrgen_1_NEXT_ADDR_LO_REG_PTR	addrgen_1_StatusReg_lo__STATUS_REG
#define	addrgen_1_NEXT_ADDR_HI_REG_PTR	addrgen_1_StatusReg_hi__STATUS_REG
#define	addrgen_1_NEXT_ADDR_LO_REG		*((uint8 *)addrgen_1_StatusReg_lo__STATUS_REG)
#define	addrgen_1_NEXT_ADDR_HI_REG		*((uint8 *)addrgen_1_StatusReg_hi__STATUS_REG)

//#define	addrgen_1_ADDR_BASE_0_REG_PTR	addrgen_1_ControlReg0__CONTROL_REG
#define	addrgen_1_ADDR_BASE_1_REG_PTR	addrgen_1_ControlReg1__CONTROL_REG
#define	addrgen_1_ADDR_BASE_2_REG_PTR	addrgen_1_ControlReg2__CONTROL_REG
#define	addrgen_1_ADDR_BASE_3_REG_PTR	addrgen_1_ControlReg3__CONTROL_REG
#define	addrgen_1_ADDR_BASE_0_REG		*((uint8 *)addrgen_1_ControlReg0__CONTROL_REG)
#define	addrgen_1_ADDR_BASE_1_REG		*((uint8 *)addrgen_1_ControlReg1__CONTROL_REG)
#define	addrgen_1_ADDR_BASE_2_REG		*((uint8 *)addrgen_1_ControlReg2__CONTROL_REG)
#define	addrgen_1_ADDR_BASE_3_REG		*((uint8 *)addrgen_1_ControlReg3__CONTROL_REG)

void addrgen_1_Start(void);
void addrgen_1_Stop(void);
void addrgen_1_SetBaseAddress(uint16 addr);
uint16 addrgen_1_GetBaseAddress(void);

//[] END OF FILE
