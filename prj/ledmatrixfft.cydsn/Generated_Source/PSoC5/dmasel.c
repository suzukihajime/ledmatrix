/*******************************************************************************
* File Name: dmasel.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Control Register.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "dmasel.h"

#if !defined(dmasel_Async_ctrl_reg__REMOVED) /* Check for removal by optimization */

/*******************************************************************************
* Function Name: dmasel_Write
********************************************************************************
*
* Summary:
*  Write a byte to the Control Register.
*
* Parameters:
*  control:  The value to be assigned to the Control Register.
*
* Return:
*  None.
*
*******************************************************************************/
void dmasel_Write(uint8 control) 
{
    dmasel_Control = control;
}


/*******************************************************************************
* Function Name: dmasel_Read
********************************************************************************
*
* Summary:
*  Reads the current value assigned to the Control Register.
*
* Parameters:
*  None.
*
* Return:
*  Returns the current value in the Control Register.
*
*******************************************************************************/
uint8 dmasel_Read(void) 
{
    return dmasel_Control;
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
