/*******************************************************************************
* File Name: OPAMP.c
* Version 1.80
*
* Description:
*  This file provides the source code to the API for OpAmp (Analog Buffer) 
*  Component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "OPAMP.h"
#include <CyLib.h>

uint8 OPAMP_initVar = 0u;


/*******************************************************************************   
* Function Name: OPAMP_Init
********************************************************************************
*
* Summary:
*  Initialize component's parameters to the parameters set by user in the 
*  customizer of the component placed onto schematic. Usually called in 
*  OPAMP_Start().
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void OPAMP_Init(void) 
{
    OPAMP_SetPower(OPAMP_DEFAULT_POWER);
}


/*******************************************************************************   
* Function Name: OPAMP_Enable
********************************************************************************
*
* Summary:
*  Enables the OpAmp block operation
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void OPAMP_Enable(void) 
{
    /* Enable negative charge pumps in ANIF */
    OPAMP_PUMP_CR1_REG  |= (OPAMP_PUMP_CR1_CLKSEL | OPAMP_PUMP_CR1_FORCE);

    /* Enable power to buffer in active mode */
    OPAMP_PM_ACT_CFG_REG |= OPAMP_ACT_PWR_EN;

    /* Enable power to buffer in alternative active mode */
    OPAMP_PM_STBY_CFG_REG |= OPAMP_STBY_PWR_EN;
}


/*******************************************************************************
* Function Name:   OPAMP_Start
********************************************************************************
*
* Summary:
*  The start function initializes the Analog Buffer with the default values and 
*  sets the power to the given level. A power level of 0, is same as 
*  executing the stop function.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Global variables:
*  OPAMP_initVar: Used to check the initial configuration, modified 
*  when this function is called for the first time.
*
*******************************************************************************/
void OPAMP_Start(void) 
{
    if(OPAMP_initVar == 0u)
    {
        OPAMP_initVar = 1u;
        OPAMP_Init();
    }

    OPAMP_Enable();
}


/*******************************************************************************
* Function Name: OPAMP_Stop
********************************************************************************
*
* Summary:
*  Powers down amplifier to lowest power state.
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void OPAMP_Stop(void) 
{
    /* Disable negative charge pumps for ANIF only if one ABuf is turned ON */
    if(OPAMP_PM_ACT_CFG_REG == OPAMP_ACT_PWR_EN)
    {
        OPAMP_PUMP_CR1_REG &= ~(OPAMP_PUMP_CR1_CLKSEL | OPAMP_PUMP_CR1_FORCE);
    }
    
    /* Disable power to buffer in active mode template */
    OPAMP_PM_ACT_CFG_REG &= ~OPAMP_ACT_PWR_EN;

    /* Disable power to buffer in alternative active mode template */
    OPAMP_PM_STBY_CFG_REG &= ~OPAMP_STBY_PWR_EN;
}


/*******************************************************************************
* Function Name: OPAMP_SetPower
********************************************************************************
*
* Summary:
*  Sets power level of Analog buffer.
*
* Parameters: 
*  power: PSoC3: Sets power level between low (1) and high power (3).
*         PSoC5: Sets power level High (0)
*
* Return:
*  void
*
**********************************************************************************/
void OPAMP_SetPower(uint8 power) 
{
    /* Only High power can be used in PSoC5 */
    #if CY_PSOC5A
        CYASSERT(power == OPAMP_HIGHPOWER);
    #endif   /* (CY_PSOC5A) */

    OPAMP_CR_REG = ((OPAMP_CR_REG & ~OPAMP_PWR_MASK) | 
                               ( power & OPAMP_PWR_MASK));   /* Set device power */
}


/* [] END OF FILE */
