/*******************************************************************************
* File Name: fout.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_fout_H) /* Pins fout_H */
#define CY_PINS_fout_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "fout_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 fout__PORT == 15 && (fout__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    fout_Write(uint8 value) ;
void    fout_SetDriveMode(uint8 mode) ;
uint8   fout_ReadDataReg(void) ;
uint8   fout_Read(void) ;
uint8   fout_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define fout_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define fout_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define fout_DM_RES_UP          PIN_DM_RES_UP
#define fout_DM_RES_DWN         PIN_DM_RES_DWN
#define fout_DM_OD_LO           PIN_DM_OD_LO
#define fout_DM_OD_HI           PIN_DM_OD_HI
#define fout_DM_STRONG          PIN_DM_STRONG
#define fout_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define fout_MASK               fout__MASK
#define fout_SHIFT              fout__SHIFT
#define fout_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define fout_PS                     (* (reg8 *) fout__PS)
/* Data Register */
#define fout_DR                     (* (reg8 *) fout__DR)
/* Port Number */
#define fout_PRT_NUM                (* (reg8 *) fout__PRT) 
/* Connect to Analog Globals */                                                  
#define fout_AG                     (* (reg8 *) fout__AG)                       
/* Analog MUX bux enable */
#define fout_AMUX                   (* (reg8 *) fout__AMUX) 
/* Bidirectional Enable */                                                        
#define fout_BIE                    (* (reg8 *) fout__BIE)
/* Bit-mask for Aliased Register Access */
#define fout_BIT_MASK               (* (reg8 *) fout__BIT_MASK)
/* Bypass Enable */
#define fout_BYP                    (* (reg8 *) fout__BYP)
/* Port wide control signals */                                                   
#define fout_CTL                    (* (reg8 *) fout__CTL)
/* Drive Modes */
#define fout_DM0                    (* (reg8 *) fout__DM0) 
#define fout_DM1                    (* (reg8 *) fout__DM1)
#define fout_DM2                    (* (reg8 *) fout__DM2) 
/* Input Buffer Disable Override */
#define fout_INP_DIS                (* (reg8 *) fout__INP_DIS)
/* LCD Common or Segment Drive */
#define fout_LCD_COM_SEG            (* (reg8 *) fout__LCD_COM_SEG)
/* Enable Segment LCD */
#define fout_LCD_EN                 (* (reg8 *) fout__LCD_EN)
/* Slew Rate Control */
#define fout_SLW                    (* (reg8 *) fout__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define fout_PRTDSI__CAPS_SEL       (* (reg8 *) fout__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define fout_PRTDSI__DBL_SYNC_IN    (* (reg8 *) fout__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define fout_PRTDSI__OE_SEL0        (* (reg8 *) fout__PRTDSI__OE_SEL0) 
#define fout_PRTDSI__OE_SEL1        (* (reg8 *) fout__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define fout_PRTDSI__OUT_SEL0       (* (reg8 *) fout__PRTDSI__OUT_SEL0) 
#define fout_PRTDSI__OUT_SEL1       (* (reg8 *) fout__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define fout_PRTDSI__SYNC_OUT       (* (reg8 *) fout__PRTDSI__SYNC_OUT) 


#if defined(fout__INTSTAT)  /* Interrupt Registers */

    #define fout_INTSTAT                (* (reg8 *) fout__INTSTAT)
    #define fout_SNAP                   (* (reg8 *) fout__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins fout_H */

#endif
/* [] END OF FILE */
