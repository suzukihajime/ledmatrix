/*******************************************************************************
* File Name: rck.h  
* Version 1.80
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_rck_H) /* Pins rck_H */
#define CY_PINS_rck_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "rck_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_80 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 rck__PORT == 15 && ((rck__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

void    rck_Write(uint8 value) ;
void    rck_SetDriveMode(uint8 mode) ;
uint8   rck_ReadDataReg(void) ;
uint8   rck_Read(void) ;
uint8   rck_ClearInterrupt(void) ;


/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define rck_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define rck_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define rck_DM_RES_UP          PIN_DM_RES_UP
#define rck_DM_RES_DWN         PIN_DM_RES_DWN
#define rck_DM_OD_LO           PIN_DM_OD_LO
#define rck_DM_OD_HI           PIN_DM_OD_HI
#define rck_DM_STRONG          PIN_DM_STRONG
#define rck_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define rck_MASK               rck__MASK
#define rck_SHIFT              rck__SHIFT
#define rck_WIDTH              1u


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define rck_PS                     (* (reg8 *) rck__PS)
/* Data Register */
#define rck_DR                     (* (reg8 *) rck__DR)
/* Port Number */
#define rck_PRT_NUM                (* (reg8 *) rck__PRT) 
/* Connect to Analog Globals */                                                  
#define rck_AG                     (* (reg8 *) rck__AG)                       
/* Analog MUX bux enable */
#define rck_AMUX                   (* (reg8 *) rck__AMUX) 
/* Bidirectional Enable */                                                        
#define rck_BIE                    (* (reg8 *) rck__BIE)
/* Bit-mask for Aliased Register Access */
#define rck_BIT_MASK               (* (reg8 *) rck__BIT_MASK)
/* Bypass Enable */
#define rck_BYP                    (* (reg8 *) rck__BYP)
/* Port wide control signals */                                                   
#define rck_CTL                    (* (reg8 *) rck__CTL)
/* Drive Modes */
#define rck_DM0                    (* (reg8 *) rck__DM0) 
#define rck_DM1                    (* (reg8 *) rck__DM1)
#define rck_DM2                    (* (reg8 *) rck__DM2) 
/* Input Buffer Disable Override */
#define rck_INP_DIS                (* (reg8 *) rck__INP_DIS)
/* LCD Common or Segment Drive */
#define rck_LCD_COM_SEG            (* (reg8 *) rck__LCD_COM_SEG)
/* Enable Segment LCD */
#define rck_LCD_EN                 (* (reg8 *) rck__LCD_EN)
/* Slew Rate Control */
#define rck_SLW                    (* (reg8 *) rck__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define rck_PRTDSI__CAPS_SEL       (* (reg8 *) rck__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define rck_PRTDSI__DBL_SYNC_IN    (* (reg8 *) rck__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define rck_PRTDSI__OE_SEL0        (* (reg8 *) rck__PRTDSI__OE_SEL0) 
#define rck_PRTDSI__OE_SEL1        (* (reg8 *) rck__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define rck_PRTDSI__OUT_SEL0       (* (reg8 *) rck__PRTDSI__OUT_SEL0) 
#define rck_PRTDSI__OUT_SEL1       (* (reg8 *) rck__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define rck_PRTDSI__SYNC_OUT       (* (reg8 *) rck__PRTDSI__SYNC_OUT) 


#if defined(rck__INTSTAT)  /* Interrupt Registers */

    #define rck_INTSTAT                (* (reg8 *) rck__INTSTAT)
    #define rck_SNAP                   (* (reg8 *) rck__SNAP)

#endif /* Interrupt Registers */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_rck_H */


/* [] END OF FILE */
