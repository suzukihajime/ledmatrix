/*******************************************************************************
* File Name: pdata.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "pdata.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 pdata__PORT == 15 && (pdata__MASK & 0xC0))

/*******************************************************************************
* Function Name: pdata_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void pdata_Write(uint8 value) 
{
    uint8 staticBits = pdata_DR & ~pdata_MASK;
    pdata_DR = staticBits | ((value << pdata_SHIFT) & pdata_MASK);
}


/*******************************************************************************
* Function Name: pdata_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void pdata_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(pdata_0, mode);
	CyPins_SetPinDriveMode(pdata_1, mode);
	CyPins_SetPinDriveMode(pdata_2, mode);
	CyPins_SetPinDriveMode(pdata_3, mode);
	CyPins_SetPinDriveMode(pdata_4, mode);
	CyPins_SetPinDriveMode(pdata_5, mode);
	CyPins_SetPinDriveMode(pdata_6, mode);
	CyPins_SetPinDriveMode(pdata_7, mode);
}


/*******************************************************************************
* Function Name: pdata_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro pdata_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 pdata_Read(void) 
{
    return (pdata_PS & pdata_MASK) >> pdata_SHIFT;
}


/*******************************************************************************
* Function Name: pdata_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 pdata_ReadDataReg(void) 
{
    return (pdata_DR & pdata_MASK) >> pdata_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(pdata_INTSTAT) 

    /*******************************************************************************
    * Function Name: pdata_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 pdata_ClearInterrupt(void) 
    {
        return (pdata_INTSTAT & pdata_MASK) >> pdata_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
