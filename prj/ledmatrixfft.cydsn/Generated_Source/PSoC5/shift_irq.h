/*******************************************************************************
* File Name: shift_irq.h
* Version 1.60
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/
#if !defined(__shift_irq_INTC_H__)
#define __shift_irq_INTC_H__


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void shift_irq_Start(void);
void shift_irq_StartEx(cyisraddress address);
void shift_irq_Stop(void) ;

CY_ISR_PROTO(shift_irq_Interrupt);

void shift_irq_SetVector(cyisraddress address) ;
cyisraddress shift_irq_GetVector(void) ;

void shift_irq_SetPriority(uint8 priority) ;
uint8 shift_irq_GetPriority(void) ;

void shift_irq_Enable(void) ;
uint8 shift_irq_GetState(void) ;
void shift_irq_Disable(void) ;

void shift_irq_SetPending(void) ;
void shift_irq_ClearPending(void) ;


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the shift_irq ISR. */
#define shift_irq_INTC_VECTOR            ((reg32 *) shift_irq__INTC_VECT)

/* Address of the shift_irq ISR priority. */
#define shift_irq_INTC_PRIOR             ((reg8 *) shift_irq__INTC_PRIOR_REG)

/* Priority of the shift_irq interrupt. */
#define shift_irq_INTC_PRIOR_NUMBER      shift_irq__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable shift_irq interrupt. */
#define shift_irq_INTC_SET_EN            ((reg32 *) shift_irq__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the shift_irq interrupt. */
#define shift_irq_INTC_CLR_EN            ((reg32 *) shift_irq__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the shift_irq interrupt state to pending. */
#define shift_irq_INTC_SET_PD            ((reg32 *) shift_irq__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the shift_irq interrupt. */
#define shift_irq_INTC_CLR_PD            ((reg32 *) shift_irq__INTC_CLR_PD_REG)



/* __shift_irq_INTC_H__ */
#endif
