/*******************************************************************************
* File Name: Neg.h  
* Version 1.80
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Neg_ALIASES_H) /* Pins Neg_ALIASES_H */
#define CY_PINS_Neg_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"



/***************************************
*              Constants        
***************************************/
#define Neg_0		Neg__0__PC

#endif /* End Pins Neg_ALIASES_H */

/* [] END OF FILE */
