/*******************************************************************************
* File Name: dma_chain.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "dma_chain.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 dma_chain__PORT == 15 && (dma_chain__MASK & 0xC0))

/*******************************************************************************
* Function Name: dma_chain_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void dma_chain_Write(uint8 value) 
{
    uint8 staticBits = dma_chain_DR & ~dma_chain_MASK;
    dma_chain_DR = staticBits | ((value << dma_chain_SHIFT) & dma_chain_MASK);
}


/*******************************************************************************
* Function Name: dma_chain_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void dma_chain_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(dma_chain_0, mode);
	CyPins_SetPinDriveMode(dma_chain_1, mode);
	CyPins_SetPinDriveMode(dma_chain_2, mode);
}


/*******************************************************************************
* Function Name: dma_chain_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro dma_chain_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 dma_chain_Read(void) 
{
    return (dma_chain_PS & dma_chain_MASK) >> dma_chain_SHIFT;
}


/*******************************************************************************
* Function Name: dma_chain_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 dma_chain_ReadDataReg(void) 
{
    return (dma_chain_DR & dma_chain_MASK) >> dma_chain_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(dma_chain_INTSTAT) 

    /*******************************************************************************
    * Function Name: dma_chain_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 dma_chain_ClearInterrupt(void) 
    {
        return (dma_chain_INTSTAT & dma_chain_MASK) >> dma_chain_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
