/*******************************************************************************
* File Name: clock_irq.c  
* Version 1.70
*
*  Description:
*   API for controlling the state of an interrupt.
*
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/


#include <CYDEVICE_TRM.H>
#include <CYLIB.H>
#include <clock_irq.H>

#if !defined(clock_irq__REMOVED) /* Check for removal by optimization */

/*******************************************************************************
*  Place your includes, defines and code here 
********************************************************************************/
/* `#START clock_irq_intc` */

/* `#END` */

#ifndef CYINT_IRQ_BASE
#define CYINT_IRQ_BASE      16
#endif /* CYINT_IRQ_BASE */
#ifndef CYINT_VECT_TABLE
#define CYINT_VECT_TABLE    ((cyisraddress **) CYREG_NVIC_VECT_OFFSET)
#endif /* CYINT_VECT_TABLE */

/* Declared in startup, used to set unused interrupts to. */
CY_ISR_PROTO(IntDefaultHandler);


/*******************************************************************************
* Function Name: clock_irq_Start
********************************************************************************
*
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   None
*
* Return:
*   None
*
*******************************************************************************/
void clock_irq_Start(void)
{
    /* For all we know the interrupt is active. */
    clock_irq_Disable();

    /* Set the ISR to point to the clock_irq Interrupt. */
    clock_irq_SetVector(&clock_irq_Interrupt);

    /* Set the priority. */
    clock_irq_SetPriority((uint8)clock_irq_INTC_PRIOR_NUMBER);

    /* Enable it. */
    clock_irq_Enable();
}


/*******************************************************************************
* Function Name: clock_irq_StartEx
********************************************************************************
*
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   address: Address of the ISR to set in the interrupt vector table.
*
* Return:
*   None
*
*******************************************************************************/
void clock_irq_StartEx(cyisraddress address)
{
    /* For all we know the interrupt is active. */
    clock_irq_Disable();

    /* Set the ISR to point to the clock_irq Interrupt. */
    clock_irq_SetVector(address);

    /* Set the priority. */
    clock_irq_SetPriority((uint8)clock_irq_INTC_PRIOR_NUMBER);

    /* Enable it. */
    clock_irq_Enable();
}


/*******************************************************************************
* Function Name: clock_irq_Stop
********************************************************************************
*
* Summary:
*   Disables and removes the interrupt.
*
* Parameters:  
*
* Return:
*   None
*
*******************************************************************************/
void clock_irq_Stop(void)
{
    /* Disable this interrupt. */
    clock_irq_Disable();

    /* Set the ISR to point to the passive one. */
    clock_irq_SetVector(&IntDefaultHandler);
}


/*******************************************************************************
* Function Name: clock_irq_Interrupt
********************************************************************************
*
* Summary:
*   The default Interrupt Service Routine for clock_irq.
*
*   Add custom code between the coments to keep the next version of this file
*   from over writting your code.
*
* Parameters:  
*
* Return:
*   None
*
*******************************************************************************/
CY_ISR(clock_irq_Interrupt)
{
    /*  Place your Interrupt code here. */
    /* `#START clock_irq_Interrupt` */

    /* `#END` */
}


/*******************************************************************************
* Function Name: clock_irq_SetVector
********************************************************************************
*
* Summary:
*   Change the ISR vector for the Interrupt. Note calling clock_irq_Start
*   will override any effect this method would have had. To set the vector 
*   before the component has been started use clock_irq_StartEx instead.
*
* Parameters:
*   address: Address of the ISR to set in the interrupt vector table.
*
* Return:
*   None
*
*******************************************************************************/
void clock_irq_SetVector(cyisraddress address)
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    ramVectorTable[CYINT_IRQ_BASE + (uint32)clock_irq__INTC_NUMBER] = address;
}


/*******************************************************************************
* Function Name: clock_irq_GetVector
********************************************************************************
*
* Summary:
*   Gets the "address" of the current ISR vector for the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   Address of the ISR in the interrupt vector table.
*
*******************************************************************************/
cyisraddress clock_irq_GetVector(void)
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    return ramVectorTable[CYINT_IRQ_BASE + (uint32)clock_irq__INTC_NUMBER];
}


/*******************************************************************************
* Function Name: clock_irq_SetPriority
********************************************************************************
*
* Summary:
*   Sets the Priority of the Interrupt. Note calling clock_irq_Start
*   or clock_irq_StartEx will override any effect this method 
*   would have had. This method should only be called after 
*   clock_irq_Start or clock_irq_StartEx has been called. To set 
*   the initial priority for the component use the cydwr file in the tool.
*
* Parameters:
*   priority: Priority of the interrupt. 0 - 7, 0 being the highest.
*
* Return:
*   None
*
*******************************************************************************/
void clock_irq_SetPriority(uint8 priority)
{
    *clock_irq_INTC_PRIOR = priority << 5;
}


/*******************************************************************************
* Function Name: clock_irq_GetPriority
********************************************************************************
*
* Summary:
*   Gets the Priority of the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   Priority of the interrupt. 0 - 7, 0 being the highest.
*
*******************************************************************************/
uint8 clock_irq_GetPriority(void)
{
    uint8 priority;


    priority = *clock_irq_INTC_PRIOR >> 5;

    return priority;
}


/*******************************************************************************
* Function Name: clock_irq_Enable
********************************************************************************
*
* Summary:
*   Enables the interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void clock_irq_Enable(void)
{
    /* Enable the general interrupt. */
    *clock_irq_INTC_SET_EN = clock_irq__INTC_MASK;
}


/*******************************************************************************
* Function Name: clock_irq_GetState
********************************************************************************
*
* Summary:
*   Gets the state (enabled, disabled) of the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   1 if enabled, 0 if disabled.
*
*******************************************************************************/
uint8 clock_irq_GetState(void)
{
    /* Get the state of the general interrupt. */
    return ((*clock_irq_INTC_SET_EN & (uint32)clock_irq__INTC_MASK) != 0u) ? 1u:0u;
}


/*******************************************************************************
* Function Name: clock_irq_Disable
********************************************************************************
*
* Summary:
*   Disables the Interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void clock_irq_Disable(void)
{
    /* Disable the general interrupt. */
    *clock_irq_INTC_CLR_EN = clock_irq__INTC_MASK;
}


/*******************************************************************************
* Function Name: clock_irq_SetPending
********************************************************************************
*
* Summary:
*   Causes the Interrupt to enter the pending state, a software method of
*   generating the interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void clock_irq_SetPending(void)
{
    *clock_irq_INTC_SET_PD = clock_irq__INTC_MASK;
}


/*******************************************************************************
* Function Name: clock_irq_ClearPending
********************************************************************************
*
* Summary:
*   Clears a pending interrupt.
*
* Parameters:
*   None
*
* Return:
*   None
*
*******************************************************************************/
void clock_irq_ClearPending(void)
{
    *clock_irq_INTC_CLR_PD = clock_irq__INTC_MASK;
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */
