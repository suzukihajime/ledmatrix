/*******************************************************************************
* File Name: Debugpdatapin.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_Debugpdatapin_ALIASES_H) /* Pins Debugpdatapin_ALIASES_H */
#define CY_PINS_Debugpdatapin_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"

/***************************************
*              Constants        
***************************************/
#define Debugpdatapin_0		Debugpdatapin__0__PC
#define Debugpdatapin_1		Debugpdatapin__1__PC

#endif /* End Pins Debugpdatapin_ALIASES_H */

/* [] END OF FILE */
