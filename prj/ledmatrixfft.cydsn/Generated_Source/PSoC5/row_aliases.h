/*******************************************************************************
* File Name: row.h  
* Version 1.80
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_row_ALIASES_H) /* Pins row_ALIASES_H */
#define CY_PINS_row_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"



/***************************************
*              Constants        
***************************************/
#define row_0		row__0__PC
#define row_1		row__1__PC
#define row_2		row__2__PC
#define row_3		row__3__PC

#endif /* End Pins row_ALIASES_H */

/* [] END OF FILE */
