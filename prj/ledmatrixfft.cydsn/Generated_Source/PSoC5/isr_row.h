/*******************************************************************************
* File Name: isr_row.h
* Version 1.60
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/
#if !defined(__isr_row_INTC_H__)
#define __isr_row_INTC_H__


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void isr_row_Start(void);
void isr_row_StartEx(cyisraddress address);
void isr_row_Stop(void) ;

CY_ISR_PROTO(isr_row_Interrupt);

void isr_row_SetVector(cyisraddress address) ;
cyisraddress isr_row_GetVector(void) ;

void isr_row_SetPriority(uint8 priority) ;
uint8 isr_row_GetPriority(void) ;

void isr_row_Enable(void) ;
uint8 isr_row_GetState(void) ;
void isr_row_Disable(void) ;

void isr_row_SetPending(void) ;
void isr_row_ClearPending(void) ;


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the isr_row ISR. */
#define isr_row_INTC_VECTOR            ((reg32 *) isr_row__INTC_VECT)

/* Address of the isr_row ISR priority. */
#define isr_row_INTC_PRIOR             ((reg8 *) isr_row__INTC_PRIOR_REG)

/* Priority of the isr_row interrupt. */
#define isr_row_INTC_PRIOR_NUMBER      isr_row__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable isr_row interrupt. */
#define isr_row_INTC_SET_EN            ((reg32 *) isr_row__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the isr_row interrupt. */
#define isr_row_INTC_CLR_EN            ((reg32 *) isr_row__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the isr_row interrupt state to pending. */
#define isr_row_INTC_SET_PD            ((reg32 *) isr_row__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the isr_row interrupt. */
#define isr_row_INTC_CLR_PD            ((reg32 *) isr_row__INTC_CLR_PD_REG)



/* __isr_row_INTC_H__ */
#endif
