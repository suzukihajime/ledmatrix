/*******************************************************************************
* File Name: fposin.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "fposin.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 fposin__PORT == 15 && (fposin__MASK & 0xC0))

/*******************************************************************************
* Function Name: fposin_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void fposin_Write(uint8 value) 
{
    uint8 staticBits = fposin_DR & ~fposin_MASK;
    fposin_DR = staticBits | ((value << fposin_SHIFT) & fposin_MASK);
}


/*******************************************************************************
* Function Name: fposin_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void fposin_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(fposin_0, mode);
}


/*******************************************************************************
* Function Name: fposin_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro fposin_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 fposin_Read(void) 
{
    return (fposin_PS & fposin_MASK) >> fposin_SHIFT;
}


/*******************************************************************************
* Function Name: fposin_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 fposin_ReadDataReg(void) 
{
    return (fposin_DR & fposin_MASK) >> fposin_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(fposin_INTSTAT) 

    /*******************************************************************************
    * Function Name: fposin_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 fposin_ClearInterrupt(void) 
    {
        return (fposin_INTSTAT & fposin_MASK) >> fposin_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
