/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <device.h>
#include "sd.h"
#include "uart.h"
#include <cytypes.h>

FATFS fatfs, *fs;
DIR rootdir;
SDSTATUS g_sd;

BYTE SD_Init(SDSTATUS *sd)
{
	uint8 res;
	// Initialize SPI (used to communicate with SD card) running at 1 MHz
	SPIM_1_Init();
	SPIM_1_Enable();
	SPIM_1_EnableInt();
	SPIM_1_Start();
	SPIM_1_ClearRxBuffer();
	SPIM_1_ClearTxBuffer();
	
	// Initialize SD card
	if(SD_Slot1Inserted() == 0) {
//		iputs("Not inserted\r\n");
		g_sd.bInserted = 0;
		g_sd.bInitialized = 0;
		g_sd.bOpen = 0;
		*sd = g_sd;
		return 0;
	}
	g_sd.bInserted = 1;
	res = f_mount(0, &fatfs);			// Mount drive #0
	uprintf("mount\r\n");
	if(res != FR_OK) { return 0; }
	res = f_opendir(&rootdir, "");	// Open root directory
	if(res != FR_OK) { return 0; }
	g_sd.bInitialized = 1;
	return 1;
}

BYTE SD_Slot1Inserted(void)
{
	return 1;
/*	if(CardDetect_Read() != 0)
		return 0;
	else
		return 1;*/
}

uint8 SD_OpenFile(SDSTATUS *sd, FIL *file, char *filename)
{
	uint32 res;
	if(SD_Slot1Inserted() == 0) {
		g_sd.bInserted = 0;
		g_sd.bInitialized = 0;
		g_sd.bOpen = 0;
		*sd = g_sd;
		return 0;
	}
	file->fs = &fatfs;
	res = f_open(file, filename, FA_READ | FA_OPEN_EXISTING);
	if(res == FR_OK) {
		g_sd.bOpen = 1;
		*sd = g_sd;
		return 1;
	} else {
		f_close(file);
		return 0;
	}
}

uint8 SD_CloseFile(SDSTATUS *sd, FIL *file)
{
	f_close(file);
	return 1;
}

uint32 SD_ReadLine(SDSTATUS *sd, FIL *file, char *buf, uint32 len)
{
	unsigned int i;
	if(SD_Slot1Inserted() == 0) {
		g_sd.bInserted = 0;
		g_sd.bInitialized = 0;
		g_sd.bOpen = 0;
		*sd = g_sd;
		return 0;
	}
	f_read(file, buf, len, &i);
	if(i != len) {
		return 0;
	}
	return(len);
}

void SD_Close(void)
{
//	f_close(&file);
//	SD_Power_Write(0);			// Power Off
	return;
}

DWORD get_fattime(void)
{
	DWORD tmr;
	
	CYGlobalIntDisable;
	/* Pack date and time into a DWORD variable */
	tmr =	  (((DWORD)2012 - 1980) << 25)
			| ((DWORD)11 << 21)
			| ((DWORD)03 << 16)
			| ((WORD)0 << 11)
			| ((WORD)0 << 5)
			| ((WORD)0 >> 1);
	CYGlobalIntEnable;

	return tmr;
}

/* [] END OF FILE */