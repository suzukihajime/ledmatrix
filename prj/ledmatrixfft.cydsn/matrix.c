/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"
#include "matrix.h"

/* dma vars */
uint8 DMA_DATA_Chan, DMA_ADDR_LO_Chan;
uint8 DMA_DATA_TD[1], DMA_ADDR_LO_TD[1];

extern uint8 disp_buf[96][16];

void dma_initialize(void)
{
	/*
	DMA_DATA
	データの転送を行う。これがrequestされる前に、DMA_DATA_TDのsrcアドレスがDMA_ADDRによって書き換えられているとする。
	*/
	DMA_DATA_Chan = DMA_DATA_DmaInitialize(1, 0,
		HI16(DMA_DATA_SRC_BASE), HI16(DMA_DATA_DST_BASE));
	DMA_DATA_TD[0] = CyDmaTdAllocate();
	CyDmaTdSetConfiguration(DMA_DATA_TD[0], 1, DMA_INVALID_TD, DMA_DATA__TD_TERMOUT_EN);
	CyDmaTdSetAddress(DMA_DATA_TD[0],
//		LO16((uint32)addrgen2_1_NEXT_ADDR_HI_REG_PTR),
		LO16((uint32)0),
		LO16((uint32)matdrv3_1_FIFO0_PTR));
//		LO16((uint32)Control_Reg_1_Control_PTR));
	CyDmaChSetInitialTd(DMA_DATA_Chan, DMA_DATA_TD[0]);
	CyDmaChEnable(DMA_DATA_Chan, 1);
	
	/*
	DMA_ADDR_LO
	DMA_DATAのTDのsrcを書き換える。addrgenのstatusregから転送する。
	*/
	DMA_ADDR_LO_Chan = DMA_ADDR_LO_DmaInitialize(0/*1*/, 0,
		HI16(DMA_ADDR_SRC_BASE), HI16(DMA_ADDR_DST_BASE));
	DMA_ADDR_LO_TD[0] = CyDmaTdAllocate();
	CyDmaTdSetConfiguration(DMA_ADDR_LO_TD[0], 2/*1*/, DMA_INVALID_TD, DMA_ADDR_LO__TD_TERMOUT_EN);
	CyDmaTdSetAddress(DMA_ADDR_LO_TD[0],
//		LO16((uint32)addrgen2_1_NEXT_ADDR_LO_REG_PTR),
		LO16((uint32)addrgen2_1_NEXT_ADDR_16BIT_REG_PTR),		// アドレスは16ビット同時に転送する。
		LO16((uint32)&((dmac_tdmem2 CYXDATA *)DMAC_TDMEM)[DMA_DATA_TD[0]].src_adr/*TD1[0]*/));
//		LO16((uint32)Control_Reg_1_Control_PTR));
	CyDmaChSetInitialTd(DMA_ADDR_LO_Chan, DMA_ADDR_LO_TD[0]);
	CyDmaChEnable(DMA_ADDR_LO_Chan, 1);
	return;
}

void Matrix_Start(void)
{
	dma_initialize();
	matdrv3_1_Start();
	addrgen2_1_Start();
	return;
}

void Matrix_Stop(void)
{
	return;
}

void Matrix_SetBaseAddress(uint8 *ptr)
{
	addrgen2_1_SetBaseAddress((uint16)ptr);
	return;
}


/* [] END OF FILE */
