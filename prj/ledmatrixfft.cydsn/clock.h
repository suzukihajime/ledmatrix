/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <cytypes.h>

enum CLOCK_WEEKNUM {
	CLOCK_SUN = 0,
	CLOCK_MON,
	CLOCK_TUE,
	CLOCK_WED,
	CLOCK_THU,
	CLOCK_FRI,
	CLOCK_SAT
};

#define	CLOCK_SEC_PERIOD	500

void clock_Start(void);
void clock_Stop(void);

//[] END OF FILE
