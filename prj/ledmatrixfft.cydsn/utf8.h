/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "device.h"

#define FIFO_SIZE	(256)

inline char fifoGetChar(void);
inline void fifoPushChar(char c);
inline void fifoPushString(char *str);
inline uint16 fifoGetContentSize(void);
inline uint16 fifoGetVacancySize(void);
uint16 fifoGetNextUtf8();

//[] END OF FILE
