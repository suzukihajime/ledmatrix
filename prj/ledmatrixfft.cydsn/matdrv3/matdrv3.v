
//`#start header` -- edit after this line, do not edit this line
// ========================================
//
// Copyright YOUR COMPANY, THE YEAR
// All Rights Reserved
// UNPUBLISHED, LICENSED SOFTWARE.
//
// CONFIDENTIAL AND PROPRIETARY INFORMATION
// WHICH IS THE PROPERTY OF your company.
//
// ========================================
`include "cypress.v"
//`#end` -- edit above this line, do not edit this line
// Generated on 10/24/2012 at 10:08
// Component: matdrv3
module matdrv3 (
	output  dma_data,
	output  row_term,
	output  rck,
	output [3:0] row,
	output [3:0] next_row,
	output [7:0] state,
	output  sck,
	output  sda,
	input   clock,
	input   reset
);

//`#start body` -- edit after this line, do not edit this line

//        Your code goes here
	/*
	クロック同期
	*/
	wire matdrv_clock;
	cy_psoc3_udb_clock_enable_v1_0 #(.sync_mode(`TRUE)) ClkSync
    (
        /* input  */    .clock_in(clock),
        /* input  */    .enable(1'b1),
        /* output */    .clock_out(matdrv_clock)
    );
	
	/*
	1周期512countとし、matdrv_count[7:0]の8ビットを使ってステートを決定する。
	matdrv_count[7:0]
	0-7:     preparetion state
	8-199:   data transmission state
	200-203: nop
	204-207: finishing state
	
	memo: できればdatapathを使うべきだと思う。上8bitと下1bitで分ける。
	*/
	reg matdrv_clock_latch;
	always @(posedge matdrv_clock) begin
		matdrv_clock_latch <= ~matdrv_clock_latch;
	end
	
	wire [6:0] matdrv_count_low;
	wire matdrv_count_low_tc;
	cy_psoc3_count7 #(.cy_period(7'd15), .cy_route_ld(`FALSE), .cy_route_en(`TRUE))
	counter_low (
		/* input */ .clock(matdrv_clock), // Clock
		/* input */ .reset(reset), // Reset
		/* input */ .load(1'b0), // Load signal used if cy_route_ld = TRUE
		/* input */ .enable(matdrv_clock_latch), // Enable signal used if cy_route_en = TRUE
		/* output [6:0] */ .count(matdrv_count_low[6:0]), // Counter value output
		/* output */ .tc(matdrv_count_low_tc) // Terminal Count output
	);
	wire [6:0] matdrv_count_high;
	wire matdrv_count_high_en = matdrv_clock_latch
							  & ~(matdrv_count_low[0] | matdrv_count_low[1]
							     | matdrv_count_low[2] | matdrv_count_low[3]);
	cy_psoc3_count7 #(.cy_period(7'd15), .cy_route_ld(`FALSE), .cy_route_en(`TRUE))
	counter_high (
		/* input */ .clock(matdrv_clock), // Clock
		/* input */ .reset(reset), // Reset
		/* input */ .load(1'b0), // Load signal used if cy_route_ld = TRUE
		/* input */ .enable(matdrv_count_high_en), // Enable signal used if cy_route_en = TRUE
		/* output [6:0] */ .count(matdrv_count_high[6:0]), // Counter value output
		/* output */ .tc() // Terminal Count output
	);
	wire [7:0] matdrv_count = {~matdrv_count_high[3:0], ~matdrv_count_low[3:0]};
	
/*	reg [8:0] matdrv_count;
	always @(posedge matdrv_clock or posedge reset) begin
		if(reset == 1'b1) begin
			matdrv_count[8:0] <= 9'b000000000;
		end else begin
			if(matdrv_count[8:0] == 9'b111111111) begin
				matdrv_count[8:0] <= 9'b000000000;
			end else begin
				matdrv_count[8:0] <= matdrv_count[8:0] + 9'b000000001;
			end
		end
	end*/
	
	/*
	state[6:0]の内容は以下の通り。
	*/
	localparam COUNTER_STATE_TRANS   = 3'd0;
	localparam COUNTER_STATE_FINISH  = 3'd1;
	localparam COUNTER_END_OF_PREP   = 3'd2;
	localparam COUNTER_END_OF_TRANS  = 3'd3;
	localparam COUNTER_END_OF_FINISH = 3'd4;
	localparam COUNTER_END_OF_NOP    = 3'd5;
	localparam COUNTER_STATE_DMA_REQ = 3'd6;
	localparam COUNTER_STATE_IDLE    = 3'd7;
	
	function [7:0] state_gen;
		input [7:0] count;
		begin
			if(count[7:0] > 8'd7 && count[7:0] < 8'd200) begin
				state_gen[COUNTER_STATE_TRANS] = 1'b1;
			end else begin
				state_gen[COUNTER_STATE_TRANS] = 1'b0;
			end
			if(count[7:0] > 8'd203 && count[7:0] < 8'd208) begin
				state_gen[COUNTER_STATE_FINISH] = 1'b1;
			end else begin
				state_gen[COUNTER_STATE_FINISH] = 1'b0;
			end
			if(count[7:0] == 8'd7) begin
				state_gen[COUNTER_END_OF_PREP] = 1'b1;
			end else begin
				state_gen[COUNTER_END_OF_PREP] = 1'b0;
			end
			if(count[7:0] == 8'd199) begin
				state_gen[COUNTER_END_OF_TRANS] = 1'b1;
			end else begin
				state_gen[COUNTER_END_OF_TRANS] = 1'b0;
			end
			if(count[7:0] == 8'd207) begin
				state_gen[COUNTER_END_OF_FINISH] = 1'b1;
			end else begin
				state_gen[COUNTER_END_OF_FINISH] = 1'b0;
			end
			if(count[7:0] == 8'd203) begin
				state_gen[COUNTER_END_OF_NOP] = 1'b1;
			end else begin
				state_gen[COUNTER_END_OF_NOP] = 1'b0;
			end
			if(count[7:0] > 8'd3 && count[7:0] < 8'd196) begin
				state_gen[COUNTER_STATE_DMA_REQ] = 1'b1;
			end else begin
				state_gen[COUNTER_STATE_DMA_REQ] = 1'b0;
			end
			if(count[7:0] > 8'd207) begin
				state_gen[COUNTER_STATE_IDLE] = 1'b1;
			end else begin
				state_gen[COUNTER_STATE_IDLE] = 1'b0;
			end
		end
	endfunction
	wire [7:0] matdrv_state = state_gen(matdrv_count[7:0]);
	
	/*
	gradint counter
	row counterと同じタイミングでカウントを進める(こいつはダウンカウンタ)
	周期を15にして、row counterとは1ずらしてある。
	*/
	wire [6:0] matdrv_grad_temp;
	wire [3:0] matdrv_grad = matdrv_grad_temp[3:0];
/*	function rowcounter_enable_gen;
		input [7:0] state;
		input count;
		begin
//			if(count[8:1] == 8'd203 && count[0] == 1'b1) begin
			if(state[COUNTER_END_OF_NOP] == 1'b1 && count == 1'b1) begin
				rowcounter_enable_gen = 1'b1;
			end else begin
				rowcounter_enable_gen = 1'b0;
			end
		end
	endfunction*/
	wire rowcounter_enable = matdrv_state[COUNTER_END_OF_NOP] & matdrv_clock_latch;
	
	cy_psoc3_count7 #(.cy_period(7'd6), .cy_route_ld(`FALSE), .cy_route_en(`TRUE))
	gradcounter1 (
		/* input */ .clock(matdrv_clock), // Clock
		/* input */ .reset(reset), // Reset
		/* input */ .load(1'b0), // Load signal used if cy_route_ld = TRUE
		/* input */ .enable(rowcounter_enable), // Enable signal used if cy_route_en = TRUE
		/* output [6:0] */ .count(matdrv_grad_temp), // Counter value output
		/* output */ .tc() // Terminal Count output
	);

	function [3:0] grad_mask;
		input [3:0] count;
		begin
			case(count[3:0])
				4'b0000: grad_mask[3:0] = 4'b0001;
				4'b0001: grad_mask[3:0] = 4'b0000;
				4'b0010: grad_mask[3:0] = 4'b0010;
				4'b0011: grad_mask[3:0] = 4'b0000;
				4'b0100: grad_mask[3:0] = 4'b0000;
				4'b0101: grad_mask[3:0] = 4'b0000;
				4'b0110: grad_mask[3:0] = 4'b0100;
				4'b0111: grad_mask[3:0] = 4'b0000;
				4'b1000: grad_mask[3:0] = 4'b0000;
				4'b1001: grad_mask[3:0] = 4'b0000;
				4'b1010: grad_mask[3:0] = 4'b0000;
				4'b1011: grad_mask[3:0] = 4'b0000;
				4'b1100: grad_mask[3:0] = 4'b0000;
				4'b1101: grad_mask[3:0] = 4'b0000;
				4'b1110: grad_mask[3:0] = 4'b1000;
				4'b1111: grad_mask[3:0] = 4'b0000;
			endcase
		end
	endfunction
	wire [3:0] matdrv_grad_mask = grad_mask(matdrv_grad[3:0]);
	wire grad_enable = matdrv_grad_mask[0] | matdrv_grad_mask[1]
					 | matdrv_grad_mask[2] | matdrv_grad_mask[3];
	
	/*
	row counter
	共通のクロックを使い、nop stateの終了時(203→204)にカウントアップする
	*/
	wire [6:0] matdrv_row_temp;
	wire [3:0] matdrv_row = {matdrv_row_temp[2:0], matdrv_row_temp[3]};

	cy_psoc3_count7 #(.cy_period(7'd15), .cy_route_ld(`FALSE), .cy_route_en(`TRUE))
	rowcounter1 (
		/* input */ .clock(matdrv_clock), // Clock
		/* input */ .reset(reset), // Reset
		/* input */ .load(1'b0), // Load signal used if cy_route_ld = TRUE
		/* input */ .enable(rowcounter_enable), // Enable signal used if cy_route_en = TRUE
		/* output [6:0] */ .count(matdrv_row_temp), // Counter value output
		/* output */ .tc() // Terminal Count output
	);

	reg [3:0] matdrv_row_latch;
	wire matdrv_row_latch_clock = matdrv_state[COUNTER_END_OF_NOP] & grad_enable;
	always @(posedge matdrv_row_latch_clock) begin
		matdrv_row_latch[3:0] <= matdrv_row[3:0];
	end
	assign row[3:0] = matdrv_row_latch[3:0];			// インターレースのため、入れ替え。->gradient実装のほうでインターレースはできるので、戻した。
//	wire [3:0] next_row_temp = matdrv_row[3:0];			// こちらで計算。datapath使ってもいいが、それほどでもないか?
	assign next_row[3:0] = matdrv_row[3:0];				// これもインターレースは必要なくなったので、入れ替えは除去した。

	assign state[7:0] = {matdrv_row_latch[3:0], matdrv_grad[3:0]};

	/*
	datapathのインスタンス化
	*/
	wire matdrv_dp0_f0_nfull;
	wire matdrv_dp0_f1_nfull;
	wire matdrv_dp0_f0_empty;
	wire matdrv_dp0_f1_empty;
	wire [7:0] matdrv_dp0_pdata;
	wire [2:0] matdrv_dp0_addr;
	reg [1:0] matdrv_dp0_state;	
	/*
	datapath制御用ステートマシン
	8-199の間、count[0] == 0でfifoロードする。
	shiftステートは使っていない。
	*/
	localparam DMA_STATE_SRIGHT    = 2'd0;
	localparam DMA_STATE_SLEFT     = 2'd1;
	localparam DMA_STATE_LOAD      = 2'd2;
	localparam DMA_STATE_IDLE      = 2'd3;

	always @(posedge matdrv_clock) begin		// こいつ同期回路だった	
		if(matdrv_clock_latch == 1'b0) begin		// 次のmatdrv_count[0] == 1'b0のときのステートを決める。
//			if((matdrv_count[8:1] > 8'd6 && matdrv_count[8:1] < 8'd199)			// 送信用
//			|| (matdrv_count[8:1] > 8'd202 && matdrv_count[8:1] < 8'd207)) begin	// fifoクリア用
			if(matdrv_state[COUNTER_END_OF_PREP] == 1'b1
			|| (matdrv_state[COUNTER_STATE_TRANS] == 1'b1 && matdrv_state[COUNTER_END_OF_TRANS] == 1'b0)
			|| matdrv_state[COUNTER_STATE_FINISH] == 1'b1) begin
				matdrv_dp0_state <= DMA_STATE_LOAD;
			end else begin
				matdrv_dp0_state <= DMA_STATE_IDLE;
			end
		end else begin
			matdrv_dp0_state <= DMA_STATE_IDLE;
		end
	end
	wire [2:0] matdrv_dp0_address = {1'b0, matdrv_dp0_state[1:0]};
	cy_psoc3_dp #(.cy_dpconfig(
	{
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP___SL, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM0:  shift left*/
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP___SR, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM1:  shift right*/
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC___F0, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM2:  fifo load*/
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM3:  idle*/
		`CS_ALU_OP_PASS, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM4:  */
		`CS_ALU_OP_PASS, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM5:  */
		`CS_ALU_OP_PASS, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC___F1,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM6:  */
		`CS_ALU_OP__XOR, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM7:  */
		  8'hFF, 8'h00,	/*CFG9:           */
		  8'hFF, 8'hFF,	/*CFG11-10:           */
		`SC_CMPB_A1_D1, `SC_CMPA_A1_D1, `SC_CI_B_ARITH,
		`SC_CI_A_ARITH, `SC_C1_MASK_DSBL, `SC_C0_MASK_DSBL,
		`SC_A_MASK_DSBL, `SC_DEF_SI_0, `SC_SI_B_DEFSI,
		`SC_SI_A_DEFSI, /*CFG13-12:           */
		`SC_A0_SRC_ACC, `SC_SHIFT_SL, 1'h0,
		1'h0, `SC_FIFO1_BUS, `SC_FIFO0_BUS,
		`SC_MSB_DSBL, `SC_MSB_BIT0, `SC_MSB_NOCHN,
		`SC_FB_NOCHN, `SC_CMP1_NOCHN,
		`SC_CMP0_NOCHN, /*CFG15-14:           */
		 10'h00, `SC_FIFO_CLK__DP,`SC_FIFO_CAP_AX,
		`SC_FIFO_LEVEL,`SC_FIFO_ASYNC,`SC_EXTCRC_DSBL,
		`SC_WRK16CAT_DSBL /*CFG17-16:           */
	})) dp0(
		/* input */ .clk(matdrv_clock), 			// Clock
		/* input [02:00] */ .cs_addr(matdrv_dp0_address), // Control Store RAM address
		/* input */ .route_si(1'b0), 		// Shift in from routing
		/* input */ .route_ci(1'b0), 		// Carry in from routing
		/* input */ .f0_load(1'b0), 		// Load FIFO 0
		/* input */ .f1_load(1'b0), 		// Load FIFO 1
		/* input */ .d0_load(1'b0), 		// Load Data Register 0
		/* input */ .d1_load(1'b0), 		// Load Data Register 1
		/* output */ .ce0(), 			// Accumulator 0 = Data register 0
		/* output */ .cl0(), 			// Accumulator 0 < Data register 0
		/* output */ .z0(), 			// Accumulator 0 = 0
		/* output */ .ff0(), 			// Accumulator 0 = FF
		/* output */ .ce1(), 			// Accumulator [0|1] = Data register 1
		/* output */ .cl1(), 			// Accumulator [0|1] < Data register 1
		/* output */ .z1(), 			// Accumulator 1 = 0
		/* output */ .ff1(), 			// Accumulator 1 = FF
		/* output */ .ov_msb(), 		// Operation over flow
		/* output */ .co_msb(), 		// Carry out
		/* output */ .cmsb(), 			// Carry out
		/* output */ .so(), 			// Shift out
		/* output */ .f0_bus_stat(matdrv_dp0_f0_nfull), 	// FIFO 0 status to uP
		/* output */ .f0_blk_stat(matdrv_dp0_f0_empty), 	// FIFO 0 status to DP
		/* output */ .f1_bus_stat(matdrv_dp0_f1_nfull), 	// FIFO 1 status to uP
		/* output */ .f1_blk_stat(matdrv_dp0_f1_empty), 	// FIFO 1 status to DP
		/* input */ .ci(1'b0), 				// Carry in from previous stage
		/* output */ .co(), 			// Carry out to next stage
		/* input */ .sir(1'b0), 			// Shift in from right side
		/* output */ .sor(), 			// Shift out to right side
		/* input */ .sil(1'b0), 			// Shift in from left side
		/* output */ .sol(), 			// Shift out to left side
		/* input */ .msbi(1'b0), 			// MSB chain in
		/* output */ .msbo(), 			// MSB chain out
		/* input [01:00] */ .cei(2'b0), 	// Compare equal in from prev stage
		/* output [01:00] */ .ceo(), 	// Compare equal out to next stage
		/* input [01:00] */ .cli(2'b0), 	// Compare less than in from prv stage
		/* output [01:00] */ .clo(), 	// Compare less than out to next stage
		/* input [01:00] */ .zi(2'b0), 		// Zero detect in from previous stage
		/* output [01:00] */ .zo(), 	// Zero detect out to next stage
		/* input [01:00] */ .fi(2'b0), 		// 0xFF detect in from previous stage
		/* output [01:00] */ .fo(), 	// 0xFF detect out to next stage
		/* input [01:00] */ .capi(2'b0),	// Capture in from previous stage
		/* output [01:00] */ .capo(),		// Capture out to next stage
		/* input */ .cfbi(1'b0), 			// CRC Feedback in from previous stage
		/* output */ .cfbo(), 			// CRC Feedback out to next stage
		/* input [07:00] */ .pi(), 		// Parallel data port
		/* output [07:00] */ .po(matdrv_dp0_pdata)
	);
	
	/*
	以降組み合わせ論理回路群
	sck, rckなどのgenを通し、その結果をgradでマスクして出力する。
	*/
	
	/*
	sck generator
	matdrv_count[8:1]が8-199の間sckを発生させる。
	matdrv_count[0]が1でsckを1とする。
	*/
	wire sck_temp = matdrv_state[COUNTER_STATE_TRANS] & matdrv_clock_latch;
/*	function sck_gen;
		input [7:0] state;
		begin
//			if(count[8:1] > 8'd7 && count[8:1] < 8'd200) begin
			if(state[COUNTER_STATE_TRANS] == 1'b1) begin
				sck_gen = count[0];
			end else begin
				sck_gen = 1'b0;
			end
		end
	endfunction
	assign sck_temp = sck_gen(matdrv_state);			// sck_genの最初のif(count[0] == 1'b0) の部分とあわせて修正。最初は反転してあった。
	*/
	/*
	rck generator
	matdrv_count[8:1]が208-255の間hiにする。
	*/
	wire rck_temp = matdrv_state[COUNTER_END_OF_NOP] | matdrv_state[COUNTER_STATE_FINISH];
/*	function rck_gen;
		input [7:0] state;
		begin
//			if(count[8:1] > 8'd207) begin
			if(state[COUNTER_STATE_IDLE] == 1'b1) begin
				rck_gen = 1'b1;
			end else begin
				rck_gen = 1'b0;
			end
		end
	endfunction
	assign rck_temp = rck_gen(matdrv_state);*/
	
	/*
	dma_data_gen
	4-195の間、count[0] == 1'b1のタイミングでdmaリクエストを発生させる。
	*/
	wire dma_data_temp = matdrv_state[COUNTER_STATE_DMA_REQ] & matdrv_clock_latch;
/*	function dma_data_gen;
		input [7:0] state;
//		input dma_req;
		begin
//			if(count[8:1] > 8'd3 && count[8:1] < 8'd196) begin
			if(state[COUTER_STATE_DMA_REQ] == 1'b1) begin
				dma_data_gen = 1'b1;
			end else begin
				dma_data_gen = 1'b0;
			end
		end
	endfunction
	assign dma_data_temp = dma_data_gen(matdrv_state) & matdrv_count_latch;*/
	
	/*
	rowが終了した信号を発生させる。(count[8:1] == 204)
	*/
	wire row_term_temp = matdrv_state[COUNTER_END_OF_FINISH];
/*	function row_term_gen;
		input [8:0] count;
		begin
			if(count[8:1] == 8'd204) begin
				row_term_gen = 1'b1;
			end else begin
				row_term_gen = 1'b0;
			end
		end
	endfunction
	assign row_term_temp = row_term_gen(matdrv_count);*/
		
	/*
	red / green切り替え
	count[4] == 1ならred / 0ならgreenを選択
	PWMは後で実装
	*/
	function [3:0] rg_sel;
//		input [7:0] state;
		input [7:0] count;
		input [7:0] pdata;
		begin
//			if(count[7:0] > 8'd7 && count[7:0] < 8'd200) begin
//			if(state[COUNTER_STATE_TRANS] == 1'b1) begin
			if(count[3] == 1'b0) begin
				rg_sel[3:0] = pdata[3:0];
			end else begin
				rg_sel[3:0] = pdata[7:4];
			end
		end
	endfunction
	wire [3:0] rg_sel_temp = rg_sel(matdrv_count, matdrv_dp0_pdata);
	wire [3:0] sda_temp = rg_sel_temp[3:0] & matdrv_grad_mask[3:0];
	assign sda = (~sda_temp[0] & ~sda_temp[1] & ~sda_temp[2] & ~sda_temp[3]) | ~matdrv_state[COUNTER_STATE_TRANS];
	
	/*
	gradによるマスクをかける。
	*/
	assign sck = grad_enable & sck_temp;
	assign rck = grad_enable & rck_temp;
//	assign sda = grad_enable & sda_temp;
	assign dma_data = grad_enable & dma_data_temp;
	assign row_term = grad_enable & row_term_temp;
endmodule
/*
module rowcounter(clock, count, reset, row);
	input clock;
	input [8:0] count;
	input reset;
	output [3:0] row;
	
	reg [3:0] cnt;
	
	always @(posedge clock or posedge reset) begin
		if(reset) begin
			cnt <= 4'b0000;
		end else begin
			if(count[8:1] == 8'd203 && count[0] == 1'b1) begin
				cnt <= cnt + 4'b0001;
			end else begin
				cnt <= cnt;
			end
		end
	end
	
	assign row[3:0] = cnt[3:0];
endmodule

module gradcounter(clock, count, reset, grad);
	input clock;
	input [8:0] count;
	input reset;
	output [3:0] grad;
	
	reg [3:0] gradcnt;
	
	always @(posedge clock or posedge reset) begin
		if(reset) begin
			gradcnt <= 4'b0000;
		end else begin
			if(count[8:1] == 8'd203 && count[0] == 1'b1) begin
/*				if(gradcnt[3:0] == 4'b0000) begin
					gradcnt[3:0] <= 4'b1111;
				end else begin* /
					gradcnt[3:0] <= gradcnt[3:0] - 4'b0001;
//				end
			end else begin
				gradcnt[3:0] <= gradcnt[3:0];
			end
		end
	end
	assign grad[3:0] = gradcnt[3:0];
endmodule
*/
/*
module rowlatch(clock, count, rowin, rowout, enable);
	input clock;
	input [3:0] rowin;
	output [3:0] rowout;
	input enable;
	
	reg [3:0] rowlatch;
	always @(posedge clock) begin
		if(enable == 1'b1) begin
			rowlatch[3:0] <= rowin[3:0];
		end else begin
			rowlatch[3:0] <= rowlatch[3:0];
		end
	end
	assign rowout[3:0] = rowlatch[3:0];
endmodule
*/
//`#end` -- edit above this line, do not edit this line

//`#start footer` -- edit after this line, do not edit this line
//`#end` -- edit above this line, do not edit this line
