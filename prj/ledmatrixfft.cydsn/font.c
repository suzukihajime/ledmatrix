/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <device.h>
#include <stdlib.h>
#include "sdc/ff.h"
#include "sd.h"
#include "font.h"
#include "uart.h"

SDSTATUS sd;
FIL hfont;
FIL htable;

uint32 unpackHex4(char *buf);
uint32 unpackHex6(char *buf);
uint8 hex2int(uint8 ch);
inline char *getBoundingBox(char *ptr, int8 *width, int8 *height, int8 *xorg, int8 *yorg);
inline char *getBitmapData(char *ptr, uint8 *bitmapbuf);
char *strsrc(char *src, char marker, uint32 max);
inline uint8 applyRasterOperation(uint8 dist, uint8 src, uint8 rop);
inline uint8 extractBitColor(uint8 *buf, uint8 i, uint8 j);


uint8 openFont(char *font, char *table)
{
	if(SD_Init(&sd) == 0) {
		return 2;
	}
	if(SD_OpenFile(&sd, &hfont, font) == 0) {
		return 6;
	}
	if(SD_OpenFile(&sd, &htable, table) == 0) {
		return 5;
	}
	return 1;
}

uint8 closeFont(void)
{
	SD_CloseFile(&sd, &hfont);
	SD_CloseFile(&sd, &htable);
	return(1);
}

uint32 getFontAddress(uint32 unicode)
{
	char buf[16];
//	uint8 i;
	unsigned int len;
	f_lseek(&htable, unicode * 7);
	f_read(&htable, (uint8*)buf, 6, &len);
//	uprintf("\r\nunicode : %x = %d (%d) is mapped on ", unicode, unicode, unicode *6);
//	for(i = 0; i < 8; i++) { uputc(buf[i]); }
//	uprintf("\r\n\r\n");
	return(unpackHex6(buf));
}

uint32 unpackHex4(char *buf)
{
	uint32 hex = 0;
	uint8 i;
	for(i = 0; i < 4; i++) {
		hex <<= 4;
		hex |= (uint32)hex2int(*buf++);
	}
	return(hex);
}

uint32 unpackHex6(char *buf)
{
	uint32 hex = 0;
	uint8 i;
	for(i = 0; i < 6; i++) {
		hex <<= 4;
		hex |= (uint32)hex2int(*buf++);
	}
	return(hex);
}

uint8 hex2int(uint8 ch)
{
	switch(toupper(ch)) {
		case '0': return(0);
		case '1': return(1);
		case '2': return(2);
		case '3': return(3);
		case '4': return(4);
		case '5': return(5);
		case '6': return(6);
		case '7': return(7);
		case '8': return(8);
		case '9': return(9);
		case 'A': return(10);
		case 'B': return(11);
		case 'C': return(12);
		case 'D': return(13);
		case 'E': return(14);
		case 'F': return(15);
	}
	return(0);
}

void expandFontBitmap(uint32 addr, uint8 *dest, uint8 color, uint8 mode)
{
	char readbuf[READ_BUFFER_SIZE];
	char *p;
	int8 height, width, i, j;
	uint8 bitmapbuf[FONT_BITMAP_BUFFER_SIZE];
	unsigned int len;
	/* get bdf font data */
	f_lseek(&hfont, addr);
	f_read(&hfont, readbuf, READ_BUFFER_SIZE, &len);
//	for(j = 0; j < len; j++) {uputc(readbuf[j]);}
//	uprintf("len = %d (%d)\r\n", len, READ_BUFFER_SIZE);
	if(READ_BUFFER_SIZE != len) {
		return;
	}
	p = readbuf;
	p = getBoundingBox(p, &width, &height, NULL, NULL);
//	uprintf("w = %d, h = %d\r\n", width, height);
	p = getBitmapData(p, bitmapbuf);
	for(i = 0; i < width; i++) {
		for(j = 0; j < height; j++) {
			/* dest[i + j*16] にアクセスする */
			dest[j+i*16] = applyRasterOperation(dest[j+i*16], extractBitColor(bitmapbuf, i, j) ? color : 0, mode);
//			uprintf("%d", dest[j+i*16]);
		}
//		uprintf("\r\n");
	}
	return;
}

inline char *getBoundingBox(char *ptr, int8 *width, int8 *height, int8 *xorg, int8 *yorg)
{
	ptr = strstr(ptr, "BBX");
	ptr = strsrc(ptr, ' ', READ_BUFFER_SIZE);
	if(width) { *width = atoi(ptr); }
	ptr = strsrc(ptr, ' ', READ_BUFFER_SIZE);
	if(height) { *height = atoi(ptr); }
	ptr = strsrc(ptr, ' ', READ_BUFFER_SIZE);
	if(xorg) { *xorg = atoi(ptr); }
	ptr = strsrc(ptr, ' ', READ_BUFFER_SIZE);
	if(yorg) { *yorg = atoi(ptr); }
	ptr = strsrc(ptr, '\n', READ_BUFFER_SIZE);
	return(ptr);
}

inline char *getBitmapData(char *ptr, uint8 *bitmapbuf)
{
	register uint16 num;
	char *pterm;
//	uint8 i;
	ptr = strstr(ptr, "BITMAP");
//	for(i = 0; i < 8; i++) { uputc(ptr[i]); }
	ptr = strsrc(ptr, '\n', READ_BUFFER_SIZE);
//	for(i = 0; i < 8; i++) { uputc(ptr[i]); }
	pterm = strstr(ptr, "ENDCHAR");
//	for(i = 0; i < 8; i++) { uputc(pterm[i]); }
	while(1) {
		num = unpackHex4(ptr);
//		uprintf("%c%c%c%c - %x\r\n", *ptr, *(ptr+1), *(ptr+2), *(ptr+3), num);
		*((uint16*)bitmapbuf) = num;
		bitmapbuf += 2;
		ptr = strsrc(ptr, '\n', READ_BUFFER_SIZE);
//		for(i = 0; i < 8; i++) { uputc(ptr[i]); }
		if(ptr >= pterm) { break; }
	}
	return(ptr);
}

// search a marker from string
char *strsrc(char *src, char marker, uint32 max)
{
	register uint32 i = 0;
	while(1) {
		if(*src == marker) { return(++src); }
		if(*src == '\n' || *src == '\0') { return 0; }
		if(++i == max) { break; }
		src++;
	}
	return 0;
}

inline uint8 applyRasterOperation(uint8 dest, uint8 src, uint8 rop)
{
	uint8 res;
	switch(rop) {
		default: 			// default: FONT_ROP_COPY
		case FONT_ROP_COPY: res = src; break;
		case FONT_ROP_ADD:  res = ((dest + src)&0x0f) | ((dest + src)&0xf0); break;
		case FONT_ROP_OR:   res = ((dest&0x0f) > (src&0x0f) ? (dest&0x0f) : (src&0x0f))
						   | ((dest&0xf0) > (src&0xf0) ? (dest&0xf0) : (src&0xf0)); break;
		case FONT_ROP_BITWISE_OR: res = dest | src; break;
		case FONT_ROP_AND:  res = ((dest&0x0f) < (src&0x0f) ? (dest&0x0f) : (src&0x0f))
						   | ((dest&0xf0) < (src&0xf0) ? (dest&0xf0) : (src&0xf0)); break;
		case FONT_ROP_BITWISE_AND: res = dest & src; break;
	}
	return(res);
}

inline uint8 extractBitColor(uint8 *buf, uint8 i, uint8 j)
{
	j <<= 1;
	if(i >= 8) {
		i-=8;
	} else {
		j++;
	}
	return((buf[j] & (0x80>>i)) ? 1 : 0);
}

/* [] END OF FILE */
