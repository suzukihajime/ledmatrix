/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/*
20121121
logスケールでの表示とカラーマッピングテーブルを実装する。
カラーマッピングはメモリが足りないので、CY8C53に移植してから実装するべき。
現時点でFFT処理は2.5ms/32ms(余裕)
*/

#include "device.h"
#include "uart.h"
#include "fft.h"
#include "table.h"			// 12-bit fixed point sin and cos table
#include "rev.h"			// bit reverse table

/* DMA Configuration for DMA_ADC */
#define DMA_ADC_BYTES_PER_BURST 2
#define DMA_ADC_REQUEST_PER_BURST 1
#define DMA_ADC_SRC_BASE (CYDEV_PERIPH_BASE)
#define DMA_ADC_DST_BASE (dat_buf)

int16 dat_buf[FFT_BUF_LENGTH+16];
int16 datr[FFT_BUF_LENGTH], dati[FFT_BUF_LENGTH];
int cnt;
uint8 silent;
/* Move these variable declarations to the top of the function */
uint8 DMA_ADC_Chan;
uint8 DMA_ADC_TD[1];

uint8 fft_disp_attr;		// attribute
uint8 fft_disp_buf[96][16];

void calculatefft(void);
void normalize(void);
void checksilent(void);
void drawdisplaybuffer(void);

extern void mixdisplaybuffer(void);		// 複数のレイヤーを重ねる関数。fft処理の最後に呼ぶようにする。

CY_ISR(fft_isr)
{
	register int i;
	TEST_Write(1);			/* ここ実行されてる? */
	CyDmaChDisable(DMA_ADC_Chan);
	
	/* 次のデータ取り込みの準備 */
	if(cnt & 0x01) {
		((dmac_tdmem2 CYXDATA *)DMAC_TDMEM)[DMA_ADC_TD[0]].dst_adr = &dat_buf[0];
		cnt = 0;
	} else {
		((dmac_tdmem2 CYXDATA *)DMAC_TDMEM)[DMA_ADC_TD[0]].dst_adr = &dat_buf[128];
		cnt = 1;
	}
	for(i = 0; i < FFT_BUF_LENGTH; i++) {
		datr[i] = dat_buf[i];
	}
	CyDmaClearPendingDrq(DMA_ADC_Chan);
	CyDmaChEnable(DMA_ADC_Chan, 1);
	Counter_ADC_WriteCounter(0);
	/* ここでfftの演算をする */
	calculatefft();
	normalize();			// logをとったり、補正をかけたり
	checksilent();
	drawdisplaybuffer();
	
	mixdisplaybuffer();
	
	TEST_Write(0);
	return;
}

inline void calculatefft(void)
{
	/* 前処理 */
	int theta = 1;
	register int i, j, k, l, iadd;
	register int16 s, c, tempr, tempi;
	for(i = 0; i < FFT_BUF_LENGTH; i++) {
		dati[i] = 0;
	}
	
	/* debug */
/*	uprintf("\nraw data\n");
	for(i = 0; i < 256; i++) {
		datr[i] -= 2048;
		uprintf("%d\n", datr[i]);
	}
*/
	/* fft loop */
	for(i = FFT_BUF_LENGTH; i > 1; i /= 2) {
		iadd = i / 2;
		for(j = 0; j < iadd; j++) {
			s = SIN_T[j * theta];
			c = COS_T[j * theta];
			for(k = j; k < FFT_BUF_LENGTH; k += i) {
				l = k + iadd;
				tempr = datr[k];
				tempi = dati[k];
				datr[k] += datr[l];
				dati[k] += dati[l];
				tempr -= datr[l];
				tempi -= dati[l];
				datr[l] = ((tempr * c)>>12) - ((tempi * s)>>12);
				dati[l] = ((tempr * s)>>12) + ((tempi * c)>>12);
				datr[k]>>=1;
				dati[k]>>=1;
			}
		}
		theta <<= 1;
	}
	return;
}

uint32 sqrt32(uint32 n)
{
	uint32 c = 0x8000;
    uint32 g = 0x8000;

	for(;;) {
		if(g*g > n)
			g ^= c;
		c >>= 1;
		if(c == 0)
			return g;
		g |= c;
	}
}

// 3次のテイラー展開による近似式。log(0) = -3754になる。
int32 log32(int32 i)
{
	register int32 a;
	i -= 2048;
	a = i/3 - 1024;
	a = (a * i)>>11;
	a += 2048;
	a = (a * i)>>11;
	return(a);
}

#if 0
void calculatenorm(void)
{
	/*
	ノルム計算
	*/
	register int i;
	register int32 tempr, tempi;
//	uprintf("\nfft\n");
	for(i = 0; i < 128; i++) {
		tempr = datr[i];
		tempi = dati[i];
		tempr = tempr*tempr;
		tempi = tempi*tempi;
		tempr = sqrt32(tempr + tempi);
//		uprintf("%d,%d\n", tempr, tempi);
		datr[i] = tempr/64;
	}
	return;
}
#endif

void normalize(void)
{
	register int i;
	register int32 tempr, tempi;
	
	for(i = 0; i < 96; i++) {
		tempr = datr[(int)BITREV[i+1]];
		tempi = dati[(int)BITREV[i+1]];
		tempr = tempr*tempr;
		tempi = tempi*tempi;
	//	s = sqrt32(tempr + tempi);
		// logをとる方式に変更
		datr[(int)BITREV[i+1]] = log32(tempr + tempi) + 3754;
		// 等ラウドネス曲線による補正。
	}
	return;
}

void checksilent(void)
{
	silent = 0;
	return;
}

void drawdisplaybuffer(void)
{
	register int i, j;
	register int32 s;
	
//	uprintf("fft\n");
	for(i = 0; i < 96; i++) {
//		uprintf(", %d\n", s);
		s = datr[(int)BITREV[i+1]] / 128;
/*		if(s > 19) { s = 19; }
		if(s < 3) { s = 3; }
		s = 19 - s;*/
		if(s > 17) { s = 17; }
		if(s < 1) { s = 1; }
		s = 17 - s;
		for(j = 15; j >= 0; j--) {
			if(j >= s) {
				fft_disp_buf[i][j] = ((15 - j)>>1) + (j<<3);
			} else {
				fft_disp_buf[i][j] =  0;
			}
		}
	}
	return;
}

uint8 FFT_isSilent(void)
{
	return(silent);
}

void FFT_Start(void)
{
	int i;
	for(i = 0; i < 256; i++) {
		datr[i] = i;
		dati[i] = 255 - i;
	}
	ADC_SAR_1_Start();
	Counter_ADC_Start();
	Counter_ADC_WriteCompare(FFT_BUF_LENGTH/2 - 1);
	IRQ_ADC_StartEx(fft_isr);
	
	DMA_ADC_Chan = DMA_ADC_DmaInitialize(DMA_ADC_BYTES_PER_BURST, DMA_ADC_REQUEST_PER_BURST, 
											HI16(DMA_ADC_SRC_BASE), HI16(DMA_ADC_DST_BASE));
	DMA_ADC_TD[0] = CyDmaTdAllocate();
	CyDmaTdSetConfiguration(DMA_ADC_TD[0], (FFT_BUF_LENGTH)/* *2 */, DMA_ADC_TD[0], TD_INC_DST_ADR);		// 20121129 半分にした。更新頻度をそのままにして2kHzまでを表示したいから。
	CyDmaTdSetAddress(DMA_ADC_TD[0], LO16((uint32)ADC_SAR_1_SAR_WRK0_PTR), LO16((uint32)dat_buf));
	CyDmaChSetInitialTd(DMA_ADC_Chan, DMA_ADC_TD[0]);
	CyDmaChEnable(DMA_ADC_Chan, 1);
	return;
}

void FFT_Stop(void)
{
	return;
}

/* [] END OF FILE */
