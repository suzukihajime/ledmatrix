/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <device.h>
#define DMA_1_BYTES_PER_BURST 1
#define DMA_1_REQUEST_PER_BURST 1
#define DMA_1_SRC_BASE (buf)
#define DMA_1_DST_BASE (CYDEV_PERIPH_BASE)

const uint8 CYCODE cdat[20] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
const uint16 BASE_OFFSET[24] = {32,33,96,97,160,161,224,225,288,289,352,353,320,321,256,257,192,193,128,129,0,1};

void main()
{
    uint8 DMA_1_Chan;
	uint8 DMA_1_TD[24];
	uint8 buf[384];
	int16 i;
	
	for(i = 0; i < 384; i++) {
//		buf[i] = (i & 0x01) ? 0xff : 0;
		buf[i] = i;
	}
	
	DMA_1_Chan = DMA_1_DmaInitialize(DMA_1_BYTES_PER_BURST, DMA_1_REQUEST_PER_BURST, 
									HI16(DMA_1_SRC_BASE), HI16(DMA_1_DST_BASE));
	for(i = 0; i < 24; i++) {
		DMA_1_TD[i] = CyDmaTdAllocate();
		CyDmaTdSetConfiguration(DMA_1_TD[i], 1, DMA_1_TD[(i+1)%24], TD_INC_SRC_ADR/* | TD_AUTO_EXEC_NEXT*/);
		CyDmaTdSetAddress(DMA_1_TD[i], LO16((uint32)buf+BASE_OFFSET[i]), LO16((uint32)dmarcv_1_FIFO_PTR));
	}
	CyDmaChSetInitialTd(DMA_1_Chan, DMA_1_TD[0]);
	CyDmaChEnable(DMA_1_Chan, 1);

    CyGlobalIntEnable;
	while(1) {
		for(i = 0; i < 30000; i++) {}
	}
}

/* [] END OF FILE */
