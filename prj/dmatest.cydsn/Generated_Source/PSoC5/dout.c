/*******************************************************************************
* File Name: dout.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "dout.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 dout__PORT == 15 && (dout__MASK & 0xC0))

/*******************************************************************************
* Function Name: dout_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void dout_Write(uint8 value) 
{
    uint8 staticBits = dout_DR & ~dout_MASK;
    dout_DR = staticBits | ((value << dout_SHIFT) & dout_MASK);
}


/*******************************************************************************
* Function Name: dout_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void dout_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(dout_0, mode);
	CyPins_SetPinDriveMode(dout_1, mode);
	CyPins_SetPinDriveMode(dout_2, mode);
	CyPins_SetPinDriveMode(dout_3, mode);
	CyPins_SetPinDriveMode(dout_4, mode);
	CyPins_SetPinDriveMode(dout_5, mode);
}


/*******************************************************************************
* Function Name: dout_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro dout_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 dout_Read(void) 
{
    return (dout_PS & dout_MASK) >> dout_SHIFT;
}


/*******************************************************************************
* Function Name: dout_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 dout_ReadDataReg(void) 
{
    return (dout_DR & dout_MASK) >> dout_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(dout_INTSTAT) 

    /*******************************************************************************
    * Function Name: dout_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 dout_ClearInterrupt(void) 
    {
        return (dout_INTSTAT & dout_MASK) >> dout_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
