/*******************************************************************************
* File Name: dmaemp.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_dmaemp_H) /* Pins dmaemp_H */
#define CY_PINS_dmaemp_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "dmaemp_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 dmaemp__PORT == 15 && (dmaemp__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    dmaemp_Write(uint8 value) ;
void    dmaemp_SetDriveMode(uint8 mode) ;
uint8   dmaemp_ReadDataReg(void) ;
uint8   dmaemp_Read(void) ;
uint8   dmaemp_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define dmaemp_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define dmaemp_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define dmaemp_DM_RES_UP          PIN_DM_RES_UP
#define dmaemp_DM_RES_DWN         PIN_DM_RES_DWN
#define dmaemp_DM_OD_LO           PIN_DM_OD_LO
#define dmaemp_DM_OD_HI           PIN_DM_OD_HI
#define dmaemp_DM_STRONG          PIN_DM_STRONG
#define dmaemp_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define dmaemp_MASK               dmaemp__MASK
#define dmaemp_SHIFT              dmaemp__SHIFT
#define dmaemp_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define dmaemp_PS                     (* (reg8 *) dmaemp__PS)
/* Data Register */
#define dmaemp_DR                     (* (reg8 *) dmaemp__DR)
/* Port Number */
#define dmaemp_PRT_NUM                (* (reg8 *) dmaemp__PRT) 
/* Connect to Analog Globals */                                                  
#define dmaemp_AG                     (* (reg8 *) dmaemp__AG)                       
/* Analog MUX bux enable */
#define dmaemp_AMUX                   (* (reg8 *) dmaemp__AMUX) 
/* Bidirectional Enable */                                                        
#define dmaemp_BIE                    (* (reg8 *) dmaemp__BIE)
/* Bit-mask for Aliased Register Access */
#define dmaemp_BIT_MASK               (* (reg8 *) dmaemp__BIT_MASK)
/* Bypass Enable */
#define dmaemp_BYP                    (* (reg8 *) dmaemp__BYP)
/* Port wide control signals */                                                   
#define dmaemp_CTL                    (* (reg8 *) dmaemp__CTL)
/* Drive Modes */
#define dmaemp_DM0                    (* (reg8 *) dmaemp__DM0) 
#define dmaemp_DM1                    (* (reg8 *) dmaemp__DM1)
#define dmaemp_DM2                    (* (reg8 *) dmaemp__DM2) 
/* Input Buffer Disable Override */
#define dmaemp_INP_DIS                (* (reg8 *) dmaemp__INP_DIS)
/* LCD Common or Segment Drive */
#define dmaemp_LCD_COM_SEG            (* (reg8 *) dmaemp__LCD_COM_SEG)
/* Enable Segment LCD */
#define dmaemp_LCD_EN                 (* (reg8 *) dmaemp__LCD_EN)
/* Slew Rate Control */
#define dmaemp_SLW                    (* (reg8 *) dmaemp__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define dmaemp_PRTDSI__CAPS_SEL       (* (reg8 *) dmaemp__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define dmaemp_PRTDSI__DBL_SYNC_IN    (* (reg8 *) dmaemp__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define dmaemp_PRTDSI__OE_SEL0        (* (reg8 *) dmaemp__PRTDSI__OE_SEL0) 
#define dmaemp_PRTDSI__OE_SEL1        (* (reg8 *) dmaemp__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define dmaemp_PRTDSI__OUT_SEL0       (* (reg8 *) dmaemp__PRTDSI__OUT_SEL0) 
#define dmaemp_PRTDSI__OUT_SEL1       (* (reg8 *) dmaemp__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define dmaemp_PRTDSI__SYNC_OUT       (* (reg8 *) dmaemp__PRTDSI__SYNC_OUT) 


#if defined(dmaemp__INTSTAT)  /* Interrupt Registers */

    #define dmaemp_INTSTAT                (* (reg8 *) dmaemp__INTSTAT)
    #define dmaemp_SNAP                   (* (reg8 *) dmaemp__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins dmaemp_H */

#endif
/* [] END OF FILE */
