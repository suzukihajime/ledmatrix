/*******************************************************************************
* File Name: stat.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_stat_H) /* Pins stat_H */
#define CY_PINS_stat_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "stat_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 stat__PORT == 15 && (stat__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    stat_Write(uint8 value) ;
void    stat_SetDriveMode(uint8 mode) ;
uint8   stat_ReadDataReg(void) ;
uint8   stat_Read(void) ;
uint8   stat_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define stat_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define stat_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define stat_DM_RES_UP          PIN_DM_RES_UP
#define stat_DM_RES_DWN         PIN_DM_RES_DWN
#define stat_DM_OD_LO           PIN_DM_OD_LO
#define stat_DM_OD_HI           PIN_DM_OD_HI
#define stat_DM_STRONG          PIN_DM_STRONG
#define stat_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define stat_MASK               stat__MASK
#define stat_SHIFT              stat__SHIFT
#define stat_WIDTH              2u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define stat_PS                     (* (reg8 *) stat__PS)
/* Data Register */
#define stat_DR                     (* (reg8 *) stat__DR)
/* Port Number */
#define stat_PRT_NUM                (* (reg8 *) stat__PRT) 
/* Connect to Analog Globals */                                                  
#define stat_AG                     (* (reg8 *) stat__AG)                       
/* Analog MUX bux enable */
#define stat_AMUX                   (* (reg8 *) stat__AMUX) 
/* Bidirectional Enable */                                                        
#define stat_BIE                    (* (reg8 *) stat__BIE)
/* Bit-mask for Aliased Register Access */
#define stat_BIT_MASK               (* (reg8 *) stat__BIT_MASK)
/* Bypass Enable */
#define stat_BYP                    (* (reg8 *) stat__BYP)
/* Port wide control signals */                                                   
#define stat_CTL                    (* (reg8 *) stat__CTL)
/* Drive Modes */
#define stat_DM0                    (* (reg8 *) stat__DM0) 
#define stat_DM1                    (* (reg8 *) stat__DM1)
#define stat_DM2                    (* (reg8 *) stat__DM2) 
/* Input Buffer Disable Override */
#define stat_INP_DIS                (* (reg8 *) stat__INP_DIS)
/* LCD Common or Segment Drive */
#define stat_LCD_COM_SEG            (* (reg8 *) stat__LCD_COM_SEG)
/* Enable Segment LCD */
#define stat_LCD_EN                 (* (reg8 *) stat__LCD_EN)
/* Slew Rate Control */
#define stat_SLW                    (* (reg8 *) stat__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define stat_PRTDSI__CAPS_SEL       (* (reg8 *) stat__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define stat_PRTDSI__DBL_SYNC_IN    (* (reg8 *) stat__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define stat_PRTDSI__OE_SEL0        (* (reg8 *) stat__PRTDSI__OE_SEL0) 
#define stat_PRTDSI__OE_SEL1        (* (reg8 *) stat__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define stat_PRTDSI__OUT_SEL0       (* (reg8 *) stat__PRTDSI__OUT_SEL0) 
#define stat_PRTDSI__OUT_SEL1       (* (reg8 *) stat__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define stat_PRTDSI__SYNC_OUT       (* (reg8 *) stat__PRTDSI__SYNC_OUT) 


#if defined(stat__INTSTAT)  /* Interrupt Registers */

    #define stat_INTSTAT                (* (reg8 *) stat__INTSTAT)
    #define stat_SNAP                   (* (reg8 *) stat__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins stat_H */

#endif
/* [] END OF FILE */
