/*******************************************************************************
* File Name: dmares.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_dmares_H) /* Pins dmares_H */
#define CY_PINS_dmares_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "dmares_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 dmares__PORT == 15 && (dmares__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    dmares_Write(uint8 value) ;
void    dmares_SetDriveMode(uint8 mode) ;
uint8   dmares_ReadDataReg(void) ;
uint8   dmares_Read(void) ;
uint8   dmares_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define dmares_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define dmares_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define dmares_DM_RES_UP          PIN_DM_RES_UP
#define dmares_DM_RES_DWN         PIN_DM_RES_DWN
#define dmares_DM_OD_LO           PIN_DM_OD_LO
#define dmares_DM_OD_HI           PIN_DM_OD_HI
#define dmares_DM_STRONG          PIN_DM_STRONG
#define dmares_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define dmares_MASK               dmares__MASK
#define dmares_SHIFT              dmares__SHIFT
#define dmares_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define dmares_PS                     (* (reg8 *) dmares__PS)
/* Data Register */
#define dmares_DR                     (* (reg8 *) dmares__DR)
/* Port Number */
#define dmares_PRT_NUM                (* (reg8 *) dmares__PRT) 
/* Connect to Analog Globals */                                                  
#define dmares_AG                     (* (reg8 *) dmares__AG)                       
/* Analog MUX bux enable */
#define dmares_AMUX                   (* (reg8 *) dmares__AMUX) 
/* Bidirectional Enable */                                                        
#define dmares_BIE                    (* (reg8 *) dmares__BIE)
/* Bit-mask for Aliased Register Access */
#define dmares_BIT_MASK               (* (reg8 *) dmares__BIT_MASK)
/* Bypass Enable */
#define dmares_BYP                    (* (reg8 *) dmares__BYP)
/* Port wide control signals */                                                   
#define dmares_CTL                    (* (reg8 *) dmares__CTL)
/* Drive Modes */
#define dmares_DM0                    (* (reg8 *) dmares__DM0) 
#define dmares_DM1                    (* (reg8 *) dmares__DM1)
#define dmares_DM2                    (* (reg8 *) dmares__DM2) 
/* Input Buffer Disable Override */
#define dmares_INP_DIS                (* (reg8 *) dmares__INP_DIS)
/* LCD Common or Segment Drive */
#define dmares_LCD_COM_SEG            (* (reg8 *) dmares__LCD_COM_SEG)
/* Enable Segment LCD */
#define dmares_LCD_EN                 (* (reg8 *) dmares__LCD_EN)
/* Slew Rate Control */
#define dmares_SLW                    (* (reg8 *) dmares__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define dmares_PRTDSI__CAPS_SEL       (* (reg8 *) dmares__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define dmares_PRTDSI__DBL_SYNC_IN    (* (reg8 *) dmares__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define dmares_PRTDSI__OE_SEL0        (* (reg8 *) dmares__PRTDSI__OE_SEL0) 
#define dmares_PRTDSI__OE_SEL1        (* (reg8 *) dmares__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define dmares_PRTDSI__OUT_SEL0       (* (reg8 *) dmares__PRTDSI__OUT_SEL0) 
#define dmares_PRTDSI__OUT_SEL1       (* (reg8 *) dmares__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define dmares_PRTDSI__SYNC_OUT       (* (reg8 *) dmares__PRTDSI__SYNC_OUT) 


#if defined(dmares__INTSTAT)  /* Interrupt Registers */

    #define dmares_INTSTAT                (* (reg8 *) dmares__INTSTAT)
    #define dmares_SNAP                   (* (reg8 *) dmares__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins dmares_H */

#endif
/* [] END OF FILE */
