/*******************************************************************************
* File Name: dout.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_dout_ALIASES_H) /* Pins dout_ALIASES_H */
#define CY_PINS_dout_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"

/***************************************
*              Constants        
***************************************/
#define dout_0		dout__0__PC
#define dout_1		dout__1__PC
#define dout_2		dout__2__PC
#define dout_3		dout__3__PC
#define dout_4		dout__4__PC
#define dout_5		dout__5__PC

#endif /* End Pins dout_ALIASES_H */

/* [] END OF FILE */
