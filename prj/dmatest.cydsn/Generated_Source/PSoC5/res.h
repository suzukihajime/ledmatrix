/*******************************************************************************
* File Name: res.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_res_H) /* Pins res_H */
#define CY_PINS_res_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "res_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 res__PORT == 15 && (res__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    res_Write(uint8 value) ;
void    res_SetDriveMode(uint8 mode) ;
uint8   res_ReadDataReg(void) ;
uint8   res_Read(void) ;
uint8   res_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define res_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define res_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define res_DM_RES_UP          PIN_DM_RES_UP
#define res_DM_RES_DWN         PIN_DM_RES_DWN
#define res_DM_OD_LO           PIN_DM_OD_LO
#define res_DM_OD_HI           PIN_DM_OD_HI
#define res_DM_STRONG          PIN_DM_STRONG
#define res_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define res_MASK               res__MASK
#define res_SHIFT              res__SHIFT
#define res_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define res_PS                     (* (reg8 *) res__PS)
/* Data Register */
#define res_DR                     (* (reg8 *) res__DR)
/* Port Number */
#define res_PRT_NUM                (* (reg8 *) res__PRT) 
/* Connect to Analog Globals */                                                  
#define res_AG                     (* (reg8 *) res__AG)                       
/* Analog MUX bux enable */
#define res_AMUX                   (* (reg8 *) res__AMUX) 
/* Bidirectional Enable */                                                        
#define res_BIE                    (* (reg8 *) res__BIE)
/* Bit-mask for Aliased Register Access */
#define res_BIT_MASK               (* (reg8 *) res__BIT_MASK)
/* Bypass Enable */
#define res_BYP                    (* (reg8 *) res__BYP)
/* Port wide control signals */                                                   
#define res_CTL                    (* (reg8 *) res__CTL)
/* Drive Modes */
#define res_DM0                    (* (reg8 *) res__DM0) 
#define res_DM1                    (* (reg8 *) res__DM1)
#define res_DM2                    (* (reg8 *) res__DM2) 
/* Input Buffer Disable Override */
#define res_INP_DIS                (* (reg8 *) res__INP_DIS)
/* LCD Common or Segment Drive */
#define res_LCD_COM_SEG            (* (reg8 *) res__LCD_COM_SEG)
/* Enable Segment LCD */
#define res_LCD_EN                 (* (reg8 *) res__LCD_EN)
/* Slew Rate Control */
#define res_SLW                    (* (reg8 *) res__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define res_PRTDSI__CAPS_SEL       (* (reg8 *) res__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define res_PRTDSI__DBL_SYNC_IN    (* (reg8 *) res__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define res_PRTDSI__OE_SEL0        (* (reg8 *) res__PRTDSI__OE_SEL0) 
#define res_PRTDSI__OE_SEL1        (* (reg8 *) res__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define res_PRTDSI__OUT_SEL0       (* (reg8 *) res__PRTDSI__OUT_SEL0) 
#define res_PRTDSI__OUT_SEL1       (* (reg8 *) res__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define res_PRTDSI__SYNC_OUT       (* (reg8 *) res__PRTDSI__SYNC_OUT) 


#if defined(res__INTSTAT)  /* Interrupt Registers */

    #define res_INTSTAT                (* (reg8 *) res__INTSTAT)
    #define res_SNAP                   (* (reg8 *) res__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins res_H */

#endif
/* [] END OF FILE */
