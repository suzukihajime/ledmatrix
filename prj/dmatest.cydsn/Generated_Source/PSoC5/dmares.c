/*******************************************************************************
* File Name: dmares.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "dmares.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 dmares__PORT == 15 && (dmares__MASK & 0xC0))

/*******************************************************************************
* Function Name: dmares_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void dmares_Write(uint8 value) 
{
    uint8 staticBits = dmares_DR & ~dmares_MASK;
    dmares_DR = staticBits | ((value << dmares_SHIFT) & dmares_MASK);
}


/*******************************************************************************
* Function Name: dmares_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void dmares_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(dmares_0, mode);
}


/*******************************************************************************
* Function Name: dmares_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro dmares_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 dmares_Read(void) 
{
    return (dmares_PS & dmares_MASK) >> dmares_SHIFT;
}


/*******************************************************************************
* Function Name: dmares_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 dmares_ReadDataReg(void) 
{
    return (dmares_DR & dmares_MASK) >> dmares_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(dmares_INTSTAT) 

    /*******************************************************************************
    * Function Name: dmares_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 dmares_ClearInterrupt(void) 
    {
        return (dmares_INTSTAT & dmares_MASK) >> dmares_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
