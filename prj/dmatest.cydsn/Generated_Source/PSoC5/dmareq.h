/*******************************************************************************
* File Name: dmareq.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_dmareq_H) /* Pins dmareq_H */
#define CY_PINS_dmareq_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "dmareq_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 dmareq__PORT == 15 && (dmareq__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    dmareq_Write(uint8 value) ;
void    dmareq_SetDriveMode(uint8 mode) ;
uint8   dmareq_ReadDataReg(void) ;
uint8   dmareq_Read(void) ;
uint8   dmareq_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define dmareq_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define dmareq_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define dmareq_DM_RES_UP          PIN_DM_RES_UP
#define dmareq_DM_RES_DWN         PIN_DM_RES_DWN
#define dmareq_DM_OD_LO           PIN_DM_OD_LO
#define dmareq_DM_OD_HI           PIN_DM_OD_HI
#define dmareq_DM_STRONG          PIN_DM_STRONG
#define dmareq_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define dmareq_MASK               dmareq__MASK
#define dmareq_SHIFT              dmareq__SHIFT
#define dmareq_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define dmareq_PS                     (* (reg8 *) dmareq__PS)
/* Data Register */
#define dmareq_DR                     (* (reg8 *) dmareq__DR)
/* Port Number */
#define dmareq_PRT_NUM                (* (reg8 *) dmareq__PRT) 
/* Connect to Analog Globals */                                                  
#define dmareq_AG                     (* (reg8 *) dmareq__AG)                       
/* Analog MUX bux enable */
#define dmareq_AMUX                   (* (reg8 *) dmareq__AMUX) 
/* Bidirectional Enable */                                                        
#define dmareq_BIE                    (* (reg8 *) dmareq__BIE)
/* Bit-mask for Aliased Register Access */
#define dmareq_BIT_MASK               (* (reg8 *) dmareq__BIT_MASK)
/* Bypass Enable */
#define dmareq_BYP                    (* (reg8 *) dmareq__BYP)
/* Port wide control signals */                                                   
#define dmareq_CTL                    (* (reg8 *) dmareq__CTL)
/* Drive Modes */
#define dmareq_DM0                    (* (reg8 *) dmareq__DM0) 
#define dmareq_DM1                    (* (reg8 *) dmareq__DM1)
#define dmareq_DM2                    (* (reg8 *) dmareq__DM2) 
/* Input Buffer Disable Override */
#define dmareq_INP_DIS                (* (reg8 *) dmareq__INP_DIS)
/* LCD Common or Segment Drive */
#define dmareq_LCD_COM_SEG            (* (reg8 *) dmareq__LCD_COM_SEG)
/* Enable Segment LCD */
#define dmareq_LCD_EN                 (* (reg8 *) dmareq__LCD_EN)
/* Slew Rate Control */
#define dmareq_SLW                    (* (reg8 *) dmareq__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define dmareq_PRTDSI__CAPS_SEL       (* (reg8 *) dmareq__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define dmareq_PRTDSI__DBL_SYNC_IN    (* (reg8 *) dmareq__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define dmareq_PRTDSI__OE_SEL0        (* (reg8 *) dmareq__PRTDSI__OE_SEL0) 
#define dmareq_PRTDSI__OE_SEL1        (* (reg8 *) dmareq__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define dmareq_PRTDSI__OUT_SEL0       (* (reg8 *) dmareq__PRTDSI__OUT_SEL0) 
#define dmareq_PRTDSI__OUT_SEL1       (* (reg8 *) dmareq__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define dmareq_PRTDSI__SYNC_OUT       (* (reg8 *) dmareq__PRTDSI__SYNC_OUT) 


#if defined(dmareq__INTSTAT)  /* Interrupt Registers */

    #define dmareq_INTSTAT                (* (reg8 *) dmareq__INTSTAT)
    #define dmareq_SNAP                   (* (reg8 *) dmareq__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins dmareq_H */

#endif
/* [] END OF FILE */
