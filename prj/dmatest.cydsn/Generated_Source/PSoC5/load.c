/*******************************************************************************
* File Name: load.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "load.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 load__PORT == 15 && (load__MASK & 0xC0))

/*******************************************************************************
* Function Name: load_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void load_Write(uint8 value) 
{
    uint8 staticBits = load_DR & ~load_MASK;
    load_DR = staticBits | ((value << load_SHIFT) & load_MASK);
}


/*******************************************************************************
* Function Name: load_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void load_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(load_0, mode);
}


/*******************************************************************************
* Function Name: load_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro load_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 load_Read(void) 
{
    return (load_PS & load_MASK) >> load_SHIFT;
}


/*******************************************************************************
* Function Name: load_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 load_ReadDataReg(void) 
{
    return (load_DR & load_MASK) >> load_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(load_INTSTAT) 

    /*******************************************************************************
    * Function Name: load_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 load_ClearInterrupt(void) 
    {
        return (load_INTSTAT & load_MASK) >> load_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
