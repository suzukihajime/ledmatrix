/*******************************************************************************
* File Name: stat.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "stat.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 stat__PORT == 15 && (stat__MASK & 0xC0))

/*******************************************************************************
* Function Name: stat_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void stat_Write(uint8 value) 
{
    uint8 staticBits = stat_DR & ~stat_MASK;
    stat_DR = staticBits | ((value << stat_SHIFT) & stat_MASK);
}


/*******************************************************************************
* Function Name: stat_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void stat_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(stat_0, mode);
	CyPins_SetPinDriveMode(stat_1, mode);
}


/*******************************************************************************
* Function Name: stat_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro stat_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 stat_Read(void) 
{
    return (stat_PS & stat_MASK) >> stat_SHIFT;
}


/*******************************************************************************
* Function Name: stat_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 stat_ReadDataReg(void) 
{
    return (stat_DR & stat_MASK) >> stat_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(stat_INTSTAT) 

    /*******************************************************************************
    * Function Name: stat_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 stat_ClearInterrupt(void) 
    {
        return (stat_INTSTAT & stat_MASK) >> stat_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
