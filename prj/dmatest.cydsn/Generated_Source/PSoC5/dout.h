/*******************************************************************************
* File Name: dout.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_dout_H) /* Pins dout_H */
#define CY_PINS_dout_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "dout_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 dout__PORT == 15 && (dout__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    dout_Write(uint8 value) ;
void    dout_SetDriveMode(uint8 mode) ;
uint8   dout_ReadDataReg(void) ;
uint8   dout_Read(void) ;
uint8   dout_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define dout_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define dout_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define dout_DM_RES_UP          PIN_DM_RES_UP
#define dout_DM_RES_DWN         PIN_DM_RES_DWN
#define dout_DM_OD_LO           PIN_DM_OD_LO
#define dout_DM_OD_HI           PIN_DM_OD_HI
#define dout_DM_STRONG          PIN_DM_STRONG
#define dout_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define dout_MASK               dout__MASK
#define dout_SHIFT              dout__SHIFT
#define dout_WIDTH              6u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define dout_PS                     (* (reg8 *) dout__PS)
/* Data Register */
#define dout_DR                     (* (reg8 *) dout__DR)
/* Port Number */
#define dout_PRT_NUM                (* (reg8 *) dout__PRT) 
/* Connect to Analog Globals */                                                  
#define dout_AG                     (* (reg8 *) dout__AG)                       
/* Analog MUX bux enable */
#define dout_AMUX                   (* (reg8 *) dout__AMUX) 
/* Bidirectional Enable */                                                        
#define dout_BIE                    (* (reg8 *) dout__BIE)
/* Bit-mask for Aliased Register Access */
#define dout_BIT_MASK               (* (reg8 *) dout__BIT_MASK)
/* Bypass Enable */
#define dout_BYP                    (* (reg8 *) dout__BYP)
/* Port wide control signals */                                                   
#define dout_CTL                    (* (reg8 *) dout__CTL)
/* Drive Modes */
#define dout_DM0                    (* (reg8 *) dout__DM0) 
#define dout_DM1                    (* (reg8 *) dout__DM1)
#define dout_DM2                    (* (reg8 *) dout__DM2) 
/* Input Buffer Disable Override */
#define dout_INP_DIS                (* (reg8 *) dout__INP_DIS)
/* LCD Common or Segment Drive */
#define dout_LCD_COM_SEG            (* (reg8 *) dout__LCD_COM_SEG)
/* Enable Segment LCD */
#define dout_LCD_EN                 (* (reg8 *) dout__LCD_EN)
/* Slew Rate Control */
#define dout_SLW                    (* (reg8 *) dout__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define dout_PRTDSI__CAPS_SEL       (* (reg8 *) dout__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define dout_PRTDSI__DBL_SYNC_IN    (* (reg8 *) dout__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define dout_PRTDSI__OE_SEL0        (* (reg8 *) dout__PRTDSI__OE_SEL0) 
#define dout_PRTDSI__OE_SEL1        (* (reg8 *) dout__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define dout_PRTDSI__OUT_SEL0       (* (reg8 *) dout__PRTDSI__OUT_SEL0) 
#define dout_PRTDSI__OUT_SEL1       (* (reg8 *) dout__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define dout_PRTDSI__SYNC_OUT       (* (reg8 *) dout__PRTDSI__SYNC_OUT) 


#if defined(dout__INTSTAT)  /* Interrupt Registers */

    #define dout_INTSTAT                (* (reg8 *) dout__INTSTAT)
    #define dout_SNAP                   (* (reg8 *) dout__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins dout_H */

#endif
/* [] END OF FILE */
