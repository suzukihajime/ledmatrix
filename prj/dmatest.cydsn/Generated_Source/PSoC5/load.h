/*******************************************************************************
* File Name: load.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_load_H) /* Pins load_H */
#define CY_PINS_load_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "load_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 load__PORT == 15 && (load__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    load_Write(uint8 value) ;
void    load_SetDriveMode(uint8 mode) ;
uint8   load_ReadDataReg(void) ;
uint8   load_Read(void) ;
uint8   load_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define load_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define load_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define load_DM_RES_UP          PIN_DM_RES_UP
#define load_DM_RES_DWN         PIN_DM_RES_DWN
#define load_DM_OD_LO           PIN_DM_OD_LO
#define load_DM_OD_HI           PIN_DM_OD_HI
#define load_DM_STRONG          PIN_DM_STRONG
#define load_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define load_MASK               load__MASK
#define load_SHIFT              load__SHIFT
#define load_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define load_PS                     (* (reg8 *) load__PS)
/* Data Register */
#define load_DR                     (* (reg8 *) load__DR)
/* Port Number */
#define load_PRT_NUM                (* (reg8 *) load__PRT) 
/* Connect to Analog Globals */                                                  
#define load_AG                     (* (reg8 *) load__AG)                       
/* Analog MUX bux enable */
#define load_AMUX                   (* (reg8 *) load__AMUX) 
/* Bidirectional Enable */                                                        
#define load_BIE                    (* (reg8 *) load__BIE)
/* Bit-mask for Aliased Register Access */
#define load_BIT_MASK               (* (reg8 *) load__BIT_MASK)
/* Bypass Enable */
#define load_BYP                    (* (reg8 *) load__BYP)
/* Port wide control signals */                                                   
#define load_CTL                    (* (reg8 *) load__CTL)
/* Drive Modes */
#define load_DM0                    (* (reg8 *) load__DM0) 
#define load_DM1                    (* (reg8 *) load__DM1)
#define load_DM2                    (* (reg8 *) load__DM2) 
/* Input Buffer Disable Override */
#define load_INP_DIS                (* (reg8 *) load__INP_DIS)
/* LCD Common or Segment Drive */
#define load_LCD_COM_SEG            (* (reg8 *) load__LCD_COM_SEG)
/* Enable Segment LCD */
#define load_LCD_EN                 (* (reg8 *) load__LCD_EN)
/* Slew Rate Control */
#define load_SLW                    (* (reg8 *) load__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define load_PRTDSI__CAPS_SEL       (* (reg8 *) load__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define load_PRTDSI__DBL_SYNC_IN    (* (reg8 *) load__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define load_PRTDSI__OE_SEL0        (* (reg8 *) load__PRTDSI__OE_SEL0) 
#define load_PRTDSI__OE_SEL1        (* (reg8 *) load__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define load_PRTDSI__OUT_SEL0       (* (reg8 *) load__PRTDSI__OUT_SEL0) 
#define load_PRTDSI__OUT_SEL1       (* (reg8 *) load__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define load_PRTDSI__SYNC_OUT       (* (reg8 *) load__PRTDSI__SYNC_OUT) 


#if defined(load__INTSTAT)  /* Interrupt Registers */

    #define load_INTSTAT                (* (reg8 *) load__INTSTAT)
    #define load_SNAP                   (* (reg8 *) load__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins load_H */

#endif
/* [] END OF FILE */
