/*******************************************************************************
* File Name: req.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_req_H) /* Pins req_H */
#define CY_PINS_req_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "req_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 req__PORT == 15 && (req__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    req_Write(uint8 value) ;
void    req_SetDriveMode(uint8 mode) ;
uint8   req_ReadDataReg(void) ;
uint8   req_Read(void) ;
uint8   req_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define req_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define req_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define req_DM_RES_UP          PIN_DM_RES_UP
#define req_DM_RES_DWN         PIN_DM_RES_DWN
#define req_DM_OD_LO           PIN_DM_OD_LO
#define req_DM_OD_HI           PIN_DM_OD_HI
#define req_DM_STRONG          PIN_DM_STRONG
#define req_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define req_MASK               req__MASK
#define req_SHIFT              req__SHIFT
#define req_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define req_PS                     (* (reg8 *) req__PS)
/* Data Register */
#define req_DR                     (* (reg8 *) req__DR)
/* Port Number */
#define req_PRT_NUM                (* (reg8 *) req__PRT) 
/* Connect to Analog Globals */                                                  
#define req_AG                     (* (reg8 *) req__AG)                       
/* Analog MUX bux enable */
#define req_AMUX                   (* (reg8 *) req__AMUX) 
/* Bidirectional Enable */                                                        
#define req_BIE                    (* (reg8 *) req__BIE)
/* Bit-mask for Aliased Register Access */
#define req_BIT_MASK               (* (reg8 *) req__BIT_MASK)
/* Bypass Enable */
#define req_BYP                    (* (reg8 *) req__BYP)
/* Port wide control signals */                                                   
#define req_CTL                    (* (reg8 *) req__CTL)
/* Drive Modes */
#define req_DM0                    (* (reg8 *) req__DM0) 
#define req_DM1                    (* (reg8 *) req__DM1)
#define req_DM2                    (* (reg8 *) req__DM2) 
/* Input Buffer Disable Override */
#define req_INP_DIS                (* (reg8 *) req__INP_DIS)
/* LCD Common or Segment Drive */
#define req_LCD_COM_SEG            (* (reg8 *) req__LCD_COM_SEG)
/* Enable Segment LCD */
#define req_LCD_EN                 (* (reg8 *) req__LCD_EN)
/* Slew Rate Control */
#define req_SLW                    (* (reg8 *) req__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define req_PRTDSI__CAPS_SEL       (* (reg8 *) req__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define req_PRTDSI__DBL_SYNC_IN    (* (reg8 *) req__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define req_PRTDSI__OE_SEL0        (* (reg8 *) req__PRTDSI__OE_SEL0) 
#define req_PRTDSI__OE_SEL1        (* (reg8 *) req__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define req_PRTDSI__OUT_SEL0       (* (reg8 *) req__PRTDSI__OUT_SEL0) 
#define req_PRTDSI__OUT_SEL1       (* (reg8 *) req__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define req_PRTDSI__SYNC_OUT       (* (reg8 *) req__PRTDSI__SYNC_OUT) 


#if defined(req__INTSTAT)  /* Interrupt Registers */

    #define req_INTSTAT                (* (reg8 *) req__INTSTAT)
    #define req_SNAP                   (* (reg8 *) req__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins req_H */

#endif
/* [] END OF FILE */
