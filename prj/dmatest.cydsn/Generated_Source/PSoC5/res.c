/*******************************************************************************
* File Name: res.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "res.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 res__PORT == 15 && (res__MASK & 0xC0))

/*******************************************************************************
* Function Name: res_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void res_Write(uint8 value) 
{
    uint8 staticBits = res_DR & ~res_MASK;
    res_DR = staticBits | ((value << res_SHIFT) & res_MASK);
}


/*******************************************************************************
* Function Name: res_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void res_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(res_0, mode);
}


/*******************************************************************************
* Function Name: res_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro res_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 res_Read(void) 
{
    return (res_PS & res_MASK) >> res_SHIFT;
}


/*******************************************************************************
* Function Name: res_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 res_ReadDataReg(void) 
{
    return (res_DR & res_MASK) >> res_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(res_INTSTAT) 

    /*******************************************************************************
    * Function Name: res_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 res_ClearInterrupt(void) 
    {
        return (res_INTSTAT & res_MASK) >> res_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
