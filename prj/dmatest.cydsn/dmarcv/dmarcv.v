
//`#start header` -- edit after this line, do not edit this line
// ========================================
//
// Copyright YOUR COMPANY, THE YEAR
// All Rights Reserved
// UNPUBLISHED, LICENSED SOFTWARE.
//
// CONFIDENTIAL AND PROPRIETARY INFORMATION
// WHICH IS THE PROPERTY OF your company.
//
// ========================================
`include "cypress.v"
//`#end` -- edit above this line, do not edit this line
// Generated on 10/20/2012 at 23:10
// Component: dmarcv
module dmarcv (
	output  dmareq,
	output  dmaemp,
	output [5:0] dout,
	output [1:0] stat,
	output [6:0] count7,
	input   clock,
	input   reset
);

//`#start body` -- edit after this line, do not edit this line

	localparam [5:0] PERIOD = 6'd16;
	wire dmarcv_clock;
	cy_psoc3_udb_clock_enable_v1_0 #(.sync_mode(`TRUE)) ClkSync
    (
        /* input  */    .clock_in(clock),
        /* input  */    .enable(1'b1),
        /* output */    .clock_out(dmarcv_clock)
    );
	
	wire dmareq0;
	wire dmareq1;
	wire dmaemp0;
	wire dmaemp1;
	wire [6:0] dmarcv_count;

	/* local net */
	wire dmarcv_dp0_f0_nfull;
	wire dmarcv_dp0_f1_nfull;
	wire dmarcv_dp0_f0_empty;
	wire dmarcv_dp0_f1_empty;
	wire [7:0] dmarcv_dp0_pdata;
	wire [2:0] dmarcv_dp0_addr;
	
	reg [1:0] dmarcv_state;
	
	assign dmareq0 = dmarcv_dp0_f0_nfull;
	assign dmareq1 = dmarcv_dp0_f1_nfull;
	assign dmaemp0 = dmarcv_dp0_f0_empty;
	assign dmaemp1 = dmarcv_dp0_f1_empty;

	assign dmareq = dmareq0;
	assign dmaemp = dmaemp0;
	assign dout[5:0] = dmarcv_dp0_pdata[5:0];
	assign stat[1:0] = dmarcv_state[1:0];

	reg [8:0] count;
	always @(posedge dmarcv_clock) begin
		if(count[8:0] == 9'b111111111) begin
			count[8:0] = 9'b000000000;
		end else begin
			count[8:0] = count[8:0] + 9'b000000001;
		end
	end
	assign dmarcv_count[8:0] = count[8:0];

	localparam DMA_STATE_HOLD      = 2'd0;
	localparam DMA_STATE_IDLE      = 2'd1;
	localparam DMA_STATE_LOAD      = 2'd2;
	localparam DMA_STATE_ZERO      = 2'd3;

	always @(posedge dmarcv_clock) begin
		if(dmarcv_count[0] == 1'b1) begin
			case(dmarcv_count[8:1])
				8'd0:	dmarcv_state <= DMA_STATE_LOAD;		/* shift left */
				8'd8:	dmarcv_state <= DMA_STATE_LOAD;
				8'd16:	dmarcv_state <= DMA_STATE_LOAD;
				8'd24:	dmarcv_state <= DMA_STATE_LOAD;
				8'd32:	dmarcv_state <= DMA_STATE_LOAD;
				8'd40:	dmarcv_state <= DMA_STATE_LOAD;
				8'd48:	dmarcv_state <= DMA_STATE_LOAD;
				8'd56:	dmarcv_state <= DMA_STATE_LOAD;
				8'd64:	dmarcv_state <= DMA_STATE_LOAD;
				8'd72:	dmarcv_state <= DMA_STATE_LOAD;
				8'd80:	dmarcv_state <= DMA_STATE_LOAD;
				8'd88:	dmarcv_state <= DMA_STATE_LOAD;
				8'd96:	dmarcv_state <= DMA_STATE_LOAD;		/* shift right */
				8'd104:	dmarcv_state <= DMA_STATE_LOAD;
				8'd112:	dmarcv_state <= DMA_STATE_LOAD;
				8'd120:	dmarcv_state <= DMA_STATE_LOAD;
				8'd128:	dmarcv_state <= DMA_STATE_LOAD;
				8'd136:	dmarcv_state <= DMA_STATE_LOAD;
				8'd144:	dmarcv_state <= DMA_STATE_LOAD;
				8'd152:	dmarcv_state <= DMA_STATE_LOAD;
				8'd160:	dmarcv_state <= DMA_STATE_LOAD;
				8'd168:	dmarcv_state <= DMA_STATE_LOAD;
				8'd176:	dmarcv_state <= DMA_STATE_LOAD;
				8'd184:	dmarcv_state <= DMA_STATE_LOAD;
			default:	dmarcv_state <= DMA_STATE_IDLE;
			endcase
		end else begin
			dmarcv_state <= DMA_STATE_IDLE;
		end
	end
	
	wire color = 1'b0;
	wire [2:0] dmarcv_dp0_address = {color, dmarcv_state[1:0]};
	
	cy_psoc3_dp #(.cy_dpconfig(
	{
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP___SR, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM0:    shift right*/
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP___SL, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM1: shift left*/
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC___F0, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM2:    fifo load*/
		`CS_ALU_OP__XOR, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM3:    zero*/
		`CS_ALU_OP_PASS, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM4:       */
		`CS_ALU_OP_PASS, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM5:       */
		`CS_ALU_OP_PASS, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC___F1,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM6:       */
		`CS_ALU_OP__XOR, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM7:       */
		  8'hFF, 8'h00,	/*CFG9:       */
		  8'hFF, 8'hFF,	/*CFG11-10:       */
		`SC_CMPB_A1_D1, `SC_CMPA_A1_D1, `SC_CI_B_ARITH,
		`SC_CI_A_ARITH, `SC_C1_MASK_DSBL, `SC_C0_MASK_DSBL,
		`SC_A_MASK_DSBL, `SC_DEF_SI_0, `SC_SI_B_DEFSI,
		`SC_SI_A_DEFSI, /*CFG13-12:       */
		`SC_A0_SRC_ACC, `SC_SHIFT_SL, 1'h0,
		1'h0, `SC_FIFO1_BUS, `SC_FIFO0_BUS,
		`SC_MSB_DSBL, `SC_MSB_BIT0, `SC_MSB_NOCHN,
		`SC_FB_NOCHN, `SC_CMP1_NOCHN,
		`SC_CMP0_NOCHN, /*CFG15-14:       */
		 10'h00, `SC_FIFO_CLK__DP,`SC_FIFO_CAP_AX,
		`SC_FIFO_LEVEL,`SC_FIFO_ASYNC,`SC_EXTCRC_DSBL,
		`SC_WRK16CAT_DSBL /*CFG17-16:       */
	})) dp0(
		/* input */ .clk(dmarcv_clock), 			// Clock
		/* input [02:00] */ .cs_addr(dmarcv_dp0_address), // Control Store RAM address
		/* input */ .route_si(1'b0), 		// Shift in from routing
		/* input */ .route_ci(1'b0), 		// Carry in from routing
		/* input */ .f0_load(1'b0), 		// Load FIFO 0
		/* input */ .f1_load(1'b0), 		// Load FIFO 1
		/* input */ .d0_load(1'b0), 		// Load Data Register 0
		/* input */ .d1_load(1'b0), 		// Load Data Register 1
		/* output */ .ce0(), 			// Accumulator 0 = Data register 0
		/* output */ .cl0(), 			// Accumulator 0 < Data register 0
		/* output */ .z0(), 			// Accumulator 0 = 0
		/* output */ .ff0(), 			// Accumulator 0 = FF
		/* output */ .ce1(), 			// Accumulator [0|1] = Data register 1
		/* output */ .cl1(), 			// Accumulator [0|1] < Data register 1
		/* output */ .z1(), 			// Accumulator 1 = 0
		/* output */ .ff1(), 			// Accumulator 1 = FF
		/* output */ .ov_msb(), 		// Operation over flow
		/* output */ .co_msb(), 		// Carry out
		/* output */ .cmsb(), 			// Carry out
		/* output */ .so(), 			// Shift out
		/* output */ .f0_bus_stat(dmarcv_dp0_f0_nfull), 	// FIFO 0 status to uP
		/* output */ .f0_blk_stat(dmarcv_dp0_f0_empty), 	// FIFO 0 status to DP
		/* output */ .f1_bus_stat(dmarcv_dp0_f1_nfull), 	// FIFO 1 status to uP
		/* output */ .f1_blk_stat(dmarcv_dp0_f1_empty), 	// FIFO 1 status to DP
		/* input */ .ci(1'b0), 				// Carry in from previous stage
		/* output */ .co(), 			// Carry out to next stage
		/* input */ .sir(1'b0), 			// Shift in from right side
		/* output */ .sor(), 			// Shift out to right side
		/* input */ .sil(1'b0), 			// Shift in from left side
		/* output */ .sol(), 			// Shift out to left side
		/* input */ .msbi(1'b0), 			// MSB chain in
		/* output */ .msbo(), 			// MSB chain out
		/* input [01:00] */ .cei(2'b0), 	// Compare equal in from prev stage
		/* output [01:00] */ .ceo(), 	// Compare equal out to next stage
		/* input [01:00] */ .cli(2'b0), 	// Compare less than in from prv stage
		/* output [01:00] */ .clo(), 	// Compare less than out to next stage
		/* input [01:00] */ .zi(2'b0), 		// Zero detect in from previous stage
		/* output [01:00] */ .zo(), 	// Zero detect out to next stage
		/* input [01:00] */ .fi(2'b0), 		// 0xFF detect in from previous stage
		/* output [01:00] */ .fo(), 	// 0xFF detect out to next stage
		/* input [01:00] */ .capi(2'b0),	// Capture in from previous stage
		/* output [01:00] */ .capo(),		// Capture out to next stage
		/* input */ .cfbi(1'b0), 			// CRC Feedback in from previous stage
		/* output */ .cfbo(), 			// CRC Feedback out to next stage
		/* input [07:00] */ .pi(), 		// Parallel data port
		/* output [07:00] */ .po(dmarcv_dp0_pdata)
	);
//`#end` -- edit above this line, do not edit this line
endmodule
//`#start footer` -- edit after this line, do not edit this line
//`#end` -- edit above this line, do not edit this line




