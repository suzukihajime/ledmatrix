/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/*
utf-8をバッファから順に取り出してデコードするのは、ちょっと筋が悪いかもしれない。
FIFOにはデコードした後のものを入れる。
*/
#include "uart.h"
#include "utf8.h"

// parseUtf8用
/*
UTF-8受信用バッファ
*/
static char utf[8] = {0};
static int cnt = 0;		// 受信文字数を示すカウンタ
/*
state : UTF-8の何文字目であるかを示す。
0 : 初期化ステート
0以外は、あと何文字受信したら0ステートに戻るかをしめしている。
*/
static char state = 0;
	
/*
uint16 fifoGetNextUtf8(void)
{
	register char c;
	register uint16 uni;
	c = fifoGetChar();
	if(c & 0x80) {
		// when msb of the first byte is 1
		if(c & 0x20) {
			if(c & 0x10) {
				uni = c & 0x07; uni <<= 6;
				uni |= fifoGetChar() & 0x3f; uni <<= 6;
				uni |= fifoGetChar() & 0x3f; uni <<= 6;
				uni |= fifoGetChar() & 0x3f;
			} else {
				uni = c & 0x0f; uni <<= 6;
				uni |= fifoGetChar() & 0x3f; uni <<= 6;
				uni |= fifoGetChar() & 0x3f;
			}
		} else {
			uni = c & 0x1f; uni <<= 6;
			uni |= fifoGetChar() & 0x3f;
		}
	} else {
		// ascii
		uni = c;
	}
	return(uni);
}
*/
/*
UTFー8分解ステートマシン
*/
uint16 parseUtf8(char c)
{
	if(c == '\r' || c == '\n') {
		state = 0;
		cnt = 0;
//		uprintf("cr/lf received\n");
	}
	if(state == 0) {
		if(c & 0x80) {
			// when msb of the first byte is 1
			if(c & 0x20) {
				if(c & 0x10) {
					state = 3;	// 4文字コード
				} else {
					state = 2;	// 3文字コード(日本語)
				}
			} else {
				state = 1;	// 2文字コード
			}
			cnt = 0;
			utf[cnt++] = c;
		} else {
			state = 0;	// ASCII
			return((int16)c);
		}
	} else {
		utf[cnt++] = c;
		if(--state == 0) {
			cnt = 0;
//			uprintf("decoded char\n");
			return(decodeUtf8(utf));
		}
	}
//	uprintf("s:%d, c:%d\n", state, cnt);
	return(0);
}

// ステートマシンをクリアする
inline void clearParser(void)
{
	state = 0;
	cnt = 0;
	int i;
	for(i = 0; i < 8; i++) {
		utf[i] = 0;
	}
	return;
}

/*
UTF-8をunicodeに分解する。基本的には上の2つの関数とやってることは同じ
*/
inline uint16 decodeUtf8(char *buf)
{
	register uint16 uni = 0;
	register char c;
	c = buf[0];
	if(c& 0x80) {
		// when msb of the first byte is 1
		if(c & 0x20) {
			if(c & 0x10) {
				uni = c & 0x07; uni <<= 6;
				uni |= buf[1] & 0x3f; uni <<= 6;
				uni |= buf[2] & 0x3f; uni <<= 6;
				uni |= buf[3] & 0x3f;
			} else {
				uni = c & 0x0f; uni <<= 6;
				uni |= buf[1] & 0x3f; uni <<= 6;
				uni |= buf[2] & 0x3f;
			}
		} else {
			uni = c & 0x1f; uni <<= 6;
			uni |= buf[1] & 0x3f;
		}
	} else {
		// ascii
		uni = c;
	}
	return(uni);
}

/* [] END OF FILE */
