/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "cytypes.h"
#include "cyfitter.h"


#define `$INSTANCE_NAME`_FIFO0_PTR    (  (reg8 *) `$INSTANCE_NAME`_dp0__F0_REG )
#define `$INSTANCE_NAME`_FIFO1_PTR    (  (reg8 *) `$INSTANCE_NAME`_dp0__F1_REG )

#define `$INSTANCE_NAME`_ROWCOUNTER_CONTROL_REG (* (reg8 *)  `$INSTANCE_NAME`_rowcounter1__CONTROL_AUX_CTL_REG)
#define `$INSTANCE_NAME`_ROWCOUNTER_CONTROL_PTR (  (reg8 *)  `$INSTANCE_NAME`_rowcounter1__CONTROL_AUX_CTL_REG)
#define `$INSTANCE_NAME`_GRADCOUNTER_CONTROL_REG (* (reg8 *)  `$INSTANCE_NAME`_gradcounter1__CONTROL_AUX_CTL_REG)
#define `$INSTANCE_NAME`_GRADCOUNTER_CONTROL_PTR (  (reg8 *)  `$INSTANCE_NAME`_gradcounter1__CONTROL_AUX_CTL_REG)
#define `$INSTANCE_NAME`_COUNTER_LO_CONTROL_REG (* (reg8 *)  `$INSTANCE_NAME`_counter_low__CONTROL_AUX_CTL_REG)
#define `$INSTANCE_NAME`_COUNTER_LO_CONTROL_PTR (  (reg8 *)  `$INSTANCE_NAME`_counter_low__CONTROL_AUX_CTL_REG)
#define `$INSTANCE_NAME`_COUNTER_HI_CONTROL_REG (* (reg8 *)  `$INSTANCE_NAME`_counter_high__CONTROL_AUX_CTL_REG)
#define `$INSTANCE_NAME`_COUNTER_HI_CONTROL_PTR (  (reg8 *)  `$INSTANCE_NAME`_counter_high__CONTROL_AUX_CTL_REG)


#define `$INSTANCE_NAME`_CNTR_ENABLE                 (0x20u)   

/*
#define matdrv_1_dp1_u0__A0_A1_REG CYREG_B1_UDB11_A0_A1
#define matdrv_1_dp1_u0__A0_REG CYREG_B1_UDB11_A0
#define matdrv_1_dp1_u0__A1_REG CYREG_B1_UDB11_A1
#define matdrv_1_dp1_u0__D0_D1_REG CYREG_B1_UDB11_D0_D1
#define matdrv_1_dp1_u0__D0_REG CYREG_B1_UDB11_D0
#define matdrv_1_dp1_u0__D1_REG CYREG_B1_UDB11_D1
#define matdrv_1_dp1_u0__DP_AUX_CTL_REG CYREG_B1_UDB11_ACTL
#define matdrv_1_dp1_u0__F0_F1_REG CYREG_B1_UDB11_F0_F1
#define matdrv_1_dp1_u0__F0_REG CYREG_B1_UDB11_F0
#define matdrv_1_dp1_u0__F1_REG CYREG_B1_UDB11_F1
*/

void `$INSTANCE_NAME`_Start(void);
void `$INSTANCE_NAME`_Stop(void);

//[] END OF FILE
