/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "CyLib.h"
#include "`$INSTANCE_NAME`_matdrv3.h"


void `$INSTANCE_NAME`_Start(void)
{
	/* nothing to do */
	uint8 enableInterrupts = 0u;    
	enableInterrupts = CyEnterCriticalSection();
	`$INSTANCE_NAME`_ROWCOUNTER_CONTROL_REG |= `$INSTANCE_NAME`_CNTR_ENABLE;
	`$INSTANCE_NAME`_GRADCOUNTER_CONTROL_REG |= `$INSTANCE_NAME`_CNTR_ENABLE;
	`$INSTANCE_NAME`_COUNTER_LO_CONTROL_REG |= `$INSTANCE_NAME`_CNTR_ENABLE;
	`$INSTANCE_NAME`_COUNTER_HI_CONTROL_REG |= `$INSTANCE_NAME`_CNTR_ENABLE;
	CyExitCriticalSection(enableInterrupts);
	return;
}

void `$INSTANCE_NAME`_Stop(void)
{
	/* nothing to do */
	return;
}

/* [] END OF FILE */
