/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <device.h>
#include "font.h"
#include "uart.h"
#include "utf8.h"
#include "matrix.h"
#include "fifo.h"

void initialize(void);
void buf_initialize(uint8 buf[][16]);
void delay_ms(uint16);
uint8 disp_buf_a[192][16] __attribute__((aligned(16), section(".data2")));
uint8 disp_buf_b[192][16] __attribute__((aligned(16), section(".data2")));

/*
const char antimayo[16][12] = {
	{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
	{0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
	{0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
	{0x74, 0x84, 0x64, 0x88, 0x9d, 0x08, 0x78, 0x42, 0x42, 0x74, 0x9e, 0x3c},
	{0x8c, 0x84, 0x84, 0x88, 0xa3, 0x14, 0x84, 0x42, 0x42, 0x8c, 0xa1, 0x42},
	{0x84, 0x84, 0x84, 0x88, 0xa1, 0x14, 0x84, 0x42, 0x42, 0x84, 0x81, 0x40},
	{0x7c, 0x84, 0x84, 0x88, 0x9f, 0x22, 0x84, 0x42, 0x42, 0x7c, 0x9e, 0x7e},
	{0x04, 0x84, 0x84, 0x88, 0x81, 0x22, 0x84, 0x42, 0x42, 0x04, 0xa0, 0x42},
	{0x84, 0xc4, 0x84, 0xcc, 0xa1, 0x22, 0x84, 0x62, 0x62, 0x84, 0xa1, 0x42},
	{0x78, 0xb9, 0xe4, 0xb3, 0x1e, 0x41, 0x78, 0x5c, 0x5c, 0x78, 0x9e, 0x3c},
	{0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
	{0x00, 0x00, 0x84, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00},
	{0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00},
	{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
	{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
	{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
	};
void antimayo_init(void)
{
	uint16 i, j;
	for(i = 0; i < 96; i++) {
		for(j = 0; j < 16; j++) {
			if(antimayo[15-j][i/8] & (0x80>>(i&0x07))) {
				disp_buf[i][j] = 0xff;
			} else {
				disp_buf[i][j] = 0x00;
			}
		}
	}
	return;
}
*/

/*
* interrupt handler
*/
uint32 rcnt = 0, rpos = 0, wpos = 0;
uint8 blank_cnt = 0;
void shift_isr(void)
{
	if((rpos % 12) < 6) {
		Matrix_SetBaseAddress((uint8*)&disp_buf_a[(rpos%6)*16 + rcnt]);
	} else {
		Matrix_SetBaseAddress((uint8*)&disp_buf_b[(rpos%6)*16 + rcnt]);
	}
	if(++rcnt >= 16) {
		rcnt = 0;
		rpos++;
//		if(++rpos >= 6) { rpos = 0; wpos = 0; }
	}
	// すでに表示が終わったバッファはクリアする。
	if((rpos % 12) == 0 && rcnt == 0) {
		buf_initialize(disp_buf_b);
	}
	if((rpos % 12) == 6 && rcnt == 0) {
		buf_initialize(disp_buf_a);
	}
	if(wpos < (rpos + 6)) {
		wpos = rpos + 6;
	}
	return;
}

void rx_isr(void)
{
	char c;
	int16 utf;
	TEST_Write(1);
	/*
	ここの処理が微妙。テストが必要。
	*/
	while(UART_1_ReadRxStatus() & UART_1_RX_STS_FIFO_NOTEMPTY) {
		c = UART_1_ReadRxData();
	//	uputc(c);
		if((utf = parseUtf8(c))) {
			fifoPushChar(utf);
//			uprintf("c = %d\n", utf);
		}
	}
	TEST_Write(0);
	return;
}

void main()
{
	int i, j;
	uint16 uni;
	volatile uint32 addr;
//	char utf[1024] = "統計的機械学習とは、観測されたデータから統計的手法を用い新たな知識を導出することである。統計的機械学習についての教科書的な内容はこちらを参照してほしい。";
	initialize();
	
	for(i = 0; i < 16; i++) {
		for(j = 0; j < 96; j++) {
			disp_buf_a[j][i] = (j<<4) + i;
		}
	}

	/*
	ループでフォントを展開する
	*/
	while(1) {
		if((rpos+12) > wpos) {
			// 新しく書き込みできる。
			if(fifoGetContentSize()) {
//				TEST_Write(1);			// 時間計測の必要あり
				uni = fifoGetChar();
				uprintf("%x\n", uni);
				addr = getFontAddress(uni);
				expandFontBitmap(addr, &disp_buf_a[((wpos+0) % 12)*16][0], 0xff, FONT_ROP_COPY);
				expandFontBitmap(addr, &disp_buf_b[((wpos+6) % 12)*16][0], 0xff, FONT_ROP_COPY);
				wpos++;
//				TEST_Write(0);
			}
		}
		uprintf("r: %d, w: %d\n", rpos, wpos);
	}
	closeFont();
}

void initialize(void)
{
	uint8 ret;
	
	UART_Start();
	Timer_Start();
	Timer_WriteCounter(0);
	buf_initialize(disp_buf_a);
	buf_initialize(disp_buf_b);
	Matrix_Start();
	Matrix_SetBaseAddress((uint8*)disp_buf_a);
//	ret = openFont("SHNMK16.BDF", "CTABLE2.TXT");
//	FFT_Start();
	
	CyGlobalIntEnable;
	shift_irq_StartEx(shift_isr);
	rx_irq_StartEx(rx_isr);
	disp_buf_a[ret][3] = 0xff;
	rpos = wpos = 0;
	return;
}

void buf_initialize(uint8 buf[][16])
{
	int16 i, j;
	for(i = 0; i < 192; i++) {
		for(j = 0; j < 16; j++) {
			buf[i][j] = 0x00;
		}
	}
	return;
}

void delay_ms(uint16 delay)
{
	Timer_WriteCounter(delay);
	while(Timer_ReadCounter() != 0) {}
	return;
}

/* [] END OF FILE */