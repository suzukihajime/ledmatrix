/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "fifo.h"
#include "utf8.h"

// fifo用
static int16 fifo[FIFO_SIZE];		// アルファベットも16bitで格納する。
static int32 rptr = 0, wptr = 0;

inline int16 fifoGetChar(void)
{
	register int16 c;
	if(fifoGetContentSize()) {
		c = fifo[rptr];
		rptr = (rptr+1) % FIFO_SIZE;
	} else {
		c = 0;
	}
	return c;
}

inline void fifoPushChar(int16 c)
{
	if(fifoGetVacancySize()) {
		fifo[wptr] = c;
		wptr = (wptr+1) % FIFO_SIZE;
	}
	return;
}

/*
UTF-8をデコードしてからFIFOにプッシュする。
*/
inline void fifoPushString(char *str)
{
	char c;
	int16 uni;
	while(c = *str++) {
//		fifoPushChar(c);
		if((uni = parseUtf8(c)) != 0) {
			fifoPushChar(uni);
		}
	}
	return;
}

inline uint16 fifoGetContentSize(void)
{
	return((wptr - rptr) % FIFO_SIZE);
}

inline uint16 fifoGetVacancySize(void)
{
	int32 offset;
	offset = (wptr >= rptr) ? FIFO_SIZE : 0;
	return(rptr + offset - wptr);
}

/* [] END OF FILE */
