//`#start header` -- edit after this line, do not edit this line
// ========================================
//
// Copyright YOUR COMPANY, THE YEAR
// All Rights Reserved
// UNPUBLISHED, LICENSED SOFTWARE.
//
// CONFIDENTIAL AND PROPRIETARY INFORMATION
// WHICH IS THE PROPERTY OF your company.
//
// ========================================
`include "cypress.v"
//`#end` -- edit above this line, do not edit this line

//`#start body` -- edit after this line, do not edit this line

//        Your code goes here

module addrgen2 (
	input [3:0] next_row,
	input req,
	input rowterm,
	input clock,
	input reset,
	output dmareq,
	input dmaterm
);
	/*
	udb_clock_enableをインスタンス化する
	*/
	wire addrgen2_clock;
	cy_psoc3_udb_clock_enable_v1_0 #(.sync_mode(`TRUE)) ClkSync
    (
        /* input  */    .clock_in(clock),
        /* input  */    .enable(1'b1),
        /* output */    .clock_out(addrgen2_clock)
    );
	/*
	転送数をカウントする。この値を元に次のアドレスを決定。
	転送終了後にカウントしたいので、posedge dma_termとしてある。
	*/
/*	reg [7:0] addrgen2_count;
	always @(posedge dmaterm or posedge rowterm) begin
		if(rowterm == 1'b1) begin
			addrgen2_count[7:0] <= 8'b00000000;
		end else begin
			if(dmaterm == 1'b1) begin
				addrgen2_count[7:0] <= addrgen2_count[7:0] + 8'b00000001;
			end else begin
				addrgen2_count[7:0] <= addrgen2_count[7:0];
			end
		end
	end*/
	
	reg dmaterm_latch;
	always @(posedge addrgen2_clock) begin
		dmaterm_latch <= dmaterm;
	end
	wire dmaterm_filt = ~dmaterm_latch & dmaterm;
	wire [7:0] addrgen2_count;
	wire [2:0] addrgen2_dp_addr = {1'b0, rowterm, dmaterm_filt};
	wire addrgen2_comp_lt;			// becomes 1 when counter is less than 96
	cy_psoc3_dp #(.cy_dpconfig(
	{
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM0:  idle state*/
	    `CS_ALU_OP__INC, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM1:  increment state*/
	    `CS_ALU_OP__XOR, `CS_SRCA_A0, `CS_SRCB_A0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM2:  zero*/
	    `CS_ALU_OP__XOR, `CS_SRCA_A0, `CS_SRCB_A0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM3: zero(2)*/
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM4:  */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM5:  */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM6:  */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM7:  */
	    8'hFF, 8'h00,  /*CFG9:  */
	    8'hFF, 8'hFF,  /*CFG11-10:  */
	    `SC_CMPB_A1_D1, `SC_CMPA_A1_D1, `SC_CI_B_ARITH,
	    `SC_CI_A_ARITH, `SC_C1_MASK_DSBL, `SC_C0_MASK_DSBL,
	    `SC_A_MASK_DSBL, `SC_DEF_SI_0, `SC_SI_B_DEFSI,
	    `SC_SI_A_DEFSI, /*CFG13-12:  */
	    `SC_A0_SRC_ACC, `SC_SHIFT_SL, 1'h0,
	    1'h0, `SC_FIFO1_BUS, `SC_FIFO0_BUS,
	    `SC_MSB_DSBL, `SC_MSB_BIT0, `SC_MSB_NOCHN,
	    `SC_FB_NOCHN, `SC_CMP1_NOCHN,
	    `SC_CMP0_NOCHN, /*CFG15-14:  */
	    10'h00, `SC_FIFO_CLK__DP,`SC_FIFO_CAP_AX,
	    `SC_FIFO_LEVEL,`SC_FIFO__SYNC,`SC_EXTCRC_DSBL,
	    `SC_WRK16CAT_DSBL /*CFG17-16:  */
	}
	)) dma_counter(
	        /*  input                   */  .reset(reset),
	        /*  input                   */  .clk(addrgen2_clock),
	        /*  input   [02:00]         */  .cs_addr(addrgen2_dp_addr[2:0]),
	        /*  input                   */  .route_si(1'b0),
	        /*  input                   */  .route_ci(1'b0),
	        /*  input                   */  .f0_load(1'b0),
	        /*  input                   */  .f1_load(1'b0),
	        /*  input                   */  .d0_load(1'b0),
	        /*  input                   */  .d1_load(1'b0),
	        /*  output                  */  .ce0(),
	        /*  output                  */  .cl0(addrgen2_comp_lt),
	        /*  output                  */  .z0(),
	        /*  output                  */  .ff0(),
	        /*  output                  */  .ce1(),
	        /*  output                  */  .cl1(),
	        /*  output                  */  .z1(),
	        /*  output                  */  .ff1(),
	        /*  output                  */  .ov_msb(),
	        /*  output                  */  .co_msb(),
	        /*  output                  */  .cmsb(),
	        /*  output                  */  .so(),
	        /*  output                  */  .f0_bus_stat(),
	        /*  output                  */  .f0_blk_stat(),
	        /*  output                  */  .f1_bus_stat(),
	        /*  output                  */  .f1_blk_stat(),
	        
	        /* input                    */  .ci(1'b0),     // Carry in from previous stage
	        /* output                   */  .co(),         // Carry out to next stage
	        /* input                    */  .sir(1'b0),    // Shift in from right side
	        /* output                   */  .sor(),        // Shift out to right side
	        /* input                    */  .sil(1'b0),    // Shift in from left side
	        /* output                   */  .sol(),        // Shift out to left side
	        /* input                    */  .msbi(1'b0),   // MSB chain in
	        /* output                   */  .msbo(),       // MSB chain out
	        /* input [01:00]            */  .cei(2'b0),    // Compare equal in from prev stage
	        /* output [01:00]           */  .ceo(),        // Compare equal out to next stage
	        /* input [01:00]            */  .cli(2'b0),    // Compare less than in from prv stage
	        /* output [01:00]           */  .clo(),        // Compare less than out to next stage
	        /* input [01:00]            */  .zi(2'b0),     // Zero detect in from previous stage
	        /* output [01:00]           */  .zo(),         // Zero detect out to next stage
	        /* input [01:00]            */  .fi(2'b0),     // 0xFF detect in from previous stage
	        /* output [01:00]           */  .fo(),         // 0xFF detect out to next stage
	        /* input [01:00]            */  .capi(2'b0),   // Software capture from previous stage
	        /* output [01:00]           */  .capo(),       // Software capture to next stage
	        /* input                    */  .cfbi(1'b0),   // CRC Feedback in from previous stage
	        /* output                   */  .cfbo(),       // CRC Feedback out to next stage
	        /* input [07:00]            */  .pi(8'b0),     // Parallel data port
	        /* output [07:00]           */  .po(addrgen2_count[7:0])          // Parallel data port
	);
	
	/*
	以下のロジックで、次のDMA転送に使うアドレスを生成する。
	for(i = 0; i < 96; i++) {
		addr_base = ((i | 0x0008)*16) + (uint16)disp_buf;
		addr_buf_lo_base[i] = addr_base & 0x00ff;
		addr_buf_hi[i] = (addr_base>>8) & 0x00ff;
	}
	for(i = 0; i < 96; i++) {
		addr_base = (((96-i) & 0xfff7)*16) + (uint16)disp_buf;
		addr_buf_lo_base[i+96] = addr_base & 0x00ff;
		addr_buf_hi[i+96] = (addr_base>>8) & 0x00ff;
	}
	*/
	reg [3:0] addrgen2_next_row;
	always @(posedge rowterm) begin
		addrgen2_next_row[3:0] <= ~next_row[3:0];
	end

	wire [7:0] count_temp_1 = addrgen2_count[7:0] | 8'b00001000;
	wire [7:0] count_temp_2 = {
		0,
		addrgen2_count[6],
		addrgen2_count[7] ^ (~addrgen2_count[6] & addrgen2_count[5]),
		~addrgen2_count[4],
		1'b0,
		~addrgen2_count[2:0]};
	function [7:0] next_addr_sel;
		input lt;
		input [7:0] count_temp_1;
		input [7:0] count_temp_2;
		begin
			if(lt == 1'b1) begin
				next_addr_sel[7:0] = count_temp_1[7:0];
			end else begin
				next_addr_sel[7:0] = count_temp_2[7:0];
			end
		end
	endfunction
	wire [7:0] count_sel = next_addr_sel(addrgen2_comp_lt, count_temp_1[7:0], count_temp_2[7:0]);
	wire adder_input_sel = 1'b0;
	wire [15:0] adder_pin = {4'b0000, count_sel[7:0], addrgen2_next_row[3:0]};
	wire [14:0] udb_conn;
/*	reg carry_reg;
	always @(posedge addrgen2_clock) begin
		carry_reg <= carry;
	end*/
	
	/*
	dma_reqはmatdrvから受け取ったreq信号をそのまま渡す。
	DMA transferが正常に終了するとdma_termシグナルがアサートされるので、
	これでcounterを+1する。
	*/
	reg dmareq_latch;
	always @(posedge addrgen2_clock) begin
		dmareq_latch <= req;
	end
	assign dmareq = req & ~dmareq_latch; //dmareq1;
	
	cy_psoc3_dp #(.cy_dpconfig(
	{
	    `CS_ALU_OP__ADD, `CS_SRCA_A0, `CS_SRCB_A1,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM0:     */
	    `CS_ALU_OP__ADD, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC___F1,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM1:     */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM2:     */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM3:     */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM4:     */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM5:     */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM6:     */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM7:     */
	    8'hFF, 8'h00,  /*CFG9:     */
	    8'hFF, 8'hFF,  /*CFG11-10:     */
	    `SC_CMPB_A1_D1, `SC_CMPA_A1_D1, `SC_CI_B_CHAIN,
	    `SC_CI_A_CHAIN, `SC_C1_MASK_DSBL, `SC_C0_MASK_DSBL,
	    `SC_A_MASK_DSBL, `SC_DEF_SI_0, `SC_SI_B_DEFSI,
	    `SC_SI_A_DEFSI, /*CFG13-12:     */
	    `SC_A0_SRC_PIN, `SC_SHIFT_SL, 1'h0,
	    1'h0, `SC_FIFO1_BUS, `SC_FIFO0_BUS,
	    `SC_MSB_DSBL, `SC_MSB_BIT0, `SC_MSB_NOCHN,
	    `SC_FB_NOCHN, `SC_CMP1_NOCHN,
	    `SC_CMP0_NOCHN, /*CFG15-14:     */
	    10'h00, `SC_FIFO_CLK__DP,`SC_FIFO_CAP_AX,
	    `SC_FIFO_LEVEL,`SC_FIFO__SYNC,`SC_EXTCRC_DSBL,
	    `SC_WRK16CAT_DSBL /*CFG17-16:     */
	}
	)) adder_hi(
	        /*  input                   */  .reset(1'b0),
	        /*  input                   */  .clk(addrgen2_clock),
	        /*  input   [02:00]         */  .cs_addr(3'b0),
	        /*  input                   */  .route_si(1'b0),
	        /*  input                   */  .route_ci(1'b0),
	        /*  input                   */  .f0_load(1'b0),
	        /*  input                   */  .f1_load(1'b0),
	        /*  input                   */  .d0_load(1'b0),
	        /*  input                   */  .d1_load(1'b0),
	        /*  output                  */  .ce0(),
	        /*  output                  */  .cl0(),
	        /*  output                  */  .z0(),
	        /*  output                  */  .ff0(),
	        /*  output                  */  .ce1(),
	        /*  output                  */  .cl1(),
	        /*  output                  */  .z1(),
	        /*  output                  */  .ff1(),
	        /*  output                  */  .ov_msb(),
	        /*  output                  */  .co_msb(),
	        /*  output                  */  .cmsb(),
	        /*  output                  */  .so(),
	        /*  output                  */  .f0_bus_stat(),
	        /*  output                  */  .f0_blk_stat(),
	        /*  output                  */  .f1_bus_stat(),
	        /*  output                  */  .f1_blk_stat(),
	        
	        /* input                    */  .ci(udb_conn[0]),     // Carry in from previous stage
	        /* output                   */  .co(),         // Carry out to next stage
	        /* input                    */  .sir(udb_conn[2]),    // Shift in from right side
	        /* output                   */  .sor(udb_conn[1]),        // Shift out to right side
	        /* input                    */  .sil(1'b0),    // Shift in from left side
	        /* output                   */  .sol(),        // Shift out to left side
	        /* input                    */  .msbi(1'b0),   // MSB chain in
	        /* output                   */  .msbo(udb_conn[3]),       // MSB chain out
	        /* input [01:00]            */  .cei(udb_conn[5:4]),    // Compare equal in from prev stage
	        /* output [01:00]           */  .ceo(),        // Compare equal out to next stage
	        /* input [01:00]            */  .cli(udb_conn[7:6]),    // Compare less than in from prv stage
	        /* output [01:00]           */  .clo(),        // Compare less than out to next stage
	        /* input [01:00]            */  .zi(udb_conn[9:8]),     // Zero detect in from previous stage
	        /* output [01:00]           */  .zo(),         // Zero detect out to next stage
	        /* input [01:00]            */  .fi(udb_conn[11:10]),     // 0xFF detect in from previous stage
	        /* output [01:00]           */  .fo(),         // 0xFF detect out to next stage
	        /* input [01:00]            */  .capi(udb_conn[13:12]),   // Software capture from previous stage
	        /* output [01:00]           */  .capo(),       // Software capture to next stage
	        /* input                    */  .cfbi(udb_conn[14]),   // CRC Feedback in from previous stage
	        /* output                   */  .cfbo(),       // CRC Feedback out to next stage
	        /* input [07:00]            */  .pi(adder_pin[15:8]),     // Parallel data port
	        /* output [07:00]           */  .po()          // Parallel data port
	);
	cy_psoc3_dp #(.cy_dpconfig(
	{
	    `CS_ALU_OP__ADD, `CS_SRCA_A0, `CS_SRCB_A1,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM0:    */
	    `CS_ALU_OP__ADD, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC___F1,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM1:    */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM2:    */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM3:    */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM4:    */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM5:    */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM6:    */
	    `CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
	    `CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
	    `CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
	    `CS_CMP_SEL_CFGA, /*CFGRAM7:    */
	    8'hFF, 8'h00,  /*CFG9:    */
	    8'hFF, 8'hFF,  /*CFG11-10:    */
	    `SC_CMPB_A1_D1, `SC_CMPA_A1_D1, `SC_CI_B_ARITH,
	    `SC_CI_A_ARITH, `SC_C1_MASK_DSBL, `SC_C0_MASK_DSBL,
	    `SC_A_MASK_DSBL, `SC_DEF_SI_0, `SC_SI_B_DEFSI,
	    `SC_SI_A_DEFSI, /*CFG13-12:    */
	    `SC_A0_SRC_PIN, `SC_SHIFT_SL, 1'h0,
	    1'h0, `SC_FIFO1_BUS, `SC_FIFO0_BUS,
	    `SC_MSB_DSBL, `SC_MSB_BIT0, `SC_MSB_NOCHN,
	    `SC_FB_NOCHN, `SC_CMP1_NOCHN,
	    `SC_CMP0_NOCHN, /*CFG15-14:    */
	    10'h00, `SC_FIFO_CLK__DP,`SC_FIFO_CAP_AX,
	    `SC_FIFO_LEVEL,`SC_FIFO__SYNC,`SC_EXTCRC_DSBL,
	    `SC_WRK16CAT_DSBL /*CFG17-16:    */
	}
	)) adder_lo(
	        /*  input                   */  .reset(1'b0),
	        /*  input                   */  .clk(addrgen2_clock),
	        /*  input   [02:00]         */  .cs_addr(3'b0),
	        /*  input                   */  .route_si(1'b0),
	        /*  input                   */  .route_ci(1'b0),
	        /*  input                   */  .f0_load(1'b0),
	        /*  input                   */  .f1_load(1'b0),
	        /*  input                   */  .d0_load(1'b0),
	        /*  input                   */  .d1_load(1'b0),
	        /*  output                  */  .ce0(),
	        /*  output                  */  .cl0(),
	        /*  output                  */  .z0(),
	        /*  output                  */  .ff0(),
	        /*  output                  */  .ce1(),
	        /*  output                  */  .cl1(),
	        /*  output                  */  .z1(),
	        /*  output                  */  .ff1(),
	        /*  output                  */  .ov_msb(),
	        /*  output                  */  .co_msb(),
	        /*  output                  */  .cmsb(),
	        /*  output                  */  .so(),
	        /*  output                  */  .f0_bus_stat(),
	        /*  output                  */  .f0_blk_stat(),
	        /*  output                  */  .f1_bus_stat(),
	        /*  output                  */  .f1_blk_stat(),
	        
	        /* input                    */  .ci(1'b0),     // Carry in from previous stage
	        /* output                   */  .co(udb_conn[0]),         // Carry out to next stage
	        /* input                    */  .sir(1'b0),    // Shift in from right side
	        /* output                   */  .sor(),        // Shift out to right side
	        /* input                    */  .sil(udb_conn[1]),    // Shift in from left side
	        /* output                   */  .sol(udb_conn[2]),        // Shift out to left side
	        /* input                    */  .msbi(udb_conn[3]),   // MSB chain in
	        /* output                   */  .msbo(),       // MSB chain out
	        /* input [01:00]            */  .cei(2'b0),    // Compare equal in from prev stage
	        /* output [01:00]           */  .ceo(udb_conn[5:4]),        // Compare equal out to next stage
	        /* input [01:00]            */  .cli(2'b0),    // Compare less than in from prv stage
	        /* output [01:00]           */  .clo(udb_conn[7:6]),        // Compare less than out to next stage
	        /* input [01:00]            */  .zi(2'b0),     // Zero detect in from previous stage
	        /* output [01:00]           */  .zo(udb_conn[9:8]),         // Zero detect out to next stage
	        /* input [01:00]            */  .fi(2'b0),     // 0xFF detect in from previous stage
	        /* output [01:00]           */  .fo(udb_conn[11:10]),         // 0xFF detect out to next stage
	        /* input [01:00]            */  .capi(2'b0),   // Software capture from previous stage
	        /* output [01:00]           */  .capo(udb_conn[13:12]),       // Software capture to next stage
	        /* input                    */  .cfbi(1'b0),   // CRC Feedback in from previous stage
	        /* output                   */  .cfbo(udb_conn[14]),       // CRC Feedback out to next stage
	        /* input [07:00]            */  .pi(adder_pin[7:0]),     // Parallel data port
	        /* output [07:00]           */  .po()          // Parallel data port
	);
endmodule

//`#end` -- edit above this line, do not edit this line

//`#start footer` -- edit after this line, do not edit this line
//`#end` -- edit above this line, do not edit this line


//[] END OF FILE






