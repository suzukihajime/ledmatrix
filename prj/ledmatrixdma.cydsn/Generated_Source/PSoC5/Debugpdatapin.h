/*******************************************************************************
* File Name: Debugpdatapin.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_Debugpdatapin_H) /* Pins Debugpdatapin_H */
#define CY_PINS_Debugpdatapin_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Debugpdatapin_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Debugpdatapin__PORT == 15 && (Debugpdatapin__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    Debugpdatapin_Write(uint8 value) ;
void    Debugpdatapin_SetDriveMode(uint8 mode) ;
uint8   Debugpdatapin_ReadDataReg(void) ;
uint8   Debugpdatapin_Read(void) ;
uint8   Debugpdatapin_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define Debugpdatapin_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define Debugpdatapin_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define Debugpdatapin_DM_RES_UP          PIN_DM_RES_UP
#define Debugpdatapin_DM_RES_DWN         PIN_DM_RES_DWN
#define Debugpdatapin_DM_OD_LO           PIN_DM_OD_LO
#define Debugpdatapin_DM_OD_HI           PIN_DM_OD_HI
#define Debugpdatapin_DM_STRONG          PIN_DM_STRONG
#define Debugpdatapin_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define Debugpdatapin_MASK               Debugpdatapin__MASK
#define Debugpdatapin_SHIFT              Debugpdatapin__SHIFT
#define Debugpdatapin_WIDTH              2u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Debugpdatapin_PS                     (* (reg8 *) Debugpdatapin__PS)
/* Data Register */
#define Debugpdatapin_DR                     (* (reg8 *) Debugpdatapin__DR)
/* Port Number */
#define Debugpdatapin_PRT_NUM                (* (reg8 *) Debugpdatapin__PRT) 
/* Connect to Analog Globals */                                                  
#define Debugpdatapin_AG                     (* (reg8 *) Debugpdatapin__AG)                       
/* Analog MUX bux enable */
#define Debugpdatapin_AMUX                   (* (reg8 *) Debugpdatapin__AMUX) 
/* Bidirectional Enable */                                                        
#define Debugpdatapin_BIE                    (* (reg8 *) Debugpdatapin__BIE)
/* Bit-mask for Aliased Register Access */
#define Debugpdatapin_BIT_MASK               (* (reg8 *) Debugpdatapin__BIT_MASK)
/* Bypass Enable */
#define Debugpdatapin_BYP                    (* (reg8 *) Debugpdatapin__BYP)
/* Port wide control signals */                                                   
#define Debugpdatapin_CTL                    (* (reg8 *) Debugpdatapin__CTL)
/* Drive Modes */
#define Debugpdatapin_DM0                    (* (reg8 *) Debugpdatapin__DM0) 
#define Debugpdatapin_DM1                    (* (reg8 *) Debugpdatapin__DM1)
#define Debugpdatapin_DM2                    (* (reg8 *) Debugpdatapin__DM2) 
/* Input Buffer Disable Override */
#define Debugpdatapin_INP_DIS                (* (reg8 *) Debugpdatapin__INP_DIS)
/* LCD Common or Segment Drive */
#define Debugpdatapin_LCD_COM_SEG            (* (reg8 *) Debugpdatapin__LCD_COM_SEG)
/* Enable Segment LCD */
#define Debugpdatapin_LCD_EN                 (* (reg8 *) Debugpdatapin__LCD_EN)
/* Slew Rate Control */
#define Debugpdatapin_SLW                    (* (reg8 *) Debugpdatapin__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Debugpdatapin_PRTDSI__CAPS_SEL       (* (reg8 *) Debugpdatapin__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Debugpdatapin_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Debugpdatapin__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Debugpdatapin_PRTDSI__OE_SEL0        (* (reg8 *) Debugpdatapin__PRTDSI__OE_SEL0) 
#define Debugpdatapin_PRTDSI__OE_SEL1        (* (reg8 *) Debugpdatapin__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Debugpdatapin_PRTDSI__OUT_SEL0       (* (reg8 *) Debugpdatapin__PRTDSI__OUT_SEL0) 
#define Debugpdatapin_PRTDSI__OUT_SEL1       (* (reg8 *) Debugpdatapin__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Debugpdatapin_PRTDSI__SYNC_OUT       (* (reg8 *) Debugpdatapin__PRTDSI__SYNC_OUT) 


#if defined(Debugpdatapin__INTSTAT)  /* Interrupt Registers */

    #define Debugpdatapin_INTSTAT                (* (reg8 *) Debugpdatapin__INTSTAT)
    #define Debugpdatapin_SNAP                   (* (reg8 *) Debugpdatapin__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins Debugpdatapin_H */

#endif
/* [] END OF FILE */
