/*******************************************************************************
* File Name: dmareq_out.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_dmareq_out_H) /* Pins dmareq_out_H */
#define CY_PINS_dmareq_out_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "dmareq_out_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 dmareq_out__PORT == 15 && (dmareq_out__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    dmareq_out_Write(uint8 value) ;
void    dmareq_out_SetDriveMode(uint8 mode) ;
uint8   dmareq_out_ReadDataReg(void) ;
uint8   dmareq_out_Read(void) ;
uint8   dmareq_out_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define dmareq_out_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define dmareq_out_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define dmareq_out_DM_RES_UP          PIN_DM_RES_UP
#define dmareq_out_DM_RES_DWN         PIN_DM_RES_DWN
#define dmareq_out_DM_OD_LO           PIN_DM_OD_LO
#define dmareq_out_DM_OD_HI           PIN_DM_OD_HI
#define dmareq_out_DM_STRONG          PIN_DM_STRONG
#define dmareq_out_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define dmareq_out_MASK               dmareq_out__MASK
#define dmareq_out_SHIFT              dmareq_out__SHIFT
#define dmareq_out_WIDTH              6u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define dmareq_out_PS                     (* (reg8 *) dmareq_out__PS)
/* Data Register */
#define dmareq_out_DR                     (* (reg8 *) dmareq_out__DR)
/* Port Number */
#define dmareq_out_PRT_NUM                (* (reg8 *) dmareq_out__PRT) 
/* Connect to Analog Globals */                                                  
#define dmareq_out_AG                     (* (reg8 *) dmareq_out__AG)                       
/* Analog MUX bux enable */
#define dmareq_out_AMUX                   (* (reg8 *) dmareq_out__AMUX) 
/* Bidirectional Enable */                                                        
#define dmareq_out_BIE                    (* (reg8 *) dmareq_out__BIE)
/* Bit-mask for Aliased Register Access */
#define dmareq_out_BIT_MASK               (* (reg8 *) dmareq_out__BIT_MASK)
/* Bypass Enable */
#define dmareq_out_BYP                    (* (reg8 *) dmareq_out__BYP)
/* Port wide control signals */                                                   
#define dmareq_out_CTL                    (* (reg8 *) dmareq_out__CTL)
/* Drive Modes */
#define dmareq_out_DM0                    (* (reg8 *) dmareq_out__DM0) 
#define dmareq_out_DM1                    (* (reg8 *) dmareq_out__DM1)
#define dmareq_out_DM2                    (* (reg8 *) dmareq_out__DM2) 
/* Input Buffer Disable Override */
#define dmareq_out_INP_DIS                (* (reg8 *) dmareq_out__INP_DIS)
/* LCD Common or Segment Drive */
#define dmareq_out_LCD_COM_SEG            (* (reg8 *) dmareq_out__LCD_COM_SEG)
/* Enable Segment LCD */
#define dmareq_out_LCD_EN                 (* (reg8 *) dmareq_out__LCD_EN)
/* Slew Rate Control */
#define dmareq_out_SLW                    (* (reg8 *) dmareq_out__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define dmareq_out_PRTDSI__CAPS_SEL       (* (reg8 *) dmareq_out__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define dmareq_out_PRTDSI__DBL_SYNC_IN    (* (reg8 *) dmareq_out__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define dmareq_out_PRTDSI__OE_SEL0        (* (reg8 *) dmareq_out__PRTDSI__OE_SEL0) 
#define dmareq_out_PRTDSI__OE_SEL1        (* (reg8 *) dmareq_out__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define dmareq_out_PRTDSI__OUT_SEL0       (* (reg8 *) dmareq_out__PRTDSI__OUT_SEL0) 
#define dmareq_out_PRTDSI__OUT_SEL1       (* (reg8 *) dmareq_out__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define dmareq_out_PRTDSI__SYNC_OUT       (* (reg8 *) dmareq_out__PRTDSI__SYNC_OUT) 


#if defined(dmareq_out__INTSTAT)  /* Interrupt Registers */

    #define dmareq_out_INTSTAT                (* (reg8 *) dmareq_out__INTSTAT)
    #define dmareq_out_SNAP                   (* (reg8 *) dmareq_out__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins dmareq_out_H */

#endif
/* [] END OF FILE */
