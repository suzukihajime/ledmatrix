/*******************************************************************************
* File Name: pdata.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_pdata_H) /* Pins pdata_H */
#define CY_PINS_pdata_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "pdata_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 pdata__PORT == 15 && (pdata__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    pdata_Write(uint8 value) ;
void    pdata_SetDriveMode(uint8 mode) ;
uint8   pdata_ReadDataReg(void) ;
uint8   pdata_Read(void) ;
uint8   pdata_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define pdata_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define pdata_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define pdata_DM_RES_UP          PIN_DM_RES_UP
#define pdata_DM_RES_DWN         PIN_DM_RES_DWN
#define pdata_DM_OD_LO           PIN_DM_OD_LO
#define pdata_DM_OD_HI           PIN_DM_OD_HI
#define pdata_DM_STRONG          PIN_DM_STRONG
#define pdata_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define pdata_MASK               pdata__MASK
#define pdata_SHIFT              pdata__SHIFT
#define pdata_WIDTH              8u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define pdata_PS                     (* (reg8 *) pdata__PS)
/* Data Register */
#define pdata_DR                     (* (reg8 *) pdata__DR)
/* Port Number */
#define pdata_PRT_NUM                (* (reg8 *) pdata__PRT) 
/* Connect to Analog Globals */                                                  
#define pdata_AG                     (* (reg8 *) pdata__AG)                       
/* Analog MUX bux enable */
#define pdata_AMUX                   (* (reg8 *) pdata__AMUX) 
/* Bidirectional Enable */                                                        
#define pdata_BIE                    (* (reg8 *) pdata__BIE)
/* Bit-mask for Aliased Register Access */
#define pdata_BIT_MASK               (* (reg8 *) pdata__BIT_MASK)
/* Bypass Enable */
#define pdata_BYP                    (* (reg8 *) pdata__BYP)
/* Port wide control signals */                                                   
#define pdata_CTL                    (* (reg8 *) pdata__CTL)
/* Drive Modes */
#define pdata_DM0                    (* (reg8 *) pdata__DM0) 
#define pdata_DM1                    (* (reg8 *) pdata__DM1)
#define pdata_DM2                    (* (reg8 *) pdata__DM2) 
/* Input Buffer Disable Override */
#define pdata_INP_DIS                (* (reg8 *) pdata__INP_DIS)
/* LCD Common or Segment Drive */
#define pdata_LCD_COM_SEG            (* (reg8 *) pdata__LCD_COM_SEG)
/* Enable Segment LCD */
#define pdata_LCD_EN                 (* (reg8 *) pdata__LCD_EN)
/* Slew Rate Control */
#define pdata_SLW                    (* (reg8 *) pdata__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define pdata_PRTDSI__CAPS_SEL       (* (reg8 *) pdata__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define pdata_PRTDSI__DBL_SYNC_IN    (* (reg8 *) pdata__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define pdata_PRTDSI__OE_SEL0        (* (reg8 *) pdata__PRTDSI__OE_SEL0) 
#define pdata_PRTDSI__OE_SEL1        (* (reg8 *) pdata__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define pdata_PRTDSI__OUT_SEL0       (* (reg8 *) pdata__PRTDSI__OUT_SEL0) 
#define pdata_PRTDSI__OUT_SEL1       (* (reg8 *) pdata__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define pdata_PRTDSI__SYNC_OUT       (* (reg8 *) pdata__PRTDSI__SYNC_OUT) 


#if defined(pdata__INTSTAT)  /* Interrupt Registers */

    #define pdata_INTSTAT                (* (reg8 *) pdata__INTSTAT)
    #define pdata_SNAP                   (* (reg8 *) pdata__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins pdata_H */

#endif
/* [] END OF FILE */
