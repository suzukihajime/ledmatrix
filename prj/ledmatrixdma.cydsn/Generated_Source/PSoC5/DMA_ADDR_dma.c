/***************************************************************************
* File Name: DMA_ADDR_dma.c  
* Version 1.60
*
*  Description:
*   Provides an API for the DMAC component. The API includes functions
*   for the DMA controller, DMA channels and Transfer Descriptors.
*
*
*   Note:
*     This module requires the developer to finish or fill in the auto
*     generated funcions and setup the dma channel and TD's.
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/
#include <CYLIB.H>
#include <CYDMAC.H>
#include <DMA_ADDR_dma.H>



/****************************************************************************
* 
* The following defines are available in Cyfitter.h
* 
* 
* 
* DMA_ADDR__DRQ_CTL_REG
* 
* 
* DMA_ADDR__DRQ_NUMBER
* 
* Number of TD's used by this channel.
* DMA_ADDR__NUMBEROF_TDS
* 
* Priority of this channel.
* DMA_ADDR__PRIORITY
* 
* True if DMA_ADDR_TERMIN_SEL is used.
* DMA_ADDR__TERMIN_EN
* 
* TERMIN interrupt line to signal terminate.
* DMA_ADDR__TERMIN_SEL
* 
* 
* True if DMA_ADDR_TERMOUT0_SEL is used.
* DMA_ADDR__TERMOUT0_EN
* 
* 
* TERMOUT0 interrupt line to signal completion.
* DMA_ADDR__TERMOUT0_SEL
* 
* 
* True if DMA_ADDR_TERMOUT1_SEL is used.
* DMA_ADDR__TERMOUT1_EN
* 
* 
* TERMOUT1 interrupt line to signal completion.
* DMA_ADDR__TERMOUT1_SEL
* 
****************************************************************************/


/* Zero based index of DMA_ADDR dma channel */
uint8 DMA_ADDR_DmaHandle = DMA_INVALID_CHANNEL;

/*********************************************************************
* Function Name: uint8 DMA_ADDR_DmaInitalize
**********************************************************************
* Summary:
*   Allocates and initialises a channel of the DMAC to be used by the
*   caller.
*
* Parameters:
*   BurstCount.
*       
*       
*   ReqestPerBurst.
*       
*       
*   UpperSrcAddress.
*       
*       
*   UpperDestAddress.
*       
*
* Return:
*   The channel that can be used by the caller for DMA activity.
*   DMA_INVALID_CHANNEL (0xFF) if there are no channels left. 
*
*
*******************************************************************/
uint8 DMA_ADDR_DmaInitialize(uint8 BurstCount, uint8 ReqestPerBurst, uint16 UpperSrcAddress, uint16 UpperDestAddress) 
{

    /* Allocate a DMA channel. */
    DMA_ADDR_DmaHandle = DMA_ADDR__DRQ_NUMBER;

    if(DMA_ADDR_DmaHandle != DMA_INVALID_CHANNEL)
    {
        /* Configure the channel. */
        CyDmaChSetConfiguration(DMA_ADDR_DmaHandle,
                                BurstCount,
                                ReqestPerBurst,
                                DMA_ADDR__TERMOUT0_SEL,
                                DMA_ADDR__TERMOUT1_SEL,
                                DMA_ADDR__TERMIN_SEL);

        /* Set the extended address for the transfers */
        CyDmaChSetExtendedAddress(DMA_ADDR_DmaHandle, UpperSrcAddress, UpperDestAddress);

        /* Set the priority for this channel */
        CyDmaChPriority(DMA_ADDR_DmaHandle, DMA_ADDR__PRIORITY);
    }

    return DMA_ADDR_DmaHandle;
}

/*********************************************************************
* Function Name: void DMA_ADDR_DmaRelease
**********************************************************************
* Summary:
*   Frees the channel associated with DMA_ADDR.
*
*
* Parameters:
*   void.
*
*
*
* Return:
*   void.
*
*******************************************************************/
void DMA_ADDR_DmaRelease(void) 
{
    /* Disable the channel, even if someone just did! */
    CyDmaChDisable(DMA_ADDR_DmaHandle);


    /* Free Transfer Descriptors. */


}

