/*******************************************************************************
* File Name: dma_chain.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_dma_chain_H) /* Pins dma_chain_H */
#define CY_PINS_dma_chain_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "dma_chain_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 dma_chain__PORT == 15 && (dma_chain__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    dma_chain_Write(uint8 value) ;
void    dma_chain_SetDriveMode(uint8 mode) ;
uint8   dma_chain_ReadDataReg(void) ;
uint8   dma_chain_Read(void) ;
uint8   dma_chain_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define dma_chain_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define dma_chain_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define dma_chain_DM_RES_UP          PIN_DM_RES_UP
#define dma_chain_DM_RES_DWN         PIN_DM_RES_DWN
#define dma_chain_DM_OD_LO           PIN_DM_OD_LO
#define dma_chain_DM_OD_HI           PIN_DM_OD_HI
#define dma_chain_DM_STRONG          PIN_DM_STRONG
#define dma_chain_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define dma_chain_MASK               dma_chain__MASK
#define dma_chain_SHIFT              dma_chain__SHIFT
#define dma_chain_WIDTH              3u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define dma_chain_PS                     (* (reg8 *) dma_chain__PS)
/* Data Register */
#define dma_chain_DR                     (* (reg8 *) dma_chain__DR)
/* Port Number */
#define dma_chain_PRT_NUM                (* (reg8 *) dma_chain__PRT) 
/* Connect to Analog Globals */                                                  
#define dma_chain_AG                     (* (reg8 *) dma_chain__AG)                       
/* Analog MUX bux enable */
#define dma_chain_AMUX                   (* (reg8 *) dma_chain__AMUX) 
/* Bidirectional Enable */                                                        
#define dma_chain_BIE                    (* (reg8 *) dma_chain__BIE)
/* Bit-mask for Aliased Register Access */
#define dma_chain_BIT_MASK               (* (reg8 *) dma_chain__BIT_MASK)
/* Bypass Enable */
#define dma_chain_BYP                    (* (reg8 *) dma_chain__BYP)
/* Port wide control signals */                                                   
#define dma_chain_CTL                    (* (reg8 *) dma_chain__CTL)
/* Drive Modes */
#define dma_chain_DM0                    (* (reg8 *) dma_chain__DM0) 
#define dma_chain_DM1                    (* (reg8 *) dma_chain__DM1)
#define dma_chain_DM2                    (* (reg8 *) dma_chain__DM2) 
/* Input Buffer Disable Override */
#define dma_chain_INP_DIS                (* (reg8 *) dma_chain__INP_DIS)
/* LCD Common or Segment Drive */
#define dma_chain_LCD_COM_SEG            (* (reg8 *) dma_chain__LCD_COM_SEG)
/* Enable Segment LCD */
#define dma_chain_LCD_EN                 (* (reg8 *) dma_chain__LCD_EN)
/* Slew Rate Control */
#define dma_chain_SLW                    (* (reg8 *) dma_chain__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define dma_chain_PRTDSI__CAPS_SEL       (* (reg8 *) dma_chain__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define dma_chain_PRTDSI__DBL_SYNC_IN    (* (reg8 *) dma_chain__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define dma_chain_PRTDSI__OE_SEL0        (* (reg8 *) dma_chain__PRTDSI__OE_SEL0) 
#define dma_chain_PRTDSI__OE_SEL1        (* (reg8 *) dma_chain__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define dma_chain_PRTDSI__OUT_SEL0       (* (reg8 *) dma_chain__PRTDSI__OUT_SEL0) 
#define dma_chain_PRTDSI__OUT_SEL1       (* (reg8 *) dma_chain__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define dma_chain_PRTDSI__SYNC_OUT       (* (reg8 *) dma_chain__PRTDSI__SYNC_OUT) 


#if defined(dma_chain__INTSTAT)  /* Interrupt Registers */

    #define dma_chain_INTSTAT                (* (reg8 *) dma_chain__INTSTAT)
    #define dma_chain_SNAP                   (* (reg8 *) dma_chain__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins dma_chain_H */

#endif
/* [] END OF FILE */
