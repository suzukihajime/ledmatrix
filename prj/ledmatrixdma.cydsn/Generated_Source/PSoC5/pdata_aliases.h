/*******************************************************************************
* File Name: pdata.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_pdata_ALIASES_H) /* Pins pdata_ALIASES_H */
#define CY_PINS_pdata_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"

/***************************************
*              Constants        
***************************************/
#define pdata_0		pdata__0__PC
#define pdata_1		pdata__1__PC
#define pdata_2		pdata__2__PC
#define pdata_3		pdata__3__PC
#define pdata_4		pdata__4__PC
#define pdata_5		pdata__5__PC
#define pdata_6		pdata__6__PC
#define pdata_7		pdata__7__PC

#endif /* End Pins pdata_ALIASES_H */

/* [] END OF FILE */
