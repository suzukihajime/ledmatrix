/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "CyLib.h"
#include "matdrv3_1_matdrv3.h"


void matdrv3_1_Start(void)
{
	/* nothing to do */
	uint8 enableInterrupts = 0u;    
	enableInterrupts = CyEnterCriticalSection();
	matdrv3_1_ROWCOUNTER_CONTROL_REG |= matdrv3_1_CNTR_ENABLE;
	matdrv3_1_GRADCOUNTER_CONTROL_REG |= matdrv3_1_CNTR_ENABLE;
	matdrv3_1_COUNTER_LO_CONTROL_REG |= matdrv3_1_CNTR_ENABLE;
	matdrv3_1_COUNTER_HI_CONTROL_REG |= matdrv3_1_CNTR_ENABLE;
	CyExitCriticalSection(enableInterrupts);
	return;
}

void matdrv3_1_Stop(void)
{
	/* nothing to do */
	return;
}

/* [] END OF FILE */
