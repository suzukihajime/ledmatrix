/*******************************************************************************
* File Name: Debugpdatapin.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "Debugpdatapin.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Debugpdatapin__PORT == 15 && (Debugpdatapin__MASK & 0xC0))

/*******************************************************************************
* Function Name: Debugpdatapin_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void Debugpdatapin_Write(uint8 value) 
{
    uint8 staticBits = Debugpdatapin_DR & ~Debugpdatapin_MASK;
    Debugpdatapin_DR = staticBits | ((value << Debugpdatapin_SHIFT) & Debugpdatapin_MASK);
}


/*******************************************************************************
* Function Name: Debugpdatapin_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void Debugpdatapin_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(Debugpdatapin_0, mode);
	CyPins_SetPinDriveMode(Debugpdatapin_1, mode);
}


/*******************************************************************************
* Function Name: Debugpdatapin_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro Debugpdatapin_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 Debugpdatapin_Read(void) 
{
    return (Debugpdatapin_PS & Debugpdatapin_MASK) >> Debugpdatapin_SHIFT;
}


/*******************************************************************************
* Function Name: Debugpdatapin_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 Debugpdatapin_ReadDataReg(void) 
{
    return (Debugpdatapin_DR & Debugpdatapin_MASK) >> Debugpdatapin_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(Debugpdatapin_INTSTAT) 

    /*******************************************************************************
    * Function Name: Debugpdatapin_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 Debugpdatapin_ClearInterrupt(void) 
    {
        return (Debugpdatapin_INTSTAT & Debugpdatapin_MASK) >> Debugpdatapin_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
