/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "cytypes.h"
#include "cyfitter.h"

//#define addrgen2_1_NEXT_ADDR_HI_REG_PTR    (  (reg8 *) addrgen2_1_adder_hi__F1_REG )
//#define addrgen2_1_NEXT_ADDR_LO_REG_PTR    (  (reg8 *) addrgen2_1_adder_lo__F1_REG )
//#define addrgen2_1_NEXT_ADDR_HI_REG        *(  (reg8 *) addrgen2_1_adder_hi__F1_REG )
//#define addrgen2_1_NEXT_ADDR_LO_REG        *(  (reg8 *) addrgen2_1_adder_lo__F1_REG )
#define addrgen2_1_ADDR_BASE_HI_REG_PTR    (  (reg8 *) addrgen2_1_adder_hi__A1_REG )
#define addrgen2_1_ADDR_BASE_LO_REG_PTR    (  (reg8 *) addrgen2_1_adder_lo__A1_REG )
#define addrgen2_1_ADDR_BASE_HI_REG        *(  (reg8 *) addrgen2_1_adder_hi__A1_REG )
#define addrgen2_1_ADDR_BASE_LO_REG        *(  (reg8 *) addrgen2_1_adder_lo__A1_REG )
#define addrgen2_1_ADDR_BASE_16BIT_REG_PTR	(  (reg8 *) addrgen2_1_adder_lo__16BIT_A1_REG
#define addrgen2_1_ADDR_BASE_16BIT_REG	*(  (reg8 *) addrgen2_1_adder_lo__16BIT_A1_REG

#define	addrgen2_1_NEXT_ADDR_LO_REG_PTR	addrgen2_1_adder_lo__A0_REG
#define	addrgen2_1_NEXT_ADDR_HI_REG_PTR	addrgen2_1_adder_hi__A0_REG
#define	addrgen2_1_NEXT_ADDR_LO_REG		*((uint8 *)addrgen2_1_adder_lo__A0_REG)
#define	addrgen2_1_NEXT_ADDR_HI_REG		*((uint8 *)addrgen2_1_adder_hi__A0_REG)
#define addrgen2_1_NEXT_ADDR_16BIT_REG_PTR	(  (reg8 *) addrgen2_1_adder_lo__16BIT_A0_REG)
#define addrgen2_1_NEXT_ADDR_16BIT_REG	*(  (reg8 *) addrgen2_1_adder_lo__16BIT_A0_REG)

#define	addrgen2_1_COMP_REG_PTR	addrgen2_1_dma_counter__D0_REG
#define	addrgen2_1_COMP_REG		*((uint8 *)addrgen2_1_dma_counter__D0_REG)


#if 0
#define	addrgen2_1_NEXT_ADDR_LO_REG_PTR	addrgen2_1_StatusReg_lo__STATUS_REG
#define	addrgen2_1_NEXT_ADDR_HI_REG_PTR	addrgen2_1_StatusReg_hi__STATUS_REG
#define	addrgen2_1_NEXT_ADDR_LO_REG		*((uint8 *)addrgen2_1_StatusReg_lo__STATUS_REG)
#define	addrgen2_1_NEXT_ADDR_HI_REG		*((uint8 *)addrgen2_1_StatusReg_hi__STATUS_REG)
#endif

#if 0
//#define	addrgen2_1_ADDR_BASE_0_REG_PTR	addrgen2_1_ControlReg0__CONTROL_REG
#define	addrgen2_1_ADDR_BASE_1_REG_PTR	addrgen2_1_ControlReg1__CONTROL_REG
#define	addrgen2_1_ADDR_BASE_2_REG_PTR	addrgen2_1_ControlReg2__CONTROL_REG
#define	addrgen2_1_ADDR_BASE_3_REG_PTR	addrgen2_1_ControlReg3__CONTROL_REG
#define	addrgen2_1_ADDR_BASE_0_REG		*((uint8 *)addrgen2_1_ControlReg0__CONTROL_REG)
#define	addrgen2_1_ADDR_BASE_1_REG		*((uint8 *)addrgen2_1_ControlReg1__CONTROL_REG)
#define	addrgen2_1_ADDR_BASE_2_REG		*((uint8 *)addrgen2_1_ControlReg2__CONTROL_REG)
#define	addrgen2_1_ADDR_BASE_3_REG		*((uint8 *)addrgen2_1_ControlReg3__CONTROL_REG)
#endif
void addrgen2_1_Start(void);
void addrgen2_1_Stop(void);
void addrgen2_1_SetBaseAddress(uint16 addr);
uint16 addrgen2_1_GetBaseAddress(void);

//[] END OF FILE
