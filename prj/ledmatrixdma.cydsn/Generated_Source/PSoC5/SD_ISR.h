/*******************************************************************************
* File Name: SD_ISR.h
* Version 1.60
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/
#if !defined(__SD_ISR_INTC_H__)
#define __SD_ISR_INTC_H__


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void SD_ISR_Start(void);
void SD_ISR_StartEx(cyisraddress address);
void SD_ISR_Stop(void) ;

CY_ISR_PROTO(SD_ISR_Interrupt);

void SD_ISR_SetVector(cyisraddress address) ;
cyisraddress SD_ISR_GetVector(void) ;

void SD_ISR_SetPriority(uint8 priority) ;
uint8 SD_ISR_GetPriority(void) ;

void SD_ISR_Enable(void) ;
uint8 SD_ISR_GetState(void) ;
void SD_ISR_Disable(void) ;

void SD_ISR_SetPending(void) ;
void SD_ISR_ClearPending(void) ;


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the SD_ISR ISR. */
#define SD_ISR_INTC_VECTOR            ((reg32 *) SD_ISR__INTC_VECT)

/* Address of the SD_ISR ISR priority. */
#define SD_ISR_INTC_PRIOR             ((reg8 *) SD_ISR__INTC_PRIOR_REG)

/* Priority of the SD_ISR interrupt. */
#define SD_ISR_INTC_PRIOR_NUMBER      SD_ISR__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable SD_ISR interrupt. */
#define SD_ISR_INTC_SET_EN            ((reg32 *) SD_ISR__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the SD_ISR interrupt. */
#define SD_ISR_INTC_CLR_EN            ((reg32 *) SD_ISR__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the SD_ISR interrupt state to pending. */
#define SD_ISR_INTC_SET_PD            ((reg32 *) SD_ISR__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the SD_ISR interrupt. */
#define SD_ISR_INTC_CLR_PD            ((reg32 *) SD_ISR__INTC_CLR_PD_REG)



/* __SD_ISR_INTC_H__ */
#endif
