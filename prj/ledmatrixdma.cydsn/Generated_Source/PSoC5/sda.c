/*******************************************************************************
* File Name: sda.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "sda.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 sda__PORT == 15 && (sda__MASK & 0xC0))

/*******************************************************************************
* Function Name: sda_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void sda_Write(uint8 value) 
{
    uint8 staticBits = sda_DR & ~sda_MASK;
    sda_DR = staticBits | ((value << sda_SHIFT) & sda_MASK);
}


/*******************************************************************************
* Function Name: sda_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void sda_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(sda_0, mode);
}


/*******************************************************************************
* Function Name: sda_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro sda_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 sda_Read(void) 
{
    return (sda_PS & sda_MASK) >> sda_SHIFT;
}


/*******************************************************************************
* Function Name: sda_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 sda_ReadDataReg(void) 
{
    return (sda_DR & sda_MASK) >> sda_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(sda_INTSTAT) 

    /*******************************************************************************
    * Function Name: sda_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 sda_ClearInterrupt(void) 
    {
        return (sda_INTSTAT & sda_MASK) >> sda_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
