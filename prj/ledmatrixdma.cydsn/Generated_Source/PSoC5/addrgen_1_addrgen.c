/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "addrgen_1_addrgen.h"

void addrgen_1_Start(void)
{
	return;
}

void addrgen_1_Stop(void)
{
	return;
}

void addrgen_1_SetBaseAddress(uint16 addr)
{
//	addrgen_1_ADDR_BASE_0_REG = addr;
	addrgen_1_ADDR_BASE_1_REG = addr>>4;
	addrgen_1_ADDR_BASE_2_REG = addr>>8;
	addrgen_1_ADDR_BASE_3_REG = addr>>12;
	return;
}

uint16 addrgen_1_GetBaseAddress(void)
{
	uint16 addr;
	addr = addrgen_1_ADDR_BASE_3_REG;
	addr <<= 4;
	addr |= (addrgen_1_ADDR_BASE_2_REG & 0x0f);
	addr <<= 4;
	addr |= (addrgen_1_ADDR_BASE_1_REG & 0x0f);
	addr <<= 4;
//	addr |= (addrgen_1_ADDR_BASE_0_REG & 0x0f);
	return(addr);
}

/* [] END OF FILE */
