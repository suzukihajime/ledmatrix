//`#start header` -- edit after this line, do not edit this line
// ========================================
//
// Copyright YOUR COMPANY, THE YEAR
// All Rights Reserved
// UNPUBLISHED, LICENSED SOFTWARE.
//
// CONFIDENTIAL AND PROPRIETARY INFORMATION
// WHICH IS THE PROPERTY OF your company.
//
// ========================================
`include "cypress.v"
//`#end` -- edit above this line, do not edit this line

//`#start body` -- edit after this line, do not edit this line

//        Your code goes here

module addrgen (
	input [3:0] next_row,
	input req,
	input rowterm,
	input clock,
	input reset,
	output dmareq,
	input dmaterm1,
	input dmaterm2
);
	/*
	udb_clock_enableをインスタンス化する
	*/
	wire addrgen_clock;
	cy_psoc3_udb_clock_enable_v1_0 #(.sync_mode(`TRUE)) ClkSync
    (
        /* input  */    .clock_in(clock),
        /* input  */    .enable(1'b1),
        /* output */    .clock_out(addrgen_clock)
    );
	
	/*
	control registerをインスタンス化(sync mode)
	addr_base_hiとaddr_base_loの2バイト
	*/
	wire addrgen_page_clock = rowterm;
	wire [7:0] addr_base0;
	wire [7:0] addr_base1;
	wire [7:0] addr_base2;
	wire [7:0] addr_base3;
	reg [3:0] addrgen_addr_base0;
	reg [3:0] addrgen_addr_base1;
	reg [3:0] addrgen_addr_base2;
	reg [3:0] addrgen_addr_base3;
	cy_psoc3_control #(.cy_force_order(1)) ControlReg0
	(
		/* output [07:00] */  .control(addr_base0)
	);
	cy_psoc3_control #(.cy_force_order(1)) ControlReg1
	(
		/* output [07:00] */  .control(addr_base1)
	);
	cy_psoc3_control #(.cy_force_order(1)) ControlReg2
	(
		/* output [07:00] */  .control(addr_base2)
	);
	cy_psoc3_control #(.cy_force_order(1)) ControlReg3
	(
		/* output [07:00] */  .control(addr_base3)
	);
	always @(posedge rowterm) begin
		if(rowterm == 1'b1) begin
			addrgen_addr_base0[3:0] <= addr_base0[3:0];
			addrgen_addr_base1[3:0] <= addr_base1[3:0];
			addrgen_addr_base2[3:0] <= addr_base2[3:0];
			addrgen_addr_base3[3:0] <= addr_base3[3:0];
		end else begin
			addrgen_addr_base0[3:0] <= addrgen_addr_base0[3:0];
			addrgen_addr_base1[3:0] <= addrgen_addr_base1[3:0];
			addrgen_addr_base2[3:0] <= addrgen_addr_base2[3:0];
			addrgen_addr_base3[3:0] <= addrgen_addr_base3[3:0];
		end
	end
	
	/*
	転送数をカウントする。この値を元に次のアドレスを決定。
	転送終了後にカウントしたいので、posedge dma_termとしてある。
	*/
	reg [7:0] addrgen_count;
	always @(posedge dmaterm2 or posedge rowterm) begin
		if(rowterm == 1'b1) begin
			addrgen_count[7:0] <= 8'b00000000;
		end else begin
			if(dmaterm2 == 1'b1) begin
				addrgen_count[7:0] <= addrgen_count[7:0] + 8'b00000001;
			end else begin
				addrgen_count[7:0] <= addrgen_count[7:0];
			end
		end
	end
	
	/*
	以下のロジックで、次のDMA転送に使うアドレスを生成する。
	for(i = 0; i < 96; i++) {
		addr_base = ((i | 0x0008)*16) + (uint16)disp_buf;
		addr_buf_lo_base[i] = addr_base & 0x00ff;
		addr_buf_hi[i] = (addr_base>>8) & 0x00ff;
	}
	for(i = 0; i < 96; i++) {
		addr_base = (((96-i) & 0xfff7)*16) + (uint16)disp_buf;
		addr_buf_lo_base[i+96] = addr_base & 0x00ff;
		addr_buf_hi[i+96] = (addr_base>>8) & 0x00ff;
	}
	*/
	reg [3:0] addrgen_next_row;
	always @(posedge rowterm) begin
		if(rowterm == 1'b1) begin
			addrgen_next_row[3:0] <= ~next_row[3:0];
		end else begin
			addrgen_next_row[3:0] <= addrgen_next_row[3:0];
		end
	end

	wire [7:0] count_temp_1 = addrgen_count[7:0] | 8'b00001000;
//	wire [7:0] count_temp_2 = (8'd96 - addrgen_count[7:0]) & 8'b11110111;
	wire [7:0] count_temp_2 = {
		0,
		addrgen_count[6],
		addrgen_count[7] ^ (~addrgen_count[6] & addrgen_count[5]),
		~addrgen_count[4],
		1'b0,
		~addrgen_count[2:0]};
	function [3:0] next_addr_sel;
		input [7:0] count;
		input [3:0] count_temp_1;
		input [3:0] count_temp_2;
		begin
			if(count[7:0] < 8'd96) begin
				next_addr_sel[3:0] = count_temp_1[3:0];
//				next_addr_gen[15:0] = {4'b0000, (count[7:0] | 8'b00001000), next_row} + addr_base[15:0];
			end else begin
				next_addr_sel[3:0] = count_temp_2[3:0];
//				next_addr_gen[15:0] = {4'b0000, ((count[7:0]) & 8'b11110111), next_row} + addr_base[15:0];
			end
		end
	endfunction
	wire [3:0] count_sel1 = next_addr_sel(addrgen_count[7:0], count_temp_1[3:0], count_temp_2[3:0]);
	wire [3:0] count_sel2 = next_addr_sel(addrgen_count[7:0], count_temp_1[7:4], count_temp_2[7:4]);
	wire [7:0] next_addr_lo;
	wire [7:0] next_addr_hi;
	wire [3:0] next_addr_temp1;
	wire carry_1;
	reg reg_carry_1;
	wire [3:0] next_addr_temp2_c1;
	wire [3:0] next_addr_temp2_nc1;
	wire carry_2_c1;
	wire carry_2_nc1;
	wire [3:0] next_addr_temp3_c1;
	wire [3:0] next_addr_temp3_nc1;

	function [4:0] carry_sel4;
		input [4:0] a;
		input [4:0] b;
		input carry;
		begin
			if(carry == 1'b1) begin
				carry_sel4[3:0] = a[3:0];
			end else begin
				carry_sel4[3:0] = b[3:0];
			end
		end
	endfunction
	
	function [7:0] carry_sel8;
		input [7:0] a;
		input [7:0] b;
		input carry;
		begin
			if(carry == 1'b1) begin
				carry_sel8[7:0] = a[7:0];
			end else begin
				carry_sel8[7:0] = b[7:0];
			end
		end
	endfunction
	
	assign {carry_1, next_addr_temp1[3:0]} = {1'b0, count_sel1[3:0]} + {1'b0, addrgen_addr_base1[3:0]}; // + {4'b0000, next_addr_temp0[4]};
/*	assign {carry_2, next_addr_temp2[3:0]} = carry_sel4(
		{1'b0, count_sel2[3:0]} + {1'b0, addrgen_addr_base2[3:0]} + 5'b00001,
		{1'b0, count_sel2[3:0]} + {1'b0, addrgen_addr_base2[3:0]},
		carry_1);*/
	assign {carry_2_c1, next_addr_temp2_c1[3:0]} = {1'b0, count_sel2[3:0]} + {1'b0, addrgen_addr_base2[3:0]} + 5'b00001;
	assign {carry_2_nc1, next_addr_temp2_nc1[3:0]} = {1'b0, count_sel2[3:0]} + {1'b0, addrgen_addr_base2[3:0]};
	assign next_addr_temp3_c1[3:0] = carry_sel4(
		{1'b0, (addrgen_addr_base3[3:0] + 5'b00001)},
		{1'b0, addrgen_addr_base3[3:0]},
		carry_2_c1);
	assign next_addr_temp3_nc1[3:0] = carry_sel4(
		{1'b0, (addrgen_addr_base3[3:0] + 5'b00001)},
		{1'b0, addrgen_addr_base3[3:0]},
		carry_2_nc1);
	
	/*
	dma_reqはmatdrvから受け取ったreq信号をそのまま渡す。
	DMA transferが正常に終了するとdma_termシグナルがアサートされるので、
	これでcounterを+1する。
	*/

//	reg dmareq1;
	assign dmareq = req; //dmareq1;
/*	always @(posedge addrgen_clock) begin
		if(req == 1'b1) begin
			next_addr_lo[7:0] <= {next_addr_temp1[3:0], addrgen_next_row[3:0]};
			reg_carry_1 <= carry_1;
			dmareq1 <= req;
//		end else begin
//			next_addr_lo[7:0] <= next_addr_lo[7:0];
//			reg_carry_1 <= reg_carry_1;
//			dmareq1 <= dmareq1;
		end
	end
	always @(posedge addrgen_clock/* or posedge dmaterm1* /) begin
		if(dmaterm1 == 1'b1) begin
			next_addr_hi[7:0] <= carry_sel8(
				{next_addr_temp3_c1[3:0], next_addr_temp2_c1[3:0]},
				{next_addr_temp3_nc1[3:0], next_addr_temp2_nc1[3:0]},
				reg_carry_1);
//		end else begin
//			next_addr_hi[7:0] <= next_addr_hi[7:0];
		end
	end*/
	assign next_addr_lo[7:0] = {next_addr_temp1[3:0], addrgen_next_row[3:0]};
	assign next_addr_hi = carry_sel8(
		{next_addr_temp3_c1[3:0], next_addr_temp2_c1[3:0]},
		{next_addr_temp3_nc1[3:0], next_addr_temp2_nc1[3:0]},
		carry_1);

	/*
	status registerのインスタンス化
	DMA転送用に、計算したアドレスを出力するレジスタとして使う。
	*/
	cy_psoc3_status #(.cy_force_order(1)) StatusReg_lo
    (
        /* input          */  .clock(1'b0),
		/* input          */  .reset(1'b0),
        /* input  [07:00] */  .status(next_addr_lo[7:0])
    );
	cy_psoc3_status #(.cy_force_order(1)) StatusReg_hi
    (
        /* input          */  .clock(1'b0),
		/* input          */  .reset(1'b0),
        /* input  [07:00] */  .status(next_addr_hi[7:0])
    );
endmodule

//`#end` -- edit above this line, do not edit this line

//`#start footer` -- edit after this line, do not edit this line
//`#end` -- edit above this line, do not edit this line


//[] END OF FILE
