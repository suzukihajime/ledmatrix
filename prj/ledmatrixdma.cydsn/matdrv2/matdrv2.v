
//`#start header` -- edit after this line, do not edit this line
// ========================================
//
// Copyright YOUR COMPANY, THE YEAR
// All Rights Reserved
// UNPUBLISHED, LICENSED SOFTWARE.
//
// CONFIDENTIAL AND PROPRIETARY INFORMATION
// WHICH IS THE PROPERTY OF your company.
//
// ========================================
`include "cypress.v"
//`#end` -- edit above this line, do not edit this line
// Generated on 10/20/2012 at 23:10
// Component: dmarcv
module matdrv2 (
	output	sda,
	output	sck,
	output	rck,
	output [3:0] row,
	output [7:0] pdata,
	output [1:0] stat,
	output [7:0] dmareq,
	output  dmares,
	output f0_nfull,
	output f0_empty,
	input   dmasel,
	input   clock,
	input   reset
);

//`#start body` -- edit after this line, do not edit this line

//	localparam [5:0] PERIOD = 8'd192;
	wire dmarcv_clock;
	cy_psoc3_udb_clock_enable_v1_0 #(.sync_mode(`TRUE)) ClkSync
    (
        /* input  */    .clock_in(clock),
        /* input  */    .enable(1'b1),
        /* output */    .clock_out(dmarcv_clock)
    );

	wire [8:0] dmarcv_count;
	wire dmarcv_sda;
	wire dmarcv_dp0_f0_nfull;
	wire dmarcv_dp0_f1_nfull;
	wire dmarcv_dp0_f0_empty;
	wire dmarcv_dp0_f1_empty;
	wire [7:0] dmarcv_dp0_pdata;
	wire [2:0] dmarcv_dp0_addr;
	reg [1:0] dmarcv_state;
	
	wire [3:0] dmarcv_row;
	assign row[3:0] = dmarcv_row[3:0];
	rowcounter rowcounter1
	(
		.clock(rck),
		.reset(reset),
		.row(dmarcv_row)
	);
	
	function sck_gen;
		input [8:0] count;
		begin
			if(count[0] == 1'b0) begin		// 1'b1で動いていたが、1'b0が正しそう。要検証。
				if(count[8:1] > 0 && count[8:1] < 8'd193) begin
					sck_gen = 1'b1;
				end else begin
					sck_gen = 1'b0;
				end
			end else begin
				sck_gen = 1'b0;
			end
		end
	endfunction
	
	function rck_gen;
		input [8:0] count;
		begin
			if(count[8:1] > 8'd193) begin
				rck_gen = 1'b1;
			end else begin
				rck_gen = 1'b0;
			end
		end
	endfunction
	/*
	function [7:0] dmareq_gen;
		input dmareq;
		input dmasel;
		input [3:0] dmarcv_row;
		begin
			case({dmasel, dmarcv_row[2], dmarcv_row[1]})
				3'b000: dmareq_gen[7:0] = {7'b0000000, dmareq};
				3'b001:	dmareq_gen[7:0] = {6'b000000, dmareq, 1'b0};
				3'b010: dmareq_gen[7:0] = {5'b00000, dmareq, 2'b00};
				3'b011: dmareq_gen[7:0] = {4'b0000, dmareq, 3'b000};
				3'b100: dmareq_gen[7:0] = {3'b000, dmareq, 4'b0000};
				3'b101: dmareq_gen[7:0] = {2'b00, dmareq, 5'b00000};
				3'b110: dmareq_gen[7:0] = {1'b0, dmareq, 6'b000000};
				3'b111: dmareq_gen[7:0] = {dmareq, 7'b0000000};
			endcase
		end
	endfunction
	*/
	function dmares_gen;
		input [8:0] count;
		input [3:0] dmarcv_row;
		begin
			if({dmarcv_row[3:0], count[8:1]} == 12'b111111111100) begin
				dmares_gen = 1'b1;
			end else begin
				dmares_gen = 1'b0;
			end
		end
	endfunction
	
	function shift_sel;
		input [8:0] count;
		input [7:0] shift;
		begin
			if(count[8:1] < 8'd98) begin
				shift_sel = shift[7];
			end else begin
				shift_sel = shift[0];
			end
		end
	endfunction
	
	assign sda = ~shift_sel(dmarcv_count, dmarcv_dp0_pdata);
	assign sck = ~sck_gen(dmarcv_count);
	assign rck = rck_gen(dmarcv_count);
//	assign dmareq[7:0] = dmareq_gen(dmarcv_dp0_f0_nfull, dmasel, dmarcv_row);
	assign dmareq[7:0] = {7'b0000000, dmarcv_dp0_f0_nfull};
	assign dmares = dmares_gen(dmarcv_count, dmarcv_row);
	assign f0_nfull = dmarcv_dp0_f0_nfull;
	assign f0_empty = dmarcv_dp0_f0_empty;
	assign pdata[7:0] = dmarcv_dp0_pdata[7:0];
	assign stat[1:0] = dmarcv_state[1:0];
	
	reg [8:0] count;
	always @(posedge dmarcv_clock) begin
		if(count[8:0] == 9'b111111111) begin
			count[8:0] <= 9'b000000000;
		end else begin
			count[8:0] <= count[8:0] + 9'b000000001;
		end
	end
	assign dmarcv_count[8:0] = count[8:0];

	localparam DMA_STATE_SRIGHT    = 2'd0;
	localparam DMA_STATE_SLEFT     = 2'd1;
	localparam DMA_STATE_LOAD      = 2'd2;
	localparam DMA_STATE_IDLE      = 2'd3;

	always @(posedge dmarcv_clock) begin
		if(dmarcv_count[0] == 1'b0) begin
			if(dmarcv_count[3:1] == 3'd0 & dmarcv_count[8:1] < 192) begin
				dmarcv_state <= DMA_STATE_LOAD;
			end else if(dmarcv_count[8:1] < 8'd96) begin
				dmarcv_state <= DMA_STATE_SRIGHT;
			end else begin
				dmarcv_state <= DMA_STATE_SLEFT;
			end
		end else begin
			dmarcv_state <= DMA_STATE_IDLE;
		end
	end
	
	wire color = 1'b0;
	wire [2:0] dmarcv_dp0_address = {color, dmarcv_state[1:0]};
	
	cy_psoc3_dp #(.cy_dpconfig(
	{
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP___SL, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM0:  shift left*/
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP___SR, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM1:  shift right*/
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC___F0, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM2:  fifo load*/
		`CS_ALU_OP_PASS, `CS_SRCA_A0, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM3:  idle*/
		`CS_ALU_OP_PASS, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC_NONE,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM4:  */
		`CS_ALU_OP_PASS, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM5:  */
		`CS_ALU_OP_PASS, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC_NONE, `CS_A1_SRC___F1,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM6:  */
		`CS_ALU_OP__XOR, `CS_SRCA_A1, `CS_SRCB_D0,
		`CS_SHFT_OP_PASS, `CS_A0_SRC__ALU, `CS_A1_SRC__ALU,
		`CS_FEEDBACK_DSBL, `CS_CI_SEL_CFGA, `CS_SI_SEL_CFGA,
		`CS_CMP_SEL_CFGA, /*CFGRAM7:  */
		  8'hFF, 8'h00,	/*CFG9:           */
		  8'hFF, 8'hFF,	/*CFG11-10:           */
		`SC_CMPB_A1_D1, `SC_CMPA_A1_D1, `SC_CI_B_ARITH,
		`SC_CI_A_ARITH, `SC_C1_MASK_DSBL, `SC_C0_MASK_DSBL,
		`SC_A_MASK_DSBL, `SC_DEF_SI_0, `SC_SI_B_DEFSI,
		`SC_SI_A_DEFSI, /*CFG13-12:           */
		`SC_A0_SRC_ACC, `SC_SHIFT_SL, 1'h0,
		1'h0, `SC_FIFO1_BUS, `SC_FIFO0_BUS,
		`SC_MSB_DSBL, `SC_MSB_BIT0, `SC_MSB_NOCHN,
		`SC_FB_NOCHN, `SC_CMP1_NOCHN,
		`SC_CMP0_NOCHN, /*CFG15-14:           */
		 10'h00, `SC_FIFO_CLK__DP,`SC_FIFO_CAP_AX,
		`SC_FIFO_LEVEL,`SC_FIFO_ASYNC,`SC_EXTCRC_DSBL,
		`SC_WRK16CAT_DSBL /*CFG17-16:           */
	})) dp0(
		/* input */ .clk(dmarcv_clock), 			// Clock
		/* input [02:00] */ .cs_addr(dmarcv_dp0_address), // Control Store RAM address
		/* input */ .route_si(1'b0), 		// Shift in from routing
		/* input */ .route_ci(1'b0), 		// Carry in from routing
		/* input */ .f0_load(1'b0), 		// Load FIFO 0
		/* input */ .f1_load(1'b0), 		// Load FIFO 1
		/* input */ .d0_load(1'b0), 		// Load Data Register 0
		/* input */ .d1_load(1'b0), 		// Load Data Register 1
		/* output */ .ce0(), 			// Accumulator 0 = Data register 0
		/* output */ .cl0(), 			// Accumulator 0 < Data register 0
		/* output */ .z0(), 			// Accumulator 0 = 0
		/* output */ .ff0(), 			// Accumulator 0 = FF
		/* output */ .ce1(), 			// Accumulator [0|1] = Data register 1
		/* output */ .cl1(), 			// Accumulator [0|1] < Data register 1
		/* output */ .z1(), 			// Accumulator 1 = 0
		/* output */ .ff1(), 			// Accumulator 1 = FF
		/* output */ .ov_msb(), 		// Operation over flow
		/* output */ .co_msb(), 		// Carry out
		/* output */ .cmsb(), 			// Carry out
		/* output */ .so(), 			// Shift out
		/* output */ .f0_bus_stat(dmarcv_dp0_f0_nfull), 	// FIFO 0 status to uP
		/* output */ .f0_blk_stat(dmarcv_dp0_f0_empty), 	// FIFO 0 status to DP
		/* output */ .f1_bus_stat(dmarcv_dp0_f1_nfull), 	// FIFO 1 status to uP
		/* output */ .f1_blk_stat(dmarcv_dp0_f1_empty), 	// FIFO 1 status to DP
		/* input */ .ci(1'b0), 				// Carry in from previous stage
		/* output */ .co(), 			// Carry out to next stage
		/* input */ .sir(1'b0), 			// Shift in from right side
		/* output */ .sor(), 			// Shift out to right side
		/* input */ .sil(1'b0), 			// Shift in from left side
		/* output */ .sol(), 			// Shift out to left side
		/* input */ .msbi(1'b0), 			// MSB chain in
		/* output */ .msbo(), 			// MSB chain out
		/* input [01:00] */ .cei(2'b0), 	// Compare equal in from prev stage
		/* output [01:00] */ .ceo(), 	// Compare equal out to next stage
		/* input [01:00] */ .cli(2'b0), 	// Compare less than in from prv stage
		/* output [01:00] */ .clo(), 	// Compare less than out to next stage
		/* input [01:00] */ .zi(2'b0), 		// Zero detect in from previous stage
		/* output [01:00] */ .zo(), 	// Zero detect out to next stage
		/* input [01:00] */ .fi(2'b0), 		// 0xFF detect in from previous stage
		/* output [01:00] */ .fo(), 	// 0xFF detect out to next stage
		/* input [01:00] */ .capi(2'b0),	// Capture in from previous stage
		/* output [01:00] */ .capo(),		// Capture out to next stage
		/* input */ .cfbi(1'b0), 			// CRC Feedback in from previous stage
		/* output */ .cfbo(), 			// CRC Feedback out to next stage
		/* input [07:00] */ .pi(), 		// Parallel data port
		/* output [07:00] */ .po(dmarcv_dp0_pdata)
	);
endmodule

module rowcounter(clock, reset, row);
	input clock;
	input reset;
	output [3:0] row;
	
	reg [3:0] cnt;
	
	always @(posedge clock or posedge reset) begin
		if(reset) begin
			cnt <= 4'b0000;
		end else begin
			cnt <= cnt + 4'b0001;
		end
	end
	
	assign row[3:1] = cnt[2:0];
	assign row[0] = cnt[3];
endmodule
/*
module datashifter(clock, reset, count, pdata, sda);
	input clock;
	input reset;
	input [8:0] count;
	input [7:0] pdata;
	output sda;
	
	reg [7:0] shift;
	wire shift_out;
	function shift_sel;
		input [8:0] count;
		input [7:0] shift;
		begin
			if(count[8:1] < 8'd92) begin
				shift_sel = shift[7];
			end else begin
				shift_sel = shift[0];
			end
		end
	endfunction
	assign sda = shift_sel(count, shift);
	
	always @(posedge clock or posedge reset) begin
		if(reset) begin
			shift <= 8'd0;
		end else begin
			if(count[3:0] == 3'b0001) begin
				shift[7:0] <= pdata[7:0];
			end else begin
				if(count[8:1] < 8'd96) begin
					shift[7:0] <= {shift[6:0], 1'b0};
				end else begin
					shift[7:0] <= {1'b0, shift[7:1]};
				end
			end
		end
	end	
endmodule
*/
//`#end` -- edit above this line, do not edit this line

//`#start footer` -- edit after this line, do not edit this line
//`#end` -- edit above this line, do not edit this line








