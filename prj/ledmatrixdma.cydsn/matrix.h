/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"

#define DMA_DATA_SRC_BASE (disp_buf_a)
#define DMA_DATA_DST_BASE (CYDEV_PERIPH_BASE)
#define DMA_ADDR_SRC_BASE (CYDEV_PERIPH_BASE)
#define DMA_ADDR_DST_BASE (CYDEV_PERIPH_BASE)

void Matrix_Start(void);
void Matrix_Stop(void);
void Matrix_SetBaseAddress(uint8 *);
//[] END OF FILE
