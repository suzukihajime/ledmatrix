/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "device.h"

void UART_Start();

//void uprintf(char *);
void uputc(char);
void uputs(char *);
void uprintf(const char *, ...);			// lcd_uprintf

char kbhit(void);
char getc(void);
char *gets(char *);

// End of File 'lcd.h'