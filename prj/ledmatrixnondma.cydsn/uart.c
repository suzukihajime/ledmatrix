/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

// Prototypes of the following functions must be included in 'device.h' or defined somewhere else
//  CS1nCS2_Write(), DnI_Write(), RnW_Write(), DATA_Write(), E_Write()
#include <string.h>
#include <stdarg.h>
#include "uart.h"

void UART_Start(void)
{
	UART_1_Start();
	
//	UART_1_EnableRxInt();
//	UART_1_EnableTxInt();
	return;
}

void uputs(char *str)
{
	while(*str != '\0')
	{
		uputc(*str++);
	}
	return;
}

inline void uputc(char ch)
{
	if(ch == '\n')
		UART_1_PutChar('\r');
	if(ch != '\r')
		UART_1_PutChar(ch);
	return;
}

void uprintf (
	const char*	str,	/* Pointer to the format string */
	...					/* Optional arguments */
)
{
	va_list arp;
	unsigned int r, i, w, f;
	unsigned long val;
	char s[16], c, d;


	va_start(arp, str);

	while((c = *str++) != 0)
	{
		if(c != '%')
		{
			uputc(c);
			continue;
		}
		c = *str++;
		f = 0;
		if(c == '0')
		{					/* Flag: '0' padded */
			f = 1;
			c = *str++;
		}
		w = 0;
		while(c >= '0' && c <= '9')
		{					/* Minimum width */
			w = w * 10 + c - '0';
			c = *str++;
		}
		if(c == 'l' || c == 'L')
		{					/* Prefix: Size is long int */
			f |= 2; c = *str++;
		}
		if(!c) break;
		d = c;
		if(d >= 'a') d -= 0x20;
		switch(d)
		{					/* Type is... */
		case 'S':					/* String */
			uputs(va_arg(arp, char*)); continue;
		case 'C':					/* Character */
			uputc((char)va_arg(arp, int)); continue;
		case 'B':					/* Binary */
			r = 2; break;
		case 'O':					/* Octal */
			r = 8; break;
		case 'D':					/* Signed decimal */
		case 'U':					/* Unsigned decimal */
			r = 10; break;
		case 'X':					/* Hexdecimal */
			r = 16; break;
		default:					/* Unknown */
			uputc(c); continue;
		}

		/* Get an argument and put it in numeral */
		val = (f & 2) ? va_arg(arp, long) : ((d == 'D') ? (long)va_arg(arp, int) : va_arg(arp, unsigned int));
		if (d == 'D' && (val & 0x80000000)) {
			val = 0 - val;
			f |= 4;
		}
		i = 0;
		do{
			d = (char)(val % r); val /= r;
			if(d > 9)
				d += (c == 'x') ? 0x27 : 0x07;
			s[i++] = d + '0';
		} while(val && i < sizeof(s));
		if(f & 4) s[i++] = '-';
		while(i < w--)
			uputc((f & 1) ? '0' : ' ');
		do{
			uputc(s[--i]);
		} while(i);
	}

	va_end(arp);
	return;
}


// End of file 'lcd.c'