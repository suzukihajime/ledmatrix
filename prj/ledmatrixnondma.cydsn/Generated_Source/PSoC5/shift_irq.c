/*******************************************************************************
* File Name: shift_irq.c  
* Version 1.60
*
*  Description:
*   API for controlling the state of an interrupt.
*
*
*  Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/


#include <CYDEVICE.H>
#include <CYDEVICE_TRM.H>
#include <CYLIB.H>
#include <shift_irq.H>


/*******************************************************************************
*  Place your includes, defines and code here 
********************************************************************************/
/* `#START shift_irq_intc` */

/* `#END` */

#ifndef CYINT_IRQ_BASE
#define CYINT_IRQ_BASE	16
#endif
#ifndef CYINT_VECT_TABLE
#define CYINT_VECT_TABLE    ((cyisraddress **) CYREG_NVIC_VECT_OFFSET)
#endif

/* Declared in startup, used to set unused interrupts to. */
CY_ISR_PROTO(IntDefaultHandler);

/*******************************************************************************
* Function Name: shift_irq_Start
********************************************************************************
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   void.
*
*
* Return:
*   void.
*
*******************************************************************************/
void shift_irq_Start(void)
{
    /* For all we know the interrupt is active. */
    shift_irq_Disable();

    /* Set the ISR to point to the shift_irq Interrupt. */
    shift_irq_SetVector(shift_irq_Interrupt);

    /* Set the priority. */
    shift_irq_SetPriority(shift_irq_INTC_PRIOR_NUMBER);

    /* Enable it. */
    shift_irq_Enable();
}

/*******************************************************************************
* Function Name: shift_irq_StartEx
********************************************************************************
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   address: Address of the ISR to set in the interrupt vector table.
*
*
* Return:
*   void.
*
*******************************************************************************/
void shift_irq_StartEx(cyisraddress address)
{
    /* For all we know the interrupt is active. */
    shift_irq_Disable();

    /* Set the ISR to point to the shift_irq Interrupt. */
    shift_irq_SetVector(address);

    /* Set the priority. */
    shift_irq_SetPriority(shift_irq_INTC_PRIOR_NUMBER);

    /* Enable it. */
    shift_irq_Enable();
}

/*******************************************************************************
* Function Name: shift_irq_Stop
********************************************************************************
* Summary:
*   Disables and removes the interrupt.
*
* Parameters:  
*
*
* Return:
*   void.
*
*******************************************************************************/
void shift_irq_Stop(void) 
{
    /* Disable this interrupt. */
    shift_irq_Disable();

    /* Set the ISR to point to the passive one. */
    shift_irq_SetVector(IntDefaultHandler);
}

/*******************************************************************************
* Function Name: shift_irq_Interrupt
********************************************************************************
* Summary:
*   The default Interrupt Service Routine for shift_irq.
*
*   Add custom code between the coments to keep the next version of this file
*   from over writting your code.
*
*
*
* Parameters:  
*
*
* Return:
*   void.
*
*******************************************************************************/
CY_ISR(shift_irq_Interrupt)
{
    /*  Place your Interrupt code here. */
    /* `#START shift_irq_Interrupt` */

    /* `#END` */
}

/*******************************************************************************
* Function Name: shift_irq_SetVector
********************************************************************************
* Summary:
*   Change the ISR vector for the Interrupt. Note calling shift_irq_Start
*   will override any effect this method would have had. To set the vector before
*   the component has been started use shift_irq_StartEx instead.
*
*
* Parameters:
*   address: Address of the ISR to set in the interrupt vector table.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void shift_irq_SetVector(cyisraddress address) 
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    ramVectorTable[CYINT_IRQ_BASE + shift_irq__INTC_NUMBER] = address;
}

/*******************************************************************************
* Function Name: shift_irq_GetVector
********************************************************************************
* Summary:
*   Gets the "address" of the current ISR vector for the Interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   Address of the ISR in the interrupt vector table.
*
*
*******************************************************************************/
cyisraddress shift_irq_GetVector(void) 
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    return ramVectorTable[CYINT_IRQ_BASE + shift_irq__INTC_NUMBER];
}

/*******************************************************************************
* Function Name: shift_irq_SetPriority
********************************************************************************
* Summary:
*   Sets the Priority of the Interrupt. Note calling shift_irq_Start
*   or shift_irq_StartEx will override any effect this method would have had. 
*	This method should only be called after shift_irq_Start or 
*	shift_irq_StartEx has been called. To set the initial
*	priority for the component use the cydwr file in the tool.
*
*
* Parameters:
*   priority: Priority of the interrupt. 0 - 7, 0 being the highest.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void shift_irq_SetPriority(uint8 priority) 
{
    *shift_irq_INTC_PRIOR = priority << 5;
}

/*******************************************************************************
* Function Name: shift_irq_GetPriority
********************************************************************************
* Summary:
*   Gets the Priority of the Interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   Priority of the interrupt. 0 - 7, 0 being the highest.
*
*
*******************************************************************************/
uint8 shift_irq_GetPriority(void) 
{
    uint8 priority;


    priority = *shift_irq_INTC_PRIOR >> 5;

    return priority;
}

/*******************************************************************************
* Function Name: shift_irq_Enable
********************************************************************************
* Summary:
*   Enables the interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void shift_irq_Enable(void) 
{
    /* Enable the general interrupt. */
    *shift_irq_INTC_SET_EN = shift_irq__INTC_MASK;
}

/*******************************************************************************
* Function Name: shift_irq_GetState
********************************************************************************
* Summary:
*   Gets the state (enabled, disabled) of the Interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   1 if enabled, 0 if disabled.
*
*
*******************************************************************************/
uint8 shift_irq_GetState(void) 
{
    /* Get the state of the general interrupt. */
    return (*shift_irq_INTC_SET_EN & shift_irq__INTC_MASK) ? 1:0;
}

/*******************************************************************************
* Function Name: shift_irq_Disable
********************************************************************************
* Summary:
*   Disables the Interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void shift_irq_Disable(void) 
{
    /* Disable the general interrupt. */
    *shift_irq_INTC_CLR_EN = shift_irq__INTC_MASK;
}

/*******************************************************************************
* Function Name: shift_irq_SetPending
********************************************************************************
* Summary:
*   Causes the Interrupt to enter the pending state, a software method of
*   generating the interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void shift_irq_SetPending(void) 
{
    *shift_irq_INTC_SET_PD = shift_irq__INTC_MASK;
}

/*******************************************************************************
* Function Name: shift_irq_ClearPending
********************************************************************************
* Summary:
*   Clears a pending interrupt.
*
* Parameters:
*   void.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void shift_irq_ClearPending(void) 
{
    *shift_irq_INTC_CLR_PD = shift_irq__INTC_MASK;
}
