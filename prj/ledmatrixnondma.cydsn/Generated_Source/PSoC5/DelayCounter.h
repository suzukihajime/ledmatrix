/*******************************************************************************
* File Name: DelayCounter.h  
* Version 2.20
*
*  Description:
*   Contains the function prototypes and constants available to the counter
*   user module.
*
*   Note:
*    None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/
    
#if !defined(CY_COUNTER_DelayCounter_H)
#define CY_COUNTER_DelayCounter_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CyLib.h" /* For CyEnterCriticalSection() and CyExitCriticalSection() functions */

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component Counter_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */


/**************************************
*           Parameter Defaults        
**************************************/

#define DelayCounter_Resolution            16u
#define DelayCounter_UsingFixedFunction    0u
#define DelayCounter_ControlRegRemoved     0u
#define DelayCounter_COMPARE_MODE_SOFTWARE 0u
#define DelayCounter_CAPTURE_MODE_SOFTWARE 0u
#define DelayCounter_RunModeUsed           0u


/***************************************
*       Type defines
***************************************/


/**************************************************************************
 * Sleep Mode API Support
 * Backup structure for Sleep Wake up operations
 *************************************************************************/
typedef struct DelayCounter_backupStruct
{
    /* Sleep BackUp structure */
    uint8 CounterEnableState; 
    #if (CY_PSOC5A)
        uint16 CounterUdb;    /* Current Counter Value      */
        uint16 CounterPeriod; /* Counter Period Value       */
        uint16 CompareValue;  /* Counter Compare Value      */           
        uint8 InterruptMaskValue; /* Counter Compare Value */
    #endif /* (CY_PSOC5A) */

    #if (CY_PSOC3 || CY_PSOC5LP)
            uint16 CounterUdb;
            uint8 InterruptMaskValue;
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */

    #if (!DelayCounter_ControlRegRemoved)
        uint8 CounterControlRegister;          /* Counter Control Register   */
    #endif /* (!DelayCounter_ControlRegRemoved) */
}DelayCounter_backupStruct;


/**************************************
 *  Function Prototypes
 *************************************/
void    DelayCounter_Start(void) ;
void    DelayCounter_Stop(void) ;
void    DelayCounter_SetInterruptMode(uint8 interruptsMask) ;
uint8   DelayCounter_ReadStatusRegister(void) ;
#define DelayCounter_GetInterruptSource() DelayCounter_ReadStatusRegister()
#if(!DelayCounter_ControlRegRemoved)
    uint8   DelayCounter_ReadControlRegister(void) ;
    void    DelayCounter_WriteControlRegister(uint8 control) \
        ;
#endif /* (!DelayCounter_ControlRegRemoved) */
#if (!(DelayCounter_UsingFixedFunction && (CY_PSOC5A)))
    void    DelayCounter_WriteCounter(uint16 counter) \
            ; 
#endif /* (!(DelayCounter_UsingFixedFunction && (CY_PSOC5A))) */
uint16  DelayCounter_ReadCounter(void) ;
uint16  DelayCounter_ReadCapture(void) ;
void    DelayCounter_WritePeriod(uint16 period) \
    ;
uint16  DelayCounter_ReadPeriod( void ) ;
#if (!DelayCounter_UsingFixedFunction)
    void    DelayCounter_WriteCompare(uint16 compare) \
        ;
    uint16  DelayCounter_ReadCompare( void ) \
        ;
#endif /* (!DelayCounter_UsingFixedFunction) */

#if (DelayCounter_COMPARE_MODE_SOFTWARE)
    void    DelayCounter_SetCompareMode(uint8 comparemode) ;
#endif /* (DelayCounter_COMPARE_MODE_SOFTWARE) */
#if (DelayCounter_CAPTURE_MODE_SOFTWARE)
    void    DelayCounter_SetCaptureMode(uint8 capturemode) ;
#endif /* (DelayCounter_CAPTURE_MODE_SOFTWARE) */
void DelayCounter_ClearFIFO(void)     ;
void DelayCounter_Init(void)          ;
void DelayCounter_Enable(void)        ;
void DelayCounter_SaveConfig(void)    ;
void DelayCounter_RestoreConfig(void) ;
void DelayCounter_Sleep(void)         ;
void DelayCounter_Wakeup(void)        ;


/***************************************
*   Enumerated Types and Parameters
***************************************/

/* Enumerated Type B_Counter__CompareModes, Used in Compare Mode retained for backward compatibility of tests*/
#define DelayCounter__B_COUNTER__LESS_THAN 1
#define DelayCounter__B_COUNTER__LESS_THAN_OR_EQUAL 2
#define DelayCounter__B_COUNTER__EQUAL 0
#define DelayCounter__B_COUNTER__GREATER_THAN 3
#define DelayCounter__B_COUNTER__GREATER_THAN_OR_EQUAL 4
#define DelayCounter__B_COUNTER__SOFTWARE 5

/* Enumerated Type Counter_CompareModes */
#define DelayCounter_CMP_MODE_LT 1u
#define DelayCounter_CMP_MODE_LTE 2u
#define DelayCounter_CMP_MODE_EQ 0u
#define DelayCounter_CMP_MODE_GT 3u
#define DelayCounter_CMP_MODE_GTE 4u
#define DelayCounter_CMP_MODE_SOFTWARE_CONTROLLED 5u

/* Enumerated Type B_Counter__CaptureModes, Used in Capture Mode retained for backward compatibility of tests*/
#define DelayCounter__B_COUNTER__NONE 0
#define DelayCounter__B_COUNTER__RISING_EDGE 1
#define DelayCounter__B_COUNTER__FALLING_EDGE 2
#define DelayCounter__B_COUNTER__EITHER_EDGE 3
#define DelayCounter__B_COUNTER__SOFTWARE_CONTROL 4

/* Enumerated Type Counter_CompareModes */
#define DelayCounter_CAP_MODE_NONE 0u
#define DelayCounter_CAP_MODE_RISE 1u
#define DelayCounter_CAP_MODE_FALL 2u
#define DelayCounter_CAP_MODE_BOTH 3u
#define DelayCounter_CAP_MODE_SOFTWARE_CONTROLLED 4u


/***************************************
 *  Initialization Values
 **************************************/
#define DelayCounter_INIT_PERIOD_VALUE       1u
#define DelayCounter_INIT_COUNTER_VALUE      1u
#if (DelayCounter_UsingFixedFunction)
#define DelayCounter_INIT_INTERRUPTS_MASK    ((0u << DelayCounter_STATUS_ZERO_INT_EN_MASK_SHIFT))
#else
#define DelayCounter_INIT_COMPARE_VALUE      1u
#define DelayCounter_INIT_INTERRUPTS_MASK ((0u << DelayCounter_STATUS_ZERO_INT_EN_MASK_SHIFT) | \
        (0u << DelayCounter_STATUS_CAPTURE_INT_EN_MASK_SHIFT) | \
        (0u << DelayCounter_STATUS_CMP_INT_EN_MASK_SHIFT) | \
        (0u << DelayCounter_STATUS_OVERFLOW_INT_EN_MASK_SHIFT) | \
        (0u << DelayCounter_STATUS_UNDERFLOW_INT_EN_MASK_SHIFT))
#define DelayCounter_DEFAULT_COMPARE_MODE    (1u << DelayCounter_CTRL_CMPMODE0_SHIFT)
#define DelayCounter_DEFAULT_CAPTURE_MODE    (0u << DelayCounter_CTRL_CAPMODE0_SHIFT)
#endif /* (DelayCounter_UsingFixedFunction) */


/**************************************
 *  Registers
 *************************************/
#if (DelayCounter_UsingFixedFunction)
    #define DelayCounter_STATICCOUNT_LSB     (*(reg16 *) DelayCounter_CounterHW__CAP0 )
    #define DelayCounter_STATICCOUNT_LSB_PTR ( (reg16 *) DelayCounter_CounterHW__CAP0 )
    #define DelayCounter_PERIOD_LSB          (*(reg16 *) DelayCounter_CounterHW__PER0 )
    #define DelayCounter_PERIOD_LSB_PTR      ( (reg16 *) DelayCounter_CounterHW__PER0 )
    /* MODE must be set to 1 to set the compare value */
    #define DelayCounter_COMPARE_LSB         (*(reg16 *) DelayCounter_CounterHW__CNT_CMP0 )
    #define DelayCounter_COMPARE_LSB_PTR     ( (reg16 *) DelayCounter_CounterHW__CNT_CMP0 )
    /* MODE must be set to 0 to get the count */
    #define DelayCounter_COUNTER_LSB         (*(reg16 *) DelayCounter_CounterHW__CNT_CMP0 )
    #define DelayCounter_COUNTER_LSB_PTR     ( (reg16 *) DelayCounter_CounterHW__CNT_CMP0 )
    #define DelayCounter_RT1                 (*(reg8 *) DelayCounter_CounterHW__RT1)
    #define DelayCounter_RT1_PTR             ( (reg8 *) DelayCounter_CounterHW__RT1)
#else
    #define DelayCounter_STATICCOUNT_LSB     (*(reg16 *) \
        DelayCounter_CounterUDB_sC16_counterdp_u0__F0_REG )
    #define DelayCounter_STATICCOUNT_LSB_PTR ( (reg16 *) \
        DelayCounter_CounterUDB_sC16_counterdp_u0__F0_REG )
    #define DelayCounter_PERIOD_LSB          (*(reg16 *) \
        DelayCounter_CounterUDB_sC16_counterdp_u0__D0_REG )
    #define DelayCounter_PERIOD_LSB_PTR      ( (reg16 *) \
        DelayCounter_CounterUDB_sC16_counterdp_u0__D0_REG )
    #define DelayCounter_COMPARE_LSB         (*(reg16 *) \
        DelayCounter_CounterUDB_sC16_counterdp_u0__D1_REG )
    #define DelayCounter_COMPARE_LSB_PTR     ( (reg16 *) \
        DelayCounter_CounterUDB_sC16_counterdp_u0__D1_REG )
    #define DelayCounter_COUNTER_LSB         (*(reg16 *) \
        DelayCounter_CounterUDB_sC16_counterdp_u0__A0_REG )
    #define DelayCounter_COUNTER_LSB_PTR     ( (reg16 *)\
        DelayCounter_CounterUDB_sC16_counterdp_u0__A0_REG )

    #define DelayCounter_AUX_CONTROLDP0 \
        (*(reg8 *) DelayCounter_CounterUDB_sC16_counterdp_u0__DP_AUX_CTL_REG)
    #define DelayCounter_AUX_CONTROLDP0_PTR \
        ( (reg8 *) DelayCounter_CounterUDB_sC16_counterdp_u0__DP_AUX_CTL_REG)
    #if (DelayCounter_Resolution == 16 || DelayCounter_Resolution == 24 || DelayCounter_Resolution == 32)
       #define DelayCounter_AUX_CONTROLDP1 \
           (*(reg8 *) DelayCounter_CounterUDB_sC16_counterdp_u1__DP_AUX_CTL_REG)
       #define DelayCounter_AUX_CONTROLDP1_PTR \
           ( (reg8 *) DelayCounter_CounterUDB_sC16_counterdp_u1__DP_AUX_CTL_REG)
    #endif /* (DelayCounter_Resolution == 16 || DelayCounter_Resolution == 24 || DelayCounter_Resolution == 32) */
    #if (DelayCounter_Resolution == 24 || DelayCounter_Resolution == 32)
       #define DelayCounter_AUX_CONTROLDP2 \
           (*(reg8 *) DelayCounter_CounterUDB_sC16_counterdp_u2__DP_AUX_CTL_REG)
       #define DelayCounter_AUX_CONTROLDP2_PTR \
           ( (reg8 *) DelayCounter_CounterUDB_sC16_counterdp_u2__DP_AUX_CTL_REG)
    #endif /* (DelayCounter_Resolution == 24 || DelayCounter_Resolution == 32) */
    #if (DelayCounter_Resolution == 32)
       #define DelayCounter_AUX_CONTROLDP3 \
           (*(reg8 *) DelayCounter_CounterUDB_sC16_counterdp_u3__DP_AUX_CTL_REG)
       #define DelayCounter_AUX_CONTROLDP3_PTR \
           ( (reg8 *) DelayCounter_CounterUDB_sC16_counterdp_u3__DP_AUX_CTL_REG)
    #endif /* (DelayCounter_Resolution == 32) */
#endif  /* (DelayCounter_UsingFixedFunction) */

#if (DelayCounter_UsingFixedFunction)
    #define DelayCounter_STATUS         (*(reg8 *) DelayCounter_CounterHW__SR0 )
    /* In Fixed Function Block Status and Mask are the same register */
    #define DelayCounter_STATUS_MASK             (*(reg8 *) DelayCounter_CounterHW__SR0 )
    #define DelayCounter_STATUS_MASK_PTR         ( (reg8 *) DelayCounter_CounterHW__SR0 )
    #define DelayCounter_CONTROL                 (*(reg8 *) DelayCounter_CounterHW__CFG0)
    #define DelayCounter_CONTROL_PTR             ( (reg8 *) DelayCounter_CounterHW__CFG0)
    #define DelayCounter_CONTROL2                (*(reg8 *) DelayCounter_CounterHW__CFG1)
    #define DelayCounter_CONTROL2_PTR            ( (reg8 *) DelayCounter_CounterHW__CFG1)
    #if (CY_PSOC3 || CY_PSOC5LP)
        #define DelayCounter_CONTROL3       (*(reg8 *) DelayCounter_CounterHW__CFG2)
        #define DelayCounter_CONTROL3_PTR   ( (reg8 *) DelayCounter_CounterHW__CFG2)
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */
    #define DelayCounter_GLOBAL_ENABLE           (*(reg8 *) DelayCounter_CounterHW__PM_ACT_CFG)
    #define DelayCounter_GLOBAL_ENABLE_PTR       ( (reg8 *) DelayCounter_CounterHW__PM_ACT_CFG)
    #define DelayCounter_GLOBAL_STBY_ENABLE      (*(reg8 *) DelayCounter_CounterHW__PM_STBY_CFG)
    #define DelayCounter_GLOBAL_STBY_ENABLE_PTR  ( (reg8 *) DelayCounter_CounterHW__PM_STBY_CFG)
    

    /********************************
    *    Constants
    ********************************/

    /* Fixed Function Block Chosen */
    #define DelayCounter_BLOCK_EN_MASK          DelayCounter_CounterHW__PM_ACT_MSK
    #define DelayCounter_BLOCK_STBY_EN_MASK     DelayCounter_CounterHW__PM_STBY_MSK 
    
    /* Control Register Bit Locations */    
    /* As defined in Register Map, part of TMRX_CFG0 register */
    #define DelayCounter_CTRL_ENABLE_SHIFT      0x00u
    #define DelayCounter_ONESHOT_SHIFT          0x02u
    /* Control Register Bit Masks */
    #define DelayCounter_CTRL_ENABLE            (0x01u << DelayCounter_CTRL_ENABLE_SHIFT)         
    #define DelayCounter_ONESHOT                (0x01u << DelayCounter_ONESHOT_SHIFT)

    /* Control2 Register Bit Masks */
    /* Set the mask for run mode */
    #if (CY_PSOC5A)
        /* Use CFG1 Mode bits to set run mode */
        #define DelayCounter_CTRL_MODE_SHIFT        0x01u    
        #define DelayCounter_CTRL_MODE_MASK         (0x07u << DelayCounter_CTRL_MODE_SHIFT)
    #endif /* (CY_PSOC5A) */
    #if (CY_PSOC3 || CY_PSOC5LP)
        /* Use CFG2 Mode bits to set run mode */
        #define DelayCounter_CTRL_MODE_SHIFT        0x00u    
        #define DelayCounter_CTRL_MODE_MASK         (0x03u << DelayCounter_CTRL_MODE_SHIFT)
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */
    /* Set the mask for interrupt (raw/status register) */
    #define DelayCounter_CTRL2_IRQ_SEL_SHIFT     0x00u
    #define DelayCounter_CTRL2_IRQ_SEL          (0x01u << DelayCounter_CTRL2_IRQ_SEL_SHIFT)     
    
    /* Status Register Bit Locations */
    #define DelayCounter_STATUS_ZERO_SHIFT      0x07u  /* As defined in Register Map, part of TMRX_SR0 register */ 

    /* Status Register Interrupt Enable Bit Locations */
    #define DelayCounter_STATUS_ZERO_INT_EN_MASK_SHIFT      (DelayCounter_STATUS_ZERO_SHIFT - 0x04u)

    /* Status Register Bit Masks */                           
    #define DelayCounter_STATUS_ZERO            (0x01u << DelayCounter_STATUS_ZERO_SHIFT)

    /* Status Register Interrupt Bit Masks*/
    #define DelayCounter_STATUS_ZERO_INT_EN_MASK       (DelayCounter_STATUS_ZERO >> 0x04u)
    
    /*RT1 Synch Constants: Applicable for PSoC3 and PSoC5LP */
    #define DelayCounter_RT1_SHIFT            0x04u
    #define DelayCounter_RT1_MASK             (0x03u << DelayCounter_RT1_SHIFT)  /* Sync TC and CMP bit masks */
    #define DelayCounter_SYNC                 (0x03u << DelayCounter_RT1_SHIFT)
    #define DelayCounter_SYNCDSI_SHIFT        0x00u
    #define DelayCounter_SYNCDSI_MASK         (0x0Fu << DelayCounter_SYNCDSI_SHIFT) /* Sync all DSI inputs */
    #define DelayCounter_SYNCDSI_EN           (0x0Fu << DelayCounter_SYNCDSI_SHIFT) /* Sync all DSI inputs */
    
#else /* !DelayCounter_UsingFixedFunction */
    #define DelayCounter_STATUS               (* (reg8 *) DelayCounter_CounterUDB_sSTSReg_nrstSts_stsreg__STATUS_REG )
    #define DelayCounter_STATUS_PTR           (  (reg8 *) DelayCounter_CounterUDB_sSTSReg_nrstSts_stsreg__STATUS_REG )
    #define DelayCounter_STATUS_MASK          (* (reg8 *) DelayCounter_CounterUDB_sSTSReg_nrstSts_stsreg__MASK_REG )
    #define DelayCounter_STATUS_MASK_PTR      (  (reg8 *) DelayCounter_CounterUDB_sSTSReg_nrstSts_stsreg__MASK_REG )
    #define DelayCounter_STATUS_AUX_CTRL      (*(reg8 *) DelayCounter_CounterUDB_sSTSReg_nrstSts_stsreg__STATUS_AUX_CTL_REG)
    #define DelayCounter_STATUS_AUX_CTRL_PTR  ( (reg8 *) DelayCounter_CounterUDB_sSTSReg_nrstSts_stsreg__STATUS_AUX_CTL_REG)
    #define DelayCounter_CONTROL              (* (reg8 *) DelayCounter_CounterUDB_sCTRLReg_AsyncCtl_ctrlreg__CONTROL_REG )
    #define DelayCounter_CONTROL_PTR          (  (reg8 *) DelayCounter_CounterUDB_sCTRLReg_AsyncCtl_ctrlreg__CONTROL_REG )


    /********************************
    *    Constants
    ********************************/
    /* Control Register Bit Locations */
    #define DelayCounter_CTRL_CMPMODE0_SHIFT    0x00u       /* As defined by Verilog Implementation */
    #define DelayCounter_CTRL_CAPMODE0_SHIFT    0x03u       /* As defined by Verilog Implementation */
    #define DelayCounter_CTRL_RESET_SHIFT       0x06u       /* As defined by Verilog Implementation */
    #define DelayCounter_CTRL_ENABLE_SHIFT      0x07u       /* As defined by Verilog Implementation */
    /* Control Register Bit Masks */
    #define DelayCounter_CTRL_CMPMODE_MASK      (0x07u << DelayCounter_CTRL_CMPMODE0_SHIFT)  
    #define DelayCounter_CTRL_CAPMODE_MASK      (0x03u << DelayCounter_CTRL_CAPMODE0_SHIFT)  
    #define DelayCounter_CTRL_RESET             (0x01u << DelayCounter_CTRL_RESET_SHIFT)  
    #define DelayCounter_CTRL_ENABLE            (0x01u << DelayCounter_CTRL_ENABLE_SHIFT) 

    /* Status Register Bit Locations */
    #define DelayCounter_STATUS_CMP_SHIFT       0x00u       /* As defined by Verilog Implementation */
    #define DelayCounter_STATUS_ZERO_SHIFT      0x01u       /* As defined by Verilog Implementation */
    #define DelayCounter_STATUS_OVERFLOW_SHIFT  0x02u       /* As defined by Verilog Implementation */
    #define DelayCounter_STATUS_UNDERFLOW_SHIFT 0x03u       /* As defined by Verilog Implementation */
    #define DelayCounter_STATUS_CAPTURE_SHIFT   0x04u       /* As defined by Verilog Implementation */
    #define DelayCounter_STATUS_FIFOFULL_SHIFT  0x05u       /* As defined by Verilog Implementation */
    #define DelayCounter_STATUS_FIFONEMP_SHIFT  0x06u       /* As defined by Verilog Implementation */
    /* Status Register Interrupt Enable Bit Locations - UDB Status Interrupt Mask match Status Bit Locations*/
    #define DelayCounter_STATUS_CMP_INT_EN_MASK_SHIFT       DelayCounter_STATUS_CMP_SHIFT       
    #define DelayCounter_STATUS_ZERO_INT_EN_MASK_SHIFT      DelayCounter_STATUS_ZERO_SHIFT      
    #define DelayCounter_STATUS_OVERFLOW_INT_EN_MASK_SHIFT  DelayCounter_STATUS_OVERFLOW_SHIFT  
    #define DelayCounter_STATUS_UNDERFLOW_INT_EN_MASK_SHIFT DelayCounter_STATUS_UNDERFLOW_SHIFT 
    #define DelayCounter_STATUS_CAPTURE_INT_EN_MASK_SHIFT   DelayCounter_STATUS_CAPTURE_SHIFT   
    #define DelayCounter_STATUS_FIFOFULL_INT_EN_MASK_SHIFT  DelayCounter_STATUS_FIFOFULL_SHIFT  
    #define DelayCounter_STATUS_FIFONEMP_INT_EN_MASK_SHIFT  DelayCounter_STATUS_FIFONEMP_SHIFT  
    /* Status Register Bit Masks */                
    #define DelayCounter_STATUS_CMP             (0x01u << DelayCounter_STATUS_CMP_SHIFT)  
    #define DelayCounter_STATUS_ZERO            (0x01u << DelayCounter_STATUS_ZERO_SHIFT) 
    #define DelayCounter_STATUS_OVERFLOW        (0x01u << DelayCounter_STATUS_OVERFLOW_SHIFT) 
    #define DelayCounter_STATUS_UNDERFLOW       (0x01u << DelayCounter_STATUS_UNDERFLOW_SHIFT) 
    #define DelayCounter_STATUS_CAPTURE         (0x01u << DelayCounter_STATUS_CAPTURE_SHIFT) 
    #define DelayCounter_STATUS_FIFOFULL        (0x01u << DelayCounter_STATUS_FIFOFULL_SHIFT)
    #define DelayCounter_STATUS_FIFONEMP        (0x01u << DelayCounter_STATUS_FIFONEMP_SHIFT)
    /* Status Register Interrupt Bit Masks  - UDB Status Interrupt Mask match Status Bit Locations */
    #define DelayCounter_STATUS_CMP_INT_EN_MASK            DelayCounter_STATUS_CMP                    
    #define DelayCounter_STATUS_ZERO_INT_EN_MASK           DelayCounter_STATUS_ZERO            
    #define DelayCounter_STATUS_OVERFLOW_INT_EN_MASK       DelayCounter_STATUS_OVERFLOW        
    #define DelayCounter_STATUS_UNDERFLOW_INT_EN_MASK      DelayCounter_STATUS_UNDERFLOW       
    #define DelayCounter_STATUS_CAPTURE_INT_EN_MASK        DelayCounter_STATUS_CAPTURE         
    #define DelayCounter_STATUS_FIFOFULL_INT_EN_MASK       DelayCounter_STATUS_FIFOFULL        
    #define DelayCounter_STATUS_FIFONEMP_INT_EN_MASK       DelayCounter_STATUS_FIFONEMP         
    

    /* StatusI Interrupt Enable bit Location in the Auxilliary Control Register */
    #define DelayCounter_STATUS_ACTL_INT_EN     0x10u /* As defined for the ACTL Register */
    
    /* Datapath Auxillary Control Register definitions */
    #define DelayCounter_AUX_CTRL_FIFO0_CLR         0x01u   /* As defined by Register map */
    #define DelayCounter_AUX_CTRL_FIFO1_CLR         0x02u   /* As defined by Register map */
    #define DelayCounter_AUX_CTRL_FIFO0_LVL         0x04u   /* As defined by Register map */
    #define DelayCounter_AUX_CTRL_FIFO1_LVL         0x08u   /* As defined by Register map */
    #define DelayCounter_STATUS_ACTL_INT_EN_MASK    0x10u   /* As defined for the ACTL Register */
    
#endif /* DelayCounter_UsingFixedFunction */

#endif  /* CY_COUNTER_DelayCounter_H */


/* [] END OF FILE */

