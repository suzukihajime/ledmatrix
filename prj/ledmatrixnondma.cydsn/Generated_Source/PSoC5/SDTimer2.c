/*******************************************************************************
* File Name: SDTimer2.c  
* Version 2.20
*
*  Description:
*     The Counter component consists of a 8, 16, 24 or 32-bit counter with
*     a selectable period between 2 and 2^Width - 1.  
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "SDTimer2.h"

uint8 SDTimer2_initVar = 0u;


/*******************************************************************************
* Function Name: SDTimer2_Init
********************************************************************************
* Summary:
*     Initialize to the schematic state
* 
* Parameters:  
*  void  
*
* Return: 
*  void
*
*******************************************************************************/
void SDTimer2_Init(void) 
{
        #if (!SDTimer2_UsingFixedFunction && !SDTimer2_ControlRegRemoved)
            uint8 ctrl;
        #endif /* (!SDTimer2_UsingFixedFunction && !SDTimer2_ControlRegRemoved) */
        
        #if(!SDTimer2_UsingFixedFunction) 
            /* Interrupt State Backup for Critical Region*/
            uint8 SDTimer2_interruptState;
        #endif /* (!SDTimer2_UsingFixedFunction) */
        
        #if (SDTimer2_UsingFixedFunction)
            /* Clear all bits but the enable bit (if it's already set for Timer operation */
            SDTimer2_CONTROL &= SDTimer2_CTRL_ENABLE;
            
            /* Clear the mode bits for continuous run mode */
            #if (CY_PSOC5A)
                SDTimer2_CONTROL2 &= ~SDTimer2_CTRL_MODE_MASK;
            #endif /* (CY_PSOC5A) */
            #if (CY_PSOC3 || CY_PSOC5LP)
                SDTimer2_CONTROL3 &= ~SDTimer2_CTRL_MODE_MASK;                
            #endif /* (CY_PSOC3 || CY_PSOC5LP) */
            /* Check if One Shot mode is enabled i.e. RunMode !=0*/
            #if (SDTimer2_RunModeUsed != 0x0u)
                /* Set 3rd bit of Control register to enable one shot mode */
                SDTimer2_CONTROL |= SDTimer2_ONESHOT;
            #endif /* (SDTimer2_RunModeUsed != 0x0u) */
            
            /* Set the IRQ to use the status register interrupts */
            SDTimer2_CONTROL2 |= SDTimer2_CTRL2_IRQ_SEL;
            
            /* Clear and Set SYNCTC and SYNCCMP bits of RT1 register */
            SDTimer2_RT1 &= ~SDTimer2_RT1_MASK;
            SDTimer2_RT1 |= SDTimer2_SYNC;     
                    
            /*Enable DSI Sync all all inputs of the Timer*/
            SDTimer2_RT1 &= ~(SDTimer2_SYNCDSI_MASK);
            SDTimer2_RT1 |= SDTimer2_SYNCDSI_EN;

        #else
            #if(!SDTimer2_ControlRegRemoved)
            /* Set the default compare mode defined in the parameter */
            ctrl = SDTimer2_CONTROL & ~SDTimer2_CTRL_CMPMODE_MASK;
            SDTimer2_CONTROL = ctrl | SDTimer2_DEFAULT_COMPARE_MODE;
            
            /* Set the default capture mode defined in the parameter */
            ctrl = SDTimer2_CONTROL & ~SDTimer2_CTRL_CAPMODE_MASK;
            SDTimer2_CONTROL = ctrl | SDTimer2_DEFAULT_CAPTURE_MODE;
            #endif /* (!SDTimer2_ControlRegRemoved) */
        #endif /* (SDTimer2_UsingFixedFunction) */
        
        /* Clear all data in the FIFO's */
        #if (!SDTimer2_UsingFixedFunction)
            SDTimer2_ClearFIFO();
        #endif /* (!SDTimer2_UsingFixedFunction) */
        
        /* Set Initial values from Configuration */
        SDTimer2_WritePeriod(SDTimer2_INIT_PERIOD_VALUE);
        #if (!(SDTimer2_UsingFixedFunction && (CY_PSOC5A)))
            SDTimer2_WriteCounter(SDTimer2_INIT_COUNTER_VALUE);
        #endif /* (!(SDTimer2_UsingFixedFunction && (CY_PSOC5A))) */
        SDTimer2_SetInterruptMode(SDTimer2_INIT_INTERRUPTS_MASK);
        
        #if (!SDTimer2_UsingFixedFunction)
            /* Read the status register to clear the unwanted interrupts */
            SDTimer2_ReadStatusRegister();
            /* Set the compare value (only available to non-fixed function implementation */
            SDTimer2_WriteCompare(SDTimer2_INIT_COMPARE_VALUE);
            /* Use the interrupt output of the status register for IRQ output */
            
            /* CyEnterCriticalRegion and CyExitCriticalRegion are used to mark following region critical*/
            /* Enter Critical Region*/
            SDTimer2_interruptState = CyEnterCriticalSection();
            
            SDTimer2_STATUS_AUX_CTRL |= SDTimer2_STATUS_ACTL_INT_EN_MASK;
            
            /* Exit Critical Region*/
            CyExitCriticalSection(SDTimer2_interruptState);
            
        #endif /* (!SDTimer2_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer2_Enable
********************************************************************************
* Summary:
*     Enable the Counter
* 
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Side Effects: 
*   If the Enable mode is set to Hardware only then this function has no effect 
*   on the operation of the counter.
*
*******************************************************************************/
void SDTimer2_Enable(void) 
{
    /* Globally Enable the Fixed Function Block chosen */
    #if (SDTimer2_UsingFixedFunction)
        SDTimer2_GLOBAL_ENABLE |= SDTimer2_BLOCK_EN_MASK;
        SDTimer2_GLOBAL_STBY_ENABLE |= SDTimer2_BLOCK_STBY_EN_MASK;
    #endif /* (SDTimer2_UsingFixedFunction) */  
        
    /* Enable the counter from the control register  */
    /* If Fixed Function then make sure Mode is set correctly */
    /* else make sure reset is clear */
    #if(!SDTimer2_ControlRegRemoved || SDTimer2_UsingFixedFunction)
        SDTimer2_CONTROL |= SDTimer2_CTRL_ENABLE;                
    #endif /* (!SDTimer2_ControlRegRemoved || SDTimer2_UsingFixedFunction) */
    
}


/*******************************************************************************
* Function Name: SDTimer2_Start
********************************************************************************
* Summary:
*  Enables the counter for operation 
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Global variables:
*  SDTimer2_initVar: Is modified when this function is called for the  
*   first time. Is used to ensure that initialization happens only once.
*
*******************************************************************************/
void SDTimer2_Start(void) 
{
    if(SDTimer2_initVar == 0u)
    {
        SDTimer2_Init();
        
        SDTimer2_initVar = 1u; /* Clear this bit for Initialization */        
    }
    
    /* Enable the Counter */
    SDTimer2_Enable();        
}


/*******************************************************************************
* Function Name: SDTimer2_Stop
********************************************************************************
* Summary:
* Halts the counter, but does not change any modes or disable interrupts.
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Side Effects: If the Enable mode is set to Hardware only then this function
*               has no effect on the operation of the counter.
*
*******************************************************************************/
void SDTimer2_Stop(void) 
{
    /* Disable Counter */
    #if(!SDTimer2_ControlRegRemoved || SDTimer2_UsingFixedFunction)
        SDTimer2_CONTROL &= ~SDTimer2_CTRL_ENABLE;        
    #endif /* (!SDTimer2_ControlRegRemoved || SDTimer2_UsingFixedFunction) */
    
    /* Globally disable the Fixed Function Block chosen */
    #if (SDTimer2_UsingFixedFunction)
        SDTimer2_GLOBAL_ENABLE &= ~SDTimer2_BLOCK_EN_MASK;
        SDTimer2_GLOBAL_STBY_ENABLE &= ~SDTimer2_BLOCK_STBY_EN_MASK;
    #endif /* (SDTimer2_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer2_SetInterruptMode
********************************************************************************
* Summary:
* Configures which interrupt sources are enabled to generate the final interrupt
*
* Parameters:  
*  InterruptsMask: This parameter is an or'd collection of the status bits
*                   which will be allowed to generate the counters interrupt.   
*
* Return: 
*  void
*
*******************************************************************************/
void SDTimer2_SetInterruptMode(uint8 interruptsMask) 
{
    SDTimer2_STATUS_MASK = interruptsMask;
}


/*******************************************************************************
* Function Name: SDTimer2_ReadStatusRegister
********************************************************************************
* Summary:
*   Reads the status register and returns it's state. This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the status register
*
* Side Effects:
*   Status register bits may be clear on read. 
*
*******************************************************************************/
uint8   SDTimer2_ReadStatusRegister(void) 
{
    return SDTimer2_STATUS;
}


#if(!SDTimer2_ControlRegRemoved)
/*******************************************************************************
* Function Name: SDTimer2_ReadControlRegister
********************************************************************************
* Summary:
*   Reads the control register and returns it's state. This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the control register
*
*******************************************************************************/
uint8   SDTimer2_ReadControlRegister(void) 
{
    return SDTimer2_CONTROL;
}


/*******************************************************************************
* Function Name: SDTimer2_WriteControlRegister
********************************************************************************
* Summary:
*   Sets the bit-field of the control register.  This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the control register
*
*******************************************************************************/
void    SDTimer2_WriteControlRegister(uint8 control) 
{
    SDTimer2_CONTROL = control;
}

#endif  /* (!SDTimer2_ControlRegRemoved) */


#if (!(SDTimer2_UsingFixedFunction && (CY_PSOC5A)))
/*******************************************************************************
* Function Name: SDTimer2_WriteCounter
********************************************************************************
* Summary:
*   This funtion is used to set the counter to a specific value
*
* Parameters:  
*  counter:  New counter value. 
*
* Return: 
*  void 
*
*******************************************************************************/
void SDTimer2_WriteCounter(uint16 counter) \
                                   
{
    #if(SDTimer2_UsingFixedFunction)
        /* assert if block is already enabled */
        CYASSERT (!(SDTimer2_GLOBAL_ENABLE & SDTimer2_BLOCK_EN_MASK));
        /* If block is disabled, enable it and then write the counter */
        SDTimer2_GLOBAL_ENABLE |= SDTimer2_BLOCK_EN_MASK;
        CY_SET_REG16(SDTimer2_COUNTER_LSB_PTR, (uint16)counter);
        SDTimer2_GLOBAL_ENABLE &= ~SDTimer2_BLOCK_EN_MASK;
    #else
        CY_SET_REG16(SDTimer2_COUNTER_LSB_PTR, counter);
    #endif /* (SDTimer2_UsingFixedFunction) */
}
#endif /* (!(SDTimer2_UsingFixedFunction && (CY_PSOC5A))) */


/*******************************************************************************
* Function Name: SDTimer2_ReadCounter
********************************************************************************
* Summary:
* Returns the current value of the counter.  It doesn't matter
* if the counter is enabled or running.
*
* Parameters:  
*  void:  
*
* Return: 
*  (uint16) The present value of the counter.
*
*******************************************************************************/
uint16 SDTimer2_ReadCounter(void) 
{
    /* Force capture by reading Accumulator */
    /* Must first do a software capture to be able to read the counter */
    /* It is up to the user code to make sure there isn't already captured data in the FIFO */
    CY_GET_REG8(SDTimer2_COUNTER_LSB_PTR);
    
    /* Read the data from the FIFO (or capture register for Fixed Function)*/
    return (CY_GET_REG16(SDTimer2_STATICCOUNT_LSB_PTR));
}


/*******************************************************************************
* Function Name: SDTimer2_ReadCapture
********************************************************************************
* Summary:
*   This function returns the last value captured.
*
* Parameters:  
*  void
*
* Return: 
*  (uint16) Present Capture value.
*
*******************************************************************************/
uint16 SDTimer2_ReadCapture(void) 
{
   return ( CY_GET_REG16(SDTimer2_STATICCOUNT_LSB_PTR) );  
}


/*******************************************************************************
* Function Name: SDTimer2_WritePeriod
********************************************************************************
* Summary:
* Changes the period of the counter.  The new period 
* will be loaded the next time terminal count is detected.
*
* Parameters:  
*  period: (uint16) A value of 0 will result in
*         the counter remaining at zero.  
*
* Return: 
*  void
*
*******************************************************************************/
void SDTimer2_WritePeriod(uint16 period) 
{
    #if(SDTimer2_UsingFixedFunction)
        CY_SET_REG16(SDTimer2_PERIOD_LSB_PTR,(uint16)period);
    #else
        CY_SET_REG16(SDTimer2_PERIOD_LSB_PTR,period);
    #endif /* (SDTimer2_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer2_ReadPeriod
********************************************************************************
* Summary:
* Reads the current period value without affecting counter operation.
*
* Parameters:  
*  void:  
*
* Return: 
*  (uint16) Present period value.
*
*******************************************************************************/
uint16 SDTimer2_ReadPeriod(void) 
{
   return ( CY_GET_REG16(SDTimer2_PERIOD_LSB_PTR));
}


#if (!SDTimer2_UsingFixedFunction)
/*******************************************************************************
* Function Name: SDTimer2_WriteCompare
********************************************************************************
* Summary:
* Changes the compare value.  The compare output will 
* reflect the new value on the next UDB clock.  The compare output will be 
* driven high when the present counter value compares true based on the 
* configured compare mode setting. 
*
* Parameters:  
*  Compare:  New compare value. 
*
* Return: 
*  void
*
*******************************************************************************/
void SDTimer2_WriteCompare(uint16 compare) \
                                   
{
    #if(SDTimer2_UsingFixedFunction)
        CY_SET_REG16(SDTimer2_COMPARE_LSB_PTR,(uint16)compare);
    #else
        CY_SET_REG16(SDTimer2_COMPARE_LSB_PTR,compare);
    #endif /* (SDTimer2_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer2_ReadCompare
********************************************************************************
* Summary:
* Returns the compare value.
*
* Parameters:  
*  void:
*
* Return: 
*  (uint16) Present compare value.
*
*******************************************************************************/
uint16 SDTimer2_ReadCompare(void) 
{
   return ( CY_GET_REG16(SDTimer2_COMPARE_LSB_PTR));
}


#if (SDTimer2_COMPARE_MODE_SOFTWARE)
/*******************************************************************************
* Function Name: SDTimer2_SetCompareMode
********************************************************************************
* Summary:
*  Sets the software controlled Compare Mode.
*
* Parameters:
*  compareMode:  Compare Mode Enumerated Type.
*
* Return:
*  void
*
*******************************************************************************/
void SDTimer2_SetCompareMode(uint8 compareMode) 
{
    /* Clear the compare mode bits in the control register */
    SDTimer2_CONTROL &= ~SDTimer2_CTRL_CMPMODE_MASK;
    
    /* Write the new setting */
    SDTimer2_CONTROL |= (compareMode << SDTimer2_CTRL_CMPMODE0_SHIFT);
}
#endif  /* (SDTimer2_COMPARE_MODE_SOFTWARE) */


#if (SDTimer2_CAPTURE_MODE_SOFTWARE)
/*******************************************************************************
* Function Name: SDTimer2_SetCaptureMode
********************************************************************************
* Summary:
*  Sets the software controlled Capture Mode.
*
* Parameters:
*  captureMode:  Capture Mode Enumerated Type.
*
* Return:
*  void
*
*******************************************************************************/
void SDTimer2_SetCaptureMode(uint8 captureMode) 
{
    /* Clear the capture mode bits in the control register */
    SDTimer2_CONTROL &= ~SDTimer2_CTRL_CAPMODE_MASK;
    
    /* Write the new setting */
    SDTimer2_CONTROL |= (captureMode << SDTimer2_CTRL_CAPMODE0_SHIFT);
}
#endif  /* (SDTimer2_CAPTURE_MODE_SOFTWARE) */


/*******************************************************************************
* Function Name: SDTimer2_ClearFIFO
********************************************************************************
* Summary:
*   This function clears all capture data from the capture FIFO
*
* Parameters:  
*  void:
*
* Return: 
*  None
*
*******************************************************************************/
void SDTimer2_ClearFIFO(void) 
{

    while(SDTimer2_ReadStatusRegister() & SDTimer2_STATUS_FIFONEMP)
    {
        SDTimer2_ReadCapture();
    }

}
#endif  /* (!SDTimer2_UsingFixedFunction) */


/* [] END OF FILE */

