/*******************************************************************************
* File Name: phase_irq.c  
* Version 1.60
*
*  Description:
*   API for controlling the state of an interrupt.
*
*
*  Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/


#include <CYDEVICE.H>
#include <CYDEVICE_TRM.H>
#include <CYLIB.H>
#include <phase_irq.H>


/*******************************************************************************
*  Place your includes, defines and code here 
********************************************************************************/
/* `#START phase_irq_intc` */

/* `#END` */

#ifndef CYINT_IRQ_BASE
#define CYINT_IRQ_BASE	16
#endif
#ifndef CYINT_VECT_TABLE
#define CYINT_VECT_TABLE    ((cyisraddress **) CYREG_NVIC_VECT_OFFSET)
#endif

/* Declared in startup, used to set unused interrupts to. */
CY_ISR_PROTO(IntDefaultHandler);

/*******************************************************************************
* Function Name: phase_irq_Start
********************************************************************************
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   void.
*
*
* Return:
*   void.
*
*******************************************************************************/
void phase_irq_Start(void)
{
    /* For all we know the interrupt is active. */
    phase_irq_Disable();

    /* Set the ISR to point to the phase_irq Interrupt. */
    phase_irq_SetVector(phase_irq_Interrupt);

    /* Set the priority. */
    phase_irq_SetPriority(phase_irq_INTC_PRIOR_NUMBER);

    /* Enable it. */
    phase_irq_Enable();
}

/*******************************************************************************
* Function Name: phase_irq_StartEx
********************************************************************************
* Summary:
*  Set up the interrupt and enable it.
*
* Parameters:  
*   address: Address of the ISR to set in the interrupt vector table.
*
*
* Return:
*   void.
*
*******************************************************************************/
void phase_irq_StartEx(cyisraddress address)
{
    /* For all we know the interrupt is active. */
    phase_irq_Disable();

    /* Set the ISR to point to the phase_irq Interrupt. */
    phase_irq_SetVector(address);

    /* Set the priority. */
    phase_irq_SetPriority(phase_irq_INTC_PRIOR_NUMBER);

    /* Enable it. */
    phase_irq_Enable();
}

/*******************************************************************************
* Function Name: phase_irq_Stop
********************************************************************************
* Summary:
*   Disables and removes the interrupt.
*
* Parameters:  
*
*
* Return:
*   void.
*
*******************************************************************************/
void phase_irq_Stop(void) 
{
    /* Disable this interrupt. */
    phase_irq_Disable();

    /* Set the ISR to point to the passive one. */
    phase_irq_SetVector(IntDefaultHandler);
}

/*******************************************************************************
* Function Name: phase_irq_Interrupt
********************************************************************************
* Summary:
*   The default Interrupt Service Routine for phase_irq.
*
*   Add custom code between the coments to keep the next version of this file
*   from over writting your code.
*
*
*
* Parameters:  
*
*
* Return:
*   void.
*
*******************************************************************************/
CY_ISR(phase_irq_Interrupt)
{
    /*  Place your Interrupt code here. */
    /* `#START phase_irq_Interrupt` */

    /* `#END` */
}

/*******************************************************************************
* Function Name: phase_irq_SetVector
********************************************************************************
* Summary:
*   Change the ISR vector for the Interrupt. Note calling phase_irq_Start
*   will override any effect this method would have had. To set the vector before
*   the component has been started use phase_irq_StartEx instead.
*
*
* Parameters:
*   address: Address of the ISR to set in the interrupt vector table.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void phase_irq_SetVector(cyisraddress address) 
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    ramVectorTable[CYINT_IRQ_BASE + phase_irq__INTC_NUMBER] = address;
}

/*******************************************************************************
* Function Name: phase_irq_GetVector
********************************************************************************
* Summary:
*   Gets the "address" of the current ISR vector for the Interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   Address of the ISR in the interrupt vector table.
*
*
*******************************************************************************/
cyisraddress phase_irq_GetVector(void) 
{
    cyisraddress * ramVectorTable;

    ramVectorTable = (cyisraddress *) *CYINT_VECT_TABLE;

    return ramVectorTable[CYINT_IRQ_BASE + phase_irq__INTC_NUMBER];
}

/*******************************************************************************
* Function Name: phase_irq_SetPriority
********************************************************************************
* Summary:
*   Sets the Priority of the Interrupt. Note calling phase_irq_Start
*   or phase_irq_StartEx will override any effect this method would have had. 
*	This method should only be called after phase_irq_Start or 
*	phase_irq_StartEx has been called. To set the initial
*	priority for the component use the cydwr file in the tool.
*
*
* Parameters:
*   priority: Priority of the interrupt. 0 - 7, 0 being the highest.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void phase_irq_SetPriority(uint8 priority) 
{
    *phase_irq_INTC_PRIOR = priority << 5;
}

/*******************************************************************************
* Function Name: phase_irq_GetPriority
********************************************************************************
* Summary:
*   Gets the Priority of the Interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   Priority of the interrupt. 0 - 7, 0 being the highest.
*
*
*******************************************************************************/
uint8 phase_irq_GetPriority(void) 
{
    uint8 priority;


    priority = *phase_irq_INTC_PRIOR >> 5;

    return priority;
}

/*******************************************************************************
* Function Name: phase_irq_Enable
********************************************************************************
* Summary:
*   Enables the interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void phase_irq_Enable(void) 
{
    /* Enable the general interrupt. */
    *phase_irq_INTC_SET_EN = phase_irq__INTC_MASK;
}

/*******************************************************************************
* Function Name: phase_irq_GetState
********************************************************************************
* Summary:
*   Gets the state (enabled, disabled) of the Interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   1 if enabled, 0 if disabled.
*
*
*******************************************************************************/
uint8 phase_irq_GetState(void) 
{
    /* Get the state of the general interrupt. */
    return (*phase_irq_INTC_SET_EN & phase_irq__INTC_MASK) ? 1:0;
}

/*******************************************************************************
* Function Name: phase_irq_Disable
********************************************************************************
* Summary:
*   Disables the Interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void phase_irq_Disable(void) 
{
    /* Disable the general interrupt. */
    *phase_irq_INTC_CLR_EN = phase_irq__INTC_MASK;
}

/*******************************************************************************
* Function Name: phase_irq_SetPending
********************************************************************************
* Summary:
*   Causes the Interrupt to enter the pending state, a software method of
*   generating the interrupt.
*
*
* Parameters:
*   void.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void phase_irq_SetPending(void) 
{
    *phase_irq_INTC_SET_PD = phase_irq__INTC_MASK;
}

/*******************************************************************************
* Function Name: phase_irq_ClearPending
********************************************************************************
* Summary:
*   Clears a pending interrupt.
*
* Parameters:
*   void.
*
*
* Return:
*   void.
*
*
*******************************************************************************/
void phase_irq_ClearPending(void) 
{
    *phase_irq_INTC_CLR_PD = phase_irq__INTC_MASK;
}
