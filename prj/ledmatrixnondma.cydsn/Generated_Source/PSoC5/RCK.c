/*******************************************************************************
* File Name: RCK.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "RCK.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 RCK__PORT == 15 && (RCK__MASK & 0xC0))

/*******************************************************************************
* Function Name: RCK_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void RCK_Write(uint8 value) 
{
    uint8 staticBits = RCK_DR & ~RCK_MASK;
    RCK_DR = staticBits | ((value << RCK_SHIFT) & RCK_MASK);
}


/*******************************************************************************
* Function Name: RCK_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void RCK_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(RCK_0, mode);
}


/*******************************************************************************
* Function Name: RCK_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro RCK_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 RCK_Read(void) 
{
    return (RCK_PS & RCK_MASK) >> RCK_SHIFT;
}


/*******************************************************************************
* Function Name: RCK_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 RCK_ReadDataReg(void) 
{
    return (RCK_DR & RCK_MASK) >> RCK_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(RCK_INTSTAT) 

    /*******************************************************************************
    * Function Name: RCK_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 RCK_ClearInterrupt(void) 
    {
        return (RCK_INTSTAT & RCK_MASK) >> RCK_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
