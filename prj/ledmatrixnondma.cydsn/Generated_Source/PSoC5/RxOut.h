/*******************************************************************************
* File Name: RxOut.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_RxOut_H) /* Pins RxOut_H */
#define CY_PINS_RxOut_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "RxOut_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 RxOut__PORT == 15 && (RxOut__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    RxOut_Write(uint8 value) ;
void    RxOut_SetDriveMode(uint8 mode) ;
uint8   RxOut_ReadDataReg(void) ;
uint8   RxOut_Read(void) ;
uint8   RxOut_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define RxOut_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define RxOut_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define RxOut_DM_RES_UP          PIN_DM_RES_UP
#define RxOut_DM_RES_DWN         PIN_DM_RES_DWN
#define RxOut_DM_OD_LO           PIN_DM_OD_LO
#define RxOut_DM_OD_HI           PIN_DM_OD_HI
#define RxOut_DM_STRONG          PIN_DM_STRONG
#define RxOut_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define RxOut_MASK               RxOut__MASK
#define RxOut_SHIFT              RxOut__SHIFT
#define RxOut_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define RxOut_PS                     (* (reg8 *) RxOut__PS)
/* Data Register */
#define RxOut_DR                     (* (reg8 *) RxOut__DR)
/* Port Number */
#define RxOut_PRT_NUM                (* (reg8 *) RxOut__PRT) 
/* Connect to Analog Globals */                                                  
#define RxOut_AG                     (* (reg8 *) RxOut__AG)                       
/* Analog MUX bux enable */
#define RxOut_AMUX                   (* (reg8 *) RxOut__AMUX) 
/* Bidirectional Enable */                                                        
#define RxOut_BIE                    (* (reg8 *) RxOut__BIE)
/* Bit-mask for Aliased Register Access */
#define RxOut_BIT_MASK               (* (reg8 *) RxOut__BIT_MASK)
/* Bypass Enable */
#define RxOut_BYP                    (* (reg8 *) RxOut__BYP)
/* Port wide control signals */                                                   
#define RxOut_CTL                    (* (reg8 *) RxOut__CTL)
/* Drive Modes */
#define RxOut_DM0                    (* (reg8 *) RxOut__DM0) 
#define RxOut_DM1                    (* (reg8 *) RxOut__DM1)
#define RxOut_DM2                    (* (reg8 *) RxOut__DM2) 
/* Input Buffer Disable Override */
#define RxOut_INP_DIS                (* (reg8 *) RxOut__INP_DIS)
/* LCD Common or Segment Drive */
#define RxOut_LCD_COM_SEG            (* (reg8 *) RxOut__LCD_COM_SEG)
/* Enable Segment LCD */
#define RxOut_LCD_EN                 (* (reg8 *) RxOut__LCD_EN)
/* Slew Rate Control */
#define RxOut_SLW                    (* (reg8 *) RxOut__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define RxOut_PRTDSI__CAPS_SEL       (* (reg8 *) RxOut__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define RxOut_PRTDSI__DBL_SYNC_IN    (* (reg8 *) RxOut__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define RxOut_PRTDSI__OE_SEL0        (* (reg8 *) RxOut__PRTDSI__OE_SEL0) 
#define RxOut_PRTDSI__OE_SEL1        (* (reg8 *) RxOut__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define RxOut_PRTDSI__OUT_SEL0       (* (reg8 *) RxOut__PRTDSI__OUT_SEL0) 
#define RxOut_PRTDSI__OUT_SEL1       (* (reg8 *) RxOut__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define RxOut_PRTDSI__SYNC_OUT       (* (reg8 *) RxOut__PRTDSI__SYNC_OUT) 


#if defined(RxOut__INTSTAT)  /* Interrupt Registers */

    #define RxOut_INTSTAT                (* (reg8 *) RxOut__INTSTAT)
    #define RxOut_SNAP                   (* (reg8 *) RxOut__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins RxOut_H */

#endif
/* [] END OF FILE */
