/*******************************************************************************
* File Name: RxIn.h  
* Version 1.70
*
* Description:
*  This file containts Control Register function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_PINS_RxIn_H) /* Pins RxIn_H */
#define CY_PINS_RxIn_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "RxIn_aliases.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component cy_pins_v1_70 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 RxIn__PORT == 15 && (RxIn__MASK & 0xC0))

/***************************************
*        Function Prototypes             
***************************************/    

void    RxIn_Write(uint8 value) ;
void    RxIn_SetDriveMode(uint8 mode) ;
uint8   RxIn_ReadDataReg(void) ;
uint8   RxIn_Read(void) ;
uint8   RxIn_ClearInterrupt(void) ;

/***************************************
*           API Constants        
***************************************/

/* Drive Modes */
#define RxIn_DM_ALG_HIZ         PIN_DM_ALG_HIZ
#define RxIn_DM_DIG_HIZ         PIN_DM_DIG_HIZ
#define RxIn_DM_RES_UP          PIN_DM_RES_UP
#define RxIn_DM_RES_DWN         PIN_DM_RES_DWN
#define RxIn_DM_OD_LO           PIN_DM_OD_LO
#define RxIn_DM_OD_HI           PIN_DM_OD_HI
#define RxIn_DM_STRONG          PIN_DM_STRONG
#define RxIn_DM_RES_UPDWN       PIN_DM_RES_UPDWN

/* Digital Port Constants */
#define RxIn_MASK               RxIn__MASK
#define RxIn_SHIFT              RxIn__SHIFT
#define RxIn_WIDTH              1u

/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define RxIn_PS                     (* (reg8 *) RxIn__PS)
/* Data Register */
#define RxIn_DR                     (* (reg8 *) RxIn__DR)
/* Port Number */
#define RxIn_PRT_NUM                (* (reg8 *) RxIn__PRT) 
/* Connect to Analog Globals */                                                  
#define RxIn_AG                     (* (reg8 *) RxIn__AG)                       
/* Analog MUX bux enable */
#define RxIn_AMUX                   (* (reg8 *) RxIn__AMUX) 
/* Bidirectional Enable */                                                        
#define RxIn_BIE                    (* (reg8 *) RxIn__BIE)
/* Bit-mask for Aliased Register Access */
#define RxIn_BIT_MASK               (* (reg8 *) RxIn__BIT_MASK)
/* Bypass Enable */
#define RxIn_BYP                    (* (reg8 *) RxIn__BYP)
/* Port wide control signals */                                                   
#define RxIn_CTL                    (* (reg8 *) RxIn__CTL)
/* Drive Modes */
#define RxIn_DM0                    (* (reg8 *) RxIn__DM0) 
#define RxIn_DM1                    (* (reg8 *) RxIn__DM1)
#define RxIn_DM2                    (* (reg8 *) RxIn__DM2) 
/* Input Buffer Disable Override */
#define RxIn_INP_DIS                (* (reg8 *) RxIn__INP_DIS)
/* LCD Common or Segment Drive */
#define RxIn_LCD_COM_SEG            (* (reg8 *) RxIn__LCD_COM_SEG)
/* Enable Segment LCD */
#define RxIn_LCD_EN                 (* (reg8 *) RxIn__LCD_EN)
/* Slew Rate Control */
#define RxIn_SLW                    (* (reg8 *) RxIn__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define RxIn_PRTDSI__CAPS_SEL       (* (reg8 *) RxIn__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define RxIn_PRTDSI__DBL_SYNC_IN    (* (reg8 *) RxIn__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define RxIn_PRTDSI__OE_SEL0        (* (reg8 *) RxIn__PRTDSI__OE_SEL0) 
#define RxIn_PRTDSI__OE_SEL1        (* (reg8 *) RxIn__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define RxIn_PRTDSI__OUT_SEL0       (* (reg8 *) RxIn__PRTDSI__OUT_SEL0) 
#define RxIn_PRTDSI__OUT_SEL1       (* (reg8 *) RxIn__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define RxIn_PRTDSI__SYNC_OUT       (* (reg8 *) RxIn__PRTDSI__SYNC_OUT) 


#if defined(RxIn__INTSTAT)  /* Interrupt Registers */

    #define RxIn_INTSTAT                (* (reg8 *) RxIn__INTSTAT)
    #define RxIn_SNAP                   (* (reg8 *) RxIn__SNAP)

#endif /* Interrupt Registers */

#endif /* End Pins RxIn_H */

#endif
/* [] END OF FILE */
