/*******************************************************************************
* File Name: PhaseCounter_PM.c  
* Version 2.20
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "PhaseCounter.h"

static PhaseCounter_backupStruct PhaseCounter_backup;


/*******************************************************************************
* Function Name: PhaseCounter_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  PhaseCounter_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void PhaseCounter_SaveConfig(void) 
{
    #if (!PhaseCounter_UsingFixedFunction)
        /* Backup the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            PhaseCounter_backup.CounterUdb = PhaseCounter_ReadCounter();
            PhaseCounter_backup.CounterPeriod = PhaseCounter_ReadPeriod();
            PhaseCounter_backup.CompareValue = PhaseCounter_ReadCompare();
            PhaseCounter_backup.InterruptMaskValue = PhaseCounter_STATUS_MASK;
        #endif /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            PhaseCounter_backup.CounterUdb = PhaseCounter_ReadCounter();
            PhaseCounter_backup.InterruptMaskValue = PhaseCounter_STATUS_MASK;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!PhaseCounter_ControlRegRemoved)
            PhaseCounter_backup.CounterControlRegister = PhaseCounter_ReadControlRegister();
        #endif /* (!PhaseCounter_ControlRegRemoved) */
    #endif /* (!PhaseCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PhaseCounter_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  PhaseCounter_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void PhaseCounter_RestoreConfig(void) 
{      
    #if (!PhaseCounter_UsingFixedFunction)     
        /* Restore the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            /* Interrupt State Backup for Critical Region*/
            uint8 PhaseCounter_interruptState;
        
            PhaseCounter_WriteCounter(PhaseCounter_backup.CounterUdb);
            PhaseCounter_WritePeriod(PhaseCounter_backup.CounterPeriod);
            PhaseCounter_WriteCompare(PhaseCounter_backup.CompareValue);
            /* Enter Critical Region*/
            PhaseCounter_interruptState = CyEnterCriticalSection();
        
            PhaseCounter_STATUS_AUX_CTRL |= PhaseCounter_STATUS_ACTL_INT_EN_MASK;
            /* Exit Critical Region*/
            CyExitCriticalSection(PhaseCounter_interruptState);
            PhaseCounter_STATUS_MASK = PhaseCounter_backup.InterruptMaskValue;
        #endif  /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            PhaseCounter_WriteCounter(PhaseCounter_backup.CounterUdb);
            PhaseCounter_STATUS_MASK = PhaseCounter_backup.InterruptMaskValue;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!PhaseCounter_ControlRegRemoved)
            PhaseCounter_WriteControlRegister(PhaseCounter_backup.CounterControlRegister);
        #endif /* (!PhaseCounter_ControlRegRemoved) */
    #endif /* (!PhaseCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PhaseCounter_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  PhaseCounter_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void PhaseCounter_Sleep(void) 
{
    #if(!PhaseCounter_ControlRegRemoved)
        /* Save Counter's enable state */
        if(PhaseCounter_CTRL_ENABLE == (PhaseCounter_CONTROL & PhaseCounter_CTRL_ENABLE))
        {
            /* Counter is enabled */
            PhaseCounter_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            PhaseCounter_backup.CounterEnableState = 0u;
        }
    #endif /* (!PhaseCounter_ControlRegRemoved) */
    PhaseCounter_Stop();
    PhaseCounter_SaveConfig();
}


/*******************************************************************************
* Function Name: PhaseCounter_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  PhaseCounter_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void PhaseCounter_Wakeup(void) 
{
    PhaseCounter_RestoreConfig();
    #if(!PhaseCounter_ControlRegRemoved)
        if(PhaseCounter_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            PhaseCounter_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!PhaseCounter_ControlRegRemoved) */
    
}


/* [] END OF FILE */
