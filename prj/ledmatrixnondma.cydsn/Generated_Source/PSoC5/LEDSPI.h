/*******************************************************************************
* File Name: LEDSPI.h
* Version 2.30
*
* Description:
*  Contains the function prototypes, constants and register definition
*  of the SPI Master Component.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SPIM_LEDSPI_H)
#define CY_SPIM_LEDSPI_H

#include "cytypes.h"

/* Check to see if required defines such as CY_PSOC5A are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5A)
    #error Component SPI_Master_v2_30 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5A) */

/***************************************
*   Conditional Compilation Parameters
***************************************/

#define LEDSPI_DataWidth                  (8u)
#define LEDSPI_InternalClockUsed          (0u)
#define LEDSPI_InternalTxInterruptEnabled (0u)
#define LEDSPI_InternalRxInterruptEnabled (0u)
#define LEDSPI_ModeUseZero                (1u)
#define LEDSPI_BidirectionalMode          (0u)
#define LEDSPI_Mode                       (1u)

/* Following definitions are for version Compatibility, they are obsolete.
*  Please do not use it in new projects 
*/
#define LEDSPI_DATAWIDHT                (LEDSPI_DataWidth)
#define LEDSPI_InternalInterruptEnabled (0u)


/***************************************
*        Data Struct Definition
***************************************/

/* Sleep Mode API Support */
typedef struct _LEDSPI_backupStruct
{
    uint8 enableState;
    uint8 cntrPeriod;
    
    #if(CY_UDB_V0) /* CY_UDB_V0 */
    
        uint8 saveSrTxIntMask;
        uint8 saveSrRxIntMask;
        
    #endif /* CY_UDB_V0 */

} LEDSPI_BACKUP_STRUCT;


/***************************************
*        Function Prototypes
***************************************/

void  LEDSPI_Init(void) ;
void  LEDSPI_Enable(void) ;
void  LEDSPI_Start(void) ;
void  LEDSPI_Stop(void) ;
void  LEDSPI_EnableTxInt(void) ;
void  LEDSPI_EnableRxInt(void) ;
void  LEDSPI_DisableTxInt(void) ;
void  LEDSPI_DisableRxInt(void) ;
void  LEDSPI_SetTxInterruptMode(uint8 intSource) ;
void  LEDSPI_SetRxInterruptMode(uint8 intSource) ;
uint8 LEDSPI_ReadTxStatus(void) ;
uint8 LEDSPI_ReadRxStatus(void) ;
void  LEDSPI_WriteTxData(uint8 txData) ;
uint8 LEDSPI_ReadRxData(void) ;
uint8 LEDSPI_GetRxBufferSize(void) ;
uint8 LEDSPI_GetTxBufferSize(void) ;
void  LEDSPI_ClearRxBuffer(void) ;
void  LEDSPI_ClearTxBuffer(void) ;
void  LEDSPI_ClearFIFO(void) ;
void  LEDSPI_PutArray(uint8 *buffer, uint8 byteCount) ;
void  LEDSPI_Sleep(void) ;
void  LEDSPI_Wakeup(void) ;
void  LEDSPI_SaveConfig(void) ;
void  LEDSPI_RestoreConfig(void) ;

#if (LEDSPI_BidirectionalMode)

    void  LEDSPI_TxEnable(void) ;
    void  LEDSPI_TxDisable(void) ;

#endif /* LEDSPI_BidirectionalMode == 1u*/

CY_ISR_PROTO(LEDSPI_TX_ISR);
CY_ISR_PROTO(LEDSPI_RX_ISR);

/* Macros for getting software status of SPIM Statusi Register */
#define LEDSPI_GET_STATUS_TX(swTxSts) (uint8)(LEDSPI_TX_STATUS_REG | \
                                                       (swTxSts & LEDSPI_TX_STS_CLR_ON_RD_BYTES_MASK))
#define LEDSPI_GET_STATUS_RX(swRxSts) (uint8)(LEDSPI_RX_STATUS_REG | \
                                                       (swRxSts & LEDSPI_RX_STS_CLR_ON_RD_BYTES_MASK))                                                  

/* Following definitions are for version Compatibility, they are obsolete.
*  Please do not use it in new projects 
*/
#define LEDSPI_WriteByte (LEDSPI_WriteTxData)
#define LEDSPI_ReadByte  (LEDSPI_ReadRxData)
void  LEDSPI_SetInterruptMode(uint8 intSource) ;
uint8 LEDSPI_ReadStatus(void) ;
void  LEDSPI_EnableInt(void) ;
void  LEDSPI_DisableInt(void) ;


/***************************************
*           API Constants
***************************************/

#define LEDSPI_TX_ISR_NUMBER     (LEDSPI_TxInternalInterrupt__INTC_NUMBER)
#define LEDSPI_RX_ISR_NUMBER     (LEDSPI_RxInternalInterrupt__INTC_NUMBER)
#define LEDSPI_TX_ISR_PRIORITY   (LEDSPI_TxInternalInterrupt__INTC_PRIOR_NUM)
#define LEDSPI_RX_ISR_PRIORITY   (LEDSPI_RxInternalInterrupt__INTC_PRIOR_NUM)


/***************************************
*    Initial Parameter Constants
***************************************/
                                               
#define LEDSPI_INT_ON_SPI_DONE    (0u << LEDSPI_STS_SPI_DONE_SHIFT)
#define LEDSPI_INT_ON_TX_EMPTY    (0u << LEDSPI_STS_TX_FIFO_EMPTY_SHIFT)
#define LEDSPI_INT_ON_TX_NOT_FULL (0u << LEDSPI_STS_TX_FIFO_NOT_FULL_SHIFT)
#define LEDSPI_INT_ON_BYTE_COMP   (0u << LEDSPI_STS_BYTE_COMPLETE_SHIFT)
#define LEDSPI_INT_ON_SPI_IDLE    (0u << LEDSPI_STS_SPI_IDLE_SHIFT)

#define LEDSPI_TX_INIT_INTERRUPTS_MASK  (LEDSPI_INT_ON_SPI_DONE | \
                                            LEDSPI_INT_ON_TX_EMPTY | LEDSPI_INT_ON_TX_NOT_FULL | \
                                            LEDSPI_INT_ON_BYTE_COMP | LEDSPI_INT_ON_SPI_IDLE)
            
#define LEDSPI_INT_ON_RX_FULL      (0u << LEDSPI_STS_RX_FIFO_FULL_SHIFT)
#define LEDSPI_INT_ON_RX_NOT_EMPTY (0u << LEDSPI_STS_RX_FIFO_NOT_EMPTY_SHIFT)
#define LEDSPI_INT_ON_RX_OVER      (0u << LEDSPI_STS_RX_FIFO_OVERRUN_SHIFT)

#define LEDSPI_RX_INIT_INTERRUPTS_MASK  (LEDSPI_INT_ON_RX_FULL | \
                                            LEDSPI_INT_ON_RX_NOT_EMPTY | LEDSPI_INT_ON_RX_OVER)
                                               
#define LEDSPI_BITCTR_INIT           ( (LEDSPI_DataWidth << 1u) - 1u)

#define LEDSPI_TXBUFFERSIZE          (4u)
#define LEDSPI_RXBUFFERSIZE          (4u)

/* Following definitions are for version Compatibility, they are obsolete.
*  Please do not use it in new projects 
*/
#define LEDSPI_INIT_INTERRUPTS_MASK  (LEDSPI_INT_ON_SPI_DONE | LEDSPI_INT_ON_TX_EMPTY | \
                                            LEDSPI_INT_ON_TX_NOT_FULL | LEDSPI_INT_ON_RX_FULL | \
                                            LEDSPI_INT_ON_RX_NOT_EMPTY | LEDSPI_INT_ON_RX_OVER | \
                                            LEDSPI_INT_ON_BYTE_COMP)


/***************************************
*             Registers
***************************************/

#define LEDSPI_TXDATA_REG         (* (reg8 *) \
        LEDSPI_BSPIM_sR8_Dp_u0__F0_REG)
#define LEDSPI_TXDATA_PTR         (  (reg8 *) \
        LEDSPI_BSPIM_sR8_Dp_u0__F0_REG)
#define LEDSPI_RXDATA_REG         (* (reg8 *) \
        LEDSPI_BSPIM_sR8_Dp_u0__F1_REG)
#define LEDSPI_RXDATA_PTR         (  (reg8 *) \
        LEDSPI_BSPIM_sR8_Dp_u0__F1_REG)

#define LEDSPI_AUX_CONTROL_DP0_REG (* (reg8 *) \
        LEDSPI_BSPIM_sR8_Dp_u0__DP_AUX_CTL_REG)
#define LEDSPI_AUX_CONTROL_DP0_PTR (  (reg8 *) \
        LEDSPI_BSPIM_sR8_Dp_u0__DP_AUX_CTL_REG)        

#if (LEDSPI_DataWidth > 8u)

    #define LEDSPI_AUX_CONTROL_DP1_REG  (* (reg8 *) \
            LEDSPI_BSPIM_sR8_Dp_u1__DP_AUX_CTL_REG)
    #define LEDSPI_AUX_CONTROL_DP1_PTR  (  (reg8 *) \
            LEDSPI_BSPIM_sR8_Dp_u1__DP_AUX_CTL_REG)
#endif /* LEDSPI_DataWidth > 8u */

#define LEDSPI_COUNTER_PERIOD_REG  (* (reg8 *)  LEDSPI_BSPIM_BitCounter__PERIOD_REG)
#define LEDSPI_COUNTER_PERIOD_PTR  (  (reg8 *)  LEDSPI_BSPIM_BitCounter__PERIOD_REG)
#define LEDSPI_COUNTER_CONTROL_REG (* (reg8 *)  LEDSPI_BSPIM_BitCounter__CONTROL_AUX_CTL_REG)
#define LEDSPI_COUNTER_CONTROL_PTR (  (reg8 *)  LEDSPI_BSPIM_BitCounter__CONTROL_AUX_CTL_REG)

#define LEDSPI_TX_STATUS_REG       (* (reg8 *)  LEDSPI_BSPIM_TxStsReg__STATUS_REG)
#define LEDSPI_TX_STATUS_PTR       (  (reg8 *)  LEDSPI_BSPIM_TxStsReg__STATUS_REG)
#define LEDSPI_RX_STATUS_REG       (* (reg8 *)  LEDSPI_BSPIM_RxStsReg__STATUS_REG)
#define LEDSPI_RX_STATUS_PTR       (  (reg8 *)  LEDSPI_BSPIM_RxStsReg__STATUS_REG)

#define LEDSPI_CONTROL_REG         (* (reg8 *)  \
        LEDSPI_BSPIM_BidirMode_AsyncCtl_CtrlReg__CONTROL_REG)
#define LEDSPI_CONTROL_PTR         (  (reg8 *)  \
        LEDSPI_BSPIM_BidirMode_AsyncCtl_CtrlReg__CONTROL_REG)
        
#define LEDSPI_TX_STATUS_MASK_REG  (* (reg8 *)  LEDSPI_BSPIM_TxStsReg__MASK_REG)
#define LEDSPI_TX_STATUS_MASK_PTR  (  (reg8 *)  LEDSPI_BSPIM_TxStsReg__MASK_REG)
#define LEDSPI_RX_STATUS_MASK_REG  (* (reg8 *)  LEDSPI_BSPIM_RxStsReg__MASK_REG)
#define LEDSPI_RX_STATUS_MASK_PTR  (  (reg8 *)  LEDSPI_BSPIM_RxStsReg__MASK_REG)

#define LEDSPI_TX_STATUS_ACTL_REG  (* (reg8 *)  LEDSPI_BSPIM_TxStsReg__STATUS_AUX_CTL_REG)
#define LEDSPI_TX_STATUS_ACTL_PTR  (  (reg8 *)  LEDSPI_BSPIM_TxStsReg__STATUS_AUX_CTL_REG)
#define LEDSPI_RX_STATUS_ACTL_REG  (* (reg8 *)  LEDSPI_BSPIM_RxStsReg__STATUS_AUX_CTL_REG)
#define LEDSPI_RX_STATUS_ACTL_PTR  (  (reg8 *)  LEDSPI_BSPIM_RxStsReg__STATUS_AUX_CTL_REG)

/* Obsolete register names. Not to be used in new designs */
#define LEDSPI_TXDATA                 (LEDSPI_TXDATA_REG)
#define LEDSPI_RXDATA                 (LEDSPI_RXDATA_REG)
#define LEDSPI_AUX_CONTROLDP0         (LEDSPI_AUX_CONTROL_DP0_REG)
#define LEDSPI_TXBUFFERREAD           (LEDSPI_txBufferRead)
#define LEDSPI_TXBUFFERWRITE          (LEDSPI_txBufferWrite)
#define LEDSPI_RXBUFFERREAD           (LEDSPI_rxBufferRead)
#define LEDSPI_RXBUFFERWRITE          (LEDSPI_rxBufferWrite)

#if(LEDSPI_DataWidth > 8u)    
    #define LEDSPI_AUX_CONTROLDP1     (LEDSPI_AUX_CONTROL_DP1_REG)    
#endif /* LEDSPI_DataWidth > 8u */
    
#define LEDSPI_COUNTER_PERIOD         (LEDSPI_COUNTER_PERIOD_REG)
#define LEDSPI_COUNTER_CONTROL        (LEDSPI_COUNTER_CONTROL_REG)
#define LEDSPI_STATUS                 (LEDSPI_TX_STATUS_REG)
#define LEDSPI_CONTROL                (LEDSPI_CONTROL_REG)
#define LEDSPI_STATUS_MASK            (LEDSPI_TX_STATUS_MASK_REG)
#define LEDSPI_STATUS_ACTL            (LEDSPI_TX_STATUS_ACTL_REG)


/***************************************
*       Register Constants 
***************************************/

/* Status Register Definitions */
#define LEDSPI_STS_SPI_DONE_SHIFT          (0x00u)
#define LEDSPI_STS_TX_FIFO_EMPTY_SHIFT     (0x01u)
#define LEDSPI_STS_TX_FIFO_NOT_FULL_SHIFT  (0x02u)
#define LEDSPI_STS_BYTE_COMPLETE_SHIFT     (0x03u)
#define LEDSPI_STS_SPI_IDLE_SHIFT          (0x04u)
#define LEDSPI_STS_RX_FIFO_FULL_SHIFT      (0x04u)
#define LEDSPI_STS_RX_FIFO_NOT_EMPTY_SHIFT (0x05u)
#define LEDSPI_STS_RX_FIFO_OVERRUN_SHIFT   (0x06u)

#define LEDSPI_STS_SPI_DONE                (0x01u << LEDSPI_STS_SPI_DONE_SHIFT)        
#define LEDSPI_STS_TX_FIFO_EMPTY           (0x01u << LEDSPI_STS_TX_FIFO_EMPTY_SHIFT)    
#define LEDSPI_STS_TX_FIFO_NOT_FULL        (0x01u << LEDSPI_STS_TX_FIFO_NOT_FULL_SHIFT)    
#define LEDSPI_STS_SPI_IDLE                (0x01u << LEDSPI_STS_SPI_IDLE_SHIFT)
#define LEDSPI_STS_RX_FIFO_FULL            (0x01u << LEDSPI_STS_RX_FIFO_FULL_SHIFT)    
#define LEDSPI_STS_RX_FIFO_NOT_EMPTY       (0x01u << LEDSPI_STS_RX_FIFO_NOT_EMPTY_SHIFT)    
#define LEDSPI_STS_RX_FIFO_OVERRUN         (0x01u << LEDSPI_STS_RX_FIFO_OVERRUN_SHIFT)  
#define LEDSPI_STS_BYTE_COMPLETE           (0x01u << LEDSPI_STS_BYTE_COMPLETE_SHIFT)

#define LEDSPI_TX_STS_CLR_ON_RD_BYTES_MASK (0x09u)
#define LEDSPI_RX_STS_CLR_ON_RD_BYTES_MASK (0x40u)

/* StatusI Register Interrupt Enable Control Bits */
/* As defined by the Register map for the AUX Control Register */
#define LEDSPI_INT_ENABLE                  (0x10u)
#define LEDSPI_FIFO_CLR                    (0x03u)
                                                                 
/* Bit Counter (7-bit) Control Register Bit Definitions */
/* As defined by the Register map for the AUX Control Register */
#define LEDSPI_CNTR_ENABLE                 (0x20u)   
                                                                  
/* Bi-Directional mode control bit */
#define LEDSPI_CTRL_TX_SIGNAL_EN           (0x01u)

/* Datapath Auxillary Control Register definitions */
#define LEDSPI_AUX_CTRL_FIFO0_CLR          (0x01u)
#define LEDSPI_AUX_CTRL_FIFO1_CLR          (0x02u)
#define LEDSPI_AUX_CTRL_FIFO0_LVL          (0x04u)
#define LEDSPI_AUX_CTRL_FIFO1_LVL          (0x08u)
#define LEDSPI_STATUS_ACTL_INT_EN_MASK     (0x10u)

#endif  /* CY_SPIM_LEDSPI_H */


/* [] END OF FILE */
