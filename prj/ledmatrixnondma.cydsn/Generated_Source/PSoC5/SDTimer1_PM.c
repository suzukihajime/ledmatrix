/*******************************************************************************
* File Name: SDTimer1_PM.c  
* Version 2.20
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "SDTimer1.h"

static SDTimer1_backupStruct SDTimer1_backup;


/*******************************************************************************
* Function Name: SDTimer1_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  SDTimer1_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void SDTimer1_SaveConfig(void) 
{
    #if (!SDTimer1_UsingFixedFunction)
        /* Backup the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            SDTimer1_backup.CounterUdb = SDTimer1_ReadCounter();
            SDTimer1_backup.CounterPeriod = SDTimer1_ReadPeriod();
            SDTimer1_backup.CompareValue = SDTimer1_ReadCompare();
            SDTimer1_backup.InterruptMaskValue = SDTimer1_STATUS_MASK;
        #endif /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            SDTimer1_backup.CounterUdb = SDTimer1_ReadCounter();
            SDTimer1_backup.InterruptMaskValue = SDTimer1_STATUS_MASK;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!SDTimer1_ControlRegRemoved)
            SDTimer1_backup.CounterControlRegister = SDTimer1_ReadControlRegister();
        #endif /* (!SDTimer1_ControlRegRemoved) */
    #endif /* (!SDTimer1_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer1_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  SDTimer1_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void SDTimer1_RestoreConfig(void) 
{      
    #if (!SDTimer1_UsingFixedFunction)     
        /* Restore the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            /* Interrupt State Backup for Critical Region*/
            uint8 SDTimer1_interruptState;
        
            SDTimer1_WriteCounter(SDTimer1_backup.CounterUdb);
            SDTimer1_WritePeriod(SDTimer1_backup.CounterPeriod);
            SDTimer1_WriteCompare(SDTimer1_backup.CompareValue);
            /* Enter Critical Region*/
            SDTimer1_interruptState = CyEnterCriticalSection();
        
            SDTimer1_STATUS_AUX_CTRL |= SDTimer1_STATUS_ACTL_INT_EN_MASK;
            /* Exit Critical Region*/
            CyExitCriticalSection(SDTimer1_interruptState);
            SDTimer1_STATUS_MASK = SDTimer1_backup.InterruptMaskValue;
        #endif  /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            SDTimer1_WriteCounter(SDTimer1_backup.CounterUdb);
            SDTimer1_STATUS_MASK = SDTimer1_backup.InterruptMaskValue;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!SDTimer1_ControlRegRemoved)
            SDTimer1_WriteControlRegister(SDTimer1_backup.CounterControlRegister);
        #endif /* (!SDTimer1_ControlRegRemoved) */
    #endif /* (!SDTimer1_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer1_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  SDTimer1_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void SDTimer1_Sleep(void) 
{
    #if(!SDTimer1_ControlRegRemoved)
        /* Save Counter's enable state */
        if(SDTimer1_CTRL_ENABLE == (SDTimer1_CONTROL & SDTimer1_CTRL_ENABLE))
        {
            /* Counter is enabled */
            SDTimer1_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            SDTimer1_backup.CounterEnableState = 0u;
        }
    #endif /* (!SDTimer1_ControlRegRemoved) */
    SDTimer1_Stop();
    SDTimer1_SaveConfig();
}


/*******************************************************************************
* Function Name: SDTimer1_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  SDTimer1_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void SDTimer1_Wakeup(void) 
{
    SDTimer1_RestoreConfig();
    #if(!SDTimer1_ControlRegRemoved)
        if(SDTimer1_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            SDTimer1_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!SDTimer1_ControlRegRemoved) */
    
}


/* [] END OF FILE */
