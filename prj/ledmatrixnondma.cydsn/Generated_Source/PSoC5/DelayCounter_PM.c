/*******************************************************************************
* File Name: DelayCounter_PM.c  
* Version 2.20
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "DelayCounter.h"

static DelayCounter_backupStruct DelayCounter_backup;


/*******************************************************************************
* Function Name: DelayCounter_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  DelayCounter_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void DelayCounter_SaveConfig(void) 
{
    #if (!DelayCounter_UsingFixedFunction)
        /* Backup the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            DelayCounter_backup.CounterUdb = DelayCounter_ReadCounter();
            DelayCounter_backup.CounterPeriod = DelayCounter_ReadPeriod();
            DelayCounter_backup.CompareValue = DelayCounter_ReadCompare();
            DelayCounter_backup.InterruptMaskValue = DelayCounter_STATUS_MASK;
        #endif /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            DelayCounter_backup.CounterUdb = DelayCounter_ReadCounter();
            DelayCounter_backup.InterruptMaskValue = DelayCounter_STATUS_MASK;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!DelayCounter_ControlRegRemoved)
            DelayCounter_backup.CounterControlRegister = DelayCounter_ReadControlRegister();
        #endif /* (!DelayCounter_ControlRegRemoved) */
    #endif /* (!DelayCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: DelayCounter_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  DelayCounter_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void DelayCounter_RestoreConfig(void) 
{      
    #if (!DelayCounter_UsingFixedFunction)     
        /* Restore the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            /* Interrupt State Backup for Critical Region*/
            uint8 DelayCounter_interruptState;
        
            DelayCounter_WriteCounter(DelayCounter_backup.CounterUdb);
            DelayCounter_WritePeriod(DelayCounter_backup.CounterPeriod);
            DelayCounter_WriteCompare(DelayCounter_backup.CompareValue);
            /* Enter Critical Region*/
            DelayCounter_interruptState = CyEnterCriticalSection();
        
            DelayCounter_STATUS_AUX_CTRL |= DelayCounter_STATUS_ACTL_INT_EN_MASK;
            /* Exit Critical Region*/
            CyExitCriticalSection(DelayCounter_interruptState);
            DelayCounter_STATUS_MASK = DelayCounter_backup.InterruptMaskValue;
        #endif  /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            DelayCounter_WriteCounter(DelayCounter_backup.CounterUdb);
            DelayCounter_STATUS_MASK = DelayCounter_backup.InterruptMaskValue;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!DelayCounter_ControlRegRemoved)
            DelayCounter_WriteControlRegister(DelayCounter_backup.CounterControlRegister);
        #endif /* (!DelayCounter_ControlRegRemoved) */
    #endif /* (!DelayCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: DelayCounter_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  DelayCounter_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void DelayCounter_Sleep(void) 
{
    #if(!DelayCounter_ControlRegRemoved)
        /* Save Counter's enable state */
        if(DelayCounter_CTRL_ENABLE == (DelayCounter_CONTROL & DelayCounter_CTRL_ENABLE))
        {
            /* Counter is enabled */
            DelayCounter_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            DelayCounter_backup.CounterEnableState = 0u;
        }
    #endif /* (!DelayCounter_ControlRegRemoved) */
    DelayCounter_Stop();
    DelayCounter_SaveConfig();
}


/*******************************************************************************
* Function Name: DelayCounter_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  DelayCounter_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void DelayCounter_Wakeup(void) 
{
    DelayCounter_RestoreConfig();
    #if(!DelayCounter_ControlRegRemoved)
        if(DelayCounter_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            DelayCounter_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!DelayCounter_ControlRegRemoved) */
    
}


/* [] END OF FILE */
