/*******************************************************************************
* File Name: phase_irq.h
* Version 1.60
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/
#if !defined(__phase_irq_INTC_H__)
#define __phase_irq_INTC_H__


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void phase_irq_Start(void);
void phase_irq_StartEx(cyisraddress address);
void phase_irq_Stop(void) ;

CY_ISR_PROTO(phase_irq_Interrupt);

void phase_irq_SetVector(cyisraddress address) ;
cyisraddress phase_irq_GetVector(void) ;

void phase_irq_SetPriority(uint8 priority) ;
uint8 phase_irq_GetPriority(void) ;

void phase_irq_Enable(void) ;
uint8 phase_irq_GetState(void) ;
void phase_irq_Disable(void) ;

void phase_irq_SetPending(void) ;
void phase_irq_ClearPending(void) ;


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the phase_irq ISR. */
#define phase_irq_INTC_VECTOR            ((reg32 *) phase_irq__INTC_VECT)

/* Address of the phase_irq ISR priority. */
#define phase_irq_INTC_PRIOR             ((reg8 *) phase_irq__INTC_PRIOR_REG)

/* Priority of the phase_irq interrupt. */
#define phase_irq_INTC_PRIOR_NUMBER      phase_irq__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable phase_irq interrupt. */
#define phase_irq_INTC_SET_EN            ((reg32 *) phase_irq__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the phase_irq interrupt. */
#define phase_irq_INTC_CLR_EN            ((reg32 *) phase_irq__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the phase_irq interrupt state to pending. */
#define phase_irq_INTC_SET_PD            ((reg32 *) phase_irq__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the phase_irq interrupt. */
#define phase_irq_INTC_CLR_PD            ((reg32 *) phase_irq__INTC_CLR_PD_REG)



/* __phase_irq_INTC_H__ */
#endif
