/*******************************************************************************
* File Name: RxOut.c  
* Version 1.70
*
* Description:
*  This file contains API to enable firmware control of a Pins component.
*
* Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cytypes.h"
#include "RxOut.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 RxOut__PORT == 15 && (RxOut__MASK & 0xC0))

/*******************************************************************************
* Function Name: RxOut_Write
********************************************************************************
* Summary:
*  Assign a new value to the digital port's data output register.  
*
* Parameters:  
*  prtValue:  The value to be assigned to the Digital Port. 
*
* Return: 
*  void 
*  
*******************************************************************************/
void RxOut_Write(uint8 value) 
{
    uint8 staticBits = RxOut_DR & ~RxOut_MASK;
    RxOut_DR = staticBits | ((value << RxOut_SHIFT) & RxOut_MASK);
}


/*******************************************************************************
* Function Name: RxOut_SetDriveMode
********************************************************************************
* Summary:
*  Change the drive mode on the pins of the port.
* 
* Parameters:  
*  mode:  Change the pins to this drive mode.
*
* Return: 
*  void
*
*******************************************************************************/
void RxOut_SetDriveMode(uint8 mode) 
{
	CyPins_SetPinDriveMode(RxOut_0, mode);
}


/*******************************************************************************
* Function Name: RxOut_Read
********************************************************************************
* Summary:
*  Read the current value on the pins of the Digital Port in right justified 
*  form.
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value of the Digital Port as a right justified number
*  
* Note:
*  Macro RxOut_ReadPS calls this function. 
*  
*******************************************************************************/
uint8 RxOut_Read(void) 
{
    return (RxOut_PS & RxOut_MASK) >> RxOut_SHIFT;
}


/*******************************************************************************
* Function Name: RxOut_ReadDataReg
********************************************************************************
* Summary:
*  Read the current value assigned to a Digital Port's data output register
*
* Parameters:  
*  void 
*
* Return: 
*  Returns the current value assigned to the Digital Port's data output register
*  
*******************************************************************************/
uint8 RxOut_ReadDataReg(void) 
{
    return (RxOut_DR & RxOut_MASK) >> RxOut_SHIFT;
}


/* If Interrupts Are Enabled for this Pins component */ 
#if defined(RxOut_INTSTAT) 

    /*******************************************************************************
    * Function Name: RxOut_ClearInterrupt
    ********************************************************************************
    * Summary:
    *  Clears any active interrupts attached to port and returns the value of the 
    *  interrupt status register.
    *
    * Parameters:  
    *  void 
    *
    * Return: 
    *  Returns the value of the interrupt status register
    *  
    *******************************************************************************/
    uint8 RxOut_ClearInterrupt(void) 
    {
        return (RxOut_INTSTAT & RxOut_MASK) >> RxOut_SHIFT;
    }

#endif /* If Interrupts Are Enabled for this Pins component */ 

#endif
/* [] END OF FILE */ 
