/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"

#define FIFO_SIZE	(256)

inline int16 fifoGetChar(void);
inline void fifoPushChar(int16 c);
inline void fifoPushString(char *str);
inline uint16 fifoGetContentSize(void);
inline uint16 fifoGetVacancySize(void);


//[] END OF FILE
