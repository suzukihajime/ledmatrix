/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"
#include "matrix.h"

// Literal table
const int ADD[8] = {0, 1, 0, 1, 32, 33, 32, 33};
const int CONV[16] =
	{7, 6, 5, 4, 3, 2, 1, 0, 15, 14, 13, 12, 11, 10, 9, 8};

uint8 disp_buf[0x400];
int g_addr;
int g_phase = 0;

CY_ISR(reload)
{
//	uprintf("reload\r\n");
	send_data(g_addr, g_phase++);
	return;
}

void Matrix_Start(void)
{
	int i;
	LEDSPI_Start();
	LEDSPI_EnableInt();
	LEDSPI_ClearRxBuffer();
	LEDSPI_ClearTxBuffer();
	RCK_Write(0);	
	phase_irq_StartEx(reload);
/*	for(i = 0; i < 128; i++) {
		disp_buf[i] = i;
	}*/
	return;
}

void Matrix_Stop(void)
{
	return;
}

uint8 *Matrix_GetDispBuffer(void)
{
	return(disp_buf);
}

void Matrix_SetBaseAddress(int addr)
{
	g_addr = addr;
	return;
}

int send_data(int addr, int phase)
{
	int i, nadr;
	
//	uprintf("st\n");
	TEST_Write(~TEST_Read());
	while(!(LEDSPI_ReadTxStatus() & LEDSPI_STS_TX_FIFO_NOT_FULL)) {}
	if(phase & 0x010) {
		nadr = 0x40 | (CONV[phase & 0xF]<<1);
		LEDSPI_WriteTxData(nadr);
	} else {
		nadr = 0x20 | (CONV[phase & 0xF]<<1);
		LEDSPI_WriteTxData(nadr);
	}
//	uprintf("ph:%d(%d)\n", phase, nadr);
	for(i = 0; i < 8; i++)
	{
		while(!(LEDSPI_ReadTxStatus() & LEDSPI_STS_TX_FIFO_NOT_FULL)) {}
		nadr = (addr + ADD[i] + ((phase & 0x0010)<<2) + ((phase & 0x000F)<<1)) & 0x3ff;
//		uprintf("%d(%d)\n", nadr, disp_buf[nadr]);
		LEDSPI_WriteTxData(disp_buf[nadr]);
//		LEDSPI_WriteTxData(disp_buf[(addr + ADD[i] + ((phase & 0x0010)<<2) + ((phase & 0x000F)<<1)) & 0x3ff]);
	}
	
	while(!(LEDSPI_ReadTxStatus() & LEDSPI_STS_TX_FIFO_NOT_FULL)) {}
	RCK_Write(1);
	for(i = 0; i < 10; i++) {}
	RCK_Write(0);
	return 0;
}

/* [] END OF FILE */
