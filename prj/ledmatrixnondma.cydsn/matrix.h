/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "device.h"

void reload(void);			// interrupt handler function
void Matrix_Start(void);
void Matrix_Stop(void);
uint8 *Matrix_GetDispBuffer(void);
void Matrix_SetBaseAddress(int addr);
int send_data(int addr, int phase);

//[] END OF FILE
