/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "device.h"

//uint16 fifoGetNextUtf8();
uint16 parseUtf8(char c);
void clearParser(void);
uint16 decodeUtf8(char *buf);


//[] END OF FILE
