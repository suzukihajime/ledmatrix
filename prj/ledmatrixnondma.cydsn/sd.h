/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <device.h>
#include <cytypes.h>
#include "sdc/ff.h"


#ifndef	SDCARD
#define	SDCARD


typedef struct {
	BYTE bInserted;
	BYTE bInitialized;
	BYTE bOpen;
	FILINFO fi;
	FIL file;
	char filename[20];
} SDSTATUS;

// Initialize SD Card
uint8 SD_Init(SDSTATUS *sd);
uint8 SD_Slot1Inserted(void);
uint8 SD_OpenFile(SDSTATUS *sd, FIL *file, char *filename);
uint8 SD_CloseFile(SDSTATUS *sd, FIL *file);
//unsigned int SD_ReadLine(SDSTATUS *sd, char *buf, int len);
void SD_Close(void);
DWORD get_fattime(void);


#endif

//[] END OF FILE
