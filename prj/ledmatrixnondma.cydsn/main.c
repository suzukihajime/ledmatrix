/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <device.h>
#include "font.h"
#include "matrix.h"
#include "utf8.h"
#include "uart.h"
#include "fifo.h"

void initialize(void);
void buf_initialize(uint8 *buf);

/*
const unsigned char num[220] = {
		0x07, 0xF0, 0x08, 0x08, 0x10, 0x04, 0x20, 0x02, 0x20, 0x02, 0x20, 0x02, 0x20, 0x02, 0x10, 0x04, 0x08, 0x08, 0x07, 0xF0,
		0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x08, 0x00, 0x08, 0x00, 0x18, 0x00, 0x3F, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		0x06, 0x06, 0x18, 0x0A, 0x10, 0x12, 0x20, 0x22, 0x20, 0x42, 0x20, 0x42, 0x20, 0x82, 0x20, 0x82, 0x11, 0x02, 0x0F, 0x02,
		0x0C, 0x38, 0x10, 0x04, 0x20, 0x02, 0x20, 0x02, 0x20, 0x82, 0x20, 0x82, 0x20, 0x82, 0x20, 0x82, 0x11, 0xC4, 0x0E, 0x38,
		0x00, 0x60, 0x00, 0xA0, 0x01, 0x20, 0x06, 0x20, 0x08, 0x20, 0x10, 0x20, 0x20, 0x20, 0x3F, 0xFE, 0x00, 0x20, 0x00, 0x20,
		0x03, 0xC4, 0x3C, 0x82, 0x21, 0x02, 0x22, 0x02, 0x22, 0x02, 0x22, 0x02, 0x22, 0x02, 0x22, 0x04, 0x21, 0x04, 0x00, 0xF8,
		0x07, 0xF0, 0x08, 0x88, 0x11, 0x04, 0x22, 0x02, 0x22, 0x02, 0x22, 0x02, 0x22, 0x02, 0x22, 0x02, 0x11, 0x04, 0x08, 0xF8,
		0x20, 0x00, 0x20, 0x00, 0x20, 0x00, 0x20, 0x02, 0x20, 0x1C, 0x20, 0x60, 0x21, 0x80, 0x2E, 0x00, 0x30, 0x00, 0x00, 0x00,
		0x0C, 0x78, 0x12, 0x84, 0x21, 0x02, 0x21, 0x02, 0x21, 0x02, 0x21, 0x02, 0x21, 0x02, 0x21, 0x02, 0x12, 0x84, 0x0C, 0x78,
		0x0F, 0x88, 0x10, 0x44, 0x20, 0x22, 0x20, 0x22, 0x20, 0x22, 0x20, 0x22, 0x20, 0x22, 0x10, 0x44, 0x08, 0x48, 0x07, 0xF0,
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x06, 0x03, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
*/
/*
int g_addr = 0;
int bpp, bpl, len;
unsigned char g_buffer[0x400];
SDSTATUS g_sd;
*/
/*
 * Initialize
 */
/*
void init(void)
{
	BITMAPINFOHEADER bih;
	BITMAPFILEHEADER bfh;
	
	CyGlobalIntEnable;
	
	LEDSPI_Start();
	LEDSPI_EnableInt();
	LEDSPI_ClearRxBuffer();
	LEDSPI_ClearTxBuffer();
	RCK_Write(0);
	
	PhaseCounter_Start();
	PhaseCounter_WritePeriod(499);
	DelayCounter_Start();
	
	isr_1_StartEx(reload);
	
	UART_Init();
	SD_Init(&g_sd);
	SD_OpenFile (&g_sd, "msg.bmp");
	
	SD_ReadLine(&g_sd, (char *)&bfh, 14);
	SD_ReadLine(&g_sd, (char *)&bih, 40);
	switch(bih.biBitCount)
	{
	  case 24: bpp = 3; break;
	  case 32: bpp = 4; break;
	  default: bpp = 0; break;
	}
	uprintf("bpp = %d\r\n", bpp);
	uprintf("height = %d\r\n", bih.biHeight);
	len = bih.biHeight;
	bpl = bih.biWidth * bpp;
	if(bpl % 4 != 0) { bpl = ((bpl / 4) + 1) * 4; }
	g_addr = 0;
	return;
}
*/

/*
 * Loop
 */
/*
void loop(void)
{
	char buf[2048];
	unsigned char temp;
	static int ptr = 0;
	int j;
	
	SD_ReadLine(&g_sd, buf, bpl);
	for(j = 0; j < 16; j++)
	{
		temp<<=1;
		temp |= (buf[j*bpp] > 10) ? 0 : 0x01;
		if(j == 7)
		{
			write((g_addr + 128) & 0x3ff, temp);
			temp = 0;
		}
		if(j == 15)
		{
			write((g_addr + 128 + 1) & 0x3ff, temp);
			temp = 0;
		}
	}
	g_addr = (g_addr + 2) & 0x3ff;
	ptr++;
	if(ptr > len)
	{
		SD_SetPointer(sizeof(BITMAPINFOHEADER) + sizeof(BITMAPFILEHEADER));
		ptr = 0;
	}
	DelayCounter_WriteCounter(30);
	while(DelayCounter_ReadCounter() != 0) {}
	return;
}
*/

uint8 *disp_buf_base;

/*
* interrupt handler
*/
uint32 rcnt = 0, rpos = 0, wpos = 0;
uint8 blank_cnt = 0;
void shift_isr(void)
{
	Matrix_SetBaseAddress((rpos*16 + rcnt*2) & 0x3ff);
	if(++rcnt >= 8) {
		rcnt = 0;
		rpos++;
		// すでに表示が終わったバッファはクリアする。
		buf_initialize(&disp_buf_base[((rpos-12)*16)&0x3ff]);
	}
	if(wpos < (rpos + 12)) {
		wpos = rpos + 12;
	}
	return;
}

void rx_isr(void)
{
	char c;
	int16 utf;
	TEST_Write(1);
	/*
	ここの処理が微妙。テストが必要。
	*/
	while(UART_1_ReadRxStatus() & UART_1_RX_STS_FIFO_NOTEMPTY) {
		c = UART_1_ReadRxData();
	//	uputc(c);
		if((utf = parseUtf8(c))) {
			fifoPushChar(utf);
//			uprintf("c = %d\n", utf);
		}
	}
	TEST_Write(0);
	return;
}

void main()
{
	int i;
	uint16 uni;
	volatile uint32 addr;
//	char utf[1024] = "統計的機械学習とは、観測されたデータから統計的手法を用い新たな知識を導出することである。統計的機械学習についての教科書的な内容はこちらを参照してほしい。";
	initialize();
	disp_buf_base = Matrix_GetDispBuffer();
/*	for(i = 0; i < 128; i++) {
		disp_buf_base[i] = i;
	}*/
	/*
	ループでフォントを展開する
	*/
	while(1) {
		if((rpos+32) > wpos) {
			// 新しく書き込みできる。
			if(fifoGetContentSize()) {
//				TEST_Write(1);			// 時間計測の必要あり
				uni = fifoGetChar();
				/* ASCIIかどうかで処理をわける */
				if(uni > 255) {
					uprintf("%x\n", uni);
					addr = getFontAddress(uni);
					if(wpos & 0x01) { wpos++; }
					expandFontBitmapPackedMemory(addr, &disp_buf_base[(wpos*16) & 0x3ff], 0xff, FONT_ROP_COPY);
					wpos += 2;
				} else {
					expandAsciiBitmap(uni, &disp_buf_base[(wpos*16) & 0x3ff], 0xff, FONT_ROP_COPY);
					wpos++;
				}
//				TEST_Write(0);
			}
		}
		uprintf("r: %d, w: %d\n", rpos, wpos);
	}
	closeFont();
}

void initialize(void)
{
	uint8 ret;
	
	UART_Start();
	Timer_Start();
	Timer_WriteCounter(0);
	buf_initialize(disp_buf_base);
	Matrix_Start();
	Matrix_SetBaseAddress(0);
	ret = openFont("JISKAN1.BDF", "CTABLE3.TXT");
	uprintf("ret = %d\n", ret);
//	FFT_Start();
	
	CyGlobalIntEnable;
	shift_irq_StartEx(shift_isr);
	rx_irq_StartEx(rx_isr);
//	disp_buf[ret] = 0xff;
	rpos = wpos = 0;
	return;
}

void buf_initialize(uint8 buf[])
{
	int16 i;
	for(i = 0; i < 16; i++) {
		buf[i] = 0x00;
	}
	return;
}

void delay_ms(uint16 delay)
{
	Timer_WriteCounter(delay);
	while(Timer_ReadCounter() != 0) {}
	return;
}

/* [] END OF FILE */
