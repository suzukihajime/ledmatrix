/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef BMP
#define	BMP


typedef struct tagBITMAPFILEHEADER {
  unsigned short bfType;
  unsigned int  bfSize;
  unsigned short bfReserved1;
  unsigned short bfReserved2;
  unsigned int  bfOffBits;
} BITMAPFILEHEADER;


typedef struct tagBITMAPINFOHEADER{
    unsigned int  biSize;
    int           biWidth;
    int           biHeight;
    unsigned short biPlanes;
    unsigned short biBitCount;
    unsigned int  biCompression;
    unsigned int  biSizeImage;
    int           biXPixPerMeter;
    int           biYPixPerMeter;
    unsigned int  biClrUsed;
    unsigned int  biClrImporant;
} BITMAPINFOHEADER;





#endif

//[] END OF FILE
