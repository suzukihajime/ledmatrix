/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <device.h>
#include "sd.h"
#include <cytypes.h>
#include "uart.h"


FATFS fatfs, *fs;
DIR rootdir;
FIL file;


SDSTATUS g_sd;


BYTE SD_Init(SDSTATUS *sd)
{
	BYTE res;
	
	g_sd.bInserted = 0;
	g_sd.bInitialized = 0;
	g_sd.bOpen = 0;
	copystatus(&g_sd, sd);
	// Initialize SPI (used to communicate with SD card) running at 1 MHz
	SPIM_1_Init();
	SPIM_1_Enable();
	SPIM_1_EnableInt();
	SPIM_1_Start();
	SPIM_1_ClearRxBuffer();
	SPIM_1_ClearTxBuffer();
	
	SDTimer1_Start();
	SDTimer2_Start();
	// Initialize SD card
	if(SD_Slot1Inserted() == 0)
	{
//		iputs("Not inserted\r\n");
		g_sd.bInserted = 0;
		g_sd.bInitialized = 0;
		g_sd.bOpen = 0;
		copystatus(&g_sd, sd);
		return 1;
	}
	g_sd.bInserted = 1;
	res = f_mount(0, &fatfs);			// Mount drive #0
	if(res != FR_OK) { return 1; }
	res = f_opendir(&rootdir, "");	// Open root directory
	if(res != FR_OK) { return 1; }
	g_sd.bInitialized = 1;
	g_sd.bOpen = 1;
	copystatus(&g_sd, sd);
	return 0;
}


BYTE SD_Slot1Inserted(void)
{
//	if(CardDetect_sRead() != 0)
//		return 0;
//	else
		return 1;
}


BYTE SD_OpenFile(SDSTATUS *sd, char *filename)
{
	int res;
	
	printf("OpenFile\r\n");
	if(SD_Slot1Inserted() == 0)
	{
		g_sd.bInserted = 0;
		g_sd.bInitialized = 0;
		g_sd.bOpen = 0;
		copystatus(&g_sd, sd);
		return 1;
	}
	file.fs = &fatfs;
	res = f_open(&file, filename, FA_READ | FA_OPEN_EXISTING);
	if(res == FR_OK)
	{
		printf("Open Succeeded\r\n");
		g_sd.bOpen = 1;
		copystatus(&g_sd, sd);
		return 0;
	}
	else
	{
		printf("Open Failed\r\n");
		f_close(&file);
		return 1;
	}
}


BYTE SD_CloseFile(SDSTATUS *sd)
{
	f_close(&file);
	return 0;
}


unsigned int SD_ReadLine(SDSTATUS *sd, char *buf, int len)
{
	unsigned int i;
	
	if(SD_Slot1Inserted() == 0)
	{
		g_sd.bInserted = 0;
		g_sd.bInitialized = 0;
		g_sd.bOpen = 0;
		copystatus(&g_sd, sd);
		return 1;
	}
	f_read(&file, buf, len, &i);
	if(i != len)
	{
		return 0;
	}
	return(len);
}


void SD_SetPointer(unsigned int ptr)
{
	f_lseek(&file, ptr);
	return;
}


void SD_Close(void)
{
	f_close(&file);
//	SD_Power_Write(0);			// Power Off
	return;
}


void copystatus(SDSTATUS *src, SDSTATUS *dest)
{
	dest->bInserted = src->bInserted;
	dest->bInitialized = src->bInitialized;
	dest->bOpen = src->bOpen;
	return;
}


DWORD get_fattime(void)
{
	DWORD tmr;
	
	CYGlobalIntDisable;
	/* Pack date and time into a DWORD variable */
	tmr =	  (((DWORD)2011 - 1980) << 25)
			| ((DWORD)11 << 21)
			| ((DWORD)21 << 16)
			| ((WORD)0 << 11)
			| ((WORD)0 << 5)
			| ((WORD)0 >> 1);
	CYGlobalIntEnable;

	return tmr;
}


void SDTimer1_Set(int ms)
{
//	Timer1_Reset_Write(1);
//	Timer1_Reset_Write(0);
	SDTimer1_WriteCounter(ms);
	return;
}


void SDTimer1_Delayms(int ms)
{
	SDTimer1_Set(ms);
	while(SDTimer1_ReadCounter() != 0) {}
	return;
}


void SDTimer2_Set(int ms)
{
//	Timer1_Reset_Write(1);
//	Timer1_Reset_Write(0);
	SDTimer2_WriteCounter(ms);
	return;
}


void SDTimer2_Delayms(int ms)
{
	SDTimer2_Set(ms);
	while(SDTimer2_ReadCounter() != 0) {}
	return;
}



/* [] END OF FILE */