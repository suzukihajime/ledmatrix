/*******************************************************************************
* File Name: LEDSPI_INT.c
* Version 2.30
*
* Description:
*  This file provides all Interrupt Service Routine (ISR) for the SPI Master
*  component.
*
* Note:
*  None.
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "LEDSPI.h"

#if (LEDSPI_RXBUFFERSIZE > 4u)

    extern volatile uint8 LEDSPI_RXBUFFER[];
    extern volatile uint8 LEDSPI_rxBufferRead;
    extern volatile uint8 LEDSPI_rxBufferWrite;
    extern volatile uint8 LEDSPI_rxBufferFull;
    
#endif /* LEDSPI_RXBUFFERSIZE > 4u */

#if (LEDSPI_TXBUFFERSIZE > 4u)

    extern volatile uint8 LEDSPI_TXBUFFER[];
    extern volatile uint8 LEDSPI_txBufferRead;
    extern volatile uint8 LEDSPI_txBufferWrite;
    extern volatile uint8 LEDSPI_txBufferFull;

#endif /* LEDSPI_TXBUFFERSIZE > 4u */

volatile uint8 LEDSPI_swStatusTx = 0u;
volatile uint8 LEDSPI_swStatusRx = 0u;

/* User code required at start of ISR */
/* `#START LEDSPI_ISR_START_DEF` */

/* `#END` */


/*******************************************************************************
* Function Name: LEDSPI_TX_ISR
********************************************************************************
*
* Summary:
*  Interrupt Service Routine for TX portion of the SPI Master.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  LEDSPI_txBufferWrite - used for the account of the bytes which
*  have been written down in the TX software buffer.
*  LEDSPI_txBufferRead - used for the account of the bytes which
*  have been read from the TX software buffer, modified when exist data to 
*  sending and FIFO Not Full.
*  LEDSPI_TXBUFFER[LEDSPI_TXBUFFERSIZE] - used to store
*  data to sending.
*  All described above Global variables are used when Software Buffer is used.
*
*******************************************************************************/
CY_ISR(LEDSPI_TX_ISR)
{     
    /* User code required at start of ISR */
    /* `#START LEDSPI_TX_ISR_START` */

    /* `#END` */
    
    #if((LEDSPI_InternalTxInterruptEnabled) && (LEDSPI_TXBUFFERSIZE > 4u))
                         
        /* See if TX data buffer is not empty and there is space in TX FIFO */
        while(LEDSPI_txBufferRead != LEDSPI_txBufferWrite)
        {
            LEDSPI_swStatusTx = LEDSPI_GET_STATUS_TX(LEDSPI_swStatusTx);
            
            if ((LEDSPI_swStatusTx & LEDSPI_STS_TX_FIFO_NOT_FULL) != 0u)
            {            
                if(LEDSPI_txBufferFull == 0u)
                {
                   LEDSPI_txBufferRead++;

                    if(LEDSPI_txBufferRead >= LEDSPI_TXBUFFERSIZE)
                    {
                        LEDSPI_txBufferRead = 0u;
                    }
                }
                else
                {
                    LEDSPI_txBufferFull = 0u;
                }
            
                /* Move data from the Buffer to the FIFO */
                CY_SET_REG8(LEDSPI_TXDATA_PTR,
                    LEDSPI_TXBUFFER[LEDSPI_txBufferRead]);
            }
            else
            {
                break;
            }            
        }
            
        /* Disable Interrupt on TX_fifo_not_empty if BUFFER is empty */
        if(LEDSPI_txBufferRead == LEDSPI_txBufferWrite)
        {
            LEDSPI_TX_STATUS_MASK_REG  &= ~LEDSPI_STS_TX_FIFO_NOT_FULL; 
        }                       
        
	#endif /* LEDSPI_InternalTxInterruptEnabled && (LEDSPI_TXBUFFERSIZE > 4u) */
    
    /* User code required at end of ISR (Optional) */
    /* `#START LEDSPI_TX_ISR_END` */

    /* `#END` */
   
}


/*******************************************************************************
* Function Name: LEDSPI_RX_ISR
********************************************************************************
*
* Summary:
*  Interrupt Service Routine for RX portion of the SPI Master.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  LEDSPI_rxBufferWrite - used for the account of the bytes which
*  have been written down in the RX software buffer modified when FIFO contains
*  new data.
*  LEDSPI_rxBufferRead - used for the account of the bytes which
*  have been read from the RX software buffer, modified when overflow occurred.
*  LEDSPI_RXBUFFER[LEDSPI_RXBUFFERSIZE] - used to store
*  received data, modified when FIFO contains new data.
*  All described above Global variables are used when Software Buffer is used.
*
*******************************************************************************/
CY_ISR(LEDSPI_RX_ISR)
{     
    #if((LEDSPI_InternalRxInterruptEnabled) && (LEDSPI_RXBUFFERSIZE > 4u))
        uint8 rxData = 0u; 
    #endif /* LEDSPI_InternalRxInterruptEnabled */  
    
    /* User code required at start of ISR */
    /* `#START LEDSPI_RX_ISR_START` */

    /* `#END` */
    
    #if((LEDSPI_InternalRxInterruptEnabled) && (LEDSPI_RXBUFFERSIZE > 4u))
         
        LEDSPI_swStatusRx = LEDSPI_GET_STATUS_RX(LEDSPI_swStatusRx);          
        
        /* See if RX data FIFO has some data and if it can be moved to the RX Buffer */
        while((LEDSPI_swStatusRx & LEDSPI_STS_RX_FIFO_NOT_EMPTY) == 
                                                                                LEDSPI_STS_RX_FIFO_NOT_EMPTY)
        {
            rxData = CY_GET_REG8(LEDSPI_RXDATA_PTR);
            
            /* Set next pointer. */
            LEDSPI_rxBufferWrite++;
            if(LEDSPI_rxBufferWrite >= LEDSPI_RXBUFFERSIZE)
            {
                LEDSPI_rxBufferWrite = 0u;
            }
            
            if(LEDSPI_rxBufferWrite == LEDSPI_rxBufferRead)
            {
                LEDSPI_rxBufferRead++;
                if(LEDSPI_rxBufferRead >= LEDSPI_RXBUFFERSIZE)
                {
                    LEDSPI_rxBufferRead = 0u;
                }
                LEDSPI_rxBufferFull = 1u;
            }
            
            /* Move data from the FIFO to the Buffer */
            LEDSPI_RXBUFFER[LEDSPI_rxBufferWrite] = rxData;
                
            LEDSPI_swStatusRx = LEDSPI_GET_STATUS_RX(LEDSPI_swStatusRx);
        }                    
        
	#endif /* LEDSPI_InternalRxInterruptEnabled  && (LEDSPI_RXBUFFERSIZE > 4u) */        
    
    /* User code required at end of ISR (Optional) */
    /* `#START LEDSPI_RX_ISR_END` */

    /* `#END` */
    
}

/* [] END OF FILE */
