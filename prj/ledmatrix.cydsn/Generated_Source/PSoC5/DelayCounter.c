/*******************************************************************************
* File Name: DelayCounter.c  
* Version 2.20
*
*  Description:
*     The Counter component consists of a 8, 16, 24 or 32-bit counter with
*     a selectable period between 2 and 2^Width - 1.  
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "DelayCounter.h"

uint8 DelayCounter_initVar = 0u;


/*******************************************************************************
* Function Name: DelayCounter_Init
********************************************************************************
* Summary:
*     Initialize to the schematic state
* 
* Parameters:  
*  void  
*
* Return: 
*  void
*
*******************************************************************************/
void DelayCounter_Init(void) 
{
        #if (!DelayCounter_UsingFixedFunction && !DelayCounter_ControlRegRemoved)
            uint8 ctrl;
        #endif /* (!DelayCounter_UsingFixedFunction && !DelayCounter_ControlRegRemoved) */
        
        #if(!DelayCounter_UsingFixedFunction) 
            /* Interrupt State Backup for Critical Region*/
            uint8 DelayCounter_interruptState;
        #endif /* (!DelayCounter_UsingFixedFunction) */
        
        #if (DelayCounter_UsingFixedFunction)
            /* Clear all bits but the enable bit (if it's already set for Timer operation */
            DelayCounter_CONTROL &= DelayCounter_CTRL_ENABLE;
            
            /* Clear the mode bits for continuous run mode */
            #if (CY_PSOC5A)
                DelayCounter_CONTROL2 &= ~DelayCounter_CTRL_MODE_MASK;
            #endif /* (CY_PSOC5A) */
            #if (CY_PSOC3 || CY_PSOC5LP)
                DelayCounter_CONTROL3 &= ~DelayCounter_CTRL_MODE_MASK;                
            #endif /* (CY_PSOC3 || CY_PSOC5LP) */
            /* Check if One Shot mode is enabled i.e. RunMode !=0*/
            #if (DelayCounter_RunModeUsed != 0x0u)
                /* Set 3rd bit of Control register to enable one shot mode */
                DelayCounter_CONTROL |= DelayCounter_ONESHOT;
            #endif /* (DelayCounter_RunModeUsed != 0x0u) */
            
            /* Set the IRQ to use the status register interrupts */
            DelayCounter_CONTROL2 |= DelayCounter_CTRL2_IRQ_SEL;
            
            /* Clear and Set SYNCTC and SYNCCMP bits of RT1 register */
            DelayCounter_RT1 &= ~DelayCounter_RT1_MASK;
            DelayCounter_RT1 |= DelayCounter_SYNC;     
                    
            /*Enable DSI Sync all all inputs of the Timer*/
            DelayCounter_RT1 &= ~(DelayCounter_SYNCDSI_MASK);
            DelayCounter_RT1 |= DelayCounter_SYNCDSI_EN;

        #else
            #if(!DelayCounter_ControlRegRemoved)
            /* Set the default compare mode defined in the parameter */
            ctrl = DelayCounter_CONTROL & ~DelayCounter_CTRL_CMPMODE_MASK;
            DelayCounter_CONTROL = ctrl | DelayCounter_DEFAULT_COMPARE_MODE;
            
            /* Set the default capture mode defined in the parameter */
            ctrl = DelayCounter_CONTROL & ~DelayCounter_CTRL_CAPMODE_MASK;
            DelayCounter_CONTROL = ctrl | DelayCounter_DEFAULT_CAPTURE_MODE;
            #endif /* (!DelayCounter_ControlRegRemoved) */
        #endif /* (DelayCounter_UsingFixedFunction) */
        
        /* Clear all data in the FIFO's */
        #if (!DelayCounter_UsingFixedFunction)
            DelayCounter_ClearFIFO();
        #endif /* (!DelayCounter_UsingFixedFunction) */
        
        /* Set Initial values from Configuration */
        DelayCounter_WritePeriod(DelayCounter_INIT_PERIOD_VALUE);
        #if (!(DelayCounter_UsingFixedFunction && (CY_PSOC5A)))
            DelayCounter_WriteCounter(DelayCounter_INIT_COUNTER_VALUE);
        #endif /* (!(DelayCounter_UsingFixedFunction && (CY_PSOC5A))) */
        DelayCounter_SetInterruptMode(DelayCounter_INIT_INTERRUPTS_MASK);
        
        #if (!DelayCounter_UsingFixedFunction)
            /* Read the status register to clear the unwanted interrupts */
            DelayCounter_ReadStatusRegister();
            /* Set the compare value (only available to non-fixed function implementation */
            DelayCounter_WriteCompare(DelayCounter_INIT_COMPARE_VALUE);
            /* Use the interrupt output of the status register for IRQ output */
            
            /* CyEnterCriticalRegion and CyExitCriticalRegion are used to mark following region critical*/
            /* Enter Critical Region*/
            DelayCounter_interruptState = CyEnterCriticalSection();
            
            DelayCounter_STATUS_AUX_CTRL |= DelayCounter_STATUS_ACTL_INT_EN_MASK;
            
            /* Exit Critical Region*/
            CyExitCriticalSection(DelayCounter_interruptState);
            
        #endif /* (!DelayCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: DelayCounter_Enable
********************************************************************************
* Summary:
*     Enable the Counter
* 
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Side Effects: 
*   If the Enable mode is set to Hardware only then this function has no effect 
*   on the operation of the counter.
*
*******************************************************************************/
void DelayCounter_Enable(void) 
{
    /* Globally Enable the Fixed Function Block chosen */
    #if (DelayCounter_UsingFixedFunction)
        DelayCounter_GLOBAL_ENABLE |= DelayCounter_BLOCK_EN_MASK;
        DelayCounter_GLOBAL_STBY_ENABLE |= DelayCounter_BLOCK_STBY_EN_MASK;
    #endif /* (DelayCounter_UsingFixedFunction) */  
        
    /* Enable the counter from the control register  */
    /* If Fixed Function then make sure Mode is set correctly */
    /* else make sure reset is clear */
    #if(!DelayCounter_ControlRegRemoved || DelayCounter_UsingFixedFunction)
        DelayCounter_CONTROL |= DelayCounter_CTRL_ENABLE;                
    #endif /* (!DelayCounter_ControlRegRemoved || DelayCounter_UsingFixedFunction) */
    
}


/*******************************************************************************
* Function Name: DelayCounter_Start
********************************************************************************
* Summary:
*  Enables the counter for operation 
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Global variables:
*  DelayCounter_initVar: Is modified when this function is called for the  
*   first time. Is used to ensure that initialization happens only once.
*
*******************************************************************************/
void DelayCounter_Start(void) 
{
    if(DelayCounter_initVar == 0u)
    {
        DelayCounter_Init();
        
        DelayCounter_initVar = 1u; /* Clear this bit for Initialization */        
    }
    
    /* Enable the Counter */
    DelayCounter_Enable();        
}


/*******************************************************************************
* Function Name: DelayCounter_Stop
********************************************************************************
* Summary:
* Halts the counter, but does not change any modes or disable interrupts.
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Side Effects: If the Enable mode is set to Hardware only then this function
*               has no effect on the operation of the counter.
*
*******************************************************************************/
void DelayCounter_Stop(void) 
{
    /* Disable Counter */
    #if(!DelayCounter_ControlRegRemoved || DelayCounter_UsingFixedFunction)
        DelayCounter_CONTROL &= ~DelayCounter_CTRL_ENABLE;        
    #endif /* (!DelayCounter_ControlRegRemoved || DelayCounter_UsingFixedFunction) */
    
    /* Globally disable the Fixed Function Block chosen */
    #if (DelayCounter_UsingFixedFunction)
        DelayCounter_GLOBAL_ENABLE &= ~DelayCounter_BLOCK_EN_MASK;
        DelayCounter_GLOBAL_STBY_ENABLE &= ~DelayCounter_BLOCK_STBY_EN_MASK;
    #endif /* (DelayCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: DelayCounter_SetInterruptMode
********************************************************************************
* Summary:
* Configures which interrupt sources are enabled to generate the final interrupt
*
* Parameters:  
*  InterruptsMask: This parameter is an or'd collection of the status bits
*                   which will be allowed to generate the counters interrupt.   
*
* Return: 
*  void
*
*******************************************************************************/
void DelayCounter_SetInterruptMode(uint8 interruptsMask) 
{
    DelayCounter_STATUS_MASK = interruptsMask;
}


/*******************************************************************************
* Function Name: DelayCounter_ReadStatusRegister
********************************************************************************
* Summary:
*   Reads the status register and returns it's state. This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the status register
*
* Side Effects:
*   Status register bits may be clear on read. 
*
*******************************************************************************/
uint8   DelayCounter_ReadStatusRegister(void) 
{
    return DelayCounter_STATUS;
}


#if(!DelayCounter_ControlRegRemoved)
/*******************************************************************************
* Function Name: DelayCounter_ReadControlRegister
********************************************************************************
* Summary:
*   Reads the control register and returns it's state. This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the control register
*
*******************************************************************************/
uint8   DelayCounter_ReadControlRegister(void) 
{
    return DelayCounter_CONTROL;
}


/*******************************************************************************
* Function Name: DelayCounter_WriteControlRegister
********************************************************************************
* Summary:
*   Sets the bit-field of the control register.  This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the control register
*
*******************************************************************************/
void    DelayCounter_WriteControlRegister(uint8 control) 
{
    DelayCounter_CONTROL = control;
}

#endif  /* (!DelayCounter_ControlRegRemoved) */


#if (!(DelayCounter_UsingFixedFunction && (CY_PSOC5A)))
/*******************************************************************************
* Function Name: DelayCounter_WriteCounter
********************************************************************************
* Summary:
*   This funtion is used to set the counter to a specific value
*
* Parameters:  
*  counter:  New counter value. 
*
* Return: 
*  void 
*
*******************************************************************************/
void DelayCounter_WriteCounter(uint16 counter) \
                                   
{
    #if(DelayCounter_UsingFixedFunction)
        /* assert if block is already enabled */
        CYASSERT (!(DelayCounter_GLOBAL_ENABLE & DelayCounter_BLOCK_EN_MASK));
        /* If block is disabled, enable it and then write the counter */
        DelayCounter_GLOBAL_ENABLE |= DelayCounter_BLOCK_EN_MASK;
        CY_SET_REG16(DelayCounter_COUNTER_LSB_PTR, (uint16)counter);
        DelayCounter_GLOBAL_ENABLE &= ~DelayCounter_BLOCK_EN_MASK;
    #else
        CY_SET_REG16(DelayCounter_COUNTER_LSB_PTR, counter);
    #endif /* (DelayCounter_UsingFixedFunction) */
}
#endif /* (!(DelayCounter_UsingFixedFunction && (CY_PSOC5A))) */


/*******************************************************************************
* Function Name: DelayCounter_ReadCounter
********************************************************************************
* Summary:
* Returns the current value of the counter.  It doesn't matter
* if the counter is enabled or running.
*
* Parameters:  
*  void:  
*
* Return: 
*  (uint16) The present value of the counter.
*
*******************************************************************************/
uint16 DelayCounter_ReadCounter(void) 
{
    /* Force capture by reading Accumulator */
    /* Must first do a software capture to be able to read the counter */
    /* It is up to the user code to make sure there isn't already captured data in the FIFO */
    CY_GET_REG8(DelayCounter_COUNTER_LSB_PTR);
    
    /* Read the data from the FIFO (or capture register for Fixed Function)*/
    return (CY_GET_REG16(DelayCounter_STATICCOUNT_LSB_PTR));
}


/*******************************************************************************
* Function Name: DelayCounter_ReadCapture
********************************************************************************
* Summary:
*   This function returns the last value captured.
*
* Parameters:  
*  void
*
* Return: 
*  (uint16) Present Capture value.
*
*******************************************************************************/
uint16 DelayCounter_ReadCapture(void) 
{
   return ( CY_GET_REG16(DelayCounter_STATICCOUNT_LSB_PTR) );  
}


/*******************************************************************************
* Function Name: DelayCounter_WritePeriod
********************************************************************************
* Summary:
* Changes the period of the counter.  The new period 
* will be loaded the next time terminal count is detected.
*
* Parameters:  
*  period: (uint16) A value of 0 will result in
*         the counter remaining at zero.  
*
* Return: 
*  void
*
*******************************************************************************/
void DelayCounter_WritePeriod(uint16 period) 
{
    #if(DelayCounter_UsingFixedFunction)
        CY_SET_REG16(DelayCounter_PERIOD_LSB_PTR,(uint16)period);
    #else
        CY_SET_REG16(DelayCounter_PERIOD_LSB_PTR,period);
    #endif /* (DelayCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: DelayCounter_ReadPeriod
********************************************************************************
* Summary:
* Reads the current period value without affecting counter operation.
*
* Parameters:  
*  void:  
*
* Return: 
*  (uint16) Present period value.
*
*******************************************************************************/
uint16 DelayCounter_ReadPeriod(void) 
{
   return ( CY_GET_REG16(DelayCounter_PERIOD_LSB_PTR));
}


#if (!DelayCounter_UsingFixedFunction)
/*******************************************************************************
* Function Name: DelayCounter_WriteCompare
********************************************************************************
* Summary:
* Changes the compare value.  The compare output will 
* reflect the new value on the next UDB clock.  The compare output will be 
* driven high when the present counter value compares true based on the 
* configured compare mode setting. 
*
* Parameters:  
*  Compare:  New compare value. 
*
* Return: 
*  void
*
*******************************************************************************/
void DelayCounter_WriteCompare(uint16 compare) \
                                   
{
    #if(DelayCounter_UsingFixedFunction)
        CY_SET_REG16(DelayCounter_COMPARE_LSB_PTR,(uint16)compare);
    #else
        CY_SET_REG16(DelayCounter_COMPARE_LSB_PTR,compare);
    #endif /* (DelayCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: DelayCounter_ReadCompare
********************************************************************************
* Summary:
* Returns the compare value.
*
* Parameters:  
*  void:
*
* Return: 
*  (uint16) Present compare value.
*
*******************************************************************************/
uint16 DelayCounter_ReadCompare(void) 
{
   return ( CY_GET_REG16(DelayCounter_COMPARE_LSB_PTR));
}


#if (DelayCounter_COMPARE_MODE_SOFTWARE)
/*******************************************************************************
* Function Name: DelayCounter_SetCompareMode
********************************************************************************
* Summary:
*  Sets the software controlled Compare Mode.
*
* Parameters:
*  compareMode:  Compare Mode Enumerated Type.
*
* Return:
*  void
*
*******************************************************************************/
void DelayCounter_SetCompareMode(uint8 compareMode) 
{
    /* Clear the compare mode bits in the control register */
    DelayCounter_CONTROL &= ~DelayCounter_CTRL_CMPMODE_MASK;
    
    /* Write the new setting */
    DelayCounter_CONTROL |= (compareMode << DelayCounter_CTRL_CMPMODE0_SHIFT);
}
#endif  /* (DelayCounter_COMPARE_MODE_SOFTWARE) */


#if (DelayCounter_CAPTURE_MODE_SOFTWARE)
/*******************************************************************************
* Function Name: DelayCounter_SetCaptureMode
********************************************************************************
* Summary:
*  Sets the software controlled Capture Mode.
*
* Parameters:
*  captureMode:  Capture Mode Enumerated Type.
*
* Return:
*  void
*
*******************************************************************************/
void DelayCounter_SetCaptureMode(uint8 captureMode) 
{
    /* Clear the capture mode bits in the control register */
    DelayCounter_CONTROL &= ~DelayCounter_CTRL_CAPMODE_MASK;
    
    /* Write the new setting */
    DelayCounter_CONTROL |= (captureMode << DelayCounter_CTRL_CAPMODE0_SHIFT);
}
#endif  /* (DelayCounter_CAPTURE_MODE_SOFTWARE) */


/*******************************************************************************
* Function Name: DelayCounter_ClearFIFO
********************************************************************************
* Summary:
*   This function clears all capture data from the capture FIFO
*
* Parameters:  
*  void:
*
* Return: 
*  None
*
*******************************************************************************/
void DelayCounter_ClearFIFO(void) 
{

    while(DelayCounter_ReadStatusRegister() & DelayCounter_STATUS_FIFONEMP)
    {
        DelayCounter_ReadCapture();
    }

}
#endif  /* (!DelayCounter_UsingFixedFunction) */


/* [] END OF FILE */

