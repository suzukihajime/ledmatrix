/*******************************************************************************
* File Name: SDTimer2_PM.c  
* Version 2.20
*
*  Description:
*    This file provides the power management source code to API for the
*    Counter.  
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "SDTimer2.h"

static SDTimer2_backupStruct SDTimer2_backup;


/*******************************************************************************
* Function Name: SDTimer2_SaveConfig
********************************************************************************
* Summary:
*     Save the current user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  SDTimer2_backup:  Variables of this global structure are modified to 
*  store the values of non retention configuration registers when Sleep() API is 
*  called.
*
*******************************************************************************/
void SDTimer2_SaveConfig(void) 
{
    #if (!SDTimer2_UsingFixedFunction)
        /* Backup the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            SDTimer2_backup.CounterUdb = SDTimer2_ReadCounter();
            SDTimer2_backup.CounterPeriod = SDTimer2_ReadPeriod();
            SDTimer2_backup.CompareValue = SDTimer2_ReadCompare();
            SDTimer2_backup.InterruptMaskValue = SDTimer2_STATUS_MASK;
        #endif /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            SDTimer2_backup.CounterUdb = SDTimer2_ReadCounter();
            SDTimer2_backup.InterruptMaskValue = SDTimer2_STATUS_MASK;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!SDTimer2_ControlRegRemoved)
            SDTimer2_backup.CounterControlRegister = SDTimer2_ReadControlRegister();
        #endif /* (!SDTimer2_ControlRegRemoved) */
    #endif /* (!SDTimer2_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer2_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  SDTimer2_backup:  Variables of this global structure are used to 
*  restore the values of non retention registers on wakeup from sleep mode.
*
*******************************************************************************/
void SDTimer2_RestoreConfig(void) 
{      
    #if (!SDTimer2_UsingFixedFunction)     
        /* Restore the UDB non-rentention registers for PSoC5A */
        #if (CY_PSOC5A)
            /* Interrupt State Backup for Critical Region*/
            uint8 SDTimer2_interruptState;
        
            SDTimer2_WriteCounter(SDTimer2_backup.CounterUdb);
            SDTimer2_WritePeriod(SDTimer2_backup.CounterPeriod);
            SDTimer2_WriteCompare(SDTimer2_backup.CompareValue);
            /* Enter Critical Region*/
            SDTimer2_interruptState = CyEnterCriticalSection();
        
            SDTimer2_STATUS_AUX_CTRL |= SDTimer2_STATUS_ACTL_INT_EN_MASK;
            /* Exit Critical Region*/
            CyExitCriticalSection(SDTimer2_interruptState);
            SDTimer2_STATUS_MASK = SDTimer2_backup.InterruptMaskValue;
        #endif  /* (CY_PSOC5A) */
        
        #if (CY_PSOC3 || CY_PSOC5LP)
            SDTimer2_WriteCounter(SDTimer2_backup.CounterUdb);
            SDTimer2_STATUS_MASK = SDTimer2_backup.InterruptMaskValue;
        #endif /* (CY_PSOC3 || CY_PSOC5LP) */
        
        #if(!SDTimer2_ControlRegRemoved)
            SDTimer2_WriteControlRegister(SDTimer2_backup.CounterControlRegister);
        #endif /* (!SDTimer2_ControlRegRemoved) */
    #endif /* (!SDTimer2_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer2_Sleep
********************************************************************************
* Summary:
*     Stop and Save the user configuration
*
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  SDTimer2_backup.enableState:  Is modified depending on the enable 
*  state of the block before entering sleep mode.
*
*******************************************************************************/
void SDTimer2_Sleep(void) 
{
    #if(!SDTimer2_ControlRegRemoved)
        /* Save Counter's enable state */
        if(SDTimer2_CTRL_ENABLE == (SDTimer2_CONTROL & SDTimer2_CTRL_ENABLE))
        {
            /* Counter is enabled */
            SDTimer2_backup.CounterEnableState = 1u;
        }
        else
        {
            /* Counter is disabled */
            SDTimer2_backup.CounterEnableState = 0u;
        }
    #endif /* (!SDTimer2_ControlRegRemoved) */
    SDTimer2_Stop();
    SDTimer2_SaveConfig();
}


/*******************************************************************************
* Function Name: SDTimer2_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  SDTimer2_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void SDTimer2_Wakeup(void) 
{
    SDTimer2_RestoreConfig();
    #if(!SDTimer2_ControlRegRemoved)
        if(SDTimer2_backup.CounterEnableState == 1u)
        {
            /* Enable Counter's operation */
            SDTimer2_Enable();
        } /* Do nothing if Counter was disabled before */    
    #endif /* (!SDTimer2_ControlRegRemoved) */
    
}


/* [] END OF FILE */
