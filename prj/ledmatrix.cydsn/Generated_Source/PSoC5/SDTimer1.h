/*******************************************************************************
* File Name: SDTimer1.h  
* Version 2.20
*
*  Description:
*   Contains the function prototypes and constants available to the counter
*   user module.
*
*   Note:
*    None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/
    
#if !defined(CY_COUNTER_SDTimer1_H)
#define CY_COUNTER_SDTimer1_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CyLib.h" /* For CyEnterCriticalSection() and CyExitCriticalSection() functions */

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component Counter_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */


/**************************************
*           Parameter Defaults        
**************************************/

#define SDTimer1_Resolution            16u
#define SDTimer1_UsingFixedFunction    0u
#define SDTimer1_ControlRegRemoved     0u
#define SDTimer1_COMPARE_MODE_SOFTWARE 0u
#define SDTimer1_CAPTURE_MODE_SOFTWARE 0u
#define SDTimer1_RunModeUsed           0u


/***************************************
*       Type defines
***************************************/


/**************************************************************************
 * Sleep Mode API Support
 * Backup structure for Sleep Wake up operations
 *************************************************************************/
typedef struct SDTimer1_backupStruct
{
    /* Sleep BackUp structure */
    uint8 CounterEnableState; 
    #if (CY_PSOC5A)
        uint16 CounterUdb;    /* Current Counter Value      */
        uint16 CounterPeriod; /* Counter Period Value       */
        uint16 CompareValue;  /* Counter Compare Value      */           
        uint8 InterruptMaskValue; /* Counter Compare Value */
    #endif /* (CY_PSOC5A) */

    #if (CY_PSOC3 || CY_PSOC5LP)
            uint16 CounterUdb;
            uint8 InterruptMaskValue;
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */

    #if (!SDTimer1_ControlRegRemoved)
        uint8 CounterControlRegister;          /* Counter Control Register   */
    #endif /* (!SDTimer1_ControlRegRemoved) */
}SDTimer1_backupStruct;


/**************************************
 *  Function Prototypes
 *************************************/
void    SDTimer1_Start(void) ;
void    SDTimer1_Stop(void) ;
void    SDTimer1_SetInterruptMode(uint8 interruptsMask) ;
uint8   SDTimer1_ReadStatusRegister(void) ;
#define SDTimer1_GetInterruptSource() SDTimer1_ReadStatusRegister()
#if(!SDTimer1_ControlRegRemoved)
    uint8   SDTimer1_ReadControlRegister(void) ;
    void    SDTimer1_WriteControlRegister(uint8 control) \
        ;
#endif /* (!SDTimer1_ControlRegRemoved) */
#if (!(SDTimer1_UsingFixedFunction && (CY_PSOC5A)))
    void    SDTimer1_WriteCounter(uint16 counter) \
            ; 
#endif /* (!(SDTimer1_UsingFixedFunction && (CY_PSOC5A))) */
uint16  SDTimer1_ReadCounter(void) ;
uint16  SDTimer1_ReadCapture(void) ;
void    SDTimer1_WritePeriod(uint16 period) \
    ;
uint16  SDTimer1_ReadPeriod( void ) ;
#if (!SDTimer1_UsingFixedFunction)
    void    SDTimer1_WriteCompare(uint16 compare) \
        ;
    uint16  SDTimer1_ReadCompare( void ) \
        ;
#endif /* (!SDTimer1_UsingFixedFunction) */

#if (SDTimer1_COMPARE_MODE_SOFTWARE)
    void    SDTimer1_SetCompareMode(uint8 comparemode) ;
#endif /* (SDTimer1_COMPARE_MODE_SOFTWARE) */
#if (SDTimer1_CAPTURE_MODE_SOFTWARE)
    void    SDTimer1_SetCaptureMode(uint8 capturemode) ;
#endif /* (SDTimer1_CAPTURE_MODE_SOFTWARE) */
void SDTimer1_ClearFIFO(void)     ;
void SDTimer1_Init(void)          ;
void SDTimer1_Enable(void)        ;
void SDTimer1_SaveConfig(void)    ;
void SDTimer1_RestoreConfig(void) ;
void SDTimer1_Sleep(void)         ;
void SDTimer1_Wakeup(void)        ;


/***************************************
*   Enumerated Types and Parameters
***************************************/

/* Enumerated Type B_Counter__CompareModes, Used in Compare Mode retained for backward compatibility of tests*/
#define SDTimer1__B_COUNTER__LESS_THAN 1
#define SDTimer1__B_COUNTER__LESS_THAN_OR_EQUAL 2
#define SDTimer1__B_COUNTER__EQUAL 0
#define SDTimer1__B_COUNTER__GREATER_THAN 3
#define SDTimer1__B_COUNTER__GREATER_THAN_OR_EQUAL 4
#define SDTimer1__B_COUNTER__SOFTWARE 5

/* Enumerated Type Counter_CompareModes */
#define SDTimer1_CMP_MODE_LT 1u
#define SDTimer1_CMP_MODE_LTE 2u
#define SDTimer1_CMP_MODE_EQ 0u
#define SDTimer1_CMP_MODE_GT 3u
#define SDTimer1_CMP_MODE_GTE 4u
#define SDTimer1_CMP_MODE_SOFTWARE_CONTROLLED 5u

/* Enumerated Type B_Counter__CaptureModes, Used in Capture Mode retained for backward compatibility of tests*/
#define SDTimer1__B_COUNTER__NONE 0
#define SDTimer1__B_COUNTER__RISING_EDGE 1
#define SDTimer1__B_COUNTER__FALLING_EDGE 2
#define SDTimer1__B_COUNTER__EITHER_EDGE 3
#define SDTimer1__B_COUNTER__SOFTWARE_CONTROL 4

/* Enumerated Type Counter_CompareModes */
#define SDTimer1_CAP_MODE_NONE 0u
#define SDTimer1_CAP_MODE_RISE 1u
#define SDTimer1_CAP_MODE_FALL 2u
#define SDTimer1_CAP_MODE_BOTH 3u
#define SDTimer1_CAP_MODE_SOFTWARE_CONTROLLED 4u


/***************************************
 *  Initialization Values
 **************************************/
#define SDTimer1_INIT_PERIOD_VALUE       1u
#define SDTimer1_INIT_COUNTER_VALUE      1u
#if (SDTimer1_UsingFixedFunction)
#define SDTimer1_INIT_INTERRUPTS_MASK    ((0u << SDTimer1_STATUS_ZERO_INT_EN_MASK_SHIFT))
#else
#define SDTimer1_INIT_COMPARE_VALUE      1u
#define SDTimer1_INIT_INTERRUPTS_MASK ((0u << SDTimer1_STATUS_ZERO_INT_EN_MASK_SHIFT) | \
        (0u << SDTimer1_STATUS_CAPTURE_INT_EN_MASK_SHIFT) | \
        (0u << SDTimer1_STATUS_CMP_INT_EN_MASK_SHIFT) | \
        (0u << SDTimer1_STATUS_OVERFLOW_INT_EN_MASK_SHIFT) | \
        (0u << SDTimer1_STATUS_UNDERFLOW_INT_EN_MASK_SHIFT))
#define SDTimer1_DEFAULT_COMPARE_MODE    (1u << SDTimer1_CTRL_CMPMODE0_SHIFT)
#define SDTimer1_DEFAULT_CAPTURE_MODE    (0u << SDTimer1_CTRL_CAPMODE0_SHIFT)
#endif /* (SDTimer1_UsingFixedFunction) */


/**************************************
 *  Registers
 *************************************/
#if (SDTimer1_UsingFixedFunction)
    #define SDTimer1_STATICCOUNT_LSB     (*(reg16 *) SDTimer1_CounterHW__CAP0 )
    #define SDTimer1_STATICCOUNT_LSB_PTR ( (reg16 *) SDTimer1_CounterHW__CAP0 )
    #define SDTimer1_PERIOD_LSB          (*(reg16 *) SDTimer1_CounterHW__PER0 )
    #define SDTimer1_PERIOD_LSB_PTR      ( (reg16 *) SDTimer1_CounterHW__PER0 )
    /* MODE must be set to 1 to set the compare value */
    #define SDTimer1_COMPARE_LSB         (*(reg16 *) SDTimer1_CounterHW__CNT_CMP0 )
    #define SDTimer1_COMPARE_LSB_PTR     ( (reg16 *) SDTimer1_CounterHW__CNT_CMP0 )
    /* MODE must be set to 0 to get the count */
    #define SDTimer1_COUNTER_LSB         (*(reg16 *) SDTimer1_CounterHW__CNT_CMP0 )
    #define SDTimer1_COUNTER_LSB_PTR     ( (reg16 *) SDTimer1_CounterHW__CNT_CMP0 )
    #define SDTimer1_RT1                 (*(reg8 *) SDTimer1_CounterHW__RT1)
    #define SDTimer1_RT1_PTR             ( (reg8 *) SDTimer1_CounterHW__RT1)
#else
    #define SDTimer1_STATICCOUNT_LSB     (*(reg16 *) \
        SDTimer1_CounterUDB_sC16_counterdp_u0__F0_REG )
    #define SDTimer1_STATICCOUNT_LSB_PTR ( (reg16 *) \
        SDTimer1_CounterUDB_sC16_counterdp_u0__F0_REG )
    #define SDTimer1_PERIOD_LSB          (*(reg16 *) \
        SDTimer1_CounterUDB_sC16_counterdp_u0__D0_REG )
    #define SDTimer1_PERIOD_LSB_PTR      ( (reg16 *) \
        SDTimer1_CounterUDB_sC16_counterdp_u0__D0_REG )
    #define SDTimer1_COMPARE_LSB         (*(reg16 *) \
        SDTimer1_CounterUDB_sC16_counterdp_u0__D1_REG )
    #define SDTimer1_COMPARE_LSB_PTR     ( (reg16 *) \
        SDTimer1_CounterUDB_sC16_counterdp_u0__D1_REG )
    #define SDTimer1_COUNTER_LSB         (*(reg16 *) \
        SDTimer1_CounterUDB_sC16_counterdp_u0__A0_REG )
    #define SDTimer1_COUNTER_LSB_PTR     ( (reg16 *)\
        SDTimer1_CounterUDB_sC16_counterdp_u0__A0_REG )

    #define SDTimer1_AUX_CONTROLDP0 \
        (*(reg8 *) SDTimer1_CounterUDB_sC16_counterdp_u0__DP_AUX_CTL_REG)
    #define SDTimer1_AUX_CONTROLDP0_PTR \
        ( (reg8 *) SDTimer1_CounterUDB_sC16_counterdp_u0__DP_AUX_CTL_REG)
    #if (SDTimer1_Resolution == 16 || SDTimer1_Resolution == 24 || SDTimer1_Resolution == 32)
       #define SDTimer1_AUX_CONTROLDP1 \
           (*(reg8 *) SDTimer1_CounterUDB_sC16_counterdp_u1__DP_AUX_CTL_REG)
       #define SDTimer1_AUX_CONTROLDP1_PTR \
           ( (reg8 *) SDTimer1_CounterUDB_sC16_counterdp_u1__DP_AUX_CTL_REG)
    #endif /* (SDTimer1_Resolution == 16 || SDTimer1_Resolution == 24 || SDTimer1_Resolution == 32) */
    #if (SDTimer1_Resolution == 24 || SDTimer1_Resolution == 32)
       #define SDTimer1_AUX_CONTROLDP2 \
           (*(reg8 *) SDTimer1_CounterUDB_sC16_counterdp_u2__DP_AUX_CTL_REG)
       #define SDTimer1_AUX_CONTROLDP2_PTR \
           ( (reg8 *) SDTimer1_CounterUDB_sC16_counterdp_u2__DP_AUX_CTL_REG)
    #endif /* (SDTimer1_Resolution == 24 || SDTimer1_Resolution == 32) */
    #if (SDTimer1_Resolution == 32)
       #define SDTimer1_AUX_CONTROLDP3 \
           (*(reg8 *) SDTimer1_CounterUDB_sC16_counterdp_u3__DP_AUX_CTL_REG)
       #define SDTimer1_AUX_CONTROLDP3_PTR \
           ( (reg8 *) SDTimer1_CounterUDB_sC16_counterdp_u3__DP_AUX_CTL_REG)
    #endif /* (SDTimer1_Resolution == 32) */
#endif  /* (SDTimer1_UsingFixedFunction) */

#if (SDTimer1_UsingFixedFunction)
    #define SDTimer1_STATUS         (*(reg8 *) SDTimer1_CounterHW__SR0 )
    /* In Fixed Function Block Status and Mask are the same register */
    #define SDTimer1_STATUS_MASK             (*(reg8 *) SDTimer1_CounterHW__SR0 )
    #define SDTimer1_STATUS_MASK_PTR         ( (reg8 *) SDTimer1_CounterHW__SR0 )
    #define SDTimer1_CONTROL                 (*(reg8 *) SDTimer1_CounterHW__CFG0)
    #define SDTimer1_CONTROL_PTR             ( (reg8 *) SDTimer1_CounterHW__CFG0)
    #define SDTimer1_CONTROL2                (*(reg8 *) SDTimer1_CounterHW__CFG1)
    #define SDTimer1_CONTROL2_PTR            ( (reg8 *) SDTimer1_CounterHW__CFG1)
    #if (CY_PSOC3 || CY_PSOC5LP)
        #define SDTimer1_CONTROL3       (*(reg8 *) SDTimer1_CounterHW__CFG2)
        #define SDTimer1_CONTROL3_PTR   ( (reg8 *) SDTimer1_CounterHW__CFG2)
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */
    #define SDTimer1_GLOBAL_ENABLE           (*(reg8 *) SDTimer1_CounterHW__PM_ACT_CFG)
    #define SDTimer1_GLOBAL_ENABLE_PTR       ( (reg8 *) SDTimer1_CounterHW__PM_ACT_CFG)
    #define SDTimer1_GLOBAL_STBY_ENABLE      (*(reg8 *) SDTimer1_CounterHW__PM_STBY_CFG)
    #define SDTimer1_GLOBAL_STBY_ENABLE_PTR  ( (reg8 *) SDTimer1_CounterHW__PM_STBY_CFG)
    

    /********************************
    *    Constants
    ********************************/

    /* Fixed Function Block Chosen */
    #define SDTimer1_BLOCK_EN_MASK          SDTimer1_CounterHW__PM_ACT_MSK
    #define SDTimer1_BLOCK_STBY_EN_MASK     SDTimer1_CounterHW__PM_STBY_MSK 
    
    /* Control Register Bit Locations */    
    /* As defined in Register Map, part of TMRX_CFG0 register */
    #define SDTimer1_CTRL_ENABLE_SHIFT      0x00u
    #define SDTimer1_ONESHOT_SHIFT          0x02u
    /* Control Register Bit Masks */
    #define SDTimer1_CTRL_ENABLE            (0x01u << SDTimer1_CTRL_ENABLE_SHIFT)         
    #define SDTimer1_ONESHOT                (0x01u << SDTimer1_ONESHOT_SHIFT)

    /* Control2 Register Bit Masks */
    /* Set the mask for run mode */
    #if (CY_PSOC5A)
        /* Use CFG1 Mode bits to set run mode */
        #define SDTimer1_CTRL_MODE_SHIFT        0x01u    
        #define SDTimer1_CTRL_MODE_MASK         (0x07u << SDTimer1_CTRL_MODE_SHIFT)
    #endif /* (CY_PSOC5A) */
    #if (CY_PSOC3 || CY_PSOC5LP)
        /* Use CFG2 Mode bits to set run mode */
        #define SDTimer1_CTRL_MODE_SHIFT        0x00u    
        #define SDTimer1_CTRL_MODE_MASK         (0x03u << SDTimer1_CTRL_MODE_SHIFT)
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */
    /* Set the mask for interrupt (raw/status register) */
    #define SDTimer1_CTRL2_IRQ_SEL_SHIFT     0x00u
    #define SDTimer1_CTRL2_IRQ_SEL          (0x01u << SDTimer1_CTRL2_IRQ_SEL_SHIFT)     
    
    /* Status Register Bit Locations */
    #define SDTimer1_STATUS_ZERO_SHIFT      0x07u  /* As defined in Register Map, part of TMRX_SR0 register */ 

    /* Status Register Interrupt Enable Bit Locations */
    #define SDTimer1_STATUS_ZERO_INT_EN_MASK_SHIFT      (SDTimer1_STATUS_ZERO_SHIFT - 0x04u)

    /* Status Register Bit Masks */                           
    #define SDTimer1_STATUS_ZERO            (0x01u << SDTimer1_STATUS_ZERO_SHIFT)

    /* Status Register Interrupt Bit Masks*/
    #define SDTimer1_STATUS_ZERO_INT_EN_MASK       (SDTimer1_STATUS_ZERO >> 0x04u)
    
    /*RT1 Synch Constants: Applicable for PSoC3 and PSoC5LP */
    #define SDTimer1_RT1_SHIFT            0x04u
    #define SDTimer1_RT1_MASK             (0x03u << SDTimer1_RT1_SHIFT)  /* Sync TC and CMP bit masks */
    #define SDTimer1_SYNC                 (0x03u << SDTimer1_RT1_SHIFT)
    #define SDTimer1_SYNCDSI_SHIFT        0x00u
    #define SDTimer1_SYNCDSI_MASK         (0x0Fu << SDTimer1_SYNCDSI_SHIFT) /* Sync all DSI inputs */
    #define SDTimer1_SYNCDSI_EN           (0x0Fu << SDTimer1_SYNCDSI_SHIFT) /* Sync all DSI inputs */
    
#else /* !SDTimer1_UsingFixedFunction */
    #define SDTimer1_STATUS               (* (reg8 *) SDTimer1_CounterUDB_sSTSReg_nrstSts_stsreg__STATUS_REG )
    #define SDTimer1_STATUS_PTR           (  (reg8 *) SDTimer1_CounterUDB_sSTSReg_nrstSts_stsreg__STATUS_REG )
    #define SDTimer1_STATUS_MASK          (* (reg8 *) SDTimer1_CounterUDB_sSTSReg_nrstSts_stsreg__MASK_REG )
    #define SDTimer1_STATUS_MASK_PTR      (  (reg8 *) SDTimer1_CounterUDB_sSTSReg_nrstSts_stsreg__MASK_REG )
    #define SDTimer1_STATUS_AUX_CTRL      (*(reg8 *) SDTimer1_CounterUDB_sSTSReg_nrstSts_stsreg__STATUS_AUX_CTL_REG)
    #define SDTimer1_STATUS_AUX_CTRL_PTR  ( (reg8 *) SDTimer1_CounterUDB_sSTSReg_nrstSts_stsreg__STATUS_AUX_CTL_REG)
    #define SDTimer1_CONTROL              (* (reg8 *) SDTimer1_CounterUDB_sCTRLReg_AsyncCtl_ctrlreg__CONTROL_REG )
    #define SDTimer1_CONTROL_PTR          (  (reg8 *) SDTimer1_CounterUDB_sCTRLReg_AsyncCtl_ctrlreg__CONTROL_REG )


    /********************************
    *    Constants
    ********************************/
    /* Control Register Bit Locations */
    #define SDTimer1_CTRL_CMPMODE0_SHIFT    0x00u       /* As defined by Verilog Implementation */
    #define SDTimer1_CTRL_CAPMODE0_SHIFT    0x03u       /* As defined by Verilog Implementation */
    #define SDTimer1_CTRL_RESET_SHIFT       0x06u       /* As defined by Verilog Implementation */
    #define SDTimer1_CTRL_ENABLE_SHIFT      0x07u       /* As defined by Verilog Implementation */
    /* Control Register Bit Masks */
    #define SDTimer1_CTRL_CMPMODE_MASK      (0x07u << SDTimer1_CTRL_CMPMODE0_SHIFT)  
    #define SDTimer1_CTRL_CAPMODE_MASK      (0x03u << SDTimer1_CTRL_CAPMODE0_SHIFT)  
    #define SDTimer1_CTRL_RESET             (0x01u << SDTimer1_CTRL_RESET_SHIFT)  
    #define SDTimer1_CTRL_ENABLE            (0x01u << SDTimer1_CTRL_ENABLE_SHIFT) 

    /* Status Register Bit Locations */
    #define SDTimer1_STATUS_CMP_SHIFT       0x00u       /* As defined by Verilog Implementation */
    #define SDTimer1_STATUS_ZERO_SHIFT      0x01u       /* As defined by Verilog Implementation */
    #define SDTimer1_STATUS_OVERFLOW_SHIFT  0x02u       /* As defined by Verilog Implementation */
    #define SDTimer1_STATUS_UNDERFLOW_SHIFT 0x03u       /* As defined by Verilog Implementation */
    #define SDTimer1_STATUS_CAPTURE_SHIFT   0x04u       /* As defined by Verilog Implementation */
    #define SDTimer1_STATUS_FIFOFULL_SHIFT  0x05u       /* As defined by Verilog Implementation */
    #define SDTimer1_STATUS_FIFONEMP_SHIFT  0x06u       /* As defined by Verilog Implementation */
    /* Status Register Interrupt Enable Bit Locations - UDB Status Interrupt Mask match Status Bit Locations*/
    #define SDTimer1_STATUS_CMP_INT_EN_MASK_SHIFT       SDTimer1_STATUS_CMP_SHIFT       
    #define SDTimer1_STATUS_ZERO_INT_EN_MASK_SHIFT      SDTimer1_STATUS_ZERO_SHIFT      
    #define SDTimer1_STATUS_OVERFLOW_INT_EN_MASK_SHIFT  SDTimer1_STATUS_OVERFLOW_SHIFT  
    #define SDTimer1_STATUS_UNDERFLOW_INT_EN_MASK_SHIFT SDTimer1_STATUS_UNDERFLOW_SHIFT 
    #define SDTimer1_STATUS_CAPTURE_INT_EN_MASK_SHIFT   SDTimer1_STATUS_CAPTURE_SHIFT   
    #define SDTimer1_STATUS_FIFOFULL_INT_EN_MASK_SHIFT  SDTimer1_STATUS_FIFOFULL_SHIFT  
    #define SDTimer1_STATUS_FIFONEMP_INT_EN_MASK_SHIFT  SDTimer1_STATUS_FIFONEMP_SHIFT  
    /* Status Register Bit Masks */                
    #define SDTimer1_STATUS_CMP             (0x01u << SDTimer1_STATUS_CMP_SHIFT)  
    #define SDTimer1_STATUS_ZERO            (0x01u << SDTimer1_STATUS_ZERO_SHIFT) 
    #define SDTimer1_STATUS_OVERFLOW        (0x01u << SDTimer1_STATUS_OVERFLOW_SHIFT) 
    #define SDTimer1_STATUS_UNDERFLOW       (0x01u << SDTimer1_STATUS_UNDERFLOW_SHIFT) 
    #define SDTimer1_STATUS_CAPTURE         (0x01u << SDTimer1_STATUS_CAPTURE_SHIFT) 
    #define SDTimer1_STATUS_FIFOFULL        (0x01u << SDTimer1_STATUS_FIFOFULL_SHIFT)
    #define SDTimer1_STATUS_FIFONEMP        (0x01u << SDTimer1_STATUS_FIFONEMP_SHIFT)
    /* Status Register Interrupt Bit Masks  - UDB Status Interrupt Mask match Status Bit Locations */
    #define SDTimer1_STATUS_CMP_INT_EN_MASK            SDTimer1_STATUS_CMP                    
    #define SDTimer1_STATUS_ZERO_INT_EN_MASK           SDTimer1_STATUS_ZERO            
    #define SDTimer1_STATUS_OVERFLOW_INT_EN_MASK       SDTimer1_STATUS_OVERFLOW        
    #define SDTimer1_STATUS_UNDERFLOW_INT_EN_MASK      SDTimer1_STATUS_UNDERFLOW       
    #define SDTimer1_STATUS_CAPTURE_INT_EN_MASK        SDTimer1_STATUS_CAPTURE         
    #define SDTimer1_STATUS_FIFOFULL_INT_EN_MASK       SDTimer1_STATUS_FIFOFULL        
    #define SDTimer1_STATUS_FIFONEMP_INT_EN_MASK       SDTimer1_STATUS_FIFONEMP         
    

    /* StatusI Interrupt Enable bit Location in the Auxilliary Control Register */
    #define SDTimer1_STATUS_ACTL_INT_EN     0x10u /* As defined for the ACTL Register */
    
    /* Datapath Auxillary Control Register definitions */
    #define SDTimer1_AUX_CTRL_FIFO0_CLR         0x01u   /* As defined by Register map */
    #define SDTimer1_AUX_CTRL_FIFO1_CLR         0x02u   /* As defined by Register map */
    #define SDTimer1_AUX_CTRL_FIFO0_LVL         0x04u   /* As defined by Register map */
    #define SDTimer1_AUX_CTRL_FIFO1_LVL         0x08u   /* As defined by Register map */
    #define SDTimer1_STATUS_ACTL_INT_EN_MASK    0x10u   /* As defined for the ACTL Register */
    
#endif /* SDTimer1_UsingFixedFunction */

#endif  /* CY_COUNTER_SDTimer1_H */


/* [] END OF FILE */

