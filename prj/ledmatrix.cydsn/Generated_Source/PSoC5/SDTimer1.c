/*******************************************************************************
* File Name: SDTimer1.c  
* Version 2.20
*
*  Description:
*     The Counter component consists of a 8, 16, 24 or 32-bit counter with
*     a selectable period between 2 and 2^Width - 1.  
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "SDTimer1.h"

uint8 SDTimer1_initVar = 0u;


/*******************************************************************************
* Function Name: SDTimer1_Init
********************************************************************************
* Summary:
*     Initialize to the schematic state
* 
* Parameters:  
*  void  
*
* Return: 
*  void
*
*******************************************************************************/
void SDTimer1_Init(void) 
{
        #if (!SDTimer1_UsingFixedFunction && !SDTimer1_ControlRegRemoved)
            uint8 ctrl;
        #endif /* (!SDTimer1_UsingFixedFunction && !SDTimer1_ControlRegRemoved) */
        
        #if(!SDTimer1_UsingFixedFunction) 
            /* Interrupt State Backup for Critical Region*/
            uint8 SDTimer1_interruptState;
        #endif /* (!SDTimer1_UsingFixedFunction) */
        
        #if (SDTimer1_UsingFixedFunction)
            /* Clear all bits but the enable bit (if it's already set for Timer operation */
            SDTimer1_CONTROL &= SDTimer1_CTRL_ENABLE;
            
            /* Clear the mode bits for continuous run mode */
            #if (CY_PSOC5A)
                SDTimer1_CONTROL2 &= ~SDTimer1_CTRL_MODE_MASK;
            #endif /* (CY_PSOC5A) */
            #if (CY_PSOC3 || CY_PSOC5LP)
                SDTimer1_CONTROL3 &= ~SDTimer1_CTRL_MODE_MASK;                
            #endif /* (CY_PSOC3 || CY_PSOC5LP) */
            /* Check if One Shot mode is enabled i.e. RunMode !=0*/
            #if (SDTimer1_RunModeUsed != 0x0u)
                /* Set 3rd bit of Control register to enable one shot mode */
                SDTimer1_CONTROL |= SDTimer1_ONESHOT;
            #endif /* (SDTimer1_RunModeUsed != 0x0u) */
            
            /* Set the IRQ to use the status register interrupts */
            SDTimer1_CONTROL2 |= SDTimer1_CTRL2_IRQ_SEL;
            
            /* Clear and Set SYNCTC and SYNCCMP bits of RT1 register */
            SDTimer1_RT1 &= ~SDTimer1_RT1_MASK;
            SDTimer1_RT1 |= SDTimer1_SYNC;     
                    
            /*Enable DSI Sync all all inputs of the Timer*/
            SDTimer1_RT1 &= ~(SDTimer1_SYNCDSI_MASK);
            SDTimer1_RT1 |= SDTimer1_SYNCDSI_EN;

        #else
            #if(!SDTimer1_ControlRegRemoved)
            /* Set the default compare mode defined in the parameter */
            ctrl = SDTimer1_CONTROL & ~SDTimer1_CTRL_CMPMODE_MASK;
            SDTimer1_CONTROL = ctrl | SDTimer1_DEFAULT_COMPARE_MODE;
            
            /* Set the default capture mode defined in the parameter */
            ctrl = SDTimer1_CONTROL & ~SDTimer1_CTRL_CAPMODE_MASK;
            SDTimer1_CONTROL = ctrl | SDTimer1_DEFAULT_CAPTURE_MODE;
            #endif /* (!SDTimer1_ControlRegRemoved) */
        #endif /* (SDTimer1_UsingFixedFunction) */
        
        /* Clear all data in the FIFO's */
        #if (!SDTimer1_UsingFixedFunction)
            SDTimer1_ClearFIFO();
        #endif /* (!SDTimer1_UsingFixedFunction) */
        
        /* Set Initial values from Configuration */
        SDTimer1_WritePeriod(SDTimer1_INIT_PERIOD_VALUE);
        #if (!(SDTimer1_UsingFixedFunction && (CY_PSOC5A)))
            SDTimer1_WriteCounter(SDTimer1_INIT_COUNTER_VALUE);
        #endif /* (!(SDTimer1_UsingFixedFunction && (CY_PSOC5A))) */
        SDTimer1_SetInterruptMode(SDTimer1_INIT_INTERRUPTS_MASK);
        
        #if (!SDTimer1_UsingFixedFunction)
            /* Read the status register to clear the unwanted interrupts */
            SDTimer1_ReadStatusRegister();
            /* Set the compare value (only available to non-fixed function implementation */
            SDTimer1_WriteCompare(SDTimer1_INIT_COMPARE_VALUE);
            /* Use the interrupt output of the status register for IRQ output */
            
            /* CyEnterCriticalRegion and CyExitCriticalRegion are used to mark following region critical*/
            /* Enter Critical Region*/
            SDTimer1_interruptState = CyEnterCriticalSection();
            
            SDTimer1_STATUS_AUX_CTRL |= SDTimer1_STATUS_ACTL_INT_EN_MASK;
            
            /* Exit Critical Region*/
            CyExitCriticalSection(SDTimer1_interruptState);
            
        #endif /* (!SDTimer1_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer1_Enable
********************************************************************************
* Summary:
*     Enable the Counter
* 
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Side Effects: 
*   If the Enable mode is set to Hardware only then this function has no effect 
*   on the operation of the counter.
*
*******************************************************************************/
void SDTimer1_Enable(void) 
{
    /* Globally Enable the Fixed Function Block chosen */
    #if (SDTimer1_UsingFixedFunction)
        SDTimer1_GLOBAL_ENABLE |= SDTimer1_BLOCK_EN_MASK;
        SDTimer1_GLOBAL_STBY_ENABLE |= SDTimer1_BLOCK_STBY_EN_MASK;
    #endif /* (SDTimer1_UsingFixedFunction) */  
        
    /* Enable the counter from the control register  */
    /* If Fixed Function then make sure Mode is set correctly */
    /* else make sure reset is clear */
    #if(!SDTimer1_ControlRegRemoved || SDTimer1_UsingFixedFunction)
        SDTimer1_CONTROL |= SDTimer1_CTRL_ENABLE;                
    #endif /* (!SDTimer1_ControlRegRemoved || SDTimer1_UsingFixedFunction) */
    
}


/*******************************************************************************
* Function Name: SDTimer1_Start
********************************************************************************
* Summary:
*  Enables the counter for operation 
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Global variables:
*  SDTimer1_initVar: Is modified when this function is called for the  
*   first time. Is used to ensure that initialization happens only once.
*
*******************************************************************************/
void SDTimer1_Start(void) 
{
    if(SDTimer1_initVar == 0u)
    {
        SDTimer1_Init();
        
        SDTimer1_initVar = 1u; /* Clear this bit for Initialization */        
    }
    
    /* Enable the Counter */
    SDTimer1_Enable();        
}


/*******************************************************************************
* Function Name: SDTimer1_Stop
********************************************************************************
* Summary:
* Halts the counter, but does not change any modes or disable interrupts.
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Side Effects: If the Enable mode is set to Hardware only then this function
*               has no effect on the operation of the counter.
*
*******************************************************************************/
void SDTimer1_Stop(void) 
{
    /* Disable Counter */
    #if(!SDTimer1_ControlRegRemoved || SDTimer1_UsingFixedFunction)
        SDTimer1_CONTROL &= ~SDTimer1_CTRL_ENABLE;        
    #endif /* (!SDTimer1_ControlRegRemoved || SDTimer1_UsingFixedFunction) */
    
    /* Globally disable the Fixed Function Block chosen */
    #if (SDTimer1_UsingFixedFunction)
        SDTimer1_GLOBAL_ENABLE &= ~SDTimer1_BLOCK_EN_MASK;
        SDTimer1_GLOBAL_STBY_ENABLE &= ~SDTimer1_BLOCK_STBY_EN_MASK;
    #endif /* (SDTimer1_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer1_SetInterruptMode
********************************************************************************
* Summary:
* Configures which interrupt sources are enabled to generate the final interrupt
*
* Parameters:  
*  InterruptsMask: This parameter is an or'd collection of the status bits
*                   which will be allowed to generate the counters interrupt.   
*
* Return: 
*  void
*
*******************************************************************************/
void SDTimer1_SetInterruptMode(uint8 interruptsMask) 
{
    SDTimer1_STATUS_MASK = interruptsMask;
}


/*******************************************************************************
* Function Name: SDTimer1_ReadStatusRegister
********************************************************************************
* Summary:
*   Reads the status register and returns it's state. This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the status register
*
* Side Effects:
*   Status register bits may be clear on read. 
*
*******************************************************************************/
uint8   SDTimer1_ReadStatusRegister(void) 
{
    return SDTimer1_STATUS;
}


#if(!SDTimer1_ControlRegRemoved)
/*******************************************************************************
* Function Name: SDTimer1_ReadControlRegister
********************************************************************************
* Summary:
*   Reads the control register and returns it's state. This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the control register
*
*******************************************************************************/
uint8   SDTimer1_ReadControlRegister(void) 
{
    return SDTimer1_CONTROL;
}


/*******************************************************************************
* Function Name: SDTimer1_WriteControlRegister
********************************************************************************
* Summary:
*   Sets the bit-field of the control register.  This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the control register
*
*******************************************************************************/
void    SDTimer1_WriteControlRegister(uint8 control) 
{
    SDTimer1_CONTROL = control;
}

#endif  /* (!SDTimer1_ControlRegRemoved) */


#if (!(SDTimer1_UsingFixedFunction && (CY_PSOC5A)))
/*******************************************************************************
* Function Name: SDTimer1_WriteCounter
********************************************************************************
* Summary:
*   This funtion is used to set the counter to a specific value
*
* Parameters:  
*  counter:  New counter value. 
*
* Return: 
*  void 
*
*******************************************************************************/
void SDTimer1_WriteCounter(uint16 counter) \
                                   
{
    #if(SDTimer1_UsingFixedFunction)
        /* assert if block is already enabled */
        CYASSERT (!(SDTimer1_GLOBAL_ENABLE & SDTimer1_BLOCK_EN_MASK));
        /* If block is disabled, enable it and then write the counter */
        SDTimer1_GLOBAL_ENABLE |= SDTimer1_BLOCK_EN_MASK;
        CY_SET_REG16(SDTimer1_COUNTER_LSB_PTR, (uint16)counter);
        SDTimer1_GLOBAL_ENABLE &= ~SDTimer1_BLOCK_EN_MASK;
    #else
        CY_SET_REG16(SDTimer1_COUNTER_LSB_PTR, counter);
    #endif /* (SDTimer1_UsingFixedFunction) */
}
#endif /* (!(SDTimer1_UsingFixedFunction && (CY_PSOC5A))) */


/*******************************************************************************
* Function Name: SDTimer1_ReadCounter
********************************************************************************
* Summary:
* Returns the current value of the counter.  It doesn't matter
* if the counter is enabled or running.
*
* Parameters:  
*  void:  
*
* Return: 
*  (uint16) The present value of the counter.
*
*******************************************************************************/
uint16 SDTimer1_ReadCounter(void) 
{
    /* Force capture by reading Accumulator */
    /* Must first do a software capture to be able to read the counter */
    /* It is up to the user code to make sure there isn't already captured data in the FIFO */
    CY_GET_REG8(SDTimer1_COUNTER_LSB_PTR);
    
    /* Read the data from the FIFO (or capture register for Fixed Function)*/
    return (CY_GET_REG16(SDTimer1_STATICCOUNT_LSB_PTR));
}


/*******************************************************************************
* Function Name: SDTimer1_ReadCapture
********************************************************************************
* Summary:
*   This function returns the last value captured.
*
* Parameters:  
*  void
*
* Return: 
*  (uint16) Present Capture value.
*
*******************************************************************************/
uint16 SDTimer1_ReadCapture(void) 
{
   return ( CY_GET_REG16(SDTimer1_STATICCOUNT_LSB_PTR) );  
}


/*******************************************************************************
* Function Name: SDTimer1_WritePeriod
********************************************************************************
* Summary:
* Changes the period of the counter.  The new period 
* will be loaded the next time terminal count is detected.
*
* Parameters:  
*  period: (uint16) A value of 0 will result in
*         the counter remaining at zero.  
*
* Return: 
*  void
*
*******************************************************************************/
void SDTimer1_WritePeriod(uint16 period) 
{
    #if(SDTimer1_UsingFixedFunction)
        CY_SET_REG16(SDTimer1_PERIOD_LSB_PTR,(uint16)period);
    #else
        CY_SET_REG16(SDTimer1_PERIOD_LSB_PTR,period);
    #endif /* (SDTimer1_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer1_ReadPeriod
********************************************************************************
* Summary:
* Reads the current period value without affecting counter operation.
*
* Parameters:  
*  void:  
*
* Return: 
*  (uint16) Present period value.
*
*******************************************************************************/
uint16 SDTimer1_ReadPeriod(void) 
{
   return ( CY_GET_REG16(SDTimer1_PERIOD_LSB_PTR));
}


#if (!SDTimer1_UsingFixedFunction)
/*******************************************************************************
* Function Name: SDTimer1_WriteCompare
********************************************************************************
* Summary:
* Changes the compare value.  The compare output will 
* reflect the new value on the next UDB clock.  The compare output will be 
* driven high when the present counter value compares true based on the 
* configured compare mode setting. 
*
* Parameters:  
*  Compare:  New compare value. 
*
* Return: 
*  void
*
*******************************************************************************/
void SDTimer1_WriteCompare(uint16 compare) \
                                   
{
    #if(SDTimer1_UsingFixedFunction)
        CY_SET_REG16(SDTimer1_COMPARE_LSB_PTR,(uint16)compare);
    #else
        CY_SET_REG16(SDTimer1_COMPARE_LSB_PTR,compare);
    #endif /* (SDTimer1_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: SDTimer1_ReadCompare
********************************************************************************
* Summary:
* Returns the compare value.
*
* Parameters:  
*  void:
*
* Return: 
*  (uint16) Present compare value.
*
*******************************************************************************/
uint16 SDTimer1_ReadCompare(void) 
{
   return ( CY_GET_REG16(SDTimer1_COMPARE_LSB_PTR));
}


#if (SDTimer1_COMPARE_MODE_SOFTWARE)
/*******************************************************************************
* Function Name: SDTimer1_SetCompareMode
********************************************************************************
* Summary:
*  Sets the software controlled Compare Mode.
*
* Parameters:
*  compareMode:  Compare Mode Enumerated Type.
*
* Return:
*  void
*
*******************************************************************************/
void SDTimer1_SetCompareMode(uint8 compareMode) 
{
    /* Clear the compare mode bits in the control register */
    SDTimer1_CONTROL &= ~SDTimer1_CTRL_CMPMODE_MASK;
    
    /* Write the new setting */
    SDTimer1_CONTROL |= (compareMode << SDTimer1_CTRL_CMPMODE0_SHIFT);
}
#endif  /* (SDTimer1_COMPARE_MODE_SOFTWARE) */


#if (SDTimer1_CAPTURE_MODE_SOFTWARE)
/*******************************************************************************
* Function Name: SDTimer1_SetCaptureMode
********************************************************************************
* Summary:
*  Sets the software controlled Capture Mode.
*
* Parameters:
*  captureMode:  Capture Mode Enumerated Type.
*
* Return:
*  void
*
*******************************************************************************/
void SDTimer1_SetCaptureMode(uint8 captureMode) 
{
    /* Clear the capture mode bits in the control register */
    SDTimer1_CONTROL &= ~SDTimer1_CTRL_CAPMODE_MASK;
    
    /* Write the new setting */
    SDTimer1_CONTROL |= (captureMode << SDTimer1_CTRL_CAPMODE0_SHIFT);
}
#endif  /* (SDTimer1_CAPTURE_MODE_SOFTWARE) */


/*******************************************************************************
* Function Name: SDTimer1_ClearFIFO
********************************************************************************
* Summary:
*   This function clears all capture data from the capture FIFO
*
* Parameters:  
*  void:
*
* Return: 
*  None
*
*******************************************************************************/
void SDTimer1_ClearFIFO(void) 
{

    while(SDTimer1_ReadStatusRegister() & SDTimer1_STATUS_FIFONEMP)
    {
        SDTimer1_ReadCapture();
    }

}
#endif  /* (!SDTimer1_UsingFixedFunction) */


/* [] END OF FILE */

