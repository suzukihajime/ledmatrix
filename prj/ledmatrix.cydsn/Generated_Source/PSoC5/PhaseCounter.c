/*******************************************************************************
* File Name: PhaseCounter.c  
* Version 2.20
*
*  Description:
*     The Counter component consists of a 8, 16, 24 or 32-bit counter with
*     a selectable period between 2 and 2^Width - 1.  
*
*   Note:
*     None
*
*******************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "PhaseCounter.h"

uint8 PhaseCounter_initVar = 0u;


/*******************************************************************************
* Function Name: PhaseCounter_Init
********************************************************************************
* Summary:
*     Initialize to the schematic state
* 
* Parameters:  
*  void  
*
* Return: 
*  void
*
*******************************************************************************/
void PhaseCounter_Init(void) 
{
        #if (!PhaseCounter_UsingFixedFunction && !PhaseCounter_ControlRegRemoved)
            uint8 ctrl;
        #endif /* (!PhaseCounter_UsingFixedFunction && !PhaseCounter_ControlRegRemoved) */
        
        #if(!PhaseCounter_UsingFixedFunction) 
            /* Interrupt State Backup for Critical Region*/
            uint8 PhaseCounter_interruptState;
        #endif /* (!PhaseCounter_UsingFixedFunction) */
        
        #if (PhaseCounter_UsingFixedFunction)
            /* Clear all bits but the enable bit (if it's already set for Timer operation */
            PhaseCounter_CONTROL &= PhaseCounter_CTRL_ENABLE;
            
            /* Clear the mode bits for continuous run mode */
            #if (CY_PSOC5A)
                PhaseCounter_CONTROL2 &= ~PhaseCounter_CTRL_MODE_MASK;
            #endif /* (CY_PSOC5A) */
            #if (CY_PSOC3 || CY_PSOC5LP)
                PhaseCounter_CONTROL3 &= ~PhaseCounter_CTRL_MODE_MASK;                
            #endif /* (CY_PSOC3 || CY_PSOC5LP) */
            /* Check if One Shot mode is enabled i.e. RunMode !=0*/
            #if (PhaseCounter_RunModeUsed != 0x0u)
                /* Set 3rd bit of Control register to enable one shot mode */
                PhaseCounter_CONTROL |= PhaseCounter_ONESHOT;
            #endif /* (PhaseCounter_RunModeUsed != 0x0u) */
            
            /* Set the IRQ to use the status register interrupts */
            PhaseCounter_CONTROL2 |= PhaseCounter_CTRL2_IRQ_SEL;
            
            /* Clear and Set SYNCTC and SYNCCMP bits of RT1 register */
            PhaseCounter_RT1 &= ~PhaseCounter_RT1_MASK;
            PhaseCounter_RT1 |= PhaseCounter_SYNC;     
                    
            /*Enable DSI Sync all all inputs of the Timer*/
            PhaseCounter_RT1 &= ~(PhaseCounter_SYNCDSI_MASK);
            PhaseCounter_RT1 |= PhaseCounter_SYNCDSI_EN;

        #else
            #if(!PhaseCounter_ControlRegRemoved)
            /* Set the default compare mode defined in the parameter */
            ctrl = PhaseCounter_CONTROL & ~PhaseCounter_CTRL_CMPMODE_MASK;
            PhaseCounter_CONTROL = ctrl | PhaseCounter_DEFAULT_COMPARE_MODE;
            
            /* Set the default capture mode defined in the parameter */
            ctrl = PhaseCounter_CONTROL & ~PhaseCounter_CTRL_CAPMODE_MASK;
            PhaseCounter_CONTROL = ctrl | PhaseCounter_DEFAULT_CAPTURE_MODE;
            #endif /* (!PhaseCounter_ControlRegRemoved) */
        #endif /* (PhaseCounter_UsingFixedFunction) */
        
        /* Clear all data in the FIFO's */
        #if (!PhaseCounter_UsingFixedFunction)
            PhaseCounter_ClearFIFO();
        #endif /* (!PhaseCounter_UsingFixedFunction) */
        
        /* Set Initial values from Configuration */
        PhaseCounter_WritePeriod(PhaseCounter_INIT_PERIOD_VALUE);
        #if (!(PhaseCounter_UsingFixedFunction && (CY_PSOC5A)))
            PhaseCounter_WriteCounter(PhaseCounter_INIT_COUNTER_VALUE);
        #endif /* (!(PhaseCounter_UsingFixedFunction && (CY_PSOC5A))) */
        PhaseCounter_SetInterruptMode(PhaseCounter_INIT_INTERRUPTS_MASK);
        
        #if (!PhaseCounter_UsingFixedFunction)
            /* Read the status register to clear the unwanted interrupts */
            PhaseCounter_ReadStatusRegister();
            /* Set the compare value (only available to non-fixed function implementation */
            PhaseCounter_WriteCompare(PhaseCounter_INIT_COMPARE_VALUE);
            /* Use the interrupt output of the status register for IRQ output */
            
            /* CyEnterCriticalRegion and CyExitCriticalRegion are used to mark following region critical*/
            /* Enter Critical Region*/
            PhaseCounter_interruptState = CyEnterCriticalSection();
            
            PhaseCounter_STATUS_AUX_CTRL |= PhaseCounter_STATUS_ACTL_INT_EN_MASK;
            
            /* Exit Critical Region*/
            CyExitCriticalSection(PhaseCounter_interruptState);
            
        #endif /* (!PhaseCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PhaseCounter_Enable
********************************************************************************
* Summary:
*     Enable the Counter
* 
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Side Effects: 
*   If the Enable mode is set to Hardware only then this function has no effect 
*   on the operation of the counter.
*
*******************************************************************************/
void PhaseCounter_Enable(void) 
{
    /* Globally Enable the Fixed Function Block chosen */
    #if (PhaseCounter_UsingFixedFunction)
        PhaseCounter_GLOBAL_ENABLE |= PhaseCounter_BLOCK_EN_MASK;
        PhaseCounter_GLOBAL_STBY_ENABLE |= PhaseCounter_BLOCK_STBY_EN_MASK;
    #endif /* (PhaseCounter_UsingFixedFunction) */  
        
    /* Enable the counter from the control register  */
    /* If Fixed Function then make sure Mode is set correctly */
    /* else make sure reset is clear */
    #if(!PhaseCounter_ControlRegRemoved || PhaseCounter_UsingFixedFunction)
        PhaseCounter_CONTROL |= PhaseCounter_CTRL_ENABLE;                
    #endif /* (!PhaseCounter_ControlRegRemoved || PhaseCounter_UsingFixedFunction) */
    
}


/*******************************************************************************
* Function Name: PhaseCounter_Start
********************************************************************************
* Summary:
*  Enables the counter for operation 
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Global variables:
*  PhaseCounter_initVar: Is modified when this function is called for the  
*   first time. Is used to ensure that initialization happens only once.
*
*******************************************************************************/
void PhaseCounter_Start(void) 
{
    if(PhaseCounter_initVar == 0u)
    {
        PhaseCounter_Init();
        
        PhaseCounter_initVar = 1u; /* Clear this bit for Initialization */        
    }
    
    /* Enable the Counter */
    PhaseCounter_Enable();        
}


/*******************************************************************************
* Function Name: PhaseCounter_Stop
********************************************************************************
* Summary:
* Halts the counter, but does not change any modes or disable interrupts.
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
* Side Effects: If the Enable mode is set to Hardware only then this function
*               has no effect on the operation of the counter.
*
*******************************************************************************/
void PhaseCounter_Stop(void) 
{
    /* Disable Counter */
    #if(!PhaseCounter_ControlRegRemoved || PhaseCounter_UsingFixedFunction)
        PhaseCounter_CONTROL &= ~PhaseCounter_CTRL_ENABLE;        
    #endif /* (!PhaseCounter_ControlRegRemoved || PhaseCounter_UsingFixedFunction) */
    
    /* Globally disable the Fixed Function Block chosen */
    #if (PhaseCounter_UsingFixedFunction)
        PhaseCounter_GLOBAL_ENABLE &= ~PhaseCounter_BLOCK_EN_MASK;
        PhaseCounter_GLOBAL_STBY_ENABLE &= ~PhaseCounter_BLOCK_STBY_EN_MASK;
    #endif /* (PhaseCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PhaseCounter_SetInterruptMode
********************************************************************************
* Summary:
* Configures which interrupt sources are enabled to generate the final interrupt
*
* Parameters:  
*  InterruptsMask: This parameter is an or'd collection of the status bits
*                   which will be allowed to generate the counters interrupt.   
*
* Return: 
*  void
*
*******************************************************************************/
void PhaseCounter_SetInterruptMode(uint8 interruptsMask) 
{
    PhaseCounter_STATUS_MASK = interruptsMask;
}


/*******************************************************************************
* Function Name: PhaseCounter_ReadStatusRegister
********************************************************************************
* Summary:
*   Reads the status register and returns it's state. This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the status register
*
* Side Effects:
*   Status register bits may be clear on read. 
*
*******************************************************************************/
uint8   PhaseCounter_ReadStatusRegister(void) 
{
    return PhaseCounter_STATUS;
}


#if(!PhaseCounter_ControlRegRemoved)
/*******************************************************************************
* Function Name: PhaseCounter_ReadControlRegister
********************************************************************************
* Summary:
*   Reads the control register and returns it's state. This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the control register
*
*******************************************************************************/
uint8   PhaseCounter_ReadControlRegister(void) 
{
    return PhaseCounter_CONTROL;
}


/*******************************************************************************
* Function Name: PhaseCounter_WriteControlRegister
********************************************************************************
* Summary:
*   Sets the bit-field of the control register.  This function should use
*       defined types for the bit-field information as the bits in this
*       register may be permuteable.
*
* Parameters:  
*  void
*
* Return: 
*  (uint8) The contents of the control register
*
*******************************************************************************/
void    PhaseCounter_WriteControlRegister(uint8 control) 
{
    PhaseCounter_CONTROL = control;
}

#endif  /* (!PhaseCounter_ControlRegRemoved) */


#if (!(PhaseCounter_UsingFixedFunction && (CY_PSOC5A)))
/*******************************************************************************
* Function Name: PhaseCounter_WriteCounter
********************************************************************************
* Summary:
*   This funtion is used to set the counter to a specific value
*
* Parameters:  
*  counter:  New counter value. 
*
* Return: 
*  void 
*
*******************************************************************************/
void PhaseCounter_WriteCounter(uint16 counter) \
                                   
{
    #if(PhaseCounter_UsingFixedFunction)
        /* assert if block is already enabled */
        CYASSERT (!(PhaseCounter_GLOBAL_ENABLE & PhaseCounter_BLOCK_EN_MASK));
        /* If block is disabled, enable it and then write the counter */
        PhaseCounter_GLOBAL_ENABLE |= PhaseCounter_BLOCK_EN_MASK;
        CY_SET_REG16(PhaseCounter_COUNTER_LSB_PTR, (uint16)counter);
        PhaseCounter_GLOBAL_ENABLE &= ~PhaseCounter_BLOCK_EN_MASK;
    #else
        CY_SET_REG16(PhaseCounter_COUNTER_LSB_PTR, counter);
    #endif /* (PhaseCounter_UsingFixedFunction) */
}
#endif /* (!(PhaseCounter_UsingFixedFunction && (CY_PSOC5A))) */


/*******************************************************************************
* Function Name: PhaseCounter_ReadCounter
********************************************************************************
* Summary:
* Returns the current value of the counter.  It doesn't matter
* if the counter is enabled or running.
*
* Parameters:  
*  void:  
*
* Return: 
*  (uint16) The present value of the counter.
*
*******************************************************************************/
uint16 PhaseCounter_ReadCounter(void) 
{
    /* Force capture by reading Accumulator */
    /* Must first do a software capture to be able to read the counter */
    /* It is up to the user code to make sure there isn't already captured data in the FIFO */
    CY_GET_REG8(PhaseCounter_COUNTER_LSB_PTR);
    
    /* Read the data from the FIFO (or capture register for Fixed Function)*/
    return (CY_GET_REG16(PhaseCounter_STATICCOUNT_LSB_PTR));
}


/*******************************************************************************
* Function Name: PhaseCounter_ReadCapture
********************************************************************************
* Summary:
*   This function returns the last value captured.
*
* Parameters:  
*  void
*
* Return: 
*  (uint16) Present Capture value.
*
*******************************************************************************/
uint16 PhaseCounter_ReadCapture(void) 
{
   return ( CY_GET_REG16(PhaseCounter_STATICCOUNT_LSB_PTR) );  
}


/*******************************************************************************
* Function Name: PhaseCounter_WritePeriod
********************************************************************************
* Summary:
* Changes the period of the counter.  The new period 
* will be loaded the next time terminal count is detected.
*
* Parameters:  
*  period: (uint16) A value of 0 will result in
*         the counter remaining at zero.  
*
* Return: 
*  void
*
*******************************************************************************/
void PhaseCounter_WritePeriod(uint16 period) 
{
    #if(PhaseCounter_UsingFixedFunction)
        CY_SET_REG16(PhaseCounter_PERIOD_LSB_PTR,(uint16)period);
    #else
        CY_SET_REG16(PhaseCounter_PERIOD_LSB_PTR,period);
    #endif /* (PhaseCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PhaseCounter_ReadPeriod
********************************************************************************
* Summary:
* Reads the current period value without affecting counter operation.
*
* Parameters:  
*  void:  
*
* Return: 
*  (uint16) Present period value.
*
*******************************************************************************/
uint16 PhaseCounter_ReadPeriod(void) 
{
   return ( CY_GET_REG16(PhaseCounter_PERIOD_LSB_PTR));
}


#if (!PhaseCounter_UsingFixedFunction)
/*******************************************************************************
* Function Name: PhaseCounter_WriteCompare
********************************************************************************
* Summary:
* Changes the compare value.  The compare output will 
* reflect the new value on the next UDB clock.  The compare output will be 
* driven high when the present counter value compares true based on the 
* configured compare mode setting. 
*
* Parameters:  
*  Compare:  New compare value. 
*
* Return: 
*  void
*
*******************************************************************************/
void PhaseCounter_WriteCompare(uint16 compare) \
                                   
{
    #if(PhaseCounter_UsingFixedFunction)
        CY_SET_REG16(PhaseCounter_COMPARE_LSB_PTR,(uint16)compare);
    #else
        CY_SET_REG16(PhaseCounter_COMPARE_LSB_PTR,compare);
    #endif /* (PhaseCounter_UsingFixedFunction) */
}


/*******************************************************************************
* Function Name: PhaseCounter_ReadCompare
********************************************************************************
* Summary:
* Returns the compare value.
*
* Parameters:  
*  void:
*
* Return: 
*  (uint16) Present compare value.
*
*******************************************************************************/
uint16 PhaseCounter_ReadCompare(void) 
{
   return ( CY_GET_REG16(PhaseCounter_COMPARE_LSB_PTR));
}


#if (PhaseCounter_COMPARE_MODE_SOFTWARE)
/*******************************************************************************
* Function Name: PhaseCounter_SetCompareMode
********************************************************************************
* Summary:
*  Sets the software controlled Compare Mode.
*
* Parameters:
*  compareMode:  Compare Mode Enumerated Type.
*
* Return:
*  void
*
*******************************************************************************/
void PhaseCounter_SetCompareMode(uint8 compareMode) 
{
    /* Clear the compare mode bits in the control register */
    PhaseCounter_CONTROL &= ~PhaseCounter_CTRL_CMPMODE_MASK;
    
    /* Write the new setting */
    PhaseCounter_CONTROL |= (compareMode << PhaseCounter_CTRL_CMPMODE0_SHIFT);
}
#endif  /* (PhaseCounter_COMPARE_MODE_SOFTWARE) */


#if (PhaseCounter_CAPTURE_MODE_SOFTWARE)
/*******************************************************************************
* Function Name: PhaseCounter_SetCaptureMode
********************************************************************************
* Summary:
*  Sets the software controlled Capture Mode.
*
* Parameters:
*  captureMode:  Capture Mode Enumerated Type.
*
* Return:
*  void
*
*******************************************************************************/
void PhaseCounter_SetCaptureMode(uint8 captureMode) 
{
    /* Clear the capture mode bits in the control register */
    PhaseCounter_CONTROL &= ~PhaseCounter_CTRL_CAPMODE_MASK;
    
    /* Write the new setting */
    PhaseCounter_CONTROL |= (captureMode << PhaseCounter_CTRL_CAPMODE0_SHIFT);
}
#endif  /* (PhaseCounter_CAPTURE_MODE_SOFTWARE) */


/*******************************************************************************
* Function Name: PhaseCounter_ClearFIFO
********************************************************************************
* Summary:
*   This function clears all capture data from the capture FIFO
*
* Parameters:  
*  void:
*
* Return: 
*  None
*
*******************************************************************************/
void PhaseCounter_ClearFIFO(void) 
{

    while(PhaseCounter_ReadStatusRegister() & PhaseCounter_STATUS_FIFONEMP)
    {
        PhaseCounter_ReadCapture();
    }

}
#endif  /* (!PhaseCounter_UsingFixedFunction) */


/* [] END OF FILE */

