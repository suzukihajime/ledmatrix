/*******************************************************************************
* File Name: LEDSPI.c
* Version 2.30
*
* Description:
*  This file provides all API functionality of the SPI Master component.
*
* Note:
*  None.
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "CyLib.h"
#include "LEDSPI.h"

#if(LEDSPI_InternalClockUsed)
    #include "LEDSPI_IntClock.h"   
#endif /* LEDSPI_InternalClockUsed */

#if (LEDSPI_TXBUFFERSIZE > 4u)

    volatile uint8 LEDSPI_TXBUFFER[LEDSPI_TXBUFFERSIZE] = {0u};
    volatile uint8 LEDSPI_txBufferRead = 0u;
    volatile uint8 LEDSPI_txBufferWrite = 0u;
    volatile uint8 LEDSPI_txBufferFull = 0u;
    
#endif /* LEDSPI_TXBUFFERSIZE > 4u */

#if (LEDSPI_RXBUFFERSIZE > 4u)

    volatile uint8 LEDSPI_RXBUFFER[LEDSPI_RXBUFFERSIZE] = {0u};
    volatile uint8 LEDSPI_rxBufferRead = 0u;
    volatile uint8 LEDSPI_rxBufferWrite = 0u;
    volatile uint8 LEDSPI_rxBufferFull = 0u;
    
#endif /* LEDSPI_RXBUFFERSIZE > 4u */

uint8 LEDSPI_initVar = 0u;

extern volatile uint8 LEDSPI_swStatusTx;
extern volatile uint8 LEDSPI_swStatusRx;


/*******************************************************************************
* Function Name: LEDSPI_Init
********************************************************************************
*
* Summary:
*  Inits/Restores default SPIM configuration provided with customizer.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Side Effects:
*  When this function is called it initializes all of the necessary parameters
*  for execution. i.e. setting the initial interrupt mask, configuring the 
*  interrupt service routine, configuring the bit-counter parameters and 
*  clearing the FIFO and Status Register.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LEDSPI_Init(void) 
{    
    /* Initialize the Bit counter */
    LEDSPI_COUNTER_PERIOD_REG = LEDSPI_BITCTR_INIT;
    
    /* ISR initialization  */  
    #if(LEDSPI_InternalTxInterruptEnabled)
    
        CyIntDisable(LEDSPI_TX_ISR_NUMBER);

        /* Set the ISR to point to the LEDSPI_isr Interrupt. */
        CyIntSetVector(LEDSPI_TX_ISR_NUMBER, LEDSPI_TX_ISR);

        /* Set the priority. */
        CyIntSetPriority(LEDSPI_TX_ISR_NUMBER, LEDSPI_TX_ISR_PRIORITY);
        
    #endif /* LEDSPI_InternalTxInterruptEnabled */                                
    
    #if(LEDSPI_InternalRxInterruptEnabled)
    
        CyIntDisable(LEDSPI_RX_ISR_NUMBER);

        /* Set the ISR to point to the LEDSPI_isr Interrupt. */
        CyIntSetVector(LEDSPI_RX_ISR_NUMBER, LEDSPI_RX_ISR);

        /* Set the priority. */
        CyIntSetPriority(LEDSPI_RX_ISR_NUMBER, LEDSPI_RX_ISR_PRIORITY);
        
    #endif /* LEDSPI_InternalRxInterruptEnabled */
    
    /* Clear any stray data from the RX and TX FIFO */    
	LEDSPI_ClearFIFO();
	
	#if(LEDSPI_RXBUFFERSIZE > 4u)
    
        LEDSPI_rxBufferRead = 0u;
        LEDSPI_rxBufferWrite = 0u;

    #endif /* LEDSPI_RXBUFFERSIZE > 4u */
	
    #if(LEDSPI_TXBUFFERSIZE > 4u)
    
        LEDSPI_txBufferRead = 0u;
        LEDSPI_txBufferWrite = 0u;

    #endif /* LEDSPI_TXBUFFERSIZE > 4u */
    
    (void) LEDSPI_ReadTxStatus(); /* Clear any pending status bits */
    (void) LEDSPI_ReadRxStatus(); /* Clear any pending status bits */
	
	/* Configure the Initial interrupt mask */
    #if (LEDSPI_TXBUFFERSIZE > 4u)
        LEDSPI_TX_STATUS_MASK_REG  = LEDSPI_TX_INIT_INTERRUPTS_MASK & 
                                                ~LEDSPI_STS_TX_FIFO_NOT_FULL;                    
	#else /* LEDSPI_TXBUFFERSIZE < 4u */    
        LEDSPI_TX_STATUS_MASK_REG  = LEDSPI_TX_INIT_INTERRUPTS_MASK;       
	#endif /* LEDSPI_TXBUFFERSIZE > 4u */
    
    LEDSPI_RX_STATUS_MASK_REG  = LEDSPI_RX_INIT_INTERRUPTS_MASK; 
}
  
    
/*******************************************************************************
* Function Name: LEDSPI_Enable
********************************************************************************
*
* Summary:
*  Enable SPIM component.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void LEDSPI_Enable(void) 
{             
    uint8 enableInterrupts = 0u;    
    
    enableInterrupts = CyEnterCriticalSection();
    
    LEDSPI_COUNTER_CONTROL_REG |= LEDSPI_CNTR_ENABLE;
    LEDSPI_TX_STATUS_ACTL_REG |= LEDSPI_INT_ENABLE;
    LEDSPI_RX_STATUS_ACTL_REG |= LEDSPI_INT_ENABLE;
    
    CyExitCriticalSection(enableInterrupts);
    
    #if(LEDSPI_InternalClockUsed)    
        LEDSPI_IntClock_Enable();        
    #endif /* LEDSPI_InternalClockUsed */
    
    #if(LEDSPI_InternalTxInterruptEnabled)    
        CyIntEnable(LEDSPI_TX_ISR_NUMBER);        
    #endif /* LEDSPI_InternalTxInterruptEnabled */
    
    #if(LEDSPI_InternalRxInterruptEnabled)    
        CyIntEnable(LEDSPI_RX_ISR_NUMBER);        
    #endif /* LEDSPI_InternalRxInterruptEnabled */
}


/*******************************************************************************
* Function Name: LEDSPI_Start
********************************************************************************
*
* Summary:
*  Initialize and Enable the SPI Master component.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  LEDSPI_initVar - used to check initial configuration, modified on
*  first function call.
*
* Theory:
*  Enable the clock input to enable operation.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LEDSPI_Start(void) 
{       
    if(LEDSPI_initVar == 0u)
    {               
        LEDSPI_Init();
        LEDSPI_initVar = 1u; 
    }                       
        
    LEDSPI_Enable();        
}


/*******************************************************************************
* Function Name: LEDSPI_Stop
********************************************************************************
*
* Summary:
*  Disable the SPI Master component.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Disable the clock input to enable operation.
*
*******************************************************************************/
void LEDSPI_Stop(void) 
{
    uint8 enableInterrupts = 0u;    
    
    enableInterrupts = CyEnterCriticalSection();
    
    LEDSPI_TX_STATUS_ACTL_REG &= ~LEDSPI_INT_ENABLE;
    LEDSPI_RX_STATUS_ACTL_REG &= ~LEDSPI_INT_ENABLE;
    
    CyExitCriticalSection(enableInterrupts);
    
    #if(LEDSPI_InternalClockUsed)    
        LEDSPI_IntClock_Disable();        
    #endif /* LEDSPI_InternalClockUsed */
    
    #if(LEDSPI_InternalTxInterruptEnabled)    
        CyIntDisable(LEDSPI_TX_ISR_NUMBER);        
    #endif /* LEDSPI_InternalTxInterruptEnabled */
    
    #if(LEDSPI_InternalRxInterruptEnabled)    
        CyIntDisable(LEDSPI_RX_ISR_NUMBER);        
    #endif /* LEDSPI_InternalRxInterruptEnabled */
}


/*******************************************************************************
* Function Name: LEDSPI_EnableTxInt
********************************************************************************
*
* Summary:
*  Enable internal Tx interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Enable the internal Tx interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void LEDSPI_EnableTxInt(void) 
{       
    #if(LEDSPI_InternalTxInterruptEnabled)    
        CyIntEnable(LEDSPI_TX_ISR_NUMBER);
    #endif /* LEDSPI_InternalTxInterruptEnabled */     
}


/*******************************************************************************
* Function Name: LEDSPI_EnableRxInt
********************************************************************************
*
* Summary:
*  Enable internal Rx interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Enable the internal Rx interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void LEDSPI_EnableRxInt(void) 
{       
    #if(LEDSPI_InternalRxInterruptEnabled)            
        CyIntEnable(LEDSPI_RX_ISR_NUMBER);
    #endif /* LEDSPI_InternalRxInterruptEnabled */     
}


/*******************************************************************************
* Function Name: LEDSPI_DisableTxInt
********************************************************************************
*
* Summary:
*  Disable internal Tx interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Disable the internal Tx interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void LEDSPI_DisableTxInt(void) 
{
    #if(LEDSPI_InternalTxInterruptEnabled)    
        CyIntDisable(LEDSPI_TX_ISR_NUMBER);        
    #endif /* LEDSPI_InternalTxInterruptEnabled */
}


/*******************************************************************************
* Function Name: LEDSPI_DisableRxInt
********************************************************************************
*
* Summary:
*  Disable internal Rx interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Disable the internal Rx interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void LEDSPI_DisableRxInt(void) 
{
    #if(LEDSPI_InternalRxInterruptEnabled)    
        CyIntDisable(LEDSPI_RX_ISR_NUMBER);        
    #endif /* LEDSPI_InternalRxInterruptEnabled */
}


/*******************************************************************************
* Function Name: LEDSPI_SetTxInterruptMode
********************************************************************************
*
* Summary:
*  Configure which status bits trigger an interrupt event.
*
* Parameters:
*  intSrc: An or'd combination of the desired status bit masks (defined in the 
*  header file).
*
* Return:
*  None.
*
* Theory:
*  Enables the output of specific status bits to the interrupt controller.
*
*******************************************************************************/
void LEDSPI_SetTxInterruptMode(uint8 intSrc) 
{
    LEDSPI_TX_STATUS_MASK_REG  = intSrc;    
}


/*******************************************************************************
* Function Name: LEDSPI_SetRxInterruptMode
********************************************************************************
*
* Summary:
*  Configure which status bits trigger an interrupt event.
*
* Parameters:
*  intSrc: An or'd combination of the desired status bit masks (defined in the 
*  header file).
*
* Return:
*  None.
*
* Theory:
*  Enables the output of specific status bits to the interrupt controller.
*
*******************************************************************************/
void LEDSPI_SetRxInterruptMode(uint8 intSrc) 
{
    LEDSPI_RX_STATUS_MASK_REG  = intSrc;
}


/*******************************************************************************
* Function Name: LEDSPI_ReadTxStatus
********************************************************************************
*
* Summary:
*  Read the Tx status register for the component.
*
* Parameters:
*  None.
*
* Return:
*  Contents of the Tx status register.
*
* Global variables:
*  LEDSPI_swStatusTx - used to store in software status register, 
*  modified every function call - resets to zero.
*
* Theory:
*  Allows the user and the API to read the Tx status register for error
*  detection and flow control.
*
* Side Effects:
*  Clear Tx status register of the component.
*
* Reentrant:
*  No.
*
*******************************************************************************/
uint8 LEDSPI_ReadTxStatus(void) 
{
    uint8 tmpStatus = 0u;
        
    #if (LEDSPI_TXBUFFERSIZE > 4u)
    
        LEDSPI_DisableTxInt();
        
        tmpStatus = LEDSPI_GET_STATUS_TX(LEDSPI_swStatusTx);                    
        
        LEDSPI_swStatusTx = 0u;        
        
        /* Enable Interrupts */
        LEDSPI_EnableTxInt();
        
    #else /* (LEDSPI_TXBUFFERSIZE < 4u) */
    
        tmpStatus = LEDSPI_TX_STATUS_REG;
        
    #endif /* (LEDSPI_TXBUFFERSIZE > 4u) */
    
    return(tmpStatus);
}


/*******************************************************************************
* Function Name: LEDSPI_ReadRxStatus
********************************************************************************
*
* Summary:
*  Read the Rx status register for the component.
*
* Parameters:
*  None.
*
* Return:
*  Contents of the Rx status register.
*
* Global variables:
*  LEDSPI_swStatusRx - used to store in software Rx status register, 
*  modified every function call - resets to zero.
*
* Theory:
*  Allows the user and the API to read the Rx status register for error 
*  detection and flow control.
*
* Side Effects:
*  Clear Rx status register of the component.
*
* Reentrant:
*  No.
*
*******************************************************************************/
uint8 LEDSPI_ReadRxStatus(void) 
{
    uint8 tmpStatus = 0u;
        
    #if (LEDSPI_RXBUFFERSIZE > 4u)
    
        LEDSPI_DisableRxInt();
        
        tmpStatus = LEDSPI_GET_STATUS_RX(LEDSPI_swStatusRx);
               
        LEDSPI_swStatusRx = 0u;
        
        /* Enable Interrupts */
        LEDSPI_EnableRxInt();
        
    #else /* (LEDSPI_RXBUFFERSIZE < 4u) */
    
        tmpStatus = LEDSPI_RX_STATUS_REG;
        
    #endif /* (LEDSPI_RXBUFFERSIZE > 4u) */
    
    return(tmpStatus);
}


/*******************************************************************************
* Function Name: LEDSPI_WriteTxData
********************************************************************************
*
* Summary:
*  Write a byte of data to be sent across the SPI.
*
* Parameters:
*  txDataByte: The data value to send across the SPI.
*
* Return:
*  None.
*
* Global variables:
*  LEDSPI_txBufferWrite - used for the account of the bytes which
*  have been written down in the TX software buffer, modified every function
*  call if TX Software Buffer is used.
*  LEDSPI_txBufferRead - used for the account of the bytes which
*  have been read from the TX software buffer.
*  LEDSPI_TXBUFFER[LEDSPI_TXBUFFERSIZE] - used to store
*  data to sending, modified every function call if TX Software Buffer is used.
*
* Theory:
*  Allows the user to transmit any byte of data in a single transfer.
*
* Side Effects:
*  If this function is called again before the previous byte is finished then
*  the next byte will be appended to the transfer with no time between
*  the byte transfers. Clear Tx status register of the component.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LEDSPI_WriteTxData(uint8 txData) 
{    
    #if(LEDSPI_TXBUFFERSIZE > 4u)

        int16 tmpTxBufferRead = 0u;
                
        /* Block if buffer is full, so we don't overwrite. */
        do
        {
            tmpTxBufferRead = LEDSPI_txBufferRead - 1u;
            if (tmpTxBufferRead < 0u)
            {
                tmpTxBufferRead = LEDSPI_TXBUFFERSIZE - 1u;        
            }    
        } while(tmpTxBufferRead == LEDSPI_txBufferWrite);               
                   
        /* Disable Interrupt to protect variables that could change on interrupt. */
        LEDSPI_DisableTxInt();
        
        LEDSPI_swStatusTx = LEDSPI_GET_STATUS_TX(LEDSPI_swStatusTx);
        
        if((LEDSPI_txBufferRead == LEDSPI_txBufferWrite) &&
            ((LEDSPI_swStatusTx & LEDSPI_STS_TX_FIFO_NOT_FULL) != 0u))
        {
            /* Add directly to the FIFO. */
            CY_SET_REG8(LEDSPI_TXDATA_PTR, txData);
        }
        else
        {
            /* Add to the software buffer. */
            LEDSPI_txBufferWrite++;
            if(LEDSPI_txBufferWrite >= LEDSPI_TXBUFFERSIZE)
            {
                LEDSPI_txBufferWrite = 0u;
            }   
                      
            if(LEDSPI_txBufferWrite == LEDSPI_txBufferRead)
            {
                LEDSPI_txBufferRead++;
                if(LEDSPI_txBufferRead >= LEDSPI_RXBUFFERSIZE)
                {
                    LEDSPI_txBufferRead = 0u;
                }
                LEDSPI_txBufferFull = 1u;
            }
            
            LEDSPI_TXBUFFER[LEDSPI_txBufferWrite] = txData;
            
            LEDSPI_TX_STATUS_MASK_REG |= LEDSPI_STS_TX_FIFO_NOT_FULL;            
        }                         
        
        /* Enable Interrupt. */
        LEDSPI_EnableTxInt();                        

    #else /* LEDSPI_TXBUFFERSIZE <= 4u */

        /* Block while FIFO is full */
        while((LEDSPI_TX_STATUS_REG & LEDSPI_STS_TX_FIFO_NOT_FULL) == 0u);
        
        /* Then write the byte */
        CY_SET_REG8(LEDSPI_TXDATA_PTR, txData);

    #endif /* LEDSPI_TXBUFFERSIZE > 4u */
}


/*******************************************************************************
* Function Name: LEDSPI_ReadRxData
********************************************************************************
*
* Summary:
*  Read the next byte of data received across the SPI.
*
* Parameters:
*  None.
*
* Return:
*  The next byte of data read from the FIFO.
*
* Global variables:
*  LEDSPI_rxBufferWrite - used for the account of the bytes which
*  have been written down in the RX software buffer.
*  LEDSPI_rxBufferRead - used for the account of the bytes which
*  have been read from the RX software buffer, modified every function
*  call if RX Software Buffer is used.
*  LEDSPI_RXBUFFER[LEDSPI_RXBUFFERSIZE] - used to store
*  received data.
*
* Theory:
*  Allows the user to read a byte of data received.
*
* Side Effects:
*  Will return invalid data if the FIFO is empty. The user should Call 
*  GetRxBufferSize() and if it returns a non-zero value then it is safe to call 
*  ReadByte() function.
*
* Reentrant:
*  No.
*
*******************************************************************************/
uint8 LEDSPI_ReadRxData(void) 
{
    uint8 rxData = 0u;

    #if(LEDSPI_RXBUFFERSIZE > 4u)
    
        /* Disable Interrupt to protect variables that could change on interrupt. */
        LEDSPI_DisableRxInt();
        
        if(LEDSPI_rxBufferRead != LEDSPI_rxBufferWrite)
        {      
            if(LEDSPI_rxBufferFull == 0u)
            {
                LEDSPI_rxBufferRead++;
                if(LEDSPI_rxBufferRead >= LEDSPI_RXBUFFERSIZE)
                {
                    LEDSPI_rxBufferRead = 0u;
                }
            }
            else
            {
                LEDSPI_rxBufferFull = 0u;
            }
        }    
        
        rxData = LEDSPI_RXBUFFER[LEDSPI_rxBufferRead];
                           
        /* Enable Interrupt. */
        LEDSPI_EnableRxInt();
    
    #else /* LEDSPI_RXBUFFERSIZE <= 4u */
    
        rxData = CY_GET_REG8(LEDSPI_RXDATA_PTR);
    
    #endif /* LEDSPI_RXBUFFERSIZE > 4u */

	return (rxData);
    
}


/*******************************************************************************
* Function Name: LEDSPI_GetRxBufferSize
********************************************************************************
*
* Summary:
*  Returns the number of bytes/words of data currently held in the RX buffer.
*  If RX Software Buffer not used then function return 0 if FIFO empty or 1 if 
*  FIFO not empty. In another case function return size of RX Software Buffer.
*
* Parameters:
*  None.
*
* Return:
*  Integer count of the number of bytes/words in the RX buffer.
*
* Global variables:
*  LEDSPI_rxBufferWrite - used for the account of the bytes which
*  have been written down in the RX software buffer.
*  LEDSPI_rxBufferRead - used for the account of the bytes which
*  have been read from the RX software buffer.
*
* Side Effects:
*  Clear status register of the component.
*
*******************************************************************************/
uint8 LEDSPI_GetRxBufferSize(void) 
{
    uint8 size = 0u;

    #if(LEDSPI_RXBUFFERSIZE > 4u)
    
        /* Disable Interrupt to protect variables that could change on interrupt. */
        LEDSPI_DisableRxInt();
    
        if(LEDSPI_rxBufferRead == LEDSPI_rxBufferWrite)
        {
            size = 0u; /* No data in RX buffer */
        }
        else if(LEDSPI_rxBufferRead < LEDSPI_rxBufferWrite)
        {
            size = (LEDSPI_rxBufferWrite - LEDSPI_rxBufferRead);
        }
        else
        {
            size = (LEDSPI_RXBUFFERSIZE - LEDSPI_rxBufferRead) + LEDSPI_rxBufferWrite;
        }
    
        /* Enable interrupt. */
        LEDSPI_EnableRxInt();
    
    #else /* LEDSPI_RXBUFFERSIZE <= 4u */
    
        /* We can only know if there is data in the fifo. */
        size = ((LEDSPI_RX_STATUS_REG & LEDSPI_STS_RX_FIFO_NOT_EMPTY) == 
                 LEDSPI_STS_RX_FIFO_NOT_EMPTY) ? 1u : 0u;
    
    #endif /* LEDSPI_RXBUFFERSIZE < 4u */

    return (size);
}


/*******************************************************************************
* Function Name: LEDSPI_GetTxBufferSize
********************************************************************************
*
* Summary:
*  Returns the number of bytes/words of data currently held in the TX buffer.
*  If TX Software Buffer not used then function return 0 - if FIFO empty, 1 - if 
*  FIFO not full, 4 - if FIFO full. In another case function return size of TX
*  Software Buffer.
*
* Parameters:
*  None.
*
* Return:
*  Integer count of the number of bytes/words in the TX buffer.
*
* Global variables:
*  LEDSPI_txBufferWrite - used for the account of the bytes which
*  have been written down in the TX software buffer.
*  LEDSPI_txBufferRead - used for the account of the bytes which
*  have been read from the TX software buffer.
*
* Side Effects:
*  Clear status register of the component.
*
*******************************************************************************/
uint8  LEDSPI_GetTxBufferSize(void) 
{
    uint8 size = 0u;

    #if(LEDSPI_TXBUFFERSIZE > 4u)
    
        /* Disable Interrupt to protect variables that could change on interrupt. */
        LEDSPI_DisableTxInt();
    
        if(LEDSPI_txBufferRead == LEDSPI_txBufferWrite)
        {
            size = 0u;
        }
        else if(LEDSPI_txBufferRead < LEDSPI_txBufferWrite)
        {
            size = (LEDSPI_txBufferWrite - LEDSPI_txBufferRead);
        }
        else
        {
            size = (LEDSPI_TXBUFFERSIZE - LEDSPI_txBufferRead) + LEDSPI_txBufferWrite;
        }
    
        /* Enable Interrupt. */
        LEDSPI_EnableTxInt();
    
    #else /* LEDSPI_TXBUFFERSIZE <= 4u */
    
        size = LEDSPI_TX_STATUS_REG;
    
        /* Is the fifo is full. */
        if((size & LEDSPI_STS_TX_FIFO_EMPTY) == LEDSPI_STS_TX_FIFO_EMPTY)
        {
            size = 0u;
        }
        else if((size & LEDSPI_STS_TX_FIFO_NOT_FULL) == LEDSPI_STS_TX_FIFO_NOT_FULL)
        {
            size = 1u;
        }
        else
        {
            /* We only know there is data in the fifo. */
            size = 4u;
        }
    
    #endif /* LEDSPI_TXBUFFERSIZE > 4u */

    return (size);
}


/*******************************************************************************
* Function Name: LEDSPI_ClearRxBuffer
********************************************************************************
*
* Summary:
*  Clear the RX RAM buffer by setting the read and write pointers both to zero.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  LEDSPI_rxBufferWrite - used for the account of the bytes which
*  have been written down in the RX software buffer, modified every function 
*  call - resets to zero.
*  LEDSPI_rxBufferRead - used for the account of the bytes which
*  have been read from the RX software buffer, modified every function call -
*  resets to zero.
*
* Theory:
*  Setting the pointers to zero makes the system believe there is no data to 
*  read and writing will resume at address 0 overwriting any data that may have
*  remained in the RAM.
*
* Side Effects:
*  Any received data not read from the RAM buffer will be lost when overwritten.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LEDSPI_ClearRxBuffer(void) 
{
	/* Clear Hardware RX FIFO */
    while((!(LEDSPI_RX_STATUS_REG & LEDSPI_STS_RX_FIFO_NOT_EMPTY)) == 0u)
    {
        CY_GET_REG8(LEDSPI_RXDATA_PTR);
    }
	
    #if(LEDSPI_RXBUFFERSIZE > 4u)
    
        /* Disable interrupt to protect variables that could change on interrupt. */        
        LEDSPI_DisableRxInt();
    
        LEDSPI_rxBufferRead = 0u;
        LEDSPI_rxBufferWrite = 0u;
    
        /* Enable Rx interrupt. */
        LEDSPI_EnableRxInt();
        
    #endif /* LEDSPI_RXBUFFERSIZE > 4u */
}


/*******************************************************************************
* Function Name: LEDSPI_ClearTxBuffer
********************************************************************************
*
* Summary:
*  Clear the TX RAM buffer by setting the read and write pointers both to zero.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global variables:
*  LEDSPI_txBufferWrite - used for the account of the bytes which
*  have been written down in the TX software buffer, modified every function
*  call - resets to zero.
*  LEDSPI_txBufferRead - used for the account of the bytes which
*  have been read from the TX software buffer, modified every function call -
*  resets to zero.
*
* Theory:
*  Setting the pointers to zero makes the system believe there is no data to 
*  read and writing will resume at address 0 overwriting any data that may have
*  remained in the RAM.
*
* Side Effects:
*  Any data not yet transmitted from the RAM buffer will be lost when 
*  overwritten.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LEDSPI_ClearTxBuffer(void) 
{
    uint8 enableInterrupts = 0u;
    
    /* Clear Hardware TX FIFO */       
    enableInterrupts = CyEnterCriticalSection();
    
    #if(LEDSPI_DataWidth <= 8u)
    
        /* Clear TX FIFO */
        LEDSPI_AUX_CONTROL_DP0_REG |= LEDSPI_FIFO_CLR;
        LEDSPI_AUX_CONTROL_DP0_REG &= ~LEDSPI_FIFO_CLR;
    
    #else
    
        /* Clear TX FIFO */
        LEDSPI_AUX_CONTROL_DP0_REG |= LEDSPI_FIFO_CLR;
        LEDSPI_AUX_CONTROL_DP0_REG &= ~LEDSPI_FIFO_CLR;
        LEDSPI_AUX_CONTROL_DP1_REG |= LEDSPI_FIFO_CLR;
        LEDSPI_AUX_CONTROL_DP1_REG &= ~LEDSPI_FIFO_CLR;
        
    #endif /* LEDSPI_DataWidth > 8u */
    
    CyExitCriticalSection(enableInterrupts);
	
    #if(LEDSPI_TXBUFFERSIZE > 4u)
    
        /* Disable Interrupt to protect variables that could change on interrupt. */
        LEDSPI_DisableTxInt();
    
        LEDSPI_txBufferRead = 0u;
        LEDSPI_txBufferWrite = 0u;
    
        /* If Buffer is empty then disable TX FIFO status interrupt */
        LEDSPI_TX_STATUS_MASK_REG &= ~LEDSPI_STS_TX_FIFO_NOT_FULL;

        /* Enable Interrupt. */
        LEDSPI_EnableTxInt();
    
    #endif /* LEDSPI_TXBUFFERSIZE > 4u */
}


#if (LEDSPI_BidirectionalMode == 1u)

    /*******************************************************************************
    * Function Name: LEDSPI_TxEnable
    ********************************************************************************
    *
    * Summary:
    *  If the SPI master is configured to use a single bi-directional pin then this
    *  will set the bi-directional pin to transmit.
    *
    * Parameters:
    *  None.
    *
    * Return:
    *  None.
    *
    *******************************************************************************/
    void LEDSPI_TxEnable(void) 
    {
        LEDSPI_CONTROL_REG |= LEDSPI_CTRL_TX_SIGNAL_EN;
    }
    
    
    /*******************************************************************************
    * Function Name: LEDSPI_TxDisable
    ********************************************************************************
    *
    * Summary:
    *  If the SPI master is configured to use a single bi-directional pin then this
    *  will set the bi-directional pin to receive.
    *
    * Parameters:
    *  None.
    *
    * Return:
    *  None.
    *
    *******************************************************************************/
    void LEDSPI_TxDisable(void) 
    {
        LEDSPI_CONTROL_REG &= ~LEDSPI_CTRL_TX_SIGNAL_EN;
    }
    
#endif /* LEDSPI_BidirectionalMode == 1u */


/*******************************************************************************
* Function Name: LEDSPI_PutArray
********************************************************************************                       
*
* Summary:
*  Write available data from ROM/RAM to the TX buffer while space is available 
*  in the TX buffer. Keep trying until all data is passed to the TX buffer.
*
* Parameters:
*  *buffer: Pointer to the location in RAM containing the data to send
*  byteCount: The number of bytes to move to the transmit buffer.
*
* Return:
*  None.
*
* Side Effects:
*  Will stay in this routine until all data has been sent.  May get locked in
*  this loop if data is not being initiated by the master if there is not
*  enough room in the TX FIFO.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LEDSPI_PutArray(uint8 *buffer, uint8 byteCount) 
{
    while(byteCount > 0u)
    {
        LEDSPI_WriteTxData(*buffer++);
        byteCount--;
    }
}


/*******************************************************************************
* Function Name: LEDSPI_ClearFIFO
********************************************************************************
*
* Summary:
*  Clear the RX and TX FIFO's of all data for a fresh start.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Side Effects:
*  Clear status register of the component.
*
*******************************************************************************/
void LEDSPI_ClearFIFO(void) 
{
    uint8 enableInterrupts = 0u;
    
    while((!(LEDSPI_RX_STATUS_REG & LEDSPI_STS_RX_FIFO_NOT_EMPTY)) == 0u)
    {
        CY_GET_REG8(LEDSPI_RXDATA_PTR);
    }
    
    enableInterrupts = CyEnterCriticalSection();
    
    #if(LEDSPI_DataWidth <= 8u)
    
        /* Clear TX FIFO */
        LEDSPI_AUX_CONTROL_DP0_REG |= LEDSPI_FIFO_CLR;
        LEDSPI_AUX_CONTROL_DP0_REG &= ~LEDSPI_FIFO_CLR;
    
    #else
    
        /* Clear TX FIFO */
        LEDSPI_AUX_CONTROL_DP0_REG |= LEDSPI_FIFO_CLR;
        LEDSPI_AUX_CONTROL_DP0_REG &= ~LEDSPI_FIFO_CLR;
        LEDSPI_AUX_CONTROL_DP1_REG |= LEDSPI_FIFO_CLR;
        LEDSPI_AUX_CONTROL_DP1_REG &= ~LEDSPI_FIFO_CLR;
        
    #endif /* LEDSPI_DataWidth > 8u */
    
    CyExitCriticalSection(enableInterrupts);
}


/* Following functions are for version Compatibility, they are obsolete.
*  Please do not use it in new projects.
*/

/*******************************************************************************
* Function Name: LEDSPI_EnableInt
********************************************************************************
*
* Summary:
*  Enable internal interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Enable the internal interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void LEDSPI_EnableInt(void) 
{       
    #if(LEDSPI_InternalTxInterruptEnabled)    
        CyIntEnable(LEDSPI_TX_ISR_NUMBER);
    #endif /* LEDSPI_InternalTxInterruptEnabled */                                
    
    #if(LEDSPI_InternalRxInterruptEnabled)           
        CyIntEnable(LEDSPI_RX_ISR_NUMBER);
    #endif /* LEDSPI_InternalRxInterruptEnabled */     
}


/*******************************************************************************
* Function Name: LEDSPI_DisableInt
********************************************************************************
*
* Summary:
*  Disable internal interrupt generation.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Theory:
*  Disable the internal interrupt output -or- the interrupt component itself.
*
*******************************************************************************/
void LEDSPI_DisableInt(void) 
{
    #if(LEDSPI_InternalTxInterruptEnabled)    
        CyIntDisable(LEDSPI_TX_ISR_NUMBER);
    #endif /* LEDSPI_InternalTxInterruptEnabled */
    
    #if(LEDSPI_InternalRxInterruptEnabled)           
        CyIntDisable(LEDSPI_RX_ISR_NUMBER);
    #endif /* LEDSPI_InternalRxInterruptEnabled */
}


/*******************************************************************************
* Function Name: LEDSPI_SetInterruptMode
********************************************************************************
*
* Summary:
*  Configure which status bits trigger an interrupt event.
*
* Parameters:
*  intSrc: An or'd combination of the desired status bit masks (defined in the 
*  header file).
*
* Return:
*  None.
*
* Theory:
*  Enables the output of specific status bits to the interrupt controller.
*
*******************************************************************************/
void LEDSPI_SetInterruptMode(uint8 intSrc) 
{
    LEDSPI_TX_STATUS_MASK_REG  = intSrc & ~(1u << LEDSPI_STS_SPI_IDLE_SHIFT);
    LEDSPI_RX_STATUS_MASK_REG  = intSrc;
}


/*******************************************************************************
* Function Name: LEDSPI_ReadStatus
********************************************************************************
*
* Summary:
*  Read the status register for the component.
*
* Parameters:
*  None.
*
* Return:
*  Contents of the status register.
*
* Global variables:
*  LEDSPI_swStatus - used to store in software status register, 
*  modified every function call - resets to zero.
*
* Theory:
*  Allows the user and the API to read the status register for error detection
*  and flow control.
*
* Side Effects:
*  Clear status register of the component.
*
* Reentrant:
*  No.
*
*******************************************************************************/
uint8 LEDSPI_ReadStatus(void) 
{
    uint8 tmpStatus;
        
    #if ((LEDSPI_TXBUFFERSIZE > 4u) || (LEDSPI_RXBUFFERSIZE > 4u))
    
        LEDSPI_DisableInt();
        
        tmpStatus = (LEDSPI_GET_STATUS_TX(LEDSPI_swStatusTx) & 
                      ~(1u << LEDSPI_STS_SPI_IDLE_SHIFT)) | 
                      LEDSPI_GET_STATUS_RX(LEDSPI_swStatusRx);
        
        LEDSPI_swStatusTx = 0u;
        LEDSPI_swStatusRx = 0u;
        
        /* Enable Interrupts */
        LEDSPI_EnableInt();
        
    #else /* (LEDSPI_TXBUFFERSIZE < 4u) && (LEDSPI_RXBUFFERSIZE < 4u) */
    
        tmpStatus = (LEDSPI_TX_STATUS_REG & ~(1u << LEDSPI_STS_SPI_IDLE_SHIFT)) |
                     LEDSPI_RX_STATUS_REG;
        
    #endif /* (LEDSPI_TXBUFFERSIZE > 4u) || (LEDSPI_RXBUFFERSIZE > 4u) */
    
    return(tmpStatus);
}


/* [] END OF FILE */
