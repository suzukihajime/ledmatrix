/*******************************************************************************
* File Name: LEDSPI_PM.c
* Version 2.30
*
* Description:
*  This file contains the setup, control and status commands to support 
*  component operations in low power mode.  
*
* Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "LEDSPI.h"

static LEDSPI_BACKUP_STRUCT LEDSPI_backup = {
                                        0u,
                                        LEDSPI_BITCTR_INIT,
                                        #if(CY_UDB_V0)
                                            LEDSPI_TX_INIT_INTERRUPTS_MASK,
                                            LEDSPI_RX_INIT_INTERRUPTS_MASK
                                        #endif /* CY_UDB_V0 */
                                        };

#if (LEDSPI_TXBUFFERSIZE > 4u)

    extern volatile uint8 LEDSPI_txBufferRead;
    extern volatile uint8 LEDSPI_txBufferWrite;
    
#endif /* LEDSPI_TXBUFFERSIZE > 4u */

#if (LEDSPI_RXBUFFERSIZE > 4u)

    extern volatile uint8 LEDSPI_rxBufferRead;
    extern volatile uint8 LEDSPI_rxBufferWrite;
    
#endif /* LEDSPI_RXBUFFERSIZE > 4u */


/*******************************************************************************
* Function Name: LEDSPI_SaveConfig
********************************************************************************
*
* Summary:
*  Saves SPIM configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
* 
* Global Variables:
*  LEDSPI_backup - modified when non-retention registers are saved.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LEDSPI_SaveConfig(void) 
{
    /* Store Status Mask registers */
    #if (CY_UDB_V0)     
    
       LEDSPI_backup.saveSrTxIntMask = LEDSPI_TX_STATUS_MASK_REG;
       LEDSPI_backup.saveSrRxIntMask = LEDSPI_RX_STATUS_MASK_REG;
       LEDSPI_backup.cntrPeriod = LEDSPI_COUNTER_PERIOD_REG;
       
    #endif /* CY_UDB_V0 */
}


/*******************************************************************************
* Function Name: LEDSPI_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores SPIM configuration.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
* Global Variables:
*  LEDSPI_backup - used when non-retention registers are restored.
*
* Side Effects:
*  If this API is called without first calling SaveConfig then in the following
*  registers will be default values from Customizer: 
*  LEDSPI_STATUS_MASK_REG and LEDSPI_COUNTER_PERIOD_REG.
*
*******************************************************************************/
void LEDSPI_RestoreConfig(void) 
{
    /* Restore the data, saved by SaveConfig() function */
    #if (CY_UDB_V0)
    
        LEDSPI_TX_STATUS_MASK_REG = LEDSPI_backup.saveSrTxIntMask;
        LEDSPI_RX_STATUS_MASK_REG = LEDSPI_backup.saveSrRxIntMask;
        LEDSPI_COUNTER_PERIOD_REG = LEDSPI_backup.cntrPeriod;
        
    #endif /* CY_UDB_V0 */
}


/*******************************************************************************
* Function Name: LEDSPI_Sleep
********************************************************************************
*
* Summary:
*  Prepare SPIM Component goes to sleep.
*
* Parameters:  
*  None.
*
* Return: 
*  None.
*
* Global Variables:
*  LEDSPI_backup - modified when non-retention registers are saved.
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LEDSPI_Sleep(void) 
{
    /* Save components enable state */
    if ((LEDSPI_TX_STATUS_ACTL_REG & LEDSPI_INT_ENABLE) == LEDSPI_INT_ENABLE)
    {
        LEDSPI_backup.enableState = 1u;
    }
    else /* Components block is disabled */
    {
        LEDSPI_backup.enableState = 0u;
    }

    LEDSPI_Stop();

    LEDSPI_SaveConfig();
}


/*******************************************************************************
* Function Name: LEDSPI_Wakeup
********************************************************************************
*
* Summary:
*  Prepare SPIM Component to wake up.
*
* Parameters:  
*  None.
*
* Return: 
*  None.
*
* Global Variables:
*  LEDSPI_backup - used when non-retention registers are restored.
*  LEDSPI_txBufferWrite - modified every function call - resets to 
*  zero.
*  LEDSPI_txBufferRead - modified every function call - resets to 
*  zero.
*  LEDSPI_rxBufferWrite - modified every function call - resets to
*  zero.
*  LEDSPI_rxBufferRead - modified every function call - resets to
*  zero. 
*
* Reentrant:
*  No.
*
*******************************************************************************/
void LEDSPI_Wakeup(void) 
{        
    LEDSPI_RestoreConfig();
         
    #if (LEDSPI_TXBUFFERSIZE > 4u)
    
        LEDSPI_txBufferRead = 0u;
        LEDSPI_txBufferWrite = 0u;
        
    #endif /* LEDSPI_TXBUFFERSIZE > 4u */
    
    #if (LEDSPI_RXBUFFERSIZE > 4u)    
    
        LEDSPI_rxBufferRead = 0u;
        LEDSPI_rxBufferWrite = 0u;
        
    #endif /* LEDSPI_RXBUFFERSIZE > 4u */ 
    
    LEDSPI_ClearFIFO();
    
    /* Restore components block enable state */
    if (LEDSPI_backup.enableState != 0u)
    {
         /* Components block was enabled */
         LEDSPI_Enable();
    } /* Do nothing if components block was disabled */
}


/* [] END OF FILE */
