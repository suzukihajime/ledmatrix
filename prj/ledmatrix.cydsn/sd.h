/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <device.h>
#include <cytypes.h>
#include "sdc/ff.h"


#ifndef	SDCARD
#define	SDCARD


typedef struct {
	BYTE bInserted;
	BYTE bInitialized;
	BYTE bOpen;
	FILINFO fi;
	FIL file;
	char filename[20];
} SDSTATUS;

// Initialize SD Card
BYTE SD_Init(SDSTATUS *sd);
BYTE SD_Slot1Inserted(void);
BYTE SD_OpenFile(SDSTATUS *sd, char *filename);
unsigned int SD_ReadLine(SDSTATUS *sd, char *buf, int len);
void SD_SetPointer(unsigned int);
void SD_Close(void);
void copystatus(SDSTATUS *src, SDSTATUS *dest);
DWORD get_fattime(void);

void SDTimer1_Set(int);
void SDTimer2_Set(int);
void SDTimer1_Deleyms(int);
void SDTimer2_Delayms(int);

#endif

//[] END OF FILE
