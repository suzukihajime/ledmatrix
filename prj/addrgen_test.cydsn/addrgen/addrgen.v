//`#start header` -- edit after this line, do not edit this line
// ========================================
//
// Copyright YOUR COMPANY, THE YEAR
// All Rights Reserved
// UNPUBLISHED, LICENSED SOFTWARE.
//
// CONFIDENTIAL AND PROPRIETARY INFORMATION
// WHICH IS THE PROPERTY OF your company.
//
// ========================================
`include "cypress.v"
//`#end` -- edit above this line, do not edit this line

//`#start body` -- edit after this line, do not edit this line

//        Your code goes here

module addrgen (
	input [3:0] row,
	input req,
	input rowterm,
	input clock,
	input reset,
	output dmareq,
	input dmaterm
);
	/*
	udb_clock_enableをインスタンス化する
	*/
	wire addrgen_clock;
	cy_psoc3_udb_clock_enable_v1_0 #(.sync_mode(`TRUE)) ClkSync
    (
        /* input  */    .clock_in(clock),
        /* input  */    .enable(1'b1),
        /* output */    .clock_out(addrgen_clock)
    );
	
	/*
	control registerをインスタンス化(sync mode)
	addr_base_hiとaddr_base_loの2バイト
	*/
	wire addrgen_page_clock = rowterm;
	wire [7:0] addrgen_addr_base_lo;
	wire [7:0] addrgen_addr_base_hi;
	cy_psoc3_control #(.cy_force_order(1)) ControlReg_lo
	(
		/* output [07:00] */  .control(addrgen_addr_base_lo)
	);
	cy_psoc3_control #(.cy_force_order(1)) ControlReg_hi
	(
		/* output [07:00] */  .control(addrgen_addr_base_hi)
	);
	
	/*
	転送数をカウントする。この値を元に次のアドレスを決定。
	転送終了後にカウントしたいので、posedge dma_termとしてある。
	*/
	reg [7:0] addrgen_count;
	always @(posedge dmaterm or posedge rowterm) begin
		if(rowterm == 1'b1) begin
			addrgen_count[7:0] <= 8'b00000000;
		end else begin
			if(dmaterm == 1'b1) begin
				addrgen_count[7:0] <= addrgen_count[7:0] + 8'b00000001;
			end else begin
				addrgen_count[7:0] <= addrgen_count[7:0];
			end
		end
	end
	
	/*
	dma_reqはmatdrvから受け取ったreq信号をそのまま渡す。
	DMA transferが正常に終了するとdma_termシグナルがアサートされるので、
	これでcounterを+1する。
	*/
	function dma_req_gen;
		input req;
		input [7:0] count;
		begin
			if(count[7:0] < 8'd192) begin
				dma_req_gen = req;
			end else begin
				dma_req_gen = 1'b0;
			end
		end
	endfunction
	assign dmareq = dma_req_gen(req, addrgen_count);
	
	/*
	以下のロジックで、次のDMA転送に使うアドレスを生成する。
	for(i = 0; i < 96; i++) {
		addr_base = ((i | 0x0008)*16) + (uint16)disp_buf;
		addr_buf_lo_base[i] = addr_base & 0x00ff;
		addr_buf_hi[i] = (addr_base>>8) & 0x00ff;
	}
	for(i = 0; i < 96; i++) {
		addr_base = (((96-i) & 0xfff7)*16) + (uint16)disp_buf;
		addr_buf_lo_base[i+96] = addr_base & 0x00ff;
		addr_buf_hi[i+96] = (addr_base>>8) & 0x00ff;
	}
	*/
	wire [3:0] next_row;
	function [3:0] gen_next_row;
		input [3:0] row;
		begin
			case(row[3:0])
				4'b0000: gen_next_row[3:0] = 4'b0010;
				4'b0001: gen_next_row[3:0] = 4'b0011;
				4'b0010: gen_next_row[3:0] = 4'b0100;
				4'b0011: gen_next_row[3:0] = 4'b0101;
				4'b0100: gen_next_row[3:0] = 4'b0110;
				4'b0101: gen_next_row[3:0] = 4'b0111;
				4'b0110: gen_next_row[3:0] = 4'b1000;
				4'b0111: gen_next_row[3:0] = 4'b1001;
				4'b1000: gen_next_row[3:0] = 4'b1010;
				4'b1001: gen_next_row[3:0] = 4'b1011;
				4'b1010: gen_next_row[3:0] = 4'b1100;
				4'b1011: gen_next_row[3:0] = 4'b1101;
				4'b1100: gen_next_row[3:0] = 4'b1110;
				4'b1101: gen_next_row[3:0] = 4'b1111;
				4'b1110: gen_next_row[3:0] = 4'b0001;
				4'b1111: gen_next_row[3:0] = 4'b0000;
				default: gen_next_row[3:0] = 4'b0000;
			endcase
		end
	endfunction
	assign next_row[3:0] = gen_next_row(row[3:0]);
	
	wire [15:0] next_addr;
	wire [7:0] count_temp_1 = addrgen_count[7:0] | 8'b00001000;
	wire [7:0] count_temp_2 = (8'd96 - addrgen_count[7:0]) & 8'b11110111;
	function [7:0] next_addr_sel;
		input [7:0] count;
		input [7:0] count_temp_1;
		input [7:0] count_temp_2;
		begin
			if(count[7:0] < 8'd96) begin
				next_addr_sel[7:0] = count_temp_1[7:0];
//				next_addr_gen[15:0] = {4'b0000, (count[7:0] | 8'b00001000), next_row} + addr_base[15:0];
			end else begin
				next_addr_sel[7:0] = count_temp_2[7:0];
//				next_addr_gen[15:0] = {4'b0000, ((count[7:0]) & 8'b11110111), next_row} + addr_base[15:0];
			end
		end
	endfunction
	wire [7:0] count_temp = next_addr_sel(addrgen_count[7:0], count_temp_1[7:0], count_temp_2[7:0]);
	wire next_addr_carry;
	wire [7:0] next_addr_lo;
	wire [7:0] next_addr_hi;
	wire [7:0] next_addr_hi_temp;
	assign {next_addr_carry, next_addr_lo[7:0]} = {count_temp[3:0], next_row[3:0]} + addrgen_addr_base_lo[7:0];
	assign next_addr_hi_temp[7:0] = {4'b0000, count_temp[7:3]} + addrgen_addr_base_hi[7:0];
	assign next_addr_hi[7:0] = next_addr_hi_temp[7:0] + {7'b0000000 + next_addr_carry};
/*	assign next_addr[15:0] = next_addr_gen(addrgen_count[7:0],
									{addrgen_addr_base_hi[7:0], addrgen_addr_base_lo[7:0]},
									next_row[3:0]);*/
//	assign next_addr[15:0] = {count_temp_1[7:0], count_temp_2[7:0]};
	/*
	status registerのインスタンス化
	DMA転送用に、計算したアドレスを出力するレジスタとして使う。
	*/
	cy_psoc3_status #(.cy_force_order(1)) StatusReg_lo
    (
        /* input          */  .clock(1'b0),
		/* input          */  .reset(1'b0),
        /* input  [06:00] */  .status(next_addr_lo[7:0])
    );
	cy_psoc3_status #(.cy_force_order(1)) StatusReg_hi
    (
        /* input          */  .clock(1'b0),
		/* input          */  .reset(1'b0),
        /* input  [06:00] */  .status(next_addr_hi[7:0])
    );
endmodule

//`#end` -- edit above this line, do not edit this line

//`#start footer` -- edit after this line, do not edit this line
//`#end` -- edit above this line, do not edit this line


//[] END OF FILE
